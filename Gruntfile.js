module.exports = function(grunt) {
    grunt.initConfig({
        concat_sourcemap: {
            options: {
                sourcesContent: true
            },
            target: {
                files: {
                    'public/dist/app.css': [
                        'web/styles/app/theme/css/animate.css',
                    ],
                    'public/dist/app.js': [
                        'web/styles/app/theme/js/jquery-2.1.1.js',
                    ]
                }
            }
        },
        uglify: {
            min: {
                src: [
                    'assets/js/jquery-3.1.1.min.js',
                    'assets/js/flickity.min.js',
                    'assets/js/easypiechart.min.js',
                    'assets/js/parallax.js',
                    'assets/js/typed.min.js',
                    'assets/js/datepicker.js',
                    'assets/js/isotope.min.js',
                    'assets/js/ytplayer.min.js',
                    'assets/js/lightbox.min.js',
                    'assets/js/granim.min.js',
                    'assets/js/jquery.steps.min.js',
                    'assets/js/countdown.min.js',
                    'assets/js/twitterfetcher.min.js',
                    'assets/js/spectragram.min.js',
                    'assets/js/smooth-scroll.min.js',
                    'assets/js/scripts.js'
                ],
                dest: "public/dist/app.js"
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('default', ['uglify']);
};