<?php

use App\User;
use App\Driver;
use Illuminate\Support\Facades\Input;

Route::group(['middleware' => ['web']], function () {

    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/', [
        'uses' => 'MainController@landing',
        'as' => 'landing'
    ]);

    Route::get('/about', [
        'uses' => 'MainController@about',
        'as' => 'about'
    ]);

    Route::get('/terms', [
        'uses' => 'MainController@terms',
        'as' => 'terms'
    ]);

    Route::get('/faq', [
        'uses' => 'MainController@faq',
        'as' => 'faq'
    ]);

    Route::get('/partner', [
        'uses' => 'PartnerController@create',
        'as' => 'partner'
    ]);

    Route::post('/partner', [
        'uses' => 'PartnerController@store',
        'as' => 'partner'
    ]);

    Route::get('/vehicle-insurance', [
        'uses' => 'VehicleInsuranceController@create',
        'as' => 'insurance'
    ]);

    Route::post('/vehicle-insurance', [
        'uses' => 'VehicleInsuranceController@store',
        'as' => 'insurance'
    ]);

    Route::get('/car-tracker-tool', [
        'uses' => 'CarTrackerController@create',
        'as' => 'tracker'
    ]);

    Route::post('/car-tracker-tool', [
        'uses' => 'CarTrackerController@store',
        'as' => 'tracker'
    ]);

    Route::get('/client-feedback-tool', [
        'uses' => 'FeedbackController@create',
        'as' => 'feedback'
    ]);

    Route::post('/client-feedback-tool', [
        'uses' => 'FeedbackController@store',
        'as' => 'feedback'
    ]);


    Route::get('/how-it-works', [
        'uses' => 'MainController@how',
        'as' => 'how-it-works'
    ]);

    Route::get('/diamond-tool', [
        'uses' => 'MainController@diamond',
        'as' => 'diamond-tool'
    ]);

    Route::get('/taxify-request-tool', [
        'uses' => 'MainController@taxify',
        'as' => 'taxify-tool'
    ]);

    Route::get('/rider-tool', [
        'uses' => 'MainController@rider',
        'as' => 'rider-tool'
    ]);

    Route::get('/learning-driving-tool', [
        'uses' => 'MainController@learning',
        'as' => 'learning-driving-tool'
    ]);


    Route::get('/api-tool', [
        'uses' => 'MainController@api',
        'as' => 'api-tool'
    ]);

    Route::get('/marketplace-tool', [
        'uses' => 'MainController@marketplace',
        'as' => 'marketplace-tool'
    ]);

    Route::get('/mobile-app', [
        'uses' => 'MainController@mobile',
        'as' => 'mobile-app'
    ]);
    Route::get('driver/pdf/{id}', [
        'uses' => 'PdfController@showPdf',
        
    ])->name('driver/pdf');;

    /**
     * Profile
     *
    */
    Route::get('edit-profile', [
        'uses' => 'ProfileController@edit',
        'as' => 'edit-profile'
    ]);

    Route::post('edit-profile', [
        'uses' => 'ProfileController@update',
        'as' => 'edit-profile'
    ]);

    Route::get('package', [
        'uses' => 'MainController@package',
        'as' => 'package'
    ]);

    /**
     * Full Time
    */
    Route::get('full-time/package', [
        'uses' => 'FullTimeBookingController@viewPackage',
        'as' => 'full-time.package',
        'middleware' => 'auth'
    ]);

    Route::post('full-time/package', [
        'uses' => 'FullTimeBookingController@getPackage',
        'as' => 'full-time.package'
    ]);

    Route::post('full-time/hire', [
        'uses' => 'FullTimeBookingController@save',
        'as' => 'full-time.hire'
    ]);

    Route::get('full-time/bookpay/{id}', [
        'uses' => 'FullTimeBookingController@makePayment',
        'as' => 'full-time.hire'
    ]);

    /**
     * Short Time
     */
    Route::get('short-term', [
        'uses' => 'ShortTimeBookingController@selectLocation',
        'as' => 'short-term',
        'middleware' => 'auth'
    ]);

    Route::post('short-term', [
        'uses' => 'ShortTimeBookingController@getDrivers',
        'as' => 'short-term',
        'middleware' => 'auth'
    ]);

    Route::post('short-term-main', [
        'uses' => 'ShortTimeBookingController@getDrivers',
        'as' => 'short-term',
    ]);

    Route::get('short-term/book/{id}', [
        'uses' => 'ShortTimeBookingController@book',
        'as' => 'short-term.book',
        'middleware' => 'auth'
    ]);

    Route::post('short-term/book/{id}', [
        'uses' => 'ShortTimeBookingController@save',
        'as' => 'short-term.book',
        'middleware' => 'auth'
    ]);

    Route::get('driver/profile/{id}', [
        'uses' => 'ShortTimeBookingController@viewDriver',
        'as' => 'driver.profile',
        'middleware' => 'auth'
    ]);

    /**
     * Uber
     *
    */
    Route::get('uber/package', [
        'uses' => 'UberBookingController@package',
        'as' => 'uber.package',
        'middleware' => 'auth'
    ]);

    Route::any('uber/book', [
        'uses' => 'UberBookingController@getPackage',
        'as' => 'uber.book',
        'middleware' => 'auth'
    ]);

    Route::post('uber/hire', [
        'uses' => 'UberBookingController@uberRequest',
        'as' => 'uber.hire'
    ]);

    Route::get('uber/bookpay/{id}', [
        'uses' => 'UberBookingController@makePayment',
        'as' => 'full-time.hire'
    ]);

     /**
     * Edm
    */
    Route::get('marketplace/package', [
        'uses' => 'EdmBookingController@viewPackage',
        'as' => 'marketplace.package',
        'middleware' => 'auth'
    ]);

    Route::get('marketplace/home', [
        'uses' => 'EdmBookingController@home',
        'as' => 'marketplace.home',
        'middleware' => 'auth'
    ]);
    Route::get('marketplace', [
        'uses' => 'EdmBookingController@welcome',
        'as' => 'marketplace.welcome'
    ]);
    Route::get('marketplace/posts', [
        'uses' => 'EdmBookingController@post',
        'as' => 'marketplace.post',
        'middleware' => 'auth'
    ]);

    Route::post('edm/hire', [
        'uses' => 'EdmBookingController@save',
        'as' => 'edm.hire'
    ]);

    Route::get('full-time/bookpay/{id}', [
        'uses' => 'EdmBookingController@makePayment',
        'as' => 'edm.hire'
    ]);

    Route::post('marketplace/addcart/{id}', [
        'uses' => 'EdmBookingController@addToCart',
        'as' => 'edm.cart'
    ]);



    /**
     * Rate Driver
     *
    */
    Route::get('driver/rate/{id}', [
        'uses' => 'RatingController@ratingView',
        'as' => 'driver.rate',
        'middleware' => 'auth'
    ]);

    Route::post('driver/rate/{id}', [
        'uses' => 'RatingController@rate',
        'as' => 'driver.rate',
        'middleware' => 'auth'
    ]);

    /**
     * Pay Per Use
     */
    Route::get('pay-use', [
        'uses' => 'PayBookingController@create',
        'as' => 'pay-use',
        'middleware' => 'auth'
    ]);

    Route::post('pay-use/hire', [
        'uses' => 'PayBookingController@save',
        'as' => 'pay-use.hire'
    ]);

    /**
     * Transfer
     */
    Route::get('transfer/book', [
        'uses' => 'TransferBookingController@create',
        'as' => 'transfer',
        'middleware' => 'auth'
    ]);

    Route::post('transfer/hire', [
        'uses' => 'TransferBookingController@save',
        'as' => 'transfer.hire'
    ]);

    /**
     * Dashboard
     *
    */
    Route::get('dashboard/full-time', [
        'uses' => 'DashboardController@listFullTime',
        'as' => 'dashboard.full-time',
        'middleware' => 'auth'
    ]);

    Route::get('dashboard/full-time/upload/{id}', [
        'uses' => 'DashboardController@viewFullTimeDocUpload',
        'as' => 'dashboard.full-time.upload',
        'middleware' => 'auth'
    ]);

    Route::post('dashboard/full-time/upload/{id}', [
        'uses' => 'DashboardController@saveFullTimeDocUpload',
        'as' => 'dashboard.full-time.upload',
        'middleware' => 'auth'
    ]);

    Route::get('dashboard/short-time', [
        'uses' => 'DashboardController@listShortTime',
        'as' => 'dashboard.short-time',
        'middleware' => 'auth'
    ]);

    Route::get('dashboard/short-time/upload/{id}', [
        'uses' => 'DashboardController@viewShortTimeDocUpload',
        'as' => 'dashboard.short-time.upload',
        'middleware' => 'auth'
    ]);

    Route::post('dashboard/short-time/upload/{id}', [
        'uses' => 'DashboardController@saveShortTimeDocUpload',
        'as' => 'dashboard.short-time.upload',
        'middleware' => 'auth'
    ]);


    Route::get('dashboard/uber', [
        'uses' => 'DashboardController@listUber',
        'as' => 'dashboard.uber',
        'middleware' => 'auth'
    ]);

    Route::get('dashboard/uber/upload/{id}', [
        'uses' => 'DashboardController@viewUberDocUpload',
        'as' => 'dashboard.uber.upload',
        'middleware' => 'auth'
    ]);

    Route::post('dashboard/uber/upload/{id}', [
        'uses' => 'DashboardController@saveUberDocUpload',
        'as' => 'dashboard.uber.upload',
        'middleware' => 'auth'
    ]);

    Route::get('dashboard/matched/drivers/{id}', [
        'uses' => 'DashboardController@viewMatchedDrivers',
        'as' => 'dashboard.matched.drivers',
        'middleware' => 'auth'
    ]);


    Route::get('dashboard/matched/driver/{id}', [
        'uses' => 'DashboardController@viewMatchedDriver',
        'as' => 'dashboard.matched.driver',
        'middleware' => 'auth'
    ]);

    Route::get('dashboard/transfer', [
        'uses' => 'DashboardController@listTransfer',
        'as' => 'dashboard.transfer',
        'middleware' => 'auth'
    ]);

    Route::get('dashboard/pay-use', [
        'uses' => 'DashboardController@listPayUse',
        'as' => 'dashboard.pay-use',
        'middleware' => 'auth'
    ]);

    Route::get('dashboard/pay-use/upload/{id}', [
        'uses' => 'DashboardController@viewPayUseDocUpload',
        'as' => 'dashboard.pay-use.upload',
        'middleware' => 'auth'
    ]);

    Route::post('dashboard/pay-use/upload/{id}', [
        'uses' => 'DashboardController@savePayUseDocUpload',
        'as' => 'dashboard.pay-use.upload',
        'middleware' => 'auth'
    ]);

    /**
     * Payment
     *
    */
    Route::post('/pay', [
        'uses' => 'PaymentController@redirectToGateway',
        'as' => 'pay'
    ]);

    Route::get('payment/callback', [
        'uses' => 'PaymentController@handleGatewayCallback'
    ]);
    Route::post('/edmpay', [
        'uses' => 'EdmPaymentController@redirectToGateway',
        'as' => 'pay'
    ]);

    Route::get('edmpayment/callback', [
        'uses' => 'EdmPaymentController@handleGatewayCallback'
    ]);

});


Route::group(['middleware' => ['admin']], function () {

    Route::get('admin', [
        'uses' => 'AdminController@dashboard',
        'as' => 'admin'
    ]);

    /**
     * Uber
     */
    Route::get('uber/new', [
        'uses' => 'UberController@create',
        'as' => 'uber.new'
    ]);

    Route::post('uber/new', [
        'uses' => 'UberController@store',
        'as' => 'uber.new'
    ]);

    Route::get('uber/list', [
        'uses' => 'UberController@show',
        'as' => 'uber.list'
    ]);

    Route::get('uber/edit/{id}', [
        'uses' => 'UberController@edit',
        'as' => 'uber.edit'
    ]);

    Route::post('uber/edit/{id}', [
        'uses' => 'UberController@update',
        'as' => 'uber.edit'
    ]);

    Route::get('uber/delete/{id}', [
        'uses' => 'UberController@delete',
        'as' => 'uber.delete'
    ]);

    Route::get('uber/verify/{id}', [
        'uses' => 'UberController@verify',
        'as' => 'uber.verify'
    ]);
    Route::get('uber/blacklist/{id}', [
        'uses' => 'UberController@blacklist',
        'as' => 'uber.blacklist'
    ]);

    Route::get('uber/guarantor/{id}', [
        'uses' => 'UberController@edit_guarantor',
        'as' => 'short-time.edit'
    ]);

    Route::post('uber/guarantor/{id}', [
        'uses' => 'UberController@guarantor',
        'as' => 'short-time.edit'
    ]);

    Route::get('uber/card/{id}', [
        'uses' => 'UberController@card',
        'as' => 'full-time.verify'
    ]);


    /**
     * Short-Time
     */
    Route::get('short-time/new', [
        'uses' => 'ShortTimeController@create',
        'as' => 'short-time.new'
    ]);

    Route::post('short-time/new', [
        'uses' => 'ShortTimeController@store',
        'as' => 'short-time.new'
    ]);

    Route::get('short-time/list', [
        'uses' => 'ShortTimeController@show',
        'as' => 'short-time.list'
    ]);

    Route::get('short-time/edit/{id}', [
        'uses' => 'ShortTimeController@edit',
        'as' => 'short-time.edit'
    ]);

    Route::post('short-time/edit/{id}', [
        'uses' => 'ShortTimeController@update',
        'as' => 'short-time.edit'
    ]);

    Route::get('short-time/delete/{id}', [
        'uses' => 'ShortTimeController@delete',
        'as' => 'short-time.delete'
    ]);

    Route::get('short-time/verify/{id}', [
        'uses' => 'ShortTimeController@verify',
        'as' => 'short-time.verify'
    ]);
    Route::get('short-time/blacklist/{id}', [
        'uses' => 'ShortTimeController@blacklist',
        'as' => 'short-time.blacklist'
    ]);

    Route::get('short-time/guarantor/{id}', [
        'uses' => 'ShortTimeController@edit_guarantor',
        'as' => 'short-time.edit'
    ]);

    Route::post('short-time/guarantor/{id}', [
        'uses' => 'ShortTimeController@guarantor',
        'as' => 'short-time.edit'
    ]);

    Route::get('short-time/card/{id}', [
        'uses' => 'ShortTimeController@card',
        'as' => 'full-time.verify'
    ]);


    /**
     * Full Time
     */
    Route::get('full-time/new', [
        'uses' => 'FullTimeController@create',
        'as' => 'full-time.new'
    ]);

    Route::post('full-time/new', [
        'uses' => 'FullTimeController@store',
        'as' => 'full-time.new'
    ]);

    Route::get('full-time/list', [
        'uses' => 'FullTimeController@show',
        'as' => 'full-time.list'
    ]);

    Route::get('full-time/profile/{id}', [
        'uses' => 'FullTimeController@showFull',
        'as' => 'full-time.list'
    ]);

    Route::get('full-time/edit/{id}', [
        'uses' => 'FullTimeController@edit',
        'as' => 'full-time.edit'
    ]);

    Route::post('full-time/edit/{id}', [
        'uses' => 'FullTimeController@update',
        'as' => 'full-time.edit'
    ]);

    Route::get('full-time/delete/{id}', [
        'uses' => 'FullTimeController@delete',
        'as' => 'full-time.delete'
    ]);
    Route::get('full-time/verify/{id}', [
        'uses' => 'FullTimeController@verify',
        'as' => 'full-time.verify'
    ]);
    Route::get('full-time/blacklist/{id}', [
        'uses' => 'FullTimeController@blacklist',
        'as' => 'full-time.blacklist'
    ]);
    Route::get('full-time/pdf/{id}', [
        'uses' => 'FullTimeController@pdfdownload',
        
    ]);

    Route::get('full-time/guarantor/{id}', [
        'uses' => 'FullTimeController@edit_guarantor',
        'as' => 'short-time.edit'
    ]);

    Route::post('full-time/guarantor/{id}', [
        'uses' => 'FullTimeController@guarantor',
        'as' => 'short-time.edit'
    ]);

    Route::get('full-time/card/{id}', [
        'uses' => 'FullTimeController@card',
        'as' => 'full-time.verify'
    ]);


    /**
     * Full-time Bookings
     *
    */
    Route::get('bookings/full-time', [
        'uses' => 'BookingController@viewFullTimeBookings',
        'as' => 'bookings.full-time'
    ]);

    Route::get('booking/full-time/view/{id}', [
        'uses' => 'BookingController@viewFullTimeBooking',
        'as' => 'bookings.full-time.view'
    ]);

    Route::get('booking/full-time/payment/{id}', [
        'uses' => 'BookingController@confirmFullTimeBookingPayment',
        'as' => 'bookings.full-time.payment'
    ]);

    Route::get('booking/full-time/payment/unconfirm/{id}', [
        'uses' => 'BookingController@unconfirmFullTimeBookingPayment',
        'as' => 'bookings.full-time.payment.unconfirm'
    ]);

    Route::get('booking/full-time/delete/{id}', [
        'uses' => 'BookingController@deleteFullTimeBooking',
        'as' => 'bookings.full-time.delete'
    ]);

    Route::get('booking/full-time/match/{id}', [
        'uses' => 'BookingController@viewFullTimeMatchDriverToClient',
        'as' => 'bookings.full-time.match'
    ]);

    Route::post('booking/full-time/match/{id}', [
        'uses' => 'BookingController@matchFullTimeDriverToClient',
        'as' => 'bookings.full-time.match'
    ]);

    Route::get('booking/full-time/upload/{id}', [
        'uses' => 'BookingController@viewFullTimeDocUpload',
        'as' => 'booking.full-time.upload',
    ]);

    Route::post('booking/full-time/upload/{id}', [
        'uses' => 'BookingController@saveFullTimeDocUpload',
        'as' => 'booking.full-time.upload',
    ]);
    Route::get('booking/full-time/uploadsla/{id}', [
        'uses' => 'BookingController@viewFullTimeSlaDocUpload',
        'as' => 'booking.full-time.upload',
    ]);

    Route::post('booking/full-time/uploadsla/{id}', [
        'uses' => 'BookingController@saveFullTimeSlaDocUpload',
        'as' => 'booking.full-time.upload',
    ]);

    /**
     * Short-time Bookings
     *
     */
    Route::get('bookings/short-time', [
        'uses' => 'BookingController@viewShortTimeBookings',
        'as' => 'bookings.short-time'
    ]);

    Route::get('booking/short-time/view/{id}', [
        'uses' => 'BookingController@viewShortTimeBooking',
        'as' => 'bookings.short-time.view'
    ]);

    Route::get('booking/short-time/payment/{id}', [
        'uses' => 'BookingController@confirmShortTimeBookingPayment',
        'as' => 'bookings.short-time.payment'
    ]);

    Route::get('booking/short-time/payment/unconfirm/{id}', [
        'uses' => 'BookingController@unconfirmShortTimeBookingPayment',
        'as' => 'bookings.short-time.payment.unconfirm'
    ]);

    Route::get('booking/short-time/delete/{id}', [
        'uses' => 'BookingController@deleteShortTimeBooking',
        'as' => 'bookings.short-time.delete'
    ]);

    Route::get('booking/short-time/match/{id}', [
        'uses' => 'BookingController@viewShortTimeMatchDriverToClient',
        'as' => 'bookings.short-time.match'
    ]);

    Route::post('booking/short-time/match/{id}', [
        'uses' => 'BookingController@matchShortTimeDriverToClient',
        'as' => 'bookings.short-time.match'
    ]);

    Route::get('booking/short-time/upload/{id}', [
        'uses' => 'BookingController@viewShortTimeDocUpload',
        'as' => 'booking.short-time.upload',
    ]);

    Route::post('booking/short-time/upload/{id}', [
        'uses' => 'BookingController@saveShortTimeDocUpload',
        'as' => 'booking.short-time.upload',
    ]);

    Route::get('booking/short-time/uploadsla/{id}', [
        'uses' => 'BookingController@viewShortTimeSlaDocUpload',
        'as' => 'booking.short-time.upload',
    ]);

    Route::post('booking/short-time/uploadsla/{id}', [
        'uses' => 'BookingController@saveShortTimeSlaDocUpload',
        'as' => 'booking.short-time.upload',
    ]);

    /**
     * Uber Bookings
     *
     */
    Route::get('bookings/uber', [
        'uses' => 'BookingController@viewUberBookings',
        'as' => 'bookings.uber'
    ]);

    Route::get('booking/uber/view/{id}', [
        'uses' => 'BookingController@viewUberBooking',
        'as' => 'bookings.uber.view'
    ]);

    Route::get('booking/uber/payment/{id}', [
        'uses' => 'BookingController@confirmUberBookingPayment',
        'as' => 'bookings.uber.payment'
    ]);

    Route::get('booking/uber/payment/unconfirm/{id}', [
        'uses' => 'BookingController@unconfirmUberBookingPayment',
        'as' => 'bookings.uber.payment.unconfirm'
    ]);

    Route::get('booking/uber/delete/{id}', [
        'uses' => 'BookingController@deleteUberBooking',
        'as' => 'bookings.uber.delete'
    ]);

    Route::get('booking/uber/match/{id}', [
        'uses' => 'BookingController@viewUberMatchDriverToClient',
        'as' => 'bookings.uber.match'
    ]);

    Route::post('booking/uber/match/{id}', [
        'uses' => 'BookingController@matchUberDriverToClient',
        'as' => 'bookings.uber.match'
    ]);

    Route::get('booking/uber/upload/{id}', [
        'uses' => 'BookingController@viewUberDocUpload',
        'as' => 'booking.uber.upload',
    ]);

    Route::post('booking/uber/upload/{id}', [
        'uses' => 'BookingController@saveUberDocUpload',
        'as' => 'booking.uber.upload',
    ]);

    Route::get('booking/uber/uploadsla/{id}', [
        'uses' => 'BookingController@viewUberSlaDocUpload',
        'as' => 'booking.uber.upload',
    ]);

    Route::post('booking/uber/uploadsla/{id}', [
        'uses' => 'BookingController@saveUberSlaDocUpload',
        'as' => 'booking.uber.upload',
    ]);

    /**
     * Transfer Bookings
     *
     */
    Route::get('bookings/transfer', [
        'uses' => 'BookingController@viewTransferBookings',
        'as' => 'bookings.transfer'
    ]);

    Route::get('booking/transfer/view/{id}', [
        'uses' => 'BookingController@viewTransferBooking',
        'as' => 'bookings.transfer.view'
    ]);

    Route::get('booking/transfer/payment/{id}', [
        'uses' => 'BookingController@confirmTransferBookingPayment',
        'as' => 'bookings.transfer.payment'
    ]);

    Route::get('booking/transfer/payment/unconfirm/{id}', [
        'uses' => 'BookingController@unconfirmTransferBookingPayment',
        'as' => 'bookings.transfer.payment.unconfirm'
    ]);

    Route::get('booking/transfer/delete/{id}', [
        'uses' => 'BookingController@deleteTransferBooking',
        'as' => 'bookings.transfer.delete'
    ]);

    Route::get('booking/transfer/match/{id}', [
        'uses' => 'BookingController@viewTransferMatchDriverToClient',
        'as' => 'bookings.transfer.match'
    ]);

    Route::post('booking/transfer/match/{id}', [
        'uses' => 'BookingController@matchTransferDriverToClient',
        'as' => 'bookings.transfer.match'
    ]);

    Route::get('booking/transfer/upload/{id}', [
        'uses' => 'BookingController@viewTransferDocUpload',
        'as' => 'booking.transfer.upload',
    ]);

    Route::post('booking/transfer/upload/{id}', [
        'uses' => 'BookingController@saveTransferDocUpload',
        'as' => 'booking.transfer.upload',
    ]);

    /**
     * Pay-Use Bookings
     *
     */
    Route::get('bookings/pay-use', [
        'uses' => 'BookingController@viewPayUseBookings',
        'as' => 'bookings.pay-use'
    ]);

    Route::get('booking/pay-use/view/{id}', [
        'uses' => 'BookingController@viewPayUseBooking',
        'as' => 'bookings.pay-use.view'
    ]);

    Route::get('booking/pay-use/payment/{id}', [
        'uses' => 'BookingController@confirmPayUseBookingPayment',
        'as' => 'bookings.pay-use.payment'
    ]);

    Route::get('booking/pay-use/payment/unconfirm/{id}', [
        'uses' => 'BookingController@unconfirmPayUseBookingPayment',
        'as' => 'bookings.pay-use.payment.unconfirm'
    ]);

    Route::get('booking/pay-use/delete/{id}', [
        'uses' => 'BookingController@deletePayUseBooking',
        'as' => 'bookings.pay-use.delete'
    ]);

    Route::get('booking/pay-use/match/{id}', [
        'uses' => 'BookingController@viewPayUseMatchDriverToClient',
        'as' => 'bookings.pay-use.match'
    ]);

    Route::post('booking/pay-use/match/{id}', [
        'uses' => 'BookingController@matchPayUseDriverToClient',
        'as' => 'bookings.pay-use.match'
    ]);

    Route::get('booking/pay-use/upload/{id}', [
        'uses' => 'BookingController@viewPayUseDocUpload',
        'as' => 'booking.pay-use.upload',
    ]);

    Route::post('booking/pay-use/upload/{id}', [
        'uses' => 'BookingController@savePayUseDocUpload',
        'as' => 'booking.pay-use.upload',
    ]);

    /**
     * Matches
     *
     */
    Route::get('matches', [
        'uses' => 'MatchController@show',
        'as' => 'matches'
    ]);

    Route::get('match/delete/{id}', [
        'uses' => 'MatchController@delete',
        'as' => 'match.delete'
    ]);


    /**
     * Payments
     *
     */
    Route::get('payments', [
        'uses' => 'PaymentController@show',
        'as' => 'payments'
    ]);

    /**
     * Partner
     *
    */
    Route::get('partners', [
        'uses' => 'PartnerController@show',
        'as' => 'partners'
    ]);

    Route::get('partner/{id}', [
        'uses' => 'PartnerController@view',
        'as' => 'partners'
    ]);

    
    /**
     * Vehicle Insurance
     *
    */
    Route::get('insurance', [
        'uses' => 'VehicleInsuranceController@show',
        'as' => 'insurance'
    ]);

    Route::get('insurance/{id}', [
        'uses' => 'VehicleInsuranceController@view',
        'as' => 'insurance'
    ]);

    /**
     * Car Tracker 
     *
    */
    Route::get('tracker', [
        'uses' => 'CarTrackerController@show',
        'as' => 'tracker'
    ]);

    Route::get('tracker/{id}', [
        'uses' => 'CarTrackerController@view',
        'as' => 'tracker'
    ]);

    /**
     * feedback
     *
    */
    Route::get('feedback', [
        'uses' => 'FeedbackController@show',
        'as' => 'feedback'
    ]);

    Route::get('feedback/{id}', [
        'uses' => 'FeedbackController@view',
        'as' => 'feedback'
    ]);

    /**
     * Users
     *
     */
    Route::get('users/list', [
        'uses' => 'UserController@show',
        'as' => 'users.list'
    ]);
});
Route::get('drive/home','DriverController@index');
Route::get('drive/sdtm','MainController@sdtm');
Route::get('drive/earn','MainController@cash');
Route::get('drive/full-time-success','DriverController@fullSuccess');
Route::get('drive/short-term-success','DriverController@shortSuccess');
Route::get('drive/uber-success','DriverController@uberSuccess');
Route::get('driver','MainController@welcome')->name('driver');
Route::get('drive/update','DriverController@update');
Route::get('drive/profile','DriversController@profile');
Route::get('drive/training','DriverController@training');
Route::get('drive/navigator','DriverController@navigator');
Route::get('drive/smartbook','DriverController@smartbook');
Route::get('drive/membership','DriverController@membership');


Route::get('drive/refer','DriverController@refer');
Route::post('drive/refer','DriverController@referMail');

Route::get('drive/full-time', [
    'uses' => 'FullTimeMessageController@create'
]);

Route::post('drive/full-time', [
    'uses' => 'FullTimeMessageController@store',
    'as' => 'fulltime.store'
]);

Route::get('drive/short-term', [
    'uses' => 'ShortTermMessageController@create'
]);

Route::post('drive/short-term', [
    'uses' => 'ShortTermMessageController@store',
    'as' => 'shortterm.store'
]);

Route::get('drive/uber', [
    'uses' => 'UberMessageController@create'
]);

Route::post('drive/uber', [
    'uses' => 'UberMessageController@store',
    'as' => 'uber.store'
]);


/**
     * Full Time
     */
    Route::get('drive/new', [
        'uses' => 'DriversController@create',
        'as' => 'full-time.new'
    ]);

    Route::post('drive/edit-profile', [
        'uses' => 'DriversController@store',
        'as' => 'full-time.new'
    ]);

    Route::get('drive/list', [
        'uses' => 'DriversController@show',
        'as' => 'full-time.list'
    ]);

    Route::get('drive/edit/{id}', [
        'uses' => 'DriversController@edit',
        'as' => 'full-time.edit'
    ]);

    Route::post('drive/edit/{id}', [
        'uses' => 'DriversController@update',
        'as' => 'full-time.edit'
    ]);

    Route::get('drive/update/{id}', [
        'uses' => 'DriversController@editSingle',
        'as' => 'full-time.edit'
    ]);

    Route::post('drive/update/{id}', [
        'uses' => 'DriversController@updateSingle',
        'as' => 'full-time.edit'
    ]);
    


    /**
     * Short Time
     */
    Route::get('drive/new-short', [
        'uses' => 'DriversControllerShort@create',
        'as' => 'full-time.new'
    ]);

    Route::post('drive/edit-profile-short', [
        'uses' => 'DriversControllerShort@store',
        'as' => 'full-time.new'
    ]);

    Route::get('drive/list-short', [
        'uses' => 'DriversControllerShort@show',
        'as' => 'full-time.list'
    ]);

    Route::get('drive/edit-short/{id}', [
        'uses' => 'DriversControllerShort@edit',
        'as' => 'full-time.edit'
    ]);

    Route::post('drive/edit-short/{id}', [
        'uses' => 'DriversControllerShort@update',
        'as' => 'full-time.edit'
    ]);


    /**
     * Uber
     */
    Route::get('drive/new-uber', [
        'uses' => 'DriversControllerUber@create',
        'as' => 'full-time.new'
    ]);

    Route::post('drive/edit-profile-uber', [
        'uses' => 'DriversControllerUber@store',
        'as' => 'full-time.new'
    ]);

    Route::get('drive/list-uber', [
        'uses' => 'DriversControllerUber@show',
        'as' => 'full-time.list'
    ]);

    Route::get('drive/edit-uber/{id}', [
        'uses' => 'DriversControllerUber@edit',
        'as' => 'full-time.edit'
    ]);

    Route::post('drive/edit-uber/{id}', [
        'uses' => 'DriversControllerUber@update',
        'as' => 'full-time.edit'
    ]);

    /**
     * Vehicle Owner Drivers
     */
    Route::get('drive/new-vehicle', [
        'uses' => 'DriversControllerVehicle@create',
        'as' => 'full-time.new'
    ]);

    Route::post('drive/edit-profile-vehicle', [
        'uses' => 'DriversControllerVehicle@store',
        'as' => 'full-time.new'
    ]);

    Route::get('drive/list-vehicle', [
        'uses' => 'DriversControllerVehicle@show',
        'as' => 'full-time.list'
    ]);

    Route::get('drive/edit-vehicle/{id}', [
        'uses' => 'DriversControllerVehicle@edit',
        'as' => 'full-time.edit'
    ]);

    Route::post('drive/edit-vehicle/{id}', [
        'uses' => 'DriversControllerVehicle@update',
        'as' => 'full-time.edit'
    ]);

    /**
     * Driving Tutors
     */
    Route::get('drive/new-tutor', [
        'uses' => 'DriversControllerTutor@create',
        'as' => 'full-time.new'
    ]);

    Route::post('drive/edit-profile-tutor', [
        'uses' => 'DriversControllerTutor@store',
        'as' => 'full-time.new'
    ]);

    Route::get('drive/list-tutor', [
        'uses' => 'DriversControllerTutor@show',
        'as' => 'full-time.list'
    ]);

    Route::get('drive/edit-tutor/{id}', [
        'uses' => 'DriversControllerVehicle@edit',
        'as' => 'full-time.edit'
    ]);

    Route::post('drive/edit-tutor/{id}', [
        'uses' => 'DriversControllerTutor@update',
        'as' => 'full-time.edit'
    ]);

    /**
     * Driving School
     */
    Route::get('drive/new-school', [
        'uses' => 'DriversControllerSchool@create',
        'as' => 'full-time.new'
    ]);

    Route::post('drive/edit-profile-school', [
        'uses' => 'DriversControllerSchool@store',
        'as' => 'full-time.new'
    ]);

    Route::get('drive/list-school', [
        'uses' => 'DriversControllerSchool@show',
        'as' => 'full-time.list'
    ]);

    Route::get('drive/edit-vehicle/{id}', [
        'uses' => 'DriversControllerSchool@edit',
        'as' => 'full-time.edit'
    ]);

    Route::post('drive/edit-vehicle/{id}', [
        'uses' => 'DriversControllerSchool@update',
        'as' => 'full-time.edit'
    ]);

    /**
     * Driverlancers
     */
    Route::get('drive/new-driverlancer', [
        'uses' => 'DriversControllerDriverlancer@create',
        'as' => 'full-time.new'
    ]);

    Route::post('drive/edit-profile-driverlancer', [
        'uses' => 'DriversControllerDriverlancer@store',
        'as' => 'full-time.new'
    ]);

    Route::get('drive/list-driverlancer', [
        'uses' => 'DriversControllerDriverlancer@show',
        'as' => 'full-time.list'
    ]);

    Route::get('drive/edit-driverlancer/{id}', [
        'uses' => 'DriversControllerDriverlancer@edit',
        'as' => 'full-time.edit'
    ]);

    Route::post('drive/edit-driverlancer/{id}', [
        'uses' => 'DriversControllerDriverlancer@update',
        'as' => 'full-time.edit'
    ]);

    /**
     * Graduate Drivers
     */
    Route::get('drive/new-graduate', [
        'uses' => 'DriversControllerGraduate@create',
        'as' => 'full-time.new'
    ]);

    Route::post('drive/edit-profile-graduate', [
        'uses' => 'DriversControllerGraduate@store',
        'as' => 'full-time.new'
    ]);

    Route::get('drive/list-graduate', [
        'uses' => 'DriversControllerGraduate@show',
        'as' => 'full-time.list'
    ]);

    Route::get('drive/edit-graduate/{id}', [
        'uses' => 'DriversControllerGraduate@edit',
        'as' => 'full-time.edit'
    ]);

    Route::post('drive/edit-graduate/{id}', [
        'uses' => 'DriversControllerGraduate@update',
        'as' => 'full-time.edit'
    ]);

    /**
     * Rider
     */
    Route::get('drive/new-rider', [
        'uses' => 'DriversControllerRider@create',
        'as' => 'full-time.new'
    ]);

    Route::post('drive/edit-profile-rider', [
        'uses' => 'DriversControllerRider@store',
        'as' => 'full-time.new'
    ]);

    Route::get('drive/list-rider', [
        'uses' => 'DriversControllerRider@show',
        'as' => 'full-time.list'
    ]);

    Route::get('drive/edit-rider/{id}', [
        'uses' => 'DriversControllerRider@edit',
        'as' => 'full-time.edit'
    ]);

    Route::post('drive/edit-rider/{id}', [
        'uses' => 'DriversControllerRider@update',
        'as' => 'full-time.edit'
    ]);



    /**
     * Remit
     */
    Route::get('remit/new', [
        'uses' => 'RemitController@create',
        'as' => 'full-time.new'
    ]);

    Route::post('remit/new', [
        'uses' => 'RemitController@store',
        'as' => 'full-time.new'
    ]);

    Route::get('remit/list', [
        'uses' => 'RemitController@show',
        'as' => 'full-time.list'
    ]);

    Route::get('remit/profile/{id}', [
        'uses' => 'RemitController@showFull',
        'as' => 'full-time.list'
    ]);

    Route::get('remit/edit/{id}', [
        'uses' => 'RemitController@edit',
        'as' => 'full-time.edit'
    ]);

    Route::post('remit/edit/{id}', [
        'uses' => 'RemitController@update',
        'as' => 'full-time.edit'
    ]);

    Route::get('remit/delete/{id}', [
        'uses' => 'RemitController@delete',
        'as' => 'full-time.delete'
    ]);
    
    Route::get('remit/pdf/{id}', [
        'uses' => 'RemitController@pdfdownload',
        
    ]);

    /**
     * Report
     */
    Route::get('report/new/{id}', [
        'uses' => 'ReportController@create',
        'as' => 'full-time.new'
    ]);

    Route::post('report/new/{id}', [
        'uses' => 'ReportController@store',
        'as' => 'full-time.new'
    ]);

    Route::get('report/list/{id}', [
        'uses' => 'ReportController@show',
        'as' => 'full-time.list'
    ]);

    Route::get('report/edit/{id}', [
        'uses' => 'ReportController@edit',
        'as' => 'full-time.edit'
    ]);

    Route::post('report/edit/{id}', [
        'uses' => 'ReportController@update',
        'as' => 'full-time.edit'
    ]);

    Route::get('report/delete/{id}', [
        'uses' => 'ReportController@delete',
        'as' => 'full-time.delete'
    ]);
    
    Route::get('report/pdf/{id}', [
        'uses' => 'ReportController@pdfdownload',
        
    ]);

    Route::get('report/sendmail/{id}', [
        'uses' => 'ReportController@sendmail',
        
    ]);

    Route::get('report/sheet/{id}', [
        'uses' => 'ReportController@showFull',
        'as' => 'full-time.list'
    ]);

    Route::any('/search',function(){
        $q = Input::get ( 'q' );
        $user = Driver::where('firstname','LIKE','%'.$q.'%')->orWhere('email','LIKE','%'.$q.'%')->orWhere('lastname','LIKE','%'.$q.'%')->get();
        if(count($user) > 0)
            return view('admin.search')->withDetails($user)->withQuery ( $q );
        else return view ('admin.search')->withMessage('No Details found. Try to search again !');
    });

Route::get('drive','Driver\LoginController@showLoginForm')->name('driver.login');
Route::post('drive','Driver\LoginController@login');

Route::get('drive/register','Driver\RegisterController@showRegistrationForm')->name('driver.register');
Route::post('drive/register','Driver\RegisterController@register');

Route::get('drive/register/{id}','Driver\RegisterController@showRegistrationForms')->name('driver.refregister');
Route::post('drive/register/{id}','Driver\RegisterController@refReg');

Route::post('driver-password/email','Driver\ForgotPasswordController@sendResetLinkEmail')->name('driver.password.email');
Route::get('driver-password/reset','Driver\ForgotPasswordController@showLinkRequestForm')->name('driver.password.request');
Route::post('driver-password/reset','Driver\ResetPasswordController@reset');
Route::get('driver-password/reset/{token}','Driver\ForgotPasswordController@showResetForm')->name('driver.password.reset');

Route::domain('drive.driversng.com')->group(function () {
    Route::get('/','MainController@welcome')->name('driver');

});