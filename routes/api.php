<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'ApiController@userLogin');
Route::post('register', 'ApiController@userRegister');
Route::post('isregistered', 'ApiController@isRegistered');
Route::post('book-full-time', 'ApiController@bookFullTime');
Route::post('add-driver', 'ApiController@addDriver');
Route::group(['middleware' => 'auth:api'],function(){
    Route::get('users', 'ApiController@userDetails');
    Route::get('users/{id}', 'ApiController@userDetail');
    Route::get('full-time', 'ApiController@fullTime');
    Route::get('short-term', 'ApiController@shortTime');
    Route::get('uber', 'ApiController@uber');
    Route::post('full-time-location', 'ApiController@fullTimeLoc');
    Route::post('short-term-location', 'ApiController@shortTimeLoc');
    Route::post('uber-location', 'ApiController@uberLoc');
    Route::post('update-user', 'ApiController@userUpdate');
    
});


