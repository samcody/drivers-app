<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'full_name' => 'DRIVERSNG ADMIN',
            'email' => 'admin@driversng.com',
            'mobile_number' => '08100000000',
            'role' => 'ROLE_ADMIN',
            'password' => bcrypt('secret'),
        ],
        [
            'full_name' => 'Customer Care',
            'email' => 'support@driversng.com',
            'mobile_number' => '08100000001',
            'role' => 'ROLE_ADMIN',
            'password' => bcrypt('customersupport'),
        ]
        );
    }
}
