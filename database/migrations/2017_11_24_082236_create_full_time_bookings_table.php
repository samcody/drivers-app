<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFullTimeBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('full_time_bookings', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('booking_id');
            $table->string('user_id');
            $table->text('driver_id')->nullable();
            $table->string('package');
            $table->integer('number_of_driver')->default(1);
            $table->integer('number_of_driver_unmatched')->default(1);
            $table->string('resumption_date');
            $table->string('driver_stay');
            $table->string('use_type');
            $table->string('resumption_time');
            $table->string('state_of_residence');
            $table->string('vehicle_type');
            $table->string('closing_time');
            $table->string('nearest_location');
            $table->string('driver_gender');
            $table->string('transmission');
            $table->string('salary_package');
            $table->string('salary_frequency');
            $table->string('provide_driver_accommodation');
            $table->string('insurance_type');
            $table->string('sla_document')->nullable();
            $table->string('guarantor_form')->nullable();
            $table->boolean('has_paid')->default(false);
            $table->boolean('is_matched')->default(false);
            $table->boolean('status')->default(true);
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('full_time_bookings');
    }
}
