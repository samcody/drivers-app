<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('serial_number')->unique();
            $table->string('firstname');
            $table->string('lastname');
            $table->string('age')->default(18);
            $table->string('marital_status');
            $table->string('religion');
            $table->string('phonenumber');
            $table->string('mobilenumber');
            $table->string('email')->unique();
            $table->text('homeaddress');
            $table->string('location');
            $table->string('residence');
            $table->string('profilepicture')->default('avatar.png');
            $table->string('accountnumber');
            $table->text('experience')->nullable();
            $table->text('can_drive')->nullable();
            $table->text('license')->nullable();
            $table->text('lasdri')->nullable();
            $table->string('smsotp')->nullable();
            $table->tinyInteger('rating')->nullable();
            $table->string('license_date')->nullable();
            $table->string('lasdri_date')->nullable();
            $table->string('preference')->nullable();
            $table->string('status')->nullable();
            $table->string('clients_number')->nullable();
            $table->string('package_client')->nullable();
            $table->integer('paired_date')->nullable();
            $table->integer('unpaired_date')->nullable();
            $table->string('driver_type');
            $table->string('bank_name')->nullable();
            $table->integer('is_matched')->default(false);
            $table->integer('is_deleted')->default(false);
            
            

            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
