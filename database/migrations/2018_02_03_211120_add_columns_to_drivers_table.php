<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drivers', function (Blueprint $table) {
            $table->integer('is_verified')->default(false);
            $table->string('hair_color')->nullable();
            $table->string('height')->nullable();
            $table->string('weight')->nullable();
            $table->string('uniform_size')->nullable();
            $table->string('educational_profile')->nullable();
            $table->string('father_name')->default('NIL');
            $table->string('father_number')->default('NIL');
            $table->string('mother_name')->default('NIL');
            $table->string('mother_number')->default('NIL');
            $table->string('wife_name')->default('NIL');
            $table->string('wife_number')->default('NIL');
            $table->string('sisterInLaw_name')->default('NIL');
            $table->string('sisterInLaw_number')->default('NIL');
            $table->string('uncle_name')->default('NIL');
            $table->string('uncle_number')->default('NIL');
            $table->string('neighbour_name')->default('NIL');
            $table->string('neighbour_number')->default('NIL');
            $table->string('fatherInLaw_name')->default('NIL');
            $table->string('fatherInLaw_number')->default('NIL');
            $table->string('uncleInLaw_name')->default('NIL');
            $table->string('uncleInLaw_number')->default('NIL');
            $table->string('brother_name')->default('NIL');
            $table->string('brother_number')->default('NIL');
            $table->string('sister_name')->default('NIL');
            $table->string('sister_number')->default('NIL');
            $table->string('brotherInLaw_name')->default('NIL');
            $table->string('brotherInLaw_number')->default('NIL');
            $table->string('auntyInLaw_name')->default('NIL');
            $table->string('auntyInLaw_number')->default('NIL');
            $table->string('employer_name')->default('NIL');
            $table->string('employer_number')->default('NIL');
            $table->string('english')->default('NIL');
            $table->string('yoruba')->default('NIL');
            $table->string('igbo')->default('NIL');
            $table->string('hausa')->default('NIL');
            $table->string('gender')->default('NIL');
            $table->integer('is_blacklisted')->default(false);
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
