<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfers', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('booking_id');
            $table->string('user_id');
            $table->text('driver_id')->nullable();
            $table->integer('number_of_driver')->default(1);
            $table->integer('number_of_driver_unmatched')->default(1);
            $table->integer('no_of_person')->default(1);
            $table->integer('no_of_luggage')->default(1);
            $table->integer('amount')->nullable();
            $table->string('resumption_time');
            $table->string('vehicle_type');
            $table->string('no_of_vehicle');
            $table->string('transfer_type');
            $table->string('transfer_mode');
            $table->string('no_of_persons');
            $table->string('start_date');
            $table->string('end_date');
            $table->string('start_state');
            $table->string('end_state');
            $table->string('pick_up_point');
            $table->string('drop_off_point');
            $table->string('luggage_image');
            $table->string('govt_id_no');
            $table->string('transfer_document')->nullable();
            $table->string('sla_document')->nullable();
            $table->string('guarantor_form')->nullable();
            $table->boolean('has_paid')->default(false);
            $table->boolean('is_matched')->default(false);
            $table->boolean('status')->default(true);
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfers');
    }
}
