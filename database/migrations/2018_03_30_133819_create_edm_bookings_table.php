<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEdmBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('edm_bookings', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('booking_id');
            $table->string('user_id');
            $table->text('driver_id')->nullable();
            $table->string('package');
            $table->boolean('has_paid')->default(false);
            $table->boolean('status')->default(true);
            $table->boolean('is_deleted')->default(false);
            $table->datetime('start_time')->nullable();
            $table->datetime('end_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('edm_bookings');
    }
}
