<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUberBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uber_bookings', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('booking_id')->nullable();
            $table->string('user_id');
            $table->string('driver_id')->nullable();
            $table->string('package');
            $table->integer('number_of_driver')->default(1);
            $table->integer('number_of_driver_unmatched')->default(1);
            $table->text('home_address');
            $table->text('office_address');
            $table->string('vehicle_specification');
            $table->string('vehicle_brand');
            $table->string('vehicle_model');
            $table->string('vehicle_year');
            $table->string('vehicle_identification_number');
            $table->string('vehicle_registration_number');
            $table->string('vehicle_transmission');
            $table->string('type_of_vehicle');
            $table->string('insurance_type');
            $table->text('vehicle_pictures')->nullable();
            $table->text('vehicle_documents')->nullable();
            $table->string('sla_document')->nullable();
            $table->string('guarantor_form')->nullable();
            $table->boolean('has_paid')->default(false);
            $table->boolean('is_matched')->default(true);
            $table->boolean('status')->default(true);
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uber_bookings');
    }
}
