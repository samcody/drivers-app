<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('week');
            $table->integer('remit_id');
            $table->string('month');
            $table->string('week_days');
            $table->string('drivers_name');
            $table->string('car_reg');
            $table->string('car_model');
            $table->integer('expected_returns');
            $table->integer('uber_returns');
            $table->integer('management_fee');
            $table->integer('maintenance');
            $table->integer('days_worked');
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report');
    }
}
