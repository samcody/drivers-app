<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShortTimeBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('short_time_bookings', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('booking_id');
            $table->string('user_id');
            $table->string('driver_id')->nullable();
            $table->string('package');
            $table->text('meeting_point');
            $table->string('nationality');
            $table->string('govt_id_no');
            $table->string('expiry_date');
            $table->string('state_of_residence');
            $table->string('next_of_kin');
            $table->string('mobile_number');
            $table->string('date_of_Birth');
            $table->string('marital_status')->default('Single');
            $table->text('home_address');
            $table->text('office_address');
            $table->string('alternate_mobile_number')->nullable();
            $table->double('amount');
            $table->string('pickup_date');
            $table->string('pickup_time');
            $table->string('end_date');
            $table->string('sla_document')->nullable();
            $table->string('guarantor_form')->nullable();
            $table->boolean('has_paid')->default(false);
            $table->boolean('is_matched')->default(true);
            $table->boolean('status')->default(true);
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('short_time_bookings');
    }
}
