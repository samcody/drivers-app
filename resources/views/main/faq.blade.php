@extends('layouts.master')
@section('title','FAQs')
@section('content')
    <section>
        <div class="container">
            <div class="boxed boxed--lg boxed--border">
                <h1 class="text-center">Frequently Asked Questions (FAQs)</h1>

                <style>
                    .accordion {
                        background-color: #eee;
                        color: #444;
                        cursor: pointer;
                        padding: 18px;
                        width: 100%;
                        border: none;
                        text-align: left;
                        outline: none;
                        font-size: 15px;
                        transition: 0.4s;
                    }
                    
                    .active, .accordion:hover {
                        background-color: #ccc; 
                    }
                    
                    .panel {
                        padding: 0 18px;
                        background-color: white;
                        max-height: 0;
                        overflow: hidden;
                        transition: max-height 0.2s ease-out;
                    }
                    .accordion:after {
                        content: '\02795'; /* Unicode character for "plus" sign (+) */
                        font-size: 13px;
                        color: #777;
                        float: right;
                        margin-left: 5px;
                    }

                    .active:after {
                        content: "\2796"; /* Unicode character for "minus" sign (-) */
                    }
                    </style>
                    </head>
                    <body>
                    
                    <button class="accordion">What is DRIVERS?</button>
                    <div class="panel">
                      <p >It’s an easy to use online platform that provides quick access to already verified and trained professional drivers for recruitment and transport purposes on demand and in real time.                    It’s an easy to use online platform that provides quick access to already verified and trained professional drivers for recruitment and transport purposes on demand and in real time.</p>
                    </div>
                    
                    <button class="accordion">How can I engage or get drivers from this platform?</button>
                    <div class="panel">
                        <p >
                            To engage drivers on our platform, click on GET STARTED or make use of the quick search bar to get your preferred drivers.
                        </p>                    </div>
                    
                    <button class="accordion">For what purpose can I engage or get drivers on your platform?</button>
                    <div class="panel">
                        <p >
                            There are variety of reasons or purpose you might want to engage a driver, we have helped you simplify that into four types of engagement. Full term basis, short term basis, ride-sharing basis and Marketplace basis. Below is a bigger and better explanation;
                            <li  style="font-size: 15px">
                            FULL TERM drivers’ engagement; this is a way of engaging drivers on a long term basis. There are two ways of engaging driver’s full time.
                            <br>
                             a) In- house-staffing drivers engagement; these are drivers engaged `with a mutual understanding agreement that their client/customer/employee shall be responsible for driver’s salary, health management, pension contribution, while the company/service provider duties shall only be responsible for providing and replacing vetted trained professional drivers (Plan that supports this model is I-PAY PLAN / ONE-OFF PLAN).
                             <br>
                              b) Outsourced-staffing driver’s engagement; these are drivers engaged with a mutual understanding agreement that the company/service provider shall be responsible for driver’s provision, driver’s salary, health management, pension contribution, driver’s replacement assurance during service delivery to client/customer/employer (Plans that supports this model are KING PLAN and PREMIUM PLAN).
                            </li>
                            <li  style="font-size: 15px">
                            Ride-sharing driver’s engagement; this is a way of engaging drivers for ride-sharing purpose. There are two ways of engaging driver’s for Uber or ride-sharing use.
                            <br>
                             a) Independent- contracted in-house staffing driver’s engagement; these are drivers engaged with a mutual understanding agreement that their client/customer/employee shall engage drivers as independent contractors and also client/customer/employee shall be responsible for their management (Plans that supports this model are Uber/Taxify Platinum Plans).
                              <br>
                             b) Independent- contracted in-house outsourced staffing drivers engagement; these are drivers engaged with a mutual understanding agreement that their client/customer/employee shall engage drivers has independent contractors and also company/service provider shall manage the drivers on client/customer/employee behalf (Plans that supports this model are Uber Diamond Plans)
                            </li>
                            <li  style="font-size: 15px">
                            SHORT TERM driver’s engagement; this is a way of engaging drivers for short term purpose via bookings. The company/service provider shall be responsible for confirming client/customer/employee bookings, track and follow up on such bookings.
                            </li>
                            <li  style="font-size: 15px">
                            MARKETPLACE driver’s engagement; this is a way of engaging drivers (and other personnel) for the purpose of directly negotiating with them without company/service provider interference. Via this engagement, client/customer/employee have access to negotiate and have a mutual understanding agreement with drivers on pricing and services to be rendered.
                            </li>
                        </p>                    </div>
                        <button class="accordion">How many drivers can DRIVERS provide on demand?</button>
                        <div class="panel">
                        <p>We can comfortably provide as many drivers as you want on demand. But when you demand special kinds of drivers like truck, motor bikes riders, keke riders and, or when the number of driver requested is more than 100 it would require a particular period of time.</p>
                        </div>
                        <button class="accordion">What kind of vehicles can DRIVERS driver drive?</button>
                        <div class="panel">
                        <p>DRIVER’S drivers can only drive comprehensive insured vehicles. DRIVER’s drivers can also drive various types of vehicles such as SUV, Buses, Vans, Cars, Light Trucks, Heavy Trucks, Tricycles and Motorbikes.</p>
                        </div>
                        <button class="accordion">What if my vehicle is not comprehensively insured?</button>
                        <div class="panel">
                        <p>DRIVERS can help clients/employers/users get their vehicle comprehensively insured, other than that we can only accept third party insurance or others in a discretionary influenced decision. To start the process of the comprehensive insurance which is affordable via our platform, make use of the vehicle insurance tool under the tools section</p>
                        </div>
                        <button class="accordion">Can DRIVERS be held responsible for driver’s misconduct?</button>
                        <div class="panel">
                        <p>All DRIVERS drivers have verified guarantors, witnesses and sworn affidavit that attest to the mutual agreement between the guarantors and DRIVERS, that they (driver’s guarantors) and their guaranteed (the driver) should be held responsible for any misconduct, breaking of government laws and DRIVERS rules and guides.</p>
                        </div>
                        <button class="accordion">What is special about drivers on DRIVERS?</button>
                        <div class="panel">
                        <p>Drivers on DRIVERS are well cultured, reliable, professionally trained and vetted to serve.</p>
                        </div>
                        <button class="accordion">Can you explain the packages/plans on your website?</button>
                        <div class="panel">
                        <p> We have various plans/packages. They are; 
                                <br><br>
                                PREMIUM PLAN: this is a full term service plan with service fee cost of NGN 20,000 only per driver which is for User who needs driver on a long term basis. On this plan the driver is an outsourced staff. User is required to get comprehensive information on `driver, 48 hours compatibility assessment and guarantor’s information while users pays the platform monthly rates.
                                <br><br>
                                KING PLAN: this is a full term service plan with service fee cost of NGN 25,000 only per driver which is for User who needs driver on a long term basis. On this plan the driver is an outsourced staff. User is required to get comprehensive information on driver, 72 hours compatibility assessment and contract (SLA) agreement while user pays the platform monthly rates.
                                <br><br>
                                I-PAY PLAN: this is a full term service plan with service fee cost of NGN 50,000 only per driver which is for User who needs driver on a long term basis. On this plan the driver is an in-house staff. User is required to get comprehensive information on driver, verified guarantors information, 72 hours compatibility assessment, replacement assurance and SLA agreement while user pays the driver monthly salaries directly, handles drivers welfare and pension management. This means driver has become full term staff of user.
                                <br><br>
                                PLANTINUM PLAN: this is a ride sharing service plan with service fee cost of NGN 5,000 only per driver which is for User who needs driver for their vehicles on ride sharing platforms. It is available for UBER and TAXIFY. On this plan the driver is an independent contracted staff. User is required to get comprehensive profile information on driver, verified guarantors reports and SLA agreement.
                                <br><br>
                                Higher Purchased PLANTINUM PLAN: this is a ride sharing service plan with service fee cost of NGN 10,000 only per driver which is for User who needs driver for their vehicles on ride sharing platforms. It is available for UBER and TAXIFY. On this plan the driver is an independent contracted staff. User is required to get comprehensive profile information on driver, verified guarantors reports, a Higher Purchased interested bound driver and SLA agreement.
                                <br><br>
                                DIAMOND PLAN: this is a ride sharing service plan with service fee cost of NGN 10,000 only per driver which is for User who needs driver for their vehicles on ride sharing platforms. It is available for UBER and TAXIFY. On this plan the driver is an independent contracted staff. Fleet and revenue management solution is offered by the company/service provider monthly.
                                <br><br>
                                Higher Purchased DIAMOND PLAN: this is a ride sharing service plan with service fee cost of NGN 10,000 only per driver which is for User who needs driver for their vehicles on ride sharing platforms. It is available for UBER and TAXIFY. On this plan the driver is an independent contracted staff. Fleet and revenue management solution is offered by the company/service provider monthly for higher purchased purposes on behalf of the customer/client.
                                <br></p>
                        </div>
                        <button class="accordion">How does it work after I make payment on premium plan for full term driver?</button>
                        <div class="panel">
                        <p>After a client/employer/user pays for premium plan client assesses assigned driver, once satisfied with driver, date of resumption is effected and at the end of the month, client pays monthly rate to DRIVERS, while DRIVERS pays drivers salaries, take care of pension, health benefits and drivers insurance.</p>
                        </div>
                        <button class="accordion">How does it work after I make payment on King Plan for full term driver?</button>
                        <div class="panel">
                        <p>Similar to the Premium Plan, After a client/employer/user pays for King plan client assesses assigned driver, once satisfied with driver, date of resumption is effected and at the end of the month, client pays monthly rate to DRIVERS, while DRIVERS pays drivers salaries, take care of pension, health benefits and drivers replacement assurance</p>
                        </div>
                        <button class="accordion">What is the difference between premium plan and king plan?</button>
                        <div class="panel">
                        <p>The striking difference between premium and King plan is contract SLA agreement and the number of days required for compatibility assessment. Premium plan has no contract SLA agreement and its 48 hours while King Plan has the contract SLA agreement and it is 72 hours required for compatibility assessment.</p>
                        </div>
                        <button class="accordion">What does Monthly rate mean?</button>
                        <div class="panel">
                        <p>This is an amount that covers driver’s salary, health management, pension contribution, driver’s replacement assurance paid monthly by the client/employer/user.</p>
                        </div>
                        <button class="accordion">Can I negotiate the monthly rate, because it is above my budget?</button>
                        <div class="panel">
                        <p>Yes, 10% discount is allowed.</p>
                        </div>
                        <button class="accordion">How does it work after I make payment on I-Pay Plan for full term driver?</button>
                        <div class="panel">
                        <p>Unlike the Premium and King Plan, After a client/employer/user pays for I-Pay plan client assesses assigned driver, once satisfied with driver, date of resumption is effected and at the end of the month, client pays monthly salary directly to his or her driver salaries, take care of pension, health benefits and while DRIVERS provides drivers replacement assurance with in 12 months.</p>
                        </div>
                        <button class="accordion">How many driver's replacement would DRIVERS make for me under I-Pay  Plan in 12 months ?</button>
                        <div class="panel">
                        <p>I - Pay plan allows three replacements within a period of one year.</p>
                        </div>
                        <button class="accordion">Can I negotiate the driver’s salary on the I-Pay Plan?</button>
                        <div class="panel">
                        <p>Yes can negotiate driver’s salary on this plan. Although we have a reasonable amount we can allow the negotiation to go for, as we consider the driver’s remuneration as an important factor in being efficient and productive.</p>
                        </div>
                        <button class="accordion">How does it work to get a driver for Ride sharing (i.e. Uber/Taxify) platinum/HP Platinum plan?</button>
                        <div class="panel">
                        <p>Before you can get a driver for your ridesharing platinum plan, ensure you already register your vehicle with the ridesharing platforms that are supported on DRIVERS. Afterwards you proceed for paying for the platinum plan, then you get to assess driver provided by DRIVERS for your specification and after, you handle the partnership and management with your assigned driver.</p>
                        </div>
                        <button class="accordion">How does it work to get a driver for Ride sharing (i.e. Uber/Taxify) Diamond/HP diamond plan?</button>
                        <div class="panel">
                        <p>Before you can get a driver for your ridesharing diamond plan, ensure you already register your vehicle with the ridesharing platforms that are supported on DRIVERS. Afterwards you proceed to get an Auto Inspection report for the vehicle you want to bring on the ridesharing business, then pay for the diamond plan, then DRIVERS handles management of your returns on investment monthly.</p>
                        </div>
                        <button class="accordion">How do you verify drivers on DRIVERS?</button>
                        <div class="panel">
                        <p>We have a verification process, which involves the following; a) physical background checks b) past employer verification c) BVN verification d) test drive verification e) security verification. But, client/employer/user can decide to engage a further verification of criminal background check and medicals (Extensive medical check) which we can also provide but at a little expense.</p>
                        </div>
                        <button class="accordion">How does DRIVERS verify driver’s guarantors?</button>
                        <div class="panel">
                        <p>At DRIVERS there are quality of guarantor’s type being accepted and we verify driver’s guarantors by confirming their physical address,  occupation, and financial capacity with consistency of information checks in line with company/individual user requirement.</p>
                        </div>
                        <button class="accordion">Do you do medical test for drivers on DRIVERS?</button>
                        <div class="panel">
                        <p>We only conduct light medical checks such as; drivers fitness level, cardio vascular checks, eye and vision checks and weight checks. In cases of extensive medical test calls, client/employer/user would be charged additional rates for test such as HIV, tuberculosis, Malaria, typhoid, etc.</p>
                        </div>
                        <button class="accordion">Why do I feel am paying to get just driver’s details if DRIVERS claims to have vetted the drivers?</button>
                        <div class="panel">
                        <p>At DRIVERS we provide more than vetted drivers as we want to ensure we drive you in comfort, style and convenience. However, we share driver’s information with you so you could also know little or more about who as being ASSIGNED to drive your vehicle.</p>
                        </div>
                        <button class="accordion">What level of education do drivers ON DRIVERS have?</button>
                        <div class="panel">
                        <p>Most of the drivers on DRIVERS are educated and have the basic knowledge on  how to read, write, drive and act responsibly.</p>
                        </div>
                        <button class="accordion">Averagely, how many years of experience do drivers on DRIVERS possess?</button>
                        <div class="panel">
                        <p> We accept drivers with over 10 years of experience on our platform, but we might accommodate drivers with lesser years of experience that demonstrates exemplary driving skills, clean track record and character during verification checks, drive test and training programs.</p>
                        </div>
                        <button class="accordion">Do you offer verification as a service (VAAS) too?</button>
                        <div class="panel">
                        <p>Yes we do. You can search through the verification tool on the homepage. There are two different plans we run. Lite verification plan which is NGN 15,000 entails physical background checks, past employer verification, BVN verification, test drive verification, security verification while the other is Premium verification plan which is NGN 50,000 entails criminal background checks, medicals checks, physical background checks, past employer verification, BVN verification, test drive verification, security verification.</p>
                        </div>
                        <button class="accordion">Do you offer car tracker installation as a service?</button>
                        <div class="panel">
                        <p>Yes we do. You can engage this service via the car tracker tool on the tools menu via the website.</p>
                        </div>
                        <button class="accordion">Does that mean I can verify my existing drivers and staffs via your platform?</button>
                        <div class="panel">
                        <p>Yes you can, all you need to do is visit our verification plans via using the verification tools.</p>
                        </div>
                        <button class="accordion">What kind of training do you give drivers on DRIVERS?</button>
                        <div class="panel">
                        <p>We provide drivers purely with technical knowledge and orientation that would aid them provide their services in best practices. We take drivers through four key pillars. Pillars of employer’s satisfaction, pillars of customer knowledge and service, pillars of security consciousness and pillars of defensive driving.</p>
                        </div>
                        <button class="accordion">For instance can DRIVERS train my existing drivers ?</button>
                        <div class="panel">
                        <p>Yes . We provide a pre-training/situation check to the current challenges facing the drivers staffing arm of your organization and proffer training solution to it, so as to enhance performance and workflow afterwards.</p>
                        </div>
                        <button class="accordion">Is there a health insurance provision for drivers working on full term?</button>
                        <div class="panel">
                        <p>Yes, we ensure all our drivers working on a long term under our supervision can have access to proper health insurance cover.</p>
                        </div>
                        <button class="accordion">Can you deliver parcels via drivers on your platform?</button>
                        <div class="panel">
                        <p>Yes we can, we have our dispatch bikes and you can place your bookings request or call our contact lines or email at us.</p>
                        </div>
                        <button class="accordion">What kind of parcels do you send?</button>
                        <div class="panel">
                        <p>We can help users send their documents to the destination they want to. Any other parcel dispatch request other than documents can be channeled to our email or contacts.</p>
                        </div>
                        <button class="accordion">Can I negotiate directly with drivers on DRIVERS?</button>
                        <div class="panel">
                        <p>Yes, we have a plan that arranges for that on DRIVERS, this is known as Marketplace. It gives you the avenue to negotiate pricing and terms of service with drivers directly.</p>
                        </div>
                        <button class="accordion">Can you send me  driver to assess and if am okay with him, I will pay for plan?</button>
                        <div class="panel">
                        <p>DRIVERS operates on Pre-paid services, that is;  user pays for service before it is being rendered, However Post-paid services are usually considered for organization requesting over 5 drivers for engagement. T&C Applies. </p>
                        </div>
                        <button class="accordion">What are your refund policies?</button>
                        <div class="panel">
                        <p>Refund is only possible when service provider has failed to match up expectations, or client is dissatisfied at service rendered. In the scenario where client has been served by service provider and still wants a refund this amounts to a 50% refund, while when client has not been served at all or expectations not met or client can’t longer bear with service provider or service provider can’t meet up within promised time, as requested by client there shall be a refund of 75% as the other 25% covers cost of mobilization, search and matching.</p>
                        </div>
                        <button class="accordion">What is the difference between Short term driver’s engagement and Marketplace driver’s engagement?</button>
                        <div class="panel">
                        <p> Short term driver’s engagement requires the user booking a driver, and we follow-up on their booking confirming whether or not that driver is available to serve. While Marketplace driver’s engagement requires the user selecting, adding the driver to cart and checking out so as to access driver’s info, making use of it to negotiate with driver without interference from us.</p>
                        </div>
                        <button class="accordion">How often does DRIVERS retrain its drivers?</button>
                        <div class="panel">
                        <p> We organize a refresher course programs for drivers on our platform on a monthly basis. It is usually communicated to all drivers on its platform monthly.</p>
                        </div>
                        <button class="accordion">What is the essence of monthly drivers’ training?</button>
                        <div class="panel">
                        <p> To ensure all drivers are updated with current trends in road traffic, transport management, driving clients safely, security consciousness and more.</p>
                        </div>
                        <button class="accordion">Does DRIVERS offers Training as a service (TAAS)?</button>
                        <div class="panel">
                        <p> Yes we do. We have a learning how to drive tool on the tools section of the website and also companies, businesses and corporate individuals can reach out to us for retraining their already existing drivers. We have experts on ground who can pass the required knowledge both practically and theoretically to drivers in training.</p>
                        </div>
                        
                    
                    <script>
                    var acc = document.getElementsByClassName("accordion");
                    var i;
                    
                    for (i = 0; i < acc.length; i++) {
                    acc[i].addEventListener("click", function() {
                        this.classList.toggle("active");
                        var panel = this.nextElementSibling;
                        if (panel.style.maxHeight){
                        panel.style.maxHeight = null;
                        } else {
                        panel.style.maxHeight = panel.scrollHeight + "px";
                        } 
                    });
                    }
                    </script>
                    
                <h3></h3>
                
                <h3></h3>
                
                <h3></h3>
                <p >
                    
                </p>
                            </div>
        </div>
    </section>
@endsection
