@extends('layouts.master')

@section('title','Partner With Us')

@section('banner')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <h1>Partner With Us.</h1>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section class="switchable feature-large bg--secondary">
        <div class="container">
            <div  class="row">
                <div class="col-sm-6 col-md-4">
                    <img alt="image" src="{{ asset('assets/img/device-1.png') }}" />
                </div>
                <div class="col-sm-6 col-md-7">
                    <div style="font-size:11px"  class="switchable__text">
                        <h2 class="text-center">Hello Prospective Partner,</h2>
                        <p class="lead">
                            We are delighted having you on our website. Actually, we have been looking forward to having you on this page.
                            <br>
                            DriversNG is a fast growing transport tech company, with operations in major cities in Nigeria and expanding to other African cities.
                            Our business has more than a billion dollars potential. We have proofed our model with an awesome team making this happen already.
                            <br>
                            You don't need to have everything, but with your network and resources, you can support DriversNG with the required partnership to create more impact in Africa.
                            Of course supporting DriversNG is meeting your objectives too. Your vehicles, networks, spaces, in Nigeria and Africa at large can help millions of users in experiencing DriversNG value proposition.
                            <br>
                            <b>Join us as we launch the future of transportation management in Africa!</b>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class=" bg--secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-12 ">
                    <div class="row">
                        <div class="boxed boxed--border">
                            @include('partial.alert')
                            <form action="{{ url('partner') }}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" name="name" placeholder="Name" required>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" name="city" placeholder="City" required>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" name="phone" placeholder="Phone" required>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" name="email" placeholder="Email" required>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <select name="nationality" required>
                                            <option value="" disabled selected>Choose Country</option>
                                            @foreach ($data['countries'] as $option)
                                                <option value="{{ $option }}">{{ $option }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <select name="partnership_purpose" id="partnership_purpose" required>
                                            <option value="" disabled selected>Choose Partnership Purpose</option>
                                            @foreach ($data['partnership_purposes'] as $option)
                                                <option value="{{ $option }}">{{ $option }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row margin-top-30" id="price_ranges">
                                    <div class="col-md-12 col-sm-12">
                                        <select name="investment_price">
                                            <option value="" disabled selected>Choose Investment Range</option>
                                            @foreach ($data['price_ranges'] as $option)
                                                <option value="{{ $option }}">{{ $option }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <div class="input-checkbox">
                                    <input id="agreement" type="checkbox" name="agreement" checked>
                                    <label for="checkbox"></label>
                                </div>
                                <span>I guarantee I am not an an investor,partner, employee, contractor, or shareholder
                                                with any company that may be considered a direct or indirect competitor of DriversNG</span>

                                <div class="row margin-top-30">
                                    <div class="col-md-12 col-sm-12">
                                        <p>
                                            Please note your interest in an investment opportunity with DriversNG and your background
                                        </p>
                                        <div>
                                <textarea name="investor_background" rows="5"
                                          placeholder="Investor Background..."></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12 boxed">
                                    <button type="submit" class="btn btn--primary type--uppercase">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        $(function() {
            $('#price_ranges').hide();
            $('#partnership_purpose').on('change', function(e) {
                e.preventDefault();
                if ($('#partnership_purpose').val() == 'Investing') {
                    var accreditedInvestor = confirm('Are you an Accredited Investor?');
                    if (accreditedInvestor) {
                        $('#price_ranges').show();
                    } else {
                        $('#price_ranges').hide();
                    }
                } else {
                    $('#price_ranges').hide();
                }
            });
        });
    </script>

@endsection