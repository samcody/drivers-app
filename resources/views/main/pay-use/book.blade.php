@extends('layouts.master')

@section('title','Book Pay Per Use Driver')

@section('banner')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-8">
                    <h1>Pay Per Use</h1>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section class=" bg--secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-12 ">
                    <div class="row">
                        <div class="boxed boxed--border">
                            <form method="post" action="{{ url('pay-use/hire') }}" class="text-left">
                                {{ csrf_field() }}
                                <div class="col-sm-3">
                                    <span>Duration</span>
                                    <select name="duration" required>
                                        <option value="">Choose Duration</option>
                                        <option value="1">1 Month</option>
                                        <option value="2">2 Months</option>
                                        <option value="3">3 Months</option>
                                        <option value="4">4 Months</option>
                                        <option value="5">5 Months</option>
                                        <option value="6">6 Months</option>
                                        <option value="7">7 Months</option>
                                        <option value="8">8 Months</option>
                                        <option value="9">9 Months</option>
                                        <option value="10">10 Months</option>
                                        <option value="11">11 Months</option>
                                        <option value="12">12 Months</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>Number of Drivers</span>
                                    <input type="number" name="number_of_driver" placeholder="Number of Drivers" required>
                                </div>
                                <div class="col-sm-3">
                                    <span>Resumption Date</span>
                                    <input type="date" name="resumption_date"  placeholder="Resumption Date" required>
                                </div>
                                <div class="col-sm-3">
                                    <span>Use Type</span>
                                    <select name="use_type" required>
                                        <option value="">Choose...</option>
                                        <option value="Private">Private</option>
                                        <option value="Company">Company</option>
                                        <option value="Industry">Industry</option>
                                        <option value="Hospital">Hospital</option>
                                        <option value="Family">Family</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>State of Residence</span>
                                    <select name="state_of_residence" required>
                                        <option value="">Choose...</option>
                                        <option value="Lagos">Lagos</option>
                                        <option value="Rivers">Rivers</option>
                                        <option value="Oyo">Oyo</option>
                                        <option value="Ogun">Ogun</option>
                                        <option value="Akwa Ibom">Akwa Ibom</option>
                                        <option value="Abuja">Abuja</option>
                                        <option value="Kaduna">Kaduna</option>
                                        <option value="Plateau">Plateau</option>
                                        <option value="Delta">Delta</option>
                                        <option value="Kano">Kano</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>Vehicle Type</span>
                                    <select name="vehicle_type" required>
                                        <option value="">Choose...</option>
                                        <option value="Car">Car</option>
                                        <option value="Bus">Bus</option>
                                        <option value="Truck">Truck</option>
                                        <option value="Van">Van</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>Resumption Time</span>
                                    <select name="resumption_time" required>
                                        <option value="">Choose Opening Time</option>
                                        <option value="6am">6am</option>
                                        <option value="7am">7am</option>
                                        <option value="8am">8am</option>
                                        <option value="9am">9am</option>
                                        <option value="10am">10am</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>Closing Time</span>
                                    <select name="closing_time" required>
                                        <option value="">Choose Closing Time</option>
                                        <option value="4pm">4pm</option>
                                        <option value="5pm">5pm</option>
                                        <option value="6pm">6pm</option>
                                        <option value="7pm">7pm</option>
                                        <option value="8pm">8pm</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>Nearest Location</span>
                                    <input type="text" name="nearest_location"  placeholder="Nearest Location" required>
                                </div>
                                <div class="col-sm-3">
                                    <span>Driver's Gender</span>
                                    <select name="driver_gender" required>
                                        <option value="">Choose...</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>Transmission</span>
                                    <select name="transmission" required>
                                        <option value="">Choose...</option>
                                        <option value="Automatic">Automatic</option>
                                        <option value="Manual">Manual</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>Select Driver's Salary Package</span>
                                    <select name="salary_package" required>
                                        <option value="">Select An Option</option>
                                        <option disabled>Private Use</option>
                                        <option value="45000">Monday to Friday (NGN 45, 000.00) </option>
                                        <option value="50000">Monday to Saturday (NGN 50,000.00)</option>
                                        <option value="75000">Monday to Sunday (NGN 75,000.00)</option>
                                        <option disabled>Company Use</option>
                                        <option value="50000">Monday to Friday (NGN 50, 000.00) </option>
                                        <option value="55000">Monday to Saturday (NGN 55,000.00)</option>
                                        <option value="85000">Monday to Sunday (NGN 85,000.00)</option>
                                        <option disabled>Spy Police Use</option>
                                        <option value="60000">Monday to Friday (NGN 60,000.00)</option>
                                        <option value="70000">Monday to Saturday (NGN 70,000.00)</option>
                                        <option value="80000">Monday to Sunday (NGN 80,000.00)</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <span>How do you want to pay us monthly rate?</span>
                                    <select name="salary_frequency" required>
                                        <option value="">Choose Salary Frequency...</option>
                                        <option value="Weekly">Weekly</option>
                                        <option value="Every Two Weeks">Every Two Weeks</option>
                                        <option value="Monthly">Monthly</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>I will provide driver&rsquo;s accommodation</span>
                                    <select name="provide_driver_accommodation" required>
                                        <option value="">Choose...</option>
                                        <option value="Yes">YES</option>
                                        <option value="No">NO</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <span>What insurance cover do you have for your vehicle</span>
                                    <select name="insurance_type"  id="insurance_type" required>
                                        <option value="">Choose...</option>
                                        <option value="Comprehensive Insurance">Comprehensive Insurance</option>
                                        <option value="Third Party Insurance">Third Party Insurance</option>
                                        <option value="Others">Others</option>
                                    </select>
                                </div>

                                <div class="col-sm-12 boxed">
                                    <button type="submit" class="btn btn--primary type--uppercase">Book Driver</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection