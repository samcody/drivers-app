@extends('layouts.master')

@section('title','Pay For Pay Per Use Plan')

@section('banner')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-8">
                    <h1>Select Payment Method</h1>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section>
        <div class="container">
            <div class="row">
                @include('partial.alert')
                <div class="col-sm-6 col-md-6 ">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>Bank Payment</h4>
                        <span class="h1"><span class="pricing__dollar">&#8358;</span>{{ number_format($amount) }}</span>
                        <p>
                            <b>Zenith Bank</b><br>
                            <b>Account Name:</b> DriversNG Recruitment Service Limited <br>
                            <b>Account Number:</b> 1014896184
                        </p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>Card Payment</h4>
                        <span class="h1"><span class="pricing__dollar">&#8358;</span>{{ number_format($amount) }} +
                            <b style="font-size: 28px">&#8358;100</b></span>
                        <br><br>
                        <form action="{{ url('pay') }}" method="post" accept-charset="UTF-8">
                            {{ csrf_field() }}
                            <input type="hidden" name="email" value="{{ Auth::User()->email }}">
                            <input type="hidden" name="metadata" value="{{ json_encode(['booking_id' => $bookingID, 'hire_type' => 'PAY-USE','user_id' => Auth::User()->id]) }}">
                            <input type="hidden" name="amount" value="{{ $amount + 100 }}00">
                            <input type="hidden" name="quantity" value="">
                            <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}">
                            <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}">
                            <button class="btn btn--primary btn--lg type--uppercase">
                                <span class="btn__text">
                                    <i class="fa fa-plus-circle fa-lg"></i> Pay Via Card
                                </span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection