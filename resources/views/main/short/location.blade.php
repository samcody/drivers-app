@extends('layouts.master')

@section('title','Choose Location')

@section('banner')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <h1>Select Service Location.</h1>

                    <form action="{{ url('short-term') }}" method="post">
                        {{ csrf_field() }}
                        <div class="col-sm-7">
                            <select class="validate-required" type="text" name="location">
                                <option value="">Choose Location</option>
                                @foreach($locations as $location)
                                    <option value="{{ $location['location'] }}">{{ $location['location'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <button type="submit" class="btn btn--primary type--uppercase">Get Drivers</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section style="margin-top: 50px">
    </section>
@endsection