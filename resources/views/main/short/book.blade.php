@extends('layouts.master')

@section('title','Book Short-Term Driver')

@section('banner')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <h1>Fill Form To Book Driver.</h1>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section class=" bg--secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-12 ">
                    <div class="row">
                        <div class="boxed boxed--border">
                            <form method="post" action="{{ url('short-term/book',['id' => $driver->id]) }}" class="text-left">
                                {{ csrf_field() }}
                                <input type="hidden" name="mobile_number" value="{{ Auth::User()->mobile_number }}">
                                    <div class="col-md-3">
                                            <label>Meet Location</label>
                                            <input type="text" name="meeting_point" placeholder="MEETING LOCATION" required>
                                    </div>
                                    <div class="col-md-3">
                                            <label>Nationality</label>
                                        <select name="nationality" required>
                                            <option value="">Select Country</option>
                                            @foreach($countries as $country)
                                                <option value="{{ $country }}">{{ $country }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                            <label>Govt. ID No.</label>
                                            <input type="text" name="govt_id_no" placeholder="GOVT. ISSUED ID. NO" required>
                                    </div>
                                    <div class="col-md-3">
                                            <label>Expiry Date</label>
                                            <input type="text" name="expiry_date" placeholder="EXP. DATE" required>
                                    </div>
                                    <div class="col-md-3">
                                            <label>State of Residence</label>
                                        <select name="state_of_residence" required>
                                            <option value="">Select State</option>
                                            @foreach($states as $state)
                                                <option value="{{ $state }}">{{ $state }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                            <label>Next of Kin</label>
                                            <input type="text" placeholder="NEXT OF KIN" name="next_of_kin" required>
                                    </div>

                                    <div class="col-md-3">
                                        <label>Date of Birth</label>
                                        <input type="date" name="date_of_birth" placeholder="D.O.B" required>
                                    </div>

                                    <div class="col-md-3">
                                        <label>Marital Status</label>
                                        <select name="marital_status" data-placeholder="MARITAL STATUS" required>
                                            <option value="">MARITAL STATUS</option>
                                            <option value="Single">SINGLE</option>
                                            <option value="Married">MARRIED</option>
                                            <option value="Engaged">ENGAGED</option>
                                            <option value="Divorced">DIVORCED</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Home Address</label>
                                        <input type="text" name="home_address" placeholder="HOME ADDRESS" required>
                                    </div>

                                    <div class="col-md-3">
                                        <label>Office Address</label>
                                        <input type="text" name="office_address" placeholder="OFFICE ADDRESS" required>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Alternate Phone</label>
                                        <input type="tel" name="alternate_mobile_number" placeholder="ALTERNATE PHONE" required>
                                    </div>

                                    {{-- <div class="col-md-3">
                                        <label>CHOOSE SERVICE</label>
                                        <select name="service" id="service" required>
                                            <option value="">CHOOSE SERVICE</option>
                                            <option value="5000">DRIVER FOR A DAY(MAX. 8 HOURS)</option>
                                            <option value="15000">WEEKDAYS MONDAYS TO FRIDAYS maximum of 12 hours/day</option>
                                            <option value="10000">WEEKEND SAT. &amp; SUNDAY (MAX. 10 HOURS)</option>
                                            <option value="28000">WEEKDAYS EVENING/NIGHT (Mon - fri) (4PM – 11 PM MAX.)</option>
                                            <option value="15000">WEEKDAYS MORNING ONLY (Mon - fri ) (6 AM – 1PM MAX.)</option>
                                            <option value="4000">SUNDAY MORNING ONLY (6AM – 1PM MAX.)</option>
                                            <option value="10000">FRIDAY EVENING/NIGHT ONLY (4PM – 5AM)</option>
                                            <option value="25000">INTERSTATE WEST A TRIP</option>
                                            <option value="35000">INTERSTATE SOUTH A TRIP</option>
                                            <option value="50000">INTERSTATE NORTH A TRIP</option>
                                            <option value="45000">INTERSTATE EAST A TRIP</option>
                                            <option value="15000">WEDDING DAY TRANSFER</option>
                                            <option value="18000">MORNING SCHOOL RUNS ONLY (Mon -Fri)</option>
                                            <option value="20000">EVENING SCHOOL RUNS ONLY (Mon-fri)</option>
                                            <option value="27000">MORNING &amp; EVENING SCHOOL RUNS (Mon - fri)</option>
                                        </select>
                                    </div> --}}
                                    <input type="hidden" name="service" id="service" value="4000">

                                    <div class="col-md-3">
                                        <label>Number Of Days/Times</label>
                                        <input type="number" name="total_number" onfocusout="estimate()" id="totalNumber" placeholder="Number of days or times for service usage" required>
                                    </div>

                                    <div class="col-md-3">
                                        <label>Pick up Date</label>
                                        <input type="date" name="pickup_date" id="pickupDate" required>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Pick up Time</label>
                                        <select name="pickup_time" id="pickupTime" required>
                                            <option>Please select</option>
                                            <option value="7am">7am</option>
                                            <option value="8am">8am</option>
                                            <option value="9am">9am</option>
                                            <option value="10am">10am</option>
                                            <option value="11am">11am</option>
                                            <option value="12noon">12noon</option>
                                            <option value="1pm">1pm</option>
                                            <option value="2pm">2pm</option>
                                            <option value="3pm">3pm</option>
                                            <option value="4pm">4pm</option>
                                            <option value="5pm">5pm</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Ending Date</label>
                                        <input type="date" name="end_date" id="endingDate" required>
                                    </div>

                                    {{--<div class="col-md-3">--}}
                                        {{--<label>Price</label>--}}
                                        {{--<input type="text" name="service_price" id="price" placeholder="Price">--}}
                                    {{--</div>--}}

                                <div class="col-sm-12 boxed">
                                    <button type="submit" class="btn btn--primary type--uppercase">Hire Driver</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        var service = document.getElementById('service');
        var serviceVal = service.options.value;
        var total = document.getElementById('totalNumber').value;
        var price = document.getElementById('price').value;

        function estimate() {
            if (total !== null && service !== null) {
                console.log('Hello Guys');
                price = service * total;
                // alert(total);
                // alert(serviceVal);
            }
        }
    </script>
@endsection