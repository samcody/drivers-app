@extends('layouts.master')

@section('title','Choose Location')

@section('banner')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <h1>Driver(s) Available In {{ $request['location'] }}.</h1>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section class="switchable feature-large">
        <div class="container">
            <div class="row">
                @if(count($drivers) < 1)
                    <div class="col-md-12">
                        <h2 class="text-center">No Driver is available at this location for now.</h2>
                    </div>
                    @else
                    @foreach($drivers as $driver)
                        <div class="col-md-12">
                            <div class="col-md-4 col-md-offset-2">
                                <img alt="Image" class="border--round" src="{{ asset('avatar/'.$driver->profilepicture) }}" />
                            </div>
                            <div class="col-md-6">
                                <div class="switchable__text">
                                    <div class="text-block">
                                        <h2>{{ ucfirst($driver->firstname) }} {{ ucfirst($driver->lastname) }}</h2>
                                        <span>{{ $driver->serial_number }} -> Nigeria</span>
                                    </div>
                                    <p class="lead">
                                        {{ $driver->experience }}
                                    </p>
                                    <a class="btn btn--success type--uppercase" href="{{ url('driver/profile',['id' => $driver->id]) }}">
                                    <span class="btn__text">
                                        My Profile
                                    </span>
                                    </a>
                                    <a class="btn btn--primary type--uppercase" href="{{ url('short-term/book',['id' => $driver->id]) }}">
                                    <span class="btn__text">
                                        Book Driver
                                    </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            {{ $drivers->links() }}
        </div>
    </section>
@endsection