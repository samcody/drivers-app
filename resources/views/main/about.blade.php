@extends('layouts.master')

@section('title','About Us')

@section('banner')
    <section class="text-center imagebg space--lg" data-overlay="3">
        <div class="background-image-holder">
            <img alt="background" src="{{ asset('assets/img/bg-1.jpg') }}" />
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-6 color--primary-1">
                    <h1>WE ARE DRIVERSNG</h1>
                    <p class="lead">
                        Just some few things we do. We do a lot more to make you happy.
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <h2>The Experience</h2>
                    <p class="lead">
                        <span>EXPERIENCE EXTRAORDINARY SECURITY,COMFORT AND SAFETY</span><br>
                        We Host drivers who have gone through a nine-phase vetting and verification process.
                        Host drivers who had been properly background-checked, orientated and trained on
                        how to render driving services host drivers who are always dressed in professional and
                        corporate attire in service area host drivers who stays
                        close to your residence or axis host drivers who you can always trust with your vehicles
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection