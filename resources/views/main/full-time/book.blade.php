@extends('layouts.master')

@section('title','Book Full-Time Driver')

@section('banner')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-8">
                    <h1>Selected Full-Time Plan [{{ $request['package'] }}]</h1>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section class=" bg--secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-12 ">
                    <div class="row">
                        <div class="boxed boxed--border">
                            <form method="post" action="{{ url('full-time/hire') }}" class="text-left">
                                {{ csrf_field() }}
                                <input type="hidden" value="{{ $request['package'] }}" name="package">
                                <input type="hidden" value="{{ $request['price'] }}" name="price">
                                <div class="col-sm-3">
                                    <span>Number of Drivers</span>
                                    <input type="number" name="number_of_driver" placeholder="Number of Drivers" >
                                </div>
                                <div class="col-sm-3">
                                    <span>Resumption Date</span>
                                    <input type="date" name="resumption_date"  placeholder="Resumption Date">
                                </div>
                                <div class="col-sm-3">
                                    <span>Driver's Stay</span>
                                    <select name="driver_stay" >
                                        <option value="">Select An Option</option>
                                        <option value="2 Months">2 Months</option>
                                        <option value="3-6 Months">3-6 Months</option>
                                        <option value="Above 6 Months">Above 6 Months</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>Use Type</span>
                                    <select name="use_type" >
                                        <option value="">Choose...</option>
                                        <option value="Private">Private</option>
                                        <option value="Company">Company</option>
                                        <option value="Industry">Industry</option>
                                        <option value="Hospital">Hospital</option>
                                        <option value="Family">Family</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>State of Residence</span>
                                    <select name="state_of_residence" >
                                        <option value="">Choose...</option>
                                        <option value="Lagos">Lagos</option>
                                        <option value="Rivers">Rivers</option>
                                        <option value="Oyo">Oyo</option>
                                        <option value="Ogun">Ogun</option>
                                        <option value="Akwa Ibom">Akwa Ibom</option>
                                        <option value="Abuja">Abuja</option>
                                        <option value="Kaduna">Kaduna</option>
                                        <option value="Plateau">Plateau</option>
                                        <option value="Delta">Delta</option>
                                        <option value="Kano">Kano</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>Vehicle Type</span>
                                    <select name="vehicle_type" >
                                        <option value="">Choose...</option>
                                        <option value="Car">Car</option>
                                        <option value="Bus">Bus</option>
                                        <option value="Truck">Truck</option>
                                        <option value="Van">Van</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>Resumption Time</span>
                                    <select name="resumption_time">
                                        <option value="">Choose Opening Time</option>
                                        <option value="6am">6am</option>
                                        <option value="7am">7am</option>
                                        <option value="8am">8am</option>
                                        <option value="9am">9am</option>
                                        <option value="10am">10am</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>Closing Time</span>
                                    <select name="closing_time">
                                        <option value="">Choose Closing Time</option>
                                        <option value="4pm">4pm</option>
                                        <option value="5pm">5pm</option>
                                        <option value="6pm">6pm</option>
                                        <option value="7pm">7pm</option>
                                        <option value="8pm">8pm</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>Nearest Location</span>
                                    <input type="text" name="nearest_location"  placeholder="Nearest Location">
                                </div>
                                <div class="col-sm-3">
                                    <span>Driver's Gender</span>
                                    <select name="driver_gender" >
                                        <option value="">Choose...</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>Transmission</span>
                                    <select name="transmission" >
                                        <option value="">Choose...</option>
                                        <option value="Automatic">Automatic</option>
                                        <option value="Manual">Manual</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>Salary Package</span>
                                    <select name="salary_package" >
                                        <option value="">Select An Option</option>
                                        <option disabled>Private Monthly rates Plan</option>
                                        <option value="55000">Monday - Friday (NGN55,000)</option>
                                        <option value="60000">Monday - Saturday(NGN60,000)</option>
                                        <option value="100000">Monday - Sunday (NGN100,000)</option>
                                        <option disabled>Company Monthly rates Plan</option>
                                        <option value="60000">Monday -Friday (NGN60,000)</option>
                                        <option value="70000">Monday - Saturday (NGN 70,000)</option>
                                        <option value="150000">Monday - Sunday (NGN150,000)</option>
                                        <option disabled>Special Driver Monthly rates Plan</option>
                                        <option value="85000">Spy Police - Mon – Friday (NGN 85,000)</option>
                                        <option value="95000">Spy Police - Mon –Saturday (NGN 95, 000)</option>
                                        <option value="180000">Spy Police - Mon – Sunday (NGN 180, 000)</option>
                                        <option value="90000">Escort driver - Mon – Friday(NGN 90, 000)</option>
                                        <option value="100000">Escort driver - Mon – Saturday (NGN 100, 000)</option>
                                        <option value="180000">Escort driver - Mon – Sunday (NGN180,000)</option>
                                        <option value="120000">Executive driver - Mon – Friday (NGN 120, 000)</option>
                                        <option value="150000">Executive driver - Mon – Saturday (NGN150,000)</option>
                                        <option value="200000">Executive driver - Mon – Sunday (NGN 200,000)</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <span>How do you want to pay us monthly rate?</span>
                                    <select name="salary_frequency" >
                                        <option value="">Choose Salary Frequency...</option>
                                        <option value="Weekly">Weekly</option>
                                        <option value="Every Two Weeks">Every Two Weeks</option>
                                        <option value="Monthly">Monthly</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>I will provide driver&rsquo;s accommodation</span>
                                    <select name="provide_driver_accommodation" >
                                        <option value="">Choose...</option>
                                        <option value="Yes">YES</option>
                                        <option value="No">NO</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <span>What insurance cover do you have for your vehicle</span>
                                    <select name="insurance_type"  id="insurance_type">
                                        <option value="">Choose...</option>
                                        <option value="Comprehensive Insurance">Comprehensive Insurance</option>
                                        <option value="Third Party Insurance">Third Party Insurance</option>
                                        <option value="Others">Others</option>
                                    </select>
                                </div>
                                <div class="col-sm-12 text-center">
                                    <div class="modal-instance">
                                        <b>Making this full-time booking means you accept these
                                            <a class="modal-trigger" href="#">
                                            <span class="btn__text">
                                                terms
                                            </span>
                                            </a></b>
                                        <div class="modal-container">
                                            <div class="modal-content">
                                                <div style="font-size:12px" class="boxed boxed--lg">
                                                    <h2>What You Need to know as You Embark on this Awesome Relationship With Us on this Plan</h2>
                                                    <hr class="short">
                                                    <p class="lead">
                                                        ▪ The company (Drivers Ng) pays driver’s salaries on the last day of every month
                                                        <br>
                                                        ▪ User pays monthly rates to company on the 25th of every month or at least 5 days to the end of every month.
                                                        <br>
                                                        ▪ Monthly rates covers driver’s salary, health management, pension, training programs and automatic replacement assurance.
                                                        <br>
                                                        ▪ Company (Drivers Ng) sends out monthly invoice a day or two days to or on the 25th of every month or to five days to the end of every month.
                                                        <br>
                                                        ▪ A 1.5% is deducted monthly from monthly driver’s salaries for admin charges, SMS/updates, documentation and profile management.
                                                        <br>
                                                        ▪ Company (Drivers Ng) shall require its drivers for monthly training program and report evaluation for salary collection, rewards and cautions. Time is 12 Noon to 1pm prompt every last Saturday of the Month.
                                                        <br>
                                                        ▪ Exemption fee for User from this Monthly training Program is NGN 5000/month
                                                        <br>
                                                        ▪ Company official time of Resumption and closing time for driver’s operation for Monday to Friday is (6am to 8pm), while for Saturday alone is (10am to 4pm), while Sunday alone is (6am to 2pm) respectively.
                                                        <br>
                                                        ▪  Normal Overtime and Allowance (8pm – 12 midnight) (Overtime is #500/hr.)
                                                        <br>
                                                        ▪  Abnormal Overtime and Allowance (12am – 6am) (Overtime is #1000/hr.)
                                                        <br>
                                                        ▪  (Interstate /Travelling Allowance is NGN 5,500 per day)
                                                        <br>
                                                        ▪  (Early Morning pick up (before 6am) NGN 1500/hr.
                                                        <br>
                                                        ▪  Public holiday charges NGN 3500/day
                                                        <br>
                                                        ▪  Weekend allowance NGN 3500/day
                                                        <br>
                                                        ▪ Outstation allowance NGN 5000/day
                                                        <br>
                                                        ▪ There shall be addition to driver’s salaries of any/all rightful and obligated overtime/interstate/allowance to be carried out by user on, before or after the 25th of the month, hence shall reflect as an addition to the monthly rate amount to be served in monthly invoice to user.
                                                        <br>
                                                        ▪ Both monthly rates and allowances additions are paid to the company (Drivers Ng)
                                                        <br>
                                                        ▪ Drivers on our platform complies with FRSC speed limit standard which is 90km/h
                                                        <br>
                                                        ▪ Driver’s on our platform style of driving is defensive & safety  driving
                                                        <br>
                                                        ▪ Drivers on our platform would always be on their Uniform Shirt based on number of work days required.
                                                        <br>
                                                        ▪ Every driver possesses a Monthly Record Card in which driver shall be properly rated, confirmed available at work by the User. The record would be used by the company to confirm salaries collection, reward and cautions accordingly.

                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 boxed">
                                    <button type="submit" class="btn btn--primary type--uppercase">Book Driver</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection