@extends('layouts.master')

@section('title','Choose Full-Time Plan')

@section('banner')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-8">
                    <h1>Full-Time Plans</h1>
                    <p class="lead">
                        Choose Full-Time Plan
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section>
        <div class="container">
            <div class="row">
                
                <div class="col-sm-6 col-md-4">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>Premium</h4>
                        <span class="h2"><span class="pricing__dollar">&#8358;</span>21,000<small style="font-size: .4em;">vat inclusive</small></span>
                        <p>
                            A verified driver + matched in 24hrs +
                            ONLY Driver's Basic details + 48 hours compatibility
                            check + Driver's guarantors Info
                        </p><br><br>
                        <form action="{{ url('full-time/package') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="package" value="PREMIUM">
                            <input type="hidden" name="price" value="21000">
                            <button class="btn btn-sm btn--primary type--uppercase">
                        <span class="btn__text">
                            Select
                        </span>
                            </button>
                        </form>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>King</h4>
                        <span class="h2"><span class="pricing__dollar">&#8358;</span>26,250<small style="font-size: .4em;">vat inclusive</small></span>
                        <p>
                            A verified driver + matched in 24 hrs +
                            FULL Driver's Profile + 72 hours compatibility check +
                            Driver's guarantors Info + Service Level Agreement
                        </p><br>
                        <form action="{{ url('full-time/package') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="package" value="KING">
                            <input type="hidden" name="price" value="26250">
                            <button class="btn btn--primary type--uppercase">
                        <span class="btn__text">
                            Select
                        </span>
                            </button>
                        </form>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>I-Pay</h4>
                        <span class="h2"><span class="pricing__dollar">&#8358;</span>52,500<small style="font-size: .4em;">vat inclusive</small></span>
                        <p>
                            A verified driver + matched in 24hrs +
                            Full driver's profile + 72 hours compatibility
                            check + driver's guarantor info + service level agreement + directly pay driver + drivers replacement for 12 months
                        </p>
                        <form action="{{ url('full-time/package') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="package" value="I-PAY">
                            <input type="hidden" name="price" value="52500">
                        <button class="btn btn--primary type--uppercase">
                            <span class="btn__text">
                                Select
                            </span>
                        </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection