@extends('layouts.master')

@section('title','Pay For Transfer Driver')

@section('banner')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-8">
                    <h1>Key Things To Note</h1>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section>
        <div class="container">
            <div class="row">
                @include('partial.alert')
                <div class="col-sm-6 col-md-12">
                    <div style="font-size:11px"  class="switchable__text">
                        <h2 class="text-center">ILLEGAL TRANSIT OF OBJECTS, ARM, DRUGS, ITEMS PROHIBITED
                            BY GOVERNMENT, HUMAN & SUBSTANCE AGREEMENT.</h2>
                        <p class="lead">
                            1.IT IS ILLEGAL TO MAKE USE OF COMPANY’S DRIVER TO CONVEY ANY
                            ILLEGAL SUBSTANCE LISTED ABOVE.
                            <br>
                            2.IT IS ILLEGAL TO USE BOTH COMPANY’S DRIVER AND IT VEHICLE FOR
                            TRANSPORTATION OR TRANSFERRING ALL OF THE ABOVE LISTED ABOVE
                            IN THE COURSE OF THIS TRANSFER.
                            <br>
                            3.ANY OF THE SUBJECT CAUGHT IN THE ACT SHALL SOLELY FACE THE
                            CONSEQUENCES OF THE LAW.
                        </p>
                        <br>
                        <div class="text-center">
                            <a href="{{ url('home') }}" class="btn btn-primary btn--lg">Accept Terms</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection