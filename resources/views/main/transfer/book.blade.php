@extends('layouts.master')

@section('title','Book Transfer Driver')

@section('banner')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-8">
                    <h1>Book Transfer Drivers</h1>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section class=" bg--secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-12 ">
                    <div class="row">
                        <div class="boxed boxed--border">
                            <form method="post" action="{{ url('transfer/hire') }}" class="text-left" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="col-sm-3">
                                    <span>Transfer Type</span>
                                    <select name="transfer_type" >
                                        <option value="">Choose Transfer Type</option>
                                        <option value="Interstate">Interstate</option>
                                        <option value="Airport Pickup">Airport Pickup</option>
                                        <option value="Airport Drop-off">Airport Drop-off</option>
                                        <option value="Events">Events</option>
                                        <option value="Wedding Ceremony">Wedding Ceremony</option>
                                        <option value="Hotel Pickup">Hotel Pickup</option>
                                        <option value="Hotel Drop-off">Hotel Drop-off</option>
                                        <option id="otherType" value="">Others</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>From Where?</span>
                                    <select name="start_state" >
                                        <option value="">Choose State</option>
                                        @foreach($states as $state)
                                            <option value="{{ $state }}">{{ $state }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>To Where?</span>
                                    <select name="end_state" >
                                        <option value="">Choose State</option>
                                        @foreach($states as $state)
                                            <option value="{{ $state }}">{{ $state }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>No Of Vehicles</span>
                                    <select name="no_of_vehicle">
                                        <option value="">Choose Number Of Vehicles</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6 and above">6 and above</option>
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <span>Point Of Pickup</span>
                                    <input type="text" name="pick_up_point"  placeholder="Pick up Point">
                                </div>

                                <div class="col-sm-3">
                                    <span>Point Of Drop-Off</span>
                                    <input type="text" name="drop_off_point"  placeholder="Drop Off Point">
                                </div>

                                <div class="col-sm-3">
                                    <span>Transfer Mode</span>
                                    <select name="transfer_mode">
                                        <option value="">Choose Transfer Mode</option>
                                        <option value="One Way">One Way</option>
                                        <option value="Drop Down">Drop Down</option>
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <span>Vehicle Type</span>
                                    <select name="vehicle_type" >
                                        <option value="">Select An Option</option>
                                        <option value="Salon Car">Salon Car</option>
                                        <option value="SUV">SUV</option>
                                        <option value="Bus">Bus</option>
                                        <option value="Van">Van</option>
                                        <option value="Truck">Truck</option>
                                    </select>
                                </div>

                                <div class="col-sm-3">
                                    <span>Pickup Time</span>
                                    <select name="resumption_time">
                                        <option value="">Choose Opening Time</option>
                                        <option value="6am">6am</option>
                                        <option value="7am">7am</option>
                                        <option value="8am">8am</option>
                                        <option value="9am">9am</option>
                                        <option value="10am">10am</option>
                                        <option value="11am">11am</option>
                                        <option value="12pm">12pm</option>
                                        <option value="1pm">1pm</option>
                                        <option value="2pm">2pm</option>
                                        <option value="3pm">3pm</option>
                                        <option value="4pm">4pm</option>
                                        <option value="5pm">5pm</option>
                                        <option value="6pm">6pm</option>
                                        <option value="7pm">7pm</option>
                                        <option value="8pm">8pm</option>
                                        <option value="9pm">9pm</option>
                                        <option value="10pm">10pm</option>
                                        <option value="11pm">11pm</option>
                                        <option value="12am">12am</option>
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <label>Start Date</label>
                                    <input type="date" name="start_date">
                                </div>

                                <div class="col-md-3">
                                    <label>End Date</label>
                                    <input type="date" name="end_date">
                                </div>

                                <div class="col-md-3">
                                    <label>Govt. ID No.</label>
                                    <input type="text" name="govt_id_no" placeholder="GOVT. ISSUED ID. NO">
                                </div>

                                <div class="col-sm-3">
                                    <span>Number Of Luggage</span>
                                    <input type="number" name="no_of_luggage"  placeholder="Number of Luggages">
                                </div>

                                <div class="col-sm-3">
                                    <span>Number Of Persons</span>
                                    <input type="number" name="no_of_persons"  placeholder="Number of Persons">
                                </div>

                                <div class="col-sm-3">
                                    <span>Upload Luggage Image</span>
                                    <input type="file" name="luggage_image">
                                </div>
                                <div class="col-sm-12 boxed">
                                    <button type="submit" class="btn btn--primary type--uppercase">Book Driver</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        document.getElementById('otherType')
    </script>

@endsection