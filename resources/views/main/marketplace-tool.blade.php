@extends('layouts.master')

@section('title','Marketplace Tool')

@section('banner')
    <section class="">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-8">
                    <h1>Marketplace tool</h1>
                    <p class="lead">
                        Experience the extra ordinary, as you can now bargain with drivers, cabs, driving tutors, driving schools and freelancers in real time. Also you will enjoy an opportunity to select the number of drivers or personnels you want to engage and bargain, with having access to setup the number of task/shift you want to post in the marketplace.
                        <br />Read more about <a href="{{ url('terms') }}">terms of use</a>
                    </p>
                
                </div>
            </div>
        </div>
    </section>
@endsection
