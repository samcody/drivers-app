@extends('layouts.master')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
<style>
    .contrast { filter: contrast(160%); }
    .grayscale { filter: grayscale(100%); }
</style>
@section('title','Hire Professional Drivers At Ease In Nigeria')
<script type="text/javascript">function add_chatinline(){var hccid=90239265;var nt=document.createElement("script");nt.async=true;nt.src="https://mylivechat.com/chatinline.aspx?hccid="+hccid;var ct=document.getElementsByTagName("script")[0];ct.parentNode.insertBefore(nt,ct);}
    add_chatinline(); </script>
@section('banner')
    <section class="cover unpad--bottom switchable bg--secondary text-center-xs">
        <div class="container">
            <div class="row">
                <div style="margin-top:20px; height:310px;" class="col-sm-6 col-md-5 mt--3">
                    {{-- <div class='imgbox'>
                        <div class='img1'>Reliable Drivers and Personnel Recruitment made easy in Real time and On-demand</div>
                        <div class='img2'>We Invest in the Right Set of drivers and personnels. After which we Automate their matches to Businesses in Real time </div>
                        <div class='img3'>Start Engaging Reliable Drivers and personnels for your Business and Experience High level of productivity.</div>
                        <div class='img4'>Use Our Marketplace tool to Bargain with Drivers, Cabs, Driving Tutors, Driving Schools and freelancers in Real time</div>
                        <div class='img5'>Use our Diamond tool to Enjoy Secured and Guaranteed Payments weekly with Automated Reports Weekly when you put your vehicles on our Ridesharing platforms</div>
                    </div> --}}

                    <div class="slider slider--inline-arrows" data-arrows="false">
                        <ul class="slides">
                            <li>
                                    <div class="testimonial">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                {{-- <h1>The Right Drivers and Solutions</h1> --}}
                                                <h3 class="">
                                                    Reliable Drivers and Personnel Recruitment made easy in Real time and On-demand
                                                </p>
                                                {{-- <a class="btn btn--primary type--uppercase" href="{{ url('package') }}">
                                                    <span class="btn__text">
                                                        Get Started
                                                    </span>
                                                </a> --}}
                                        </div>
                                    </div>
                            </li>
                            <li>
                                    <div class="testimonial">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            {{-- <h1>Book Full-Time Drivers</h1> --}}
                                            <h3 class="">
                                                We Invest in the Right Set of drivers and personnels. After which we Automate their matches to Businesses in Real time
                                            </p>
                                            {{-- <a class="btn btn--primary type--uppercase" href="{{ url('package') }}">
                                                <span class="btn__text">
                                                    Get Started
                                                </span>
                                            </a> --}}
                                    </div>
                                    </div>
                            </li>
                            <li>
                                    <div class="testimonial">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            {{-- <h1>Book Drivers For Your Uber</h1> --}}
                                            <h3 class="">
                                                Start Engaging Reliable Drivers and personnels for your Business and Experience High level of productivity
                                            </p>
                                            {{-- <a class="btn btn--primary type--uppercase" href="{{ url('package') }}">
                                                <span class="btn__text">
                                                    Get Started
                                                </span>
                                            </a> --}}
                                    </div>
                                    </div>
                            </li>
                            <li>
                                <div class="testimonial">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        {{-- <h1>Book Short-Term Drivers</h1> --}}
                                        <h3 class="">
                                            Use Our Marketplace tool to Bargain with Drivers, Cabs, Driving Tutors, Driving Schools and freelancers in Real time
                                        </p>
                                        {{-- <a class="btn btn--primary type--uppercase" href="{{ url('package') }}">
                                            <span class="btn__text">
                                                Get Started
                                            </span>
                                        </a> --}}
                                </div>
                                </div>
                            </li>
                            <li>
                                <div class="testimonial">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        {{-- <h1>Travel and Ride in  Convenience of Comfort</h1> --}}
                                        <h3 class="">
                                            Use our Diamond tool to Enjoy Secured and Guaranteed Payments weekly with Automated Reports Weekly when you put your vehicles on our Ridesharing platforms
                                        </p>
                                        {{-- <a class="btn btn--primary type--uppercase" href="{{ url('package') }}">
                                            <span class="btn__text">
                                                Get Started
                                            </span>
                                        </a> --}}
                                </div>
                                </div>
                            </li>
                    
                        </ul>
                    </div>

                    <form style="padding-left:15px;position:absolute; bottom: 0;" action="{{ url('short-term-main') }}" method="post">
                        {{ csrf_field() }}
                        <h5 style="margin-bottom:0;">Easily Search Through 8516 Drivers ON DEMAND</h5>
                        
                        <div style="padding:0;margin-right:0;" class="col-md-12">
                            <select class="validate-required" type="text" name="location">
                                <option value="">Choose Location</option>
                                @foreach($locations as $location)
                                    <option value="{{ $location['location'] }}">{{ $location['location'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div style="padding-left:0;" class="col-md-5 pull-left">
                            <button type="submit" class="btn btn--primary type--uppercase">Search</button>
                        </div>  
                    </form>
                    {{-- <a style="position:absolute; bottom: 0;" class="btn btn--primary type--uppercase" href="{{ url('package') }}">
                        <span class="btn__text">
                            Get Started
                        </span>
                    </a> --}}
                    
                </div>
                <div class="col-sm-6">
                    <img alt="Image" src="{{ asset('assets/img/bg-1.jpg') }}" />
                    <br>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
<marquee style="height: 4em; margin-bottom: -200px">
        <img alt="image" style="height: 100%; margin-left: 2em;"  class="grayscale" src="{{ asset('assets/img/uber logo.png') }}" />
        <img alt="image" style="height: 100%; margin-left: 2em;"  class="grayscale" src="{{ asset('assets/img/kangpe logo.png') }}" />
        <img alt="image" style="height: 100%; margin-left: 2em;"  class="grayscale" src="{{ asset('assets/img/vp logo.png') }}" />
        <img alt="image" style="height: 100%; margin-left: 2em;"  class="grayscale" src="{{ asset('assets/img/Techpoint logo.png') }}" />
        <img alt="image" style="height: 100%; margin-left: 2em;"  class="grayscale" src="{{ asset('assets/img/TheNerve_Africa Logo.png') }}" />
        <img alt="image" style="height: 100%; margin-left: 2em;"  class="grayscale" src="{{ asset('assets/img/TVC-ENTERTAINMENT logo.png') }}" />
        <img alt="image" style="height: 100%; margin-left: 2em;"  class="grayscale" src="{{ asset('assets/img/ITNewsAfrica_logo.gif') }}" />
        <img alt="image" style="height: 100%; margin-left: 2em;"  class="grayscale" src="{{ asset('assets/img/disrupt-africa logo.png') }}" />
        <img alt="image" style="height: 100%; margin-left: 2em;"  class="grayscale" src="{{ asset('assets/img/connect-nigeria logo.png') }}" />
        <img alt="image" style="height: 100%; margin-left: 2em;"  class="grayscale" src="{{ asset('assets/img/cnn logo.png') }}" />
        <img alt="image" style="height: 100%; margin-left: 2em;"  class="grayscale" src="{{ asset('assets/img/Channels-TV.png') }}" />
        <img alt="image" style="height: 100%; margin-left: 2em;"  class="grayscale" src="{{ asset('assets/img/1960 bet logo.png') }}" />
        <img alt="image" style="height: 100%; margin-left: 2em;"  class="grayscale" src="{{ asset('assets/img/africa arena tour logo.jpg') }}" />
        <img alt="image" style="height: 100%; margin-left: 2em;"  class="grayscale" src="{{ asset('assets/img/noella foundation logo.jpg') }}" />
        <img alt="image" style="height: 100%; margin-left: 2em;"  class="grayscale" src="{{ asset('assets/img/interswitch.png') }}" />
        <img alt="image" style="height: 100%; margin-left: 2em;"  class="grayscale" src="{{ asset('assets/img/taxify.png') }}" />

    </marquee>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4><i class="icon icon-Duplicate-Window icon--xs color--primary float-left"></i>Full-Time</h4>
                        <i class="fas fa-clock fa-6x" style="color:#0c2e4f"></i>
                        <p>
                            Engage smart drivers and transport on-demand. Get a professionally trained full-time driver within 24 hours or less.
                        </p>
                        <a class="btn btn--primary type--uppercase" href="{{ url('full-time/package') }}">
                        <span class="btn__text">
                            Hire Driver
                        </span>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4><i class="icon icon-Duplicate-Window icon--xs color--primary float-left"></i>Uber</h4>
                        <i class="fab fa-uber fa-6x" style="color:#0c2e4f"></i>
                        <p>
                            Engage verified drivers for your uber cars.
                            Get properly background &amp; security checked professional drivers within 24 hours
                        </p>
                        <a class="btn btn--primary type--uppercase" href="{{ url('uber/package') }}">
                        <span class="btn__text">
                            Hire Driver
                        </span>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4><i class="icon icon-Duplicate-Window icon--xs color--primary float-left"></i>Short Term</h4>
                        <i class="fas fa-hourglass-half fa-6x"style="color:#0c2e4f"></i>
                        <p>
                            Engage smart drivers and transport on-demand. Get a professionally trained short time driver within 24 hours or less.
                        </p>
                        <a class="btn btn--primary type--uppercase" href="{{ url('short-term') }}">
                        <span class="btn__text">
                            Hire Driver
                        </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    

    <section class="switchable feature-large bg--secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-5">
                    <img alt="image" src="{{ asset('assets/img/device-1.png') }}" />
                </div>
                <div class="col-sm-6 col-md-7">
                    <div class="switchable__text">
                        <h2 class="text-center">We are Africa’s Choice for Top notch Transportation and Driving Management Solutions</h2>
                        <p class="lead">
                            DriversNG helps in matching highly experienced professionals, trained and verified drivers to employers within 24 HOURS or even ON DEMAND. You can even view, choose and book professional drivers by web or app.
                            OUR SOLUTION avails trained, verified and ready drivers to drive your cars, vans, buses, trucks or SUVs ON FULL TIME OR SHORT TIME BASIS for your Hospitals, Companies, Private Use, Schools, Churches, Wedding Events, Cars on Ubers and what have you.
                            GET ASSIGNED A professional driver to drive you INTER-STATE & INTRA-CITY
                            We Also Match you to the vehicle of your choice for every Hotel Transfers, Airport transfers and anywhere you want to go
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="cover height-80 imagebg text-center" data-overlay="6">
        <div class="background-image-holder">
            <img alt="background" src="{{ asset('assets/img/bg-1.jpg') }}" />
        </div>
        <div class="container pos-vertical-center">
            <div class="row">
                <div class="col-sm-6">
                    <span class="h1">We drive you in safety, style and comfort</span>
                    <p class="lead">
                        We help match highly experienced, trained and verified drivers to employers within 24 HOURS or even ON DEMAND
                    </p>
                    {{--<div class="modal-instance block">--}}
                        {{--<div class="video-play-icon video-play-icon--sm modal-trigger"></div>--}}
                        {{--<span>--}}
                                    {{--<strong>See Us in action</strong>&nbsp;&nbsp;&nbsp;104 Seconds</span>--}}
                        {{--<div class="modal-container">--}}
                            {{--<div class="modal-content bg-dark" data-width="60%" data-height="60%">--}}
                                {{--<iframe data-src="https://www.youtube.com/embed/6p45ooZOOPo?autoplay=1" allowfullscreen="allowfullscreen"></iframe>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </section>
    <section class="text-center bg--secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <h2>Our Drivers In A Glimpse</h2>
                    <p class="lead">
                        For your maximum satisfaction, we have a wide range of drivers skilled under us.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="bg--secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="feature boxed boxed--border border--round text-center">
                        <div class="feature__body">
                            <i class="icon icon-Checked-User icon--lg color--primary"></i>
                            <br><br>
                            <h5>SHORT TERM DRIVERS</h5>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="feature boxed boxed--border border--round text-center">
                        <div class="feature__body">
                            <i class="icon icon-Checked-User icon--lg color--primary"></i>
                            <br><br>
                            <h5 class="text-uppercase">Drivers for Executives</h5>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="feature boxed boxed--border border--round text-center">
                        <div class="feature__body">
                            <i class="icon icon-Checked-User icon--lg color--primary"></i>
                            <br><br>
                            <h5>FULL TERM DRIVERS</h5>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="feature boxed boxed--border border--round text-center">
                        <div class="feature__body">
                            <i class="icon icon-Checked-User icon--lg color--primary"></i>
                            <br><br>
                            <h5>DRIVERS FOR UBER</h5>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="feature boxed boxed--border border--round text-center">
                        <div class="feature__body">
                            <i class="icon icon-Checked-User icon--lg color--primary"></i>
                            <br><br>
                            <h5>TRUCK DRIVERS</h5>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="feature boxed boxed--border border--round text-center">
                        <div class="feature__body">
                            <i class="icon icon-Checked-User icon--lg color--primary"></i>
                            <br><br>
                            <h5 class="text-uppercase">Spy-police Drivers</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class=" ">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="slider slider--inline-arrows" data-arrows="true">
                        <ul class="slides">
                            <li>
                                <div class="row">
                                    <div class="testimonial">
                                        <div class="col-md-9 col-md-offset-1 col-sm-8 col-xs-12">
                                            <span class="h3">
                                                &ldquo;I am really impressed by the driver I got from them, the driver assigned to me matched my persona, and very hardworking, drives smartly using GPS and google map.&rdquo;
                                            </span>
                                            <h5>Miss Esther</h5>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="testimonial">
                                        <div class="col-md-9 col-md-offset-1 col-sm-8 col-xs-12">
                                            <span class="h3">
                                                &ldquo;DriversNG is pretty organized and I am super impressed with that level of organization. They delivered exactly on time. this is awesome! &rdquo;
                                            </span>
                                            <h5>Chime</h5>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="testimonial">
                                        <div class="col-md-9 col-md-offset-1 col-sm-8 col-xs-12">
                                            <span class="h3">
                                                &ldquo;Everyone should try out DriversNG again and again, they have a smooth operations..&rdquo;
                                            </span>
                                            <h5>Fola Olatunji</h5>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="text-center imagebg" data-gradient-bg='#4876BD,#8F48BD,#BD48B1'>
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-6">
                    <div class="cta">
                        <h2>To Get A Verified and Tested Driver Is Now Easier</h2>
                        <p class="lead">
                            It is now easy to get professional & vetted drivers in Nigeria. We help you achieve this in 2 minutes.
                        </p>
                        <a class="btn btn--primary type--uppercase" href="{{ url('package') }}">
                            <span class="btn__text">
                                Get Started
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
@endsection