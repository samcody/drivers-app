@extends('layouts.master')

@section('title','About Us')

@section('banner')
    <section class="text-center imagebg space--lg" data-overlay="3">
        <div class="background-image-holder">
            <img alt="background" src="{{ asset('assets/img/bg-1.jpg') }}" />
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-6 color--primary-1">
                    <h1>Terms Of Use</h1>
                    <p class="lead">
                        We want to serve you better. Our Terms & Condition helps us serve you.
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <a href="{{ asset('pdf/Terms of use for updated website.pdf') }}" download="term-condition.pdf" class="btn btn-primary btn--lg">
                        Download Terms & Conditions
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection