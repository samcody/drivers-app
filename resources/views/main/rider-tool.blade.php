@extends('layouts.master')

@section('title','Rider Tool')

@section('banner')
    <section class="">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-8">
                    <h1>Rider tool</h1>
                    <p class="lead">
                        It is now possible to become a rider or request for the services of riders on the DriversNG platform. A rider is a special kind of driver that focuses on riding or driving as you may suggest, a motor cycle, bicycle, tricycle or a Power bike. Riders services are made use for activities like product deliveries, motorcycle pooling (a new form of transport), commercial transport, e.t.c. Our riders are disciplined, vetted, reliable and well trained.
                    </p>
                
                </div>
            </div>
        </div>
    </section>
@endsection
