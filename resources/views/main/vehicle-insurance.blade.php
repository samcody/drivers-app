
@extends('layouts.master')

@section('title','Vehicle Insurance')

@section('banner')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <h1>Vehicle Insurance</h1>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section class="switchable feature-large bg--secondary">
        <div class="container">
            <div  class="row">
                <div class="col-sm-6 col-md-5">
                    <img alt="image"src="{{ asset('assets/img/insurance.jpg') }}" />
                </div>
                <div class="col-sm-6 col-md-7">
                    <div style="font-size:15px">
                        <p class="lead">
                                You can now conveniently start and complete your full comprehensive insurance via our platform, and have access to the following benefits;
                            <br>
                            
                            <li>Comprehensive insurance at discounted rate of 2.5%</li>
                            <li>Free car tracker installation (for  3 million and above worth of vehicles)</li>
                            <li>Free vehicle towing (for Lagos only)</li>
                            <li>Can accommodate two installment payments</li>
                    
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class=" bg--secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-12 ">
                    <div class="row">
                        <div class="boxed boxed--border">
                            @include('partial.alert')
                            <form action="{{ url('vehicle-insurance') }}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <h3 class="text-center">Let’s know about your interest and vehicle</h3>
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" name="name" placeholder="Name" required>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" name="address" placeholder="Home/Office Address" required>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" name="phone" placeholder="Phone Number" required>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" name="email" placeholder="Email Address" required>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" name="vehicle_type" placeholder="Vehicle Type" required>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" name="vehicle_transmission" placeholder="Vehicle Transmission" required>
                                    </div>
                                </div>

                                <div class="col-sm-12 boxed">
                                    <button type="submit" class="btn btn--primary type--uppercase">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        $(function() {
            $('#price_ranges').hide();
            $('#partnership_purpose').on('change', function(e) {
                e.preventDefault();
                if ($('#partnership_purpose').val() == 'Investing') {
                    var accreditedInvestor = confirm('Are you an Accredited Investor?');
                    if (accreditedInvestor) {
                        $('#price_ranges').show();
                    } else {
                        $('#price_ranges').hide();
                    }
                } else {
                    $('#price_ranges').hide();
                }
            });
        });
    </script>

@endsection