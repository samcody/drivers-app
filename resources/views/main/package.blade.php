@extends('layouts.master')

@section('title','Choose Package')

@section('banner')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-8">
                    <h1>Packages</h1>
                    <p class="lead">
                        You can hire drivers for any of the following purpose.
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>Full-Time</h4>
                        <i class="icon icon-Duplicate-Window icon--lg color--primary"></i>
                        <p>
                            Engage smart drivers and transport on-demand. Get a professionally trained full-time driver within 24 hours or less.
                        </p>
                        <a class="btn btn--primary type--uppercase" href="{{ url('full-time/package') }}">
                        <span class="btn__text">
                            Book Driver
                        </span>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>Uber</h4>
                        <i class="icon icon-Duplicate-Window icon--lg color--primary"></i>
                        <p>
                            Engage verified drivers for your uber cars.
                            Get properly background &amp; security checked professional drivers within 24 hours
                        </p>
                        <a class="btn btn--primary type--uppercase" href="{{ url('uber/package') }}">
                        <span class="btn__text">
                            Book Driver
                        </span>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>Short Term</h4>
                        <i class="icon icon-Duplicate-Window icon--lg color--primary"></i>
                        <p>
                            Book verified drivers for all your short term driving needs. Needs like, drivers for a day, week, weekend, school runs and so on.
                        </p>
                        <a class="btn btn--primary type--uppercase" href="{{ url('short-term') }}">
                        <span class="btn__text">
                            Book Driver
                        </span>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>Pay Per Use</h4>
                        <i class="icon icon-Duplicate-Window icon--lg color--primary"></i>
                        <p>
                            Engage Smart, Verified and Reliable drivers as you go. Express your liberty and experience satisfaction at its best.
                        </p>
                        <a class="btn btn--primary type--uppercase" href="{{ url('pay-use') }}">
                        <span class="btn__text">
                            Book Driver
                        </span>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>Transfer</h4>
                        <i class="icon icon-Duplicate-Window icon--lg color--primary"></i>
                        <p>
                            Enjoy Smart Transport anywhere you feel like going to. Get yourself or your goods transferred
                            in a comfortable and convenient vehicle.
                        </p>
                        <a class="btn btn--primary type--uppercase" href="{{ url('transfer/book') }}">
                        <span class="btn__text">
                            Book Driver
                        </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection