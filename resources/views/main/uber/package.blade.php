@extends('layouts.master')

@section('title','Choose Uber Plan')

@section('banner')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-8">
                    <h1>Uber Plans</h1>
                    <p class="lead">
                        Choose Uber Driver Plan
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section>
        <div style="width:1470px;" class="container">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>Platinum</h4>
                        <span class="h2"><span class="pricing__dollar">&#8358;</span>5,250<small style="font-size: .4em;">vat inclusive</small></span>
                        <p>
                            A verified Driver + Matched in 24HOURS + SLA & Guarantors details
                        </p>
                        <form action="{{ url('uber/book') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="package" value="PLATINUM">
                            <input type="hidden" name="price" value="5250">
                        <button class="btn type--uppercase btn--primary">
                            <span class="btn__text">
                                Select
                            </span>
                        </button>
                        </form>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>Diamond</h4>
                        <span class="h2"><span class="pricing__dollar">&#8358;</span>10,500<small style="font-size: .4em;">vat inclusive</small></span>
                        <p>
                            Guaranteed Agreed Weekly Payment Delivery + Vehicle & Drivers Management + NGN 5,000 monthly admin & mgt. + Company bears NGN 5,000 for maintenance & damages + SLA
                        </p>
                        <form action="{{ url('uber/book') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="package" value="DIAMOND">
                            <input type="hidden" name="price" value="10500">
                            <button class="btn btn-sm type--uppercase btn--primary">
                                <span class="btn__text">
                                    Select
                                </span>
                            </button>
                        </form>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>HP-Diamond</h4>
                        <span class="h2"><span class="pricing__dollar">&#8358;</span>10,500<small style="font-size: .4em;">vat inclusive</small></span>
                        <p>
                            Guaranteed Agreed Monthly Payment Delivery + Vehicle Ownership transfer + NGN 10,000 monthly admin & mgt. + Company bears NGN 5,000 for maintenance & damages + SLA
                        </p>
                        <form action="{{ url('uber/book') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="package" value="HP-DIAMOND">
                            <input type="hidden" name="price" value="10500">
                            <button class="btn btn-sm type--uppercase btn--primary">
                                <span class="btn__text">
                                    Select
                                </span>
                            </button>
                        </form>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>HP-Platinum</h4>
                        <span class="h2"><span class="pricing__dollar">&#8358;</span>10,500<small style="font-size: .4em;">vat inclusive</small></span>
                        <p>
                            A Verified Higher Purchase driver + Serious Minded + SLA & Guarantors Details
                        </p>
                        <form action="{{ url('uber/book') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="package" value="HP-PLATINUM">
                            <input type="hidden" name="price" value="10500">
                            <button class="btn type--uppercase btn--primary">
                            <span class="btn__text">
                                Select
                            </span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection