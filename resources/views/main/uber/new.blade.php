@extends('layouts.master')

@section('title','Hire Uber Driver')

@section('banner')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-8">
                    <h1>Hire Uber Driver</h1>
                    <p class="lead">
                        You selected {{ $request['package'] }} Package
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section class=" bg--secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-12 ">
                    <div class="row">
                        <div class="boxed boxed--border">
                            @include('partial.alert')
                            <form action="{{ url('uber/hire') }}" method="post" enctype="multipart/form-data" role="form"  class="text-left">
                                {{ csrf_field() }}
                                <input type="hidden" value="{{ $request['package'] }}" name="package">
                                <input type="hidden" value="{{ $request['price'] }}" name="price">
                                <div class="col-sm-3">
                                    <span>Number of Drivers</span>
                                    <input type="number" name="number_of_driver" placeholder="Number of Drivers" required>
                                </div>
                                <div class="col-sm-3">
                                    <span>Home Address</span>
                                    <input type="text" name="home_address"  placeholder="Home Address" required>
                                </div>
                                <div class="col-sm-3">
                                    <span>Office Address</span>
                                    <input type="text" name="office_address"  placeholder="Office Address" required>
                                </div>
                                <div class="col-sm-3">
                                    <span>Vehicle Specification</span>
                                    <select name="vehicle_specification" required>
                                        <option value="">Select An Option</option>
                                        <option value="Salon Car">Salon Car</option>
                                        <option value="SUV">SUV</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>Vehicle Brand</span>
                                    <input type="text" name="vehicle_brand"  placeholder="Vehicle Brand" required>
                                </div>
                                <div class="col-sm-3">
                                    <span>Vehicle Model</span>
                                    <input type="text" name="vehicle_model"  placeholder="Vehicle Model" required>
                                </div>
                                <div class="col-sm-3">
                                    <span>Vehicle Year</span>
                                    <input type="text" name="vehicle_year"  placeholder="Vehicle Year" required>
                                </div>
                                <div class="col-sm-3">
                                    <span>Vehicle Identification Number</span>
                                    <input type="text" name="vehicle_identification_number"  placeholder="Vehicle Identification Number" required>
                                </div>
                                <div class="col-sm-3">
                                    <span>Vehicle Registration Number</span>
                                    <input type="text" name="vehicle_registration_number"  placeholder="Vehicle Registration Number" required>
                                </div>
                                <div class="col-sm-3">
                                    <span>Vehicle Transmission</span>
                                    <select name="vehicle_transmission" required>
                                        <option value="">Choose an Option</option>
                                        <option value="Automatic">Automatic</option>
                                        <option value="Manual">Manual</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>Insurance Type</span>
                                    <select name="insurance_type" required>
                                        <option value="">Choose an Option</option>
                                        <option value="Comprehensive">Comprehensive</option>
                                        <option value="3rd-party insurance">3rd-party insurance</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <span>Type of Vehicle</span>
                                    <input type="text" name="type_of_vehicle"   placeholder="Type of Vehicle" required>
                                </div>

                                <div class="col-sm-3">
                                    <span>Upload Vehicle Documents</span>
                                    <input type="file" accept="image/*" name="vehicle_documents" required>
                                </div>

                                <div class="col-sm-3">
                                    <span>Upload Vehicle Pictures</span>
                                    <input type="file" accept="image/*" name="vehicle_pictures" required>
                                </div>

                                <div class="col-sm-12 boxed">
                                    <button type="submit" class="btn btn--primary type--uppercase">Hire Driver</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection