@extends('layouts.master')

@section('title','Client Feedback')

@section('banner')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <h1>Client Feedback</h1>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section class="switchable feature-large bg--secondary">
        <div class="container">
            <div  class="row">
                <div class="col-sm-6 col-md-4">
                    <img alt="image" src="{{ asset('assets/img/device-1.png') }}" />
                </div>
                <div class="col-sm-6 col-md-7">
                    <div style="font-size:15px">
                        <p class="lead">
                            You can assist us with quality feedback via this page. We would deeply appreciate your support having us know how you feel towards our service.
                            
                        </p>
                    </div>
                </div>
            </div>
        </div>
    

    <div class=" bg--secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-12 ">
                    <div class="row">
                        <div class="boxed boxed--border">
                            @include('partial.alert')
                            <form action="{{ url('client-feedback-tool') }}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <input type="text" name="name" placeholder="Name" required>
                                    </div>
                                    <br />
                                </div>

                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" name="phone" placeholder="Phone Number" required>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" name="email" placeholder="Email Address" required>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <p>Kindly select where you have been dissatisfied with our service</p>
                                        <div class="input-checkbox">
                                            <input value="Customer Service" type="checkbox" name="dissatisfied" >
                                            <label for="checkbox"></label>
                                        </div>
                                        <span>Customer Service</span> </br>
            
                                        <div class="input-checkbox">
                                            <input value="Capacity & Capability" type="checkbox" name="dissatisfied" >
                                            <label for="checkbox"></label>
                                        </div>
                                        <span>Capacity &amp; Capability</span> </br>
            
                                        <div class="input-checkbox">
                                            <input value="Value Proposition" type="checkbox" name="dissatisfied" >
                                            <label for="checkbox"></label>
                                        </div>
                                        <span>Value Proposition</span> </br>

                                        <div class="input-checkbox">
                                            <input value="Product Promise" type="checkbox" name="dissatisfied" >
                                            <label for="checkbox"></label>
                                        </div>
                                        <span>Product Promise</span> </br>

                                        <div class="input-checkbox">
                                            <input value="Service Experience" type="checkbox" name="dissatisfied" >
                                            <label for="checkbox"></label>
                                        </div>
                                        <span>Service Experience</span> </br>

                                        <div class="input-checkbox">
                                            <input value="Drivers Behaviour" type="checkbox" name="dissatisfied" >
                                            <label for="checkbox"></label>
                                        </div>
                                        <span>Drivers Behaviour</span> </br>

                                        <div class="input-checkbox">
                                            <input value="Follow up" type="checkbox" name="dissatisfied" >
                                            <label for="checkbox"></label>
                                        </div>
                                        <span>Follow up</span> </br>

                                        <div class="input-checkbox">
                                            <input value="Company Location" type="checkbox" name="dissatisfied" >
                                            <label for="checkbox"></label>
                                        </div>
                                        <span>Company Location</span> </br>

                                        <div class="input-checkbox">
                                            <input value="Service Operations" type="checkbox" name="dissatisfied" >
                                            <label for="checkbox"></label>
                                        </div>
                                        <span>Service Operations</span> </br>

                                        <div class="input-checkbox">
                                            <input value="Technical Support" type="checkbox" name="dissatisfied" >
                                            <label for="checkbox"></label>
                                        </div>
                                        <span>Technical Support</span> </br>

                                        <div class="input-checkbox">
                                            <input value="Other" type="checkbox" name="dissatisfied" >
                                            <label for="checkbox"></label>
                                        </div>
                                        <span>Other</span> </br>
            
                                    </div>
                                    
                                    <div class="col-md-6 col-sm-6">
                                            <p>In what way have you found our service helpful?</p>
                                            <div class="input-checkbox">
                                                <input value="Turn around time" type="checkbox" name="helped" >
                                                <label for="checkbox"></label>
                                            </div>
                                            <span>Turn Around Time</span> </br>
                
                                            <div class="input-checkbox">
                                                <input value="Customer Service" type="checkbox" name="helped" >
                                                <label for="checkbox"></label>
                                            </div>
                                            <span>Customer Service</span> </br>
                
                                            <div class="input-checkbox">
                                                <input value="User service experience" type="checkbox" name="helped" >
                                                <label for="checkbox"></label>
                                            </div>
                                            <span>User Service Experience</span> </br>
    
                                            <div class="input-checkbox">
                                                <input value="Drivers Services" type="checkbox" name="helped" >
                                                <label for="checkbox"></label>
                                            </div>
                                            <span>Drivers Services</span> </br>
    
                                            <div class="input-checkbox">
                                                <input value="Service Operations" type="checkbox" name="helped" >
                                                <label for="checkbox"></label>
                                            </div>
                                            <span>Service Operations</span> </br>
    
                                            <div class="input-checkbox">
                                                <input value="Follow-up" type="checkbox" name="helped" >
                                                <label for="checkbox"></label>
                                            </div>
                                            <span>Follow-up</span> </br>
    
                                            <div class="input-checkbox">
                                                <input value="Other" type="checkbox" name="dissatisfied" >
                                                <label for="checkbox"></label>
                                            </div>
                                            <span>Other</span> </br>
                                    </div>
                                </div>

                                <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                                <p>Kindly click &amp; share the way you think you can assist or support DriversNG to serve you better next time</p>
                                                <div class="input-checkbox">
                                                    <input value="Funding" type="checkbox" name="support" >
                                                    <label for="checkbox"></label>
                                                </div>
                                                <span>Funding (I can help with some funds to make the business serve me better)</span> </br>
                    
                                                <div class="input-checkbox">
                                                    <input value="Investments" type="checkbox" name="support" >
                                                    <label for="checkbox"></label>
                                                </div>
                                                <span>Investments (I would love to discuss investment opportunities with the company, I think it would make things more available for better customer experience)</span> </br>
                    
                                                <div class="input-checkbox">
                                                    <input value="Networking/Partnerships" type="checkbox" name="support" >
                                                    <label for="checkbox"></label>
                                                </div>
                                                <span>Networking/Partnerships (I can assist with introductions, partnerships &amp; networking that allows customer experience blossom)</span> </br>
        
                                                <div class="input-checkbox">
                                                    <input value="Vehicles" type="checkbox" name="support" >
                                                    <label for="checkbox"></label>
                                                </div>
                                                <span>Vehicles (I am not currently using these vehicles again. Let me provide it to)</span> </br>
        
                                                <div class="input-checkbox">
                                                    <input value="Other" type="checkbox" name="support" >
                                                    <label for="checkbox"></label>
                                                </div>
                                                <span>Other</span> </br>
                                        </div>
                                </div>

                                <div class="col-sm-12 boxed">
                                    <button type="submit" class="btn btn--primary type--uppercase">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>

@endsection