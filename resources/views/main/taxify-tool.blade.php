{{-- @extends('layouts.master')

@section('title','Taxify Request Tool')

@section('banner')
    <section class="">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-8">
                    <h1>Taxify Request Tool</h1>
                    <p class="lead">
                        Taxify vehicle owners can get drivers recruited to their vehicles when the need arises. We would be on ground to ensure that you can enjoy quicker access to already verified and well trained drivers.
                    </p>
                    <p class="lead">
                        kindly choose from any of the plans so as to serve you better.
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section>
        <div style="width:1470px;" class="container">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>Platinum</h4>
                        <span class="h1"><span class="pricing__dollar">&#8358;</span>5,000</span>
                        <p>
                            A verified Driver + Matched in 24HOURS + SLA & Guarantors details
                        </p>
                        <form action="{{ url('uber/book') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="package" value="PLATINUM">
                            <input type="hidden" name="price" value="5000">
                        <button class="btn type--uppercase btn--primary">
                            <span class="btn__text">
                                Select
                            </span>
                        </button>
                        </form>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>HP-Platinum</h4>
                        <span class="h1"><span class="pricing__dollar">&#8358;</span>10,000</span>
                        <p>
                            A Verified Higher Purchase driver + Serious Minded + SLA & Guarantors Details
                        </p>
                        <form action="{{ url('uber/book') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="package" value="HP-PLATINUM">
                            <input type="hidden" name="price" value="10000">
                            <button class="btn type--uppercase btn--primary">
                            <span class="btn__text">
                                Select
                            </span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection --}}


@extends('layouts.master')

@section('title','Taxify Request Tool')

@section('banner')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <h1>Taxify Request Tool</h1>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section class="switchable feature-large bg--secondary">
        <div class="container">
            <div  class="row">
                <div class="col-sm-4 col-md-2">
                    
                </div>
                <div class="col-sm-4 col-md-10">
                    <div style="font-size:15px">
                        <p class="lead">
                                Taxify vehicle owners can get drivers recruited to their vehicles when the need arises. We would be on ground to ensure that you can enjoy quicker access to already verified and well trained drivers.
                                <br>
                                kindly choose from any of the plans so as to serve you better.
                        </p>
                    </div>
                </div>
                <div class="col-sm-4 col-md-2">
                    
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-sm-6 col-md-4">
                        <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>Platinum</h4>
                        <span class="h1"><span class="pricing__dollar">&#8358;</span>5,000</span>
                        <p>
                            A verified Driver + Matched in 24HOURS + SLA & Guarantors details
                        </p>
                        <form action="{{ url('uber/book') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="package" value="PLATINUM">
                            <input type="hidden" name="price" value="5000">
                        <button class="btn type--uppercase btn--primary">
                            <span class="btn__text">
                                Select
                            </span>
                        </button>
                        </form>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                        <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                                <h4>HP-Platinum</h4>
                                <span class="h1"><span class="pricing__dollar">&#8358;</span>10,000</span>
                                <p>
                                    A Verified Higher Purchase driver + Serious Minded + SLA & Guarantors Details
                                </p>
                                <form action="{{ url('uber/book') }}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="package" value="HP-PLATINUM">
                                    <input type="hidden" name="price" value="10000">
                                    <button class="btn type--uppercase btn--primary">
                                    <span class="btn__text">
                                        Select
                                    </span>
                                    </button>
                                </form>
                            </div>
                </div>

                <div class="col-md-2"></div>
                
            </div>
        </div>
    </section>

@endsection

