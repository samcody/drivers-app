@extends('layouts.master')

@section('title','How It Works')

@section('content')
    <section class="height-90">
        <div class="container pos-vertical-center">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-md-offset-2">
                    <div class="wizard bg--white">
                        <h5>Welcome</h5>
                        <section class="text-center">
                            <div class="pos-vertical-center">
                                <img alt="pic" src="{{ asset('assets/img/device-1.png') }}" />
                                <h4>Let's take you through how DRIVERSNG works!</h4>
                                {{--<p class="type--fade type--fine-print">--}}
                                    {{--* Use the next button, or your keyboard arrows to advance--}}
                                {{--</p>--}}
                            </div>
                        </section>
                        <h5>Booking</h5>
                        <section class="text-center">
                            <div class="pos-vertical-center">
                                <i class="icon icon-Duplicate-Window icon--lg color--primary"></i><br><br>
                                <h4>Request A Driver!</h4>
                                {{--<p class="type--fade type--fine-print">--}}
                                    {{--* Use the sidebar and click "Add Media"--}}
                                {{--</p>--}}
                            </div>
                        </section>
                        <h5>Meeting</h5>
                        <section class="text-center">
                            <div class="pos-vertical-center">
                                <i class="icon icon-Checked-User icon--lg color--primary"></i><br><br>
                                <h4>Once Payment is accepted,your driver is assigned to you</h4>
                                {{--<p class="type--fade type--fine-print">--}}
                                    {{--* The device guide is located in the sidebar--}}
                                {{--</p>--}}
                            </div>
                        </section>
                        <h5>Satisfaction</h5>
                        <section class="text-center">
                            <div class="pos-vertical-center">
                                <img alt="pic" src="{{ asset('assets/img/art-5.png') }}" />
                                <h4>You can now relax as we serve you</h4>
                                {{--<p class="type--fade type--fine-print">--}}
                                    {{--* Find more help articles under "Help" in the sidebar--}}
                                {{--</p>--}}
                                <a class="btn btn--primary type--uppercase" href="{{ url('package') }}">
                                    <span class="btn__text">
                                        Get Started
                                    </span>
                                </a>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection