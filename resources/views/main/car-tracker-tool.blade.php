
@extends('layouts.master')

@section('title','Car Tracker Tool')

@section('banner')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <h1>Car Tracker Tool</h1>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section class="switchable feature-large bg--secondary">
        <div class="container">
            <div  class="row">
                <div class="col-sm-6 col-md-4">
                    <img alt="image" src="{{ asset('assets/img/tracker.png') }}" />
                </div>
                <div class="col-sm-6 col-md-7">
                    <div style="font-size:15px"  class="switchable__text">
                        <p class="lead">
                                Install high sense car trackers in your cars, buses and vans to mitigate the risks of insecurities on the road. Kindly indicate your interest in either purchasing our car trackers or vehicle trackers and or installing them.
                            
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class=" bg--secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-12 ">
                    <div class="row">
                        <div class="boxed boxed--border">
                            @include('partial.alert')
                            <form action="{{ url('car-tracker-tool') }}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" name="name" placeholder="Name" required>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" name="address" placeholder="Home/Office Address" required>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" name="phone" placeholder="Phone Number" required>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" name="email" placeholder="Email Address" required>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" name="vehicle_type" placeholder="Vehicle Type" required>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <input type="text" name="vehicle_transmission" placeholder="Vehicle Transmission" required>
                                    </div>
                                </div>

                                <div class="col-sm-12 boxed">
                                    <button type="submit" class="btn btn--primary type--uppercase">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    
@endsection