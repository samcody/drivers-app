
@extends('layouts.master')

@section('title','Diamond Tool')

@section('banner')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <h1>Diamond Tool</h1>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section class="switchable feature-large bg--secondary">
        <div class="container">
            <div  class="row">
                <div class="col-sm-4 col-md-2">
                    
                </div>
                <div class="col-sm-4 col-md-10">
                    <div style="font-size:15px">
                        <p class="lead">
                            Make use of our Diamond tool to start enjoying secured and guaranteed payments weekly with automated reports weekly when you put your cars on [our fleet management and ride sharing platform]
                            Popular ride sharing platforms like Uber &amp; Taxify, and enjoy benefits such as;
                            <br>
                            <li>Comprehensive Insurance at discounted rate of 2.5%</li>
                            <li>Free car tracker installation (For 3 million above worth of cars)</li>
                            <li>Quick response to towing of your car</li>
                            <li>Driver management</li>
                            <li>Guaranteed weekly returns</li>
                            <li>Automated weekly reports</li>
                            <li>Drivers replacement</li>
                        </p>
                    </div>
                </div>
                <div class="col-sm-4 col-md-2">
                    
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-sm-6 col-md-4">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>Diamond</h4>
                        <span class="h1"><span class="pricing__dollar">&#8358;</span>10,000</span>
                        <p>
                            Guaranteed Agreed Weekly Payment Delivery + Vehicle & Drivers Management + NGN 5,000 monthly admin & mgt. + Company bears NGN 5,000 for maintenance & damages + SLA
                        </p>
                        <form action="{{ url('uber/book') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="package" value="DIAMOND">
                            <input type="hidden" name="price" value="10000">
                            <button class="btn btn-sm type--uppercase btn--primary">
                                <span class="btn__text">
                                    Select
                                </span>
                            </button>
                        </form>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>HP-Diamond</h4>
                        <span class="h1"><span class="pricing__dollar">&#8358;</span>10,000</span>
                        <p>
                            Guaranteed Agreed Monthly Payment Delivery + Vehicle Ownership transfer + NGN 10,000 monthly admin & mgt. + Company bears NGN 5,000 for maintenance & damages + SLA
                        </p>
                        <form action="{{ url('uber/book') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="package" value="HP-DIAMOND">
                            <input type="hidden" name="price" value="10000">
                            <button class="btn btn-sm type--uppercase btn--primary">
                                <span class="btn__text">
                                    Select
                                </span>
                            </button>
                        </form>
                    </div>
                </div>

                <div class="col-md-2"></div>
                
            </div>
        </div>
    </section>

@endsection

