@extends('layouts.master')

@section('title','API')

@section('banner')
    <section class="">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-8">
                    <h1>API(Developers' Tool)</h1>
                    <p class="lead">
                        We have made our recruitment solution alot more easier for you as a large pool drivers base company. We build specialized APIs for your drivers recruitment effort, so you could focus on other core of your business that really matters
                        <br />
                        Our APIs can these benefits ad attributes below: 
                        <li>Access to recruit 100 verified and well trained professional drivers bi-annually</li>
                        <li>Access to guarantors documents or other related documents that are available</li>
                        <li>Easy to understand and to use platform by developers</li>
                    </p>
                
                </div>
            </div>
        </div>
    </section>
@endsection
