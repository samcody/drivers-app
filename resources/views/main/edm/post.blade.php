@extends('layouts.edm-packages')

@section('title','Post tasks/shifts')

@section('content')
<div class="container" style="margin-top:10%">
<div class="col-md-5">
  <div class="calendar" id="calendar-app">
    <div class="calendar--day-view" id="day-view">
      <span class="day-view-exit" id="day-view-exit">&times;</span>
      <span class="day-view-date" id="day-view-date">MAY 29 2016</span>
      <div class="day-view-content">
        <div class="day-highlight">
          You <span class="day-events" id="day-events">had no posts/shifts for today</span>. &nbsp; <span tabindex="0" onkeyup="if(event.keyCode != 13) return; this.click();" class="day-events-link" id="add-event" data-date>Create New Shift/Task</span>
        </div>
        <div class="day-add-event" id="add-day-event-box" data-active="false">
          <div class="row">
            <div class="half">
              <label class="add-event-label">
                 Name of Post
                <input type="text" class="add-event-edit add-event-edit--long" placeholder="New post" id="input-add-event-name">
               
              </label>
            </div>
            <div class="qtr">
              <label class="add-event-label">
            Start Time
                <input type="text" class="add-event-edit" placeholder="8:15" id="input-add-event-start-time" data-options="1,2,3,4,5,6,7,8,9,10,11,12" data-format="datetime">
                <input type="text" class="add-event-edit" placeholder="am" id="input-add-event-start-ampm" data-options="a,p,am,pm">
              </label>
            </div>
            <div class="qtr">
              <label class="add-event-label">
            End Time
                <input type="text" class="add-event-edit" placeholder="9" id="input-add-event-end-time" data-options="1,2,3,4,5,6,7,8,9,10,11,12" data-format="datetime">
                <input type="text" class="add-event-edit" placeholder="am" id="input-add-event-end-ampm" data-options="a,p,am,pm">
              </label>
            </div>
            <div class="half">
              <a onkeyup="if(event.keyCode != 13) return; this.click();" tabindex="0" id="add-event-save" class="event-btn--save event-btn">save</a>
              <a tabindex="0" id="add-event-cancel" class="event-btn--cancel event-btn">cancel</a>
            </div>
          </div>
          
        </div>
        <div id="day-events-list" class="day-events-list">
          
        </div>
        <div class="day-inspiration-quote" id="inspirational-quote">
          Every child is an artist.  The problem is how to remain an artist once he grows up. –Pablo Picasso
        </div>
      </div>
    </div>
    <div class="calendar--view" id="calendar-view">
      <div class="cview__month">
        <span class="cview__month-last" id="calendar-month-last">Apr</span>
        <span class="cview__month-current" id="calendar-month">May</span>
        <span class="cview__month-next" id="calendar-month-next">Jun</span>
      </div>
      <div class="cview__header">Sun</div>
      <div class="cview__header">Mon</div>
      <div class="cview__header">Tue</div>
      <div class="cview__header">Wed</div>
      <div class="cview__header">Thu</div>
      <div class="cview__header">Fri</div>
      <div class="cview__header">Sat</div>
      <div class="calendar--view" id="dates">
      </div>
    </div>
</div>
      

        <div class="footer">
          <span><span id="footer-date" class="footer__link">Today is May 30</span></span>    
        </div>
      </div>
      
<div class="col-md-7">
  <div class="panel panel-default" style="margin-top:1.5%">
    <div class="panel-heading">
      <h1 style = "font-weight: bolder;font-size:25px; text-align:center;" class="panel-title">How To Post A Task/Shift</h1>
    </div>
    <div style = "font-size:15px;" class="panel-body">
      <p>
        This is the page where you post Shifts/Tasks to the DRIVERSNG Marketplace, for drivers, graduate drivers, driving tutors, drivers with vehicles and training schools.
        
      </p>
      <li>Use the calendar on the left to choose the day you want to post shifts/tasks for </li>
      <li>Then click on Create New Shift </li>
      <li>After creating at the center top, check your post by clicking Tasks/Shifts</li>
    </div>
    <div style = "font-size:12px;"  class="panel-footer">
      <p> If you have any question, you can reach us  at business@driversng.com
    </div>
  </div>
  <p>You can also subscribe again to follow updates and feedbacks, those who responded to the task you needed</p>

</div>
</div>
<script src="{{ asset('edmm/package/js/calendar.js') }}"></script> <!-- calendar js -->
      

@endsection