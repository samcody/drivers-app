@extends('layouts.edm-packages')

@section('title','Drivers Market Place')

@section('content')
<style>
        body{padding-top:20px;}
    .carousel {
        margin-bottom: 0;
        padding: 0 40px 30px 40px;
    }
    /* The controlsy */
    .carousel-control {
        left: -12px;
        height: 40px;
        width: 40px;
        background: none repeat scroll 0 0 #222222;
        border: 4px solid #FFFFFF;
        border-radius: 23px 23px 23px 23px;
        margin-top: 90px;
    }
    .carousel-control.right {
        right: -12px;
    }
    /* The indicators */
    .carousel-indicators {
        right: 50%;
        top: auto;
        bottom: -10px;
        margin-right: -19px;
    }
    /* The colour of the indicators */
    .carousel-indicators li {
        background: #cecece;
    }
    .carousel-indicators .active {
    background: #428bca;
    }
        </style>

    <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 style="color:black">Short Term Drivers</h3>
                        <div id="Carousel2" class="carousel slide">
                        
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            
                        <div class="item active">
                            <div class="row">
                                @foreach($short as $s)
                            <div class="col-md-3"><a href="#" class="thumbnail"><img src="{{ asset('avatar/') }}{{'/'.$s->profilepicture}}" alt="Image" style="max-width:100%;" data-toggle="modal" data-target="#myModal{{$s->id}}"></a></div>
                                @endforeach
                          
                        </div><!--.row-->
                        </div><!--.item-->
                        {{ $short->links() }}
                        </div><!--.carousel-inner-->
                        </div><!--.Carousel-->
                         
                </div>
            </div>
            
        </div><!--.container-->
        @foreach($short as $s)
        <!-- Modal -->
        <div id="myModal{{$s->id}}" class="modal fade" role="dialog">
                <div class="modal-dialog">
              
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">{{$s->firstname.' '.$s->lastname}}</h4>
                    </div>
                    <div class="modal-body">
                            
                            <div class="row">
                                    <div class="col-md-6">
                                            <a href="#" class="thumbnail"><img src="{{ asset('avatar/') }}{{'/'.$s->profilepicture}}" alt="Image" style="max-width:100%;"></a>
                                    </div>
                                    <div class="col-md-6">
                                    <div align = "center" style="background:#a1a1a1;color:white;border-radius:7px;"><p style="font-weight:bold;font-size:20px;font-family:Comic Sans Ms;padding:7px;">{{$s->firstname.' '.$s->lastname}}</p></div>
                                    <div align = "center" style="background:#a1a1a1;color:white;border-radius:7px;"><p style="font-weight:bold;font-size:20px;font-family:Comic Sans Ms;padding:7px;">{{$s->age}}</p></div>
                                    <div align = "center" style="background:#a1a1a1;color:white;border-radius:7px;"><p style="font-weight:bold;font-size:20px;font-family:Comic Sans Ms;padding:7px;">{{$s->residence.', '.$s->location}}</p></div>
                                    <div align = "center" style="background:#a1a1a1;color:white;border-radius:7px;"><p style="font-weight:bold;font-size:20px;font-family:Comic Sans Ms;padding:7px;">{{$s->driver_type.' DRIVER'}}</p></div>
                                    </div>
                                  </div>
                    </div>
                    <div class="modal-footer">
                            <form class="form-horizontal" action="{{ url('marketplace/addcart',['id' => $s->id]) }}" method="post" enctype="multipart/form-data" role="form">
                                {{ csrf_field() }}
                            <input type ="hidden" name="cart" value="{{$s->id}}" />
                            <input type="submit" class="btn btn-primary" value="Add to Cart">
                            
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </form>
                    </div>
                  </div>
              
                </div>
              </div>
            
              @endforeach

        <div class="container">
                <div class="row">
                    <div class="col-md-12">
                            <h3 style="color:black">Uber Drivers</h3>
                            <div id="Carousel3" class="carousel slide">
                            
                            <!-- Carousel items -->
                            <div class="carousel-inner">
                                
                                <div class="item active">
                                    <div class="row">
                                        @foreach($uber as $u)
                                        <div class="col-md-3"><a href="#" class="thumbnail"><img src="{{ asset('avatar/') }}{{'/'.$u->profilepicture}}" alt="Image" style="max-width:100%;" data-toggle="modal" data-target="#myModal{{$u->id}}"></a></div>
                                        @endforeach
                                  
                                </div><!--.row-->
                                </div><!--.item-->
                                {{ $uber->links() }}
                            </div><!--.carousel-inner-->
                              
                            </div><!--.Carousel-->
                             
                    </div>
                </div>
                
            </div><!--.container-->

    @foreach($uber as $u)
  <!-- Modal -->
  <div id="myModal{{$u->id}}" class="modal fade" role="dialog">
    <div class="modal-dialog">
  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{$u->firstname.' '.$u->lastname}}</h4>
        </div>
        <div class="modal-body">
                
                <div class="row">
                        <div class="col-md-6">
                                <a href="#" class="thumbnail"><img src="{{ asset('avatar/') }}{{'/'.$u->profilepicture}}" alt="Image" style="max-width:100%;"></a>
                        </div>
                        <div class="col-md-6">
                        <div align = "center" style="background:#a1a1a1;color:white;border-radius:7px;"><p style="font-weight:bold;font-size:20px;font-family:Comic Sans Ms;padding:7px;">{{$u->firstname.' '.$u->lastname}}</p></div>
                        <div align = "center" style="background:#a1a1a1;color:white;border-radius:7px;"><p style="font-weight:bold;font-size:20px;font-family:Comic Sans Ms;padding:7px;">{{$u->age}}</p></div>
                        <div align = "center" style="background:#a1a1a1;color:white;border-radius:7px;"><p style="font-weight:bold;font-size:20px;font-family:Comic Sans Ms;padding:7px;">{{$u->residence.', '.$u->location}}</p></div>
                        <div align = "center" style="background:#a1a1a1;color:white;border-radius:7px;"><p style="font-weight:bold;font-size:20px;font-family:Comic Sans Ms;padding:7px;">{{$u->driver_type.' DRIVER'}}</p></div>
                        </div>
                      </div>
        </div>
        <div class="modal-footer">
            <form class="form-horizontal" action="{{ url('marketplace/addcart',['id' => $u->id]) }}" method="post" enctype="multipart/form-data" role="form">
                    {{ csrf_field() }}
                <input type ="hidden" name="cart" value="{{$u->id}}" />
                <input type="submit" class="btn btn-primary" value="Add to Cart">
                
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </form>
        </div>
      </div>
  
    </div>
  </div>
  @endforeach
                                  
@endsection