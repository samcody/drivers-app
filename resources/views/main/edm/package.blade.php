@extends('layouts.edm-packages')
@section('title','Pick Your Package')

@section('content')
<style>
  .btn-outline {
    background-color: transparent;
    color: inherit;
    transition: all .5s;
}

.btn-primary.btn-outline {
    color: #428bca;
}

.btn-success.btn-outline {
    color: #5cb85c;
}

.btn-info.btn-outline {
    color: #5bc0de;
}

.btn-warning.btn-outline {
    color: #f0ad4e;
}

.btn-danger.btn-outline {
    color: #d9534f;
}

.btn-primary.btn-outline:hover,
.btn-success.btn-outline:hover,
.btn-info.btn-outline:hover,
.btn-warning.btn-outline:hover,
.btn-danger.btn-outline:hover {
    color: #fff;
}
</style>
  <header class="cd-main-header">
    <h1 style="color:black;">Select a Package</h1>
  </header>

  <ul class="cd-pricing">
    <li>
        <form action="{{ url('edmpay') }}" method="post" accept-charset="UTF-8">
            {{ csrf_field() }}
      <header class="cd-pricing-header">
        <h2>Hourly Sub</h2>

        <div class="cd-price">
            @php
            $price = 3000;
            @endphp
          <span>&#8358;{{number_format($price)}}</span>
          <span>Hour</span>
        </div>
      </header> <!-- .cd-pricing-header -->

      <div class="cd-pricing-features">
        <ul>
          <li class="available"><em>Access to select 1 Driver</em></li>
           
        </ul>
      </div> <!-- .cd-pricing-features -->
      <input type="hidden" name="email" value="{{ Auth::User()->email }}">
   <input type="hidden" name="metadata" value="{{ json_encode(['hire_type' => 'hour','user_id' => Auth::User()->id,'booking_id' => Auth::User()->id]) }}">
   <input type="hidden" name="amount" value="{{ $price }}00">
   <input type="hidden" name="quantity" value="">
   <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}">
   <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}">

      <footer class="cd-pricing-footer">
        <input class="btn btn-primary btn-outline" type="submit" value="Select"  />
      </footer> <!-- .cd-pricing-footer -->
        </form>
    </li>
   
    
    <li>
        <form action="{{ url('edmpay') }}" method="post" accept-charset="UTF-8">
            {{ csrf_field() }}
      <header class="cd-pricing-header">
        <h2>A day Sub</h2>

        <div class="cd-price">
            @php
            $price = 10000;
            @endphp
          <span>&#8358;{{number_format($price)}}</span>
          <span>Day</span>
        </div>
      </header> <!-- .cd-pricing-header -->

      <div class="cd-pricing-features">
        <ul>
          <li class="available"><em>Access to select 3 Drivers</em></li>
          
        </ul>
      </div> <!-- .cd-pricing-features -->
      <input type="hidden" name="email" value="{{ Auth::User()->email }}">
      <input type="hidden" name="metadata" value="{{ json_encode(['hire_type' => 'day','user_id' => Auth::User()->id,'booking_id' => Auth::User()->id]) }}">
      <input type="hidden" name="amount" value="{{ $price }}00">
      <input type="hidden" name="quantity" value="">
      <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}">
      <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}">
   
         <footer class="cd-pricing-footer">
           <input class="btn btn-primary btn-outline" type="submit" value="Select"  />
         </footer> <!-- .cd-pricing-footer -->
           </form>
    </li>

    <li>
        <form action="{{ url('edmpay') }}" method="post" accept-charset="UTF-8">
            {{ csrf_field() }}
      <header class="cd-pricing-header">
        <h2>A Week Sub</h2>

        <div class="cd-price">
            @php
            $price = 30000;
            @endphp
          <span>&#8358;{{number_format($price)}}</span>
          <span>Week</span>
        </div>
      </header> <!-- .cd-pricing-header -->

      <div class="cd-pricing-features">
        <ul>
          <li class="available"><em>Access to select 10 Drivers</em></li>
          
        </ul>
      </div> <!-- .cd-pricing-features -->
      <input type="hidden" name="email" value="{{ Auth::User()->email }}">
      <input type="hidden" name="metadata" value="{{ json_encode(['hire_type' => 'week','user_id' => Auth::User()->id,'booking_id' => Auth::User()->id]) }}">
      <input type="hidden" name="amount" value="{{ $price }}00">
      <input type="hidden" name="quantity" value="">
      <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}">
      <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}">
   
         <footer class="cd-pricing-footer">
           <input class="btn btn-primary btn-outline" type="submit" value="Select"  />
         </footer> <!-- .cd-pricing-footer -->
           </form>
        </li>
        
        
    </ul> <!-- .cd-pricing -->
    <br /><br />
    <ul  class="cd-pricing">
        <li style="margin-top:50px;">
            <form action="{{ url('edmpay') }}" method="post" accept-charset="UTF-8">
                {{ csrf_field() }}
      <header  class="cd-pricing-header">
        <h2>A Month Sub</h2>

        <div class="cd-price">
            @php
            $price = 50000;
            @endphp
          <span>&#8358;{{number_format($price)}}</span>
          <span>Month</span>
        </div>
      </header> <!-- .cd-pricing-header -->

      <div class="cd-pricing-features">
        <ul>
          <li class="available"><em>Access to select 20 Drivers</em></li>
          
        </ul>
      </div> <!-- .cd-pricing-features -->
      <input type="hidden" name="email" value="{{ Auth::User()->email }}">
      <input type="hidden" name="metadata" value="{{ json_encode(['hire_type' => 'month','user_id' => Auth::User()->id,'booking_id' => Auth::User()->id]) }}">
      <input type="hidden" name="amount" value="{{ $price }}00">
      <input type="hidden" name="quantity" value="">
      <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}">
      <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}">
   
         <footer class="cd-pricing-footer">
           <input class="btn btn-primary btn-outline" type="submit" value="Select"  />
         </footer> <!-- .cd-pricing-footer -->
           </form>
        </li>
        
        <li style="margin-top:50px;">
          <form action="{{ url('edmpay') }}" method="post" accept-charset="UTF-8">
      {{ csrf_field() }}
      <header class="cd-pricing-header">
        <h2>A Year Sub</h2>
        

        <div class="cd-price">
            @php
            $price = 300000;
            @endphp
          <span>&#8358;{{number_format($price)}}</span>
          <span>Year</span>
          
            
        </div>
      </header> <!-- .cd-pricing-header -->

      <div class="cd-pricing-features">
        <ul>
          <li class="available"><em>Access to Unlimited Number of Drivers</em></li>
          
        </ul>
      </div> <!-- .cd-pricing-features -->
      <input type="hidden" name="email" value="{{ Auth::User()->email }}">
      <input type="hidden" name="metadata" value="{{ json_encode(['hire_type' => 'year','user_id' => Auth::User()->id,'booking_id' => Auth::User()->id]) }}">
      <input type="hidden" name="amount" value="{{ $price }}00">
      <input type="hidden" name="quantity" value="">
      <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}">
      <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}">
   
         <footer class="cd-pricing-footer">
           <input class="btn btn-primary btn-outline" type="submit" value="Select"  />
         </footer> <!-- .cd-pricing-footer -->
           </form>
    </li>
    </ul>

  
  
  <div class="cd-overlay"></div> <!-- shadow layer -->

</body>
</html>

@endsection