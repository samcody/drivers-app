@extends('layouts.master')

@section('title','ENTERPRISE DRIVERS MARKET')

@section('content')
    <section class="r">
            <div class="container">
                    <div class="row">
                        <div class="boxed boxed--lg boxed--border">
                        <div class="col-sm-4">
                                <img alt="Image" src="{{ asset('assets/img/bgg-2.png') }}" />
                                <br>
                        </div>
                        <div class="col-sm-8 col-md-6 color--primary-1 text-center">
                                @include('partial.alert')
                            <h2>ENTERPRISE DRIVERS MARKET</h2>
                            <h5>
                                
Edm is a marketplace arm of driversng.com. The system works for card users
Only who are enterprises or corporate individuals interested in hiring the
Services of experienced, vetted and verified professional drivers for their
Transport needs. The users and drivers on this platform possesses the power to
Negotiate and come to an agreement/mou. Under this platform the company is
Not responsible for managerial (salary payment, contract agreement, driver's
Welfare and replacement ) roles , the user is solely responsible to handle all of
This by continual subscription. However, user can place request for driversng
Uniform for drivers,, documentation of verification for guarantors and
Referees which comes at additional cost to be charged to card. User can
Subscribe to hourly, daily, monthly or yearly plans. A user can also get roll
Overs if subscription plans are not exhausted.
                            </h5>
                        <a class="btn btn--primary" href="{{ url('marketplace/package') }}">
                            <span class="btn__text">
                                Checkout Marketplace
                            </span>
                        </a>
                        <br><br>
                        <p>
                                Access : The Best of Verified Freelanced Drivers, Driving Tutors,Driving Schools, Cabs, Taxis and Trucks. Marketplace for Driving and Transport Solutions at Your Fingertips
                        </p>
                            
                        </div>
                    </div>
                </div>
                </div>
                </div>
            </div>
        </div>
    </section>
    
@endsection