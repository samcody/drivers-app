<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ config('app.name') }} | Download Mobile App</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="DriversNG | No. 1 platform to hire professional drivers">
    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.png') }}">
    <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('assets/css/stack-interface.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('assets/css/socicon.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('assets/css/lightbox.min.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('assets/css/flickity.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('assets/css/iconsmind.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('assets/css/jquery.steps.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('assets/css/theme.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('assets/css/font-rubiklato.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700%7CRubik:300,400,500" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body class="">
<a id="start"></a>
<div class="nav-container ">
    <nav class="bar bar-4 bar--transparent bar--absolute" data-fixed-at="200">
        <div class="container">
            <div class="row">
                <div class="col-md-1 col-md-offset-0 col-sm-2 col-sm-offset-0 col-xs-4 col-xs-offset-4">
                    <div class="bar__module">
                        <a href="{{ url('/') }}">
                            <img class="logo logo-dark" style="max-height: 5.4em; margin-top: -22px;" alt="logo" src="{{ asset('assets/img/logo.png') }}" />
                            <img class="logo logo-light pull-right" style="max-height: 5.4em; margin-top: -22px;" alt="logo" src="{{ asset('assets/img/logo.png') }}" />
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-0 col-sm-5 col-sm-offset-0 col-xs-8 col-xs-offset-2">
                    <div class="bar__module">
                        <a class="btn btn--sm type--uppercase" href="{{ url('login') }}">
                                    <span class="btn__text">
                                        Login
                                    </span>
                        </a>
                        <a class="btn btn--sm btn--primary type--uppercase" href="{{ url('package') }}">
                            <span class="btn__text">
                                Get Started
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>
<div class="main-container">
    <section class="imageblock switchable feature-large height-100 ">
        <div class="imageblock__content col-md-6 col-sm-4 pos-right" data-overlay="1">
            <div class="background-image-holder">
                <img alt="image" src="{{ asset('assets/img/bg-1.jpg') }}" />
            </div>
            {{--<div class="modal-instance">--}}
                {{--<div class="video-play-icon modal-trigger"></div>--}}
                {{--<div class="modal-container">--}}
                    {{--<div class="modal-content bg-dark" data-width="60%" data-height="60%">--}}
                        {{--<iframe data-src="https://www.youtube.com/embed/6p45ooZOOPo?autoplay=1" allowfullscreen="allowfullscreen"></iframe>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
        <div class="container pos-vertical-center">
            <div class="row">
                <div class="col-md-5 col-sm-7">
                    <h1>Download Our <br> Mobile Application</h1>
                    <p class="lead">
                        We have made it a lot easier for us to serve you.
                    </p>
                    <div class="bg--secondary boxed boxed--border">
                        <form action="http://mrare.us8.list-manage.com/subscribe/post?u=77142ece814d3cff52058a51f&amp;id=f300c9cce8" data-success="Thanks for signing up.  Please check your inbox for a confirmation email." data-error="Please provide your name and email address and agree to the terms.">

                            <a href="https://play.google.com/store/apps/details?id=com.driversng" class="btn btn--primary btn-block type--uppercase"><span class="btn__text">Download Now</span></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!--<div class="loader"></div>-->
<a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
    <i class="stack-interface stack-up-open-big"></i>
</a>

<script src="{{ asset('assets/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('assets/js/flickity.min.js') }}"></script>
<script src="{{ asset('assets/js/easypiechart.min.js') }}"></script>
<script src="{{ asset('assets/js/parallax.js') }}"></script>
<script src="{{ asset('assets/js/typed.min.js') }}"></script>
<script src="{{ asset('assets/js/datepicker.js') }}"></script>
<script src="{{ asset('assets/js/isotope.min.js') }}"></script>
<script src="{{ asset('assets/js/ytplayer.min.js') }}"></script>
<script src="{{ asset('assets/js/lightbox.min.js') }}"></script>
<script src="{{ asset('assets/js/granim.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.steps.min.js') }}"></script>
<script src="{{ asset('assets/js/countdown.min.js') }}"></script>
<script src="{{ asset('assets/js/twitterfetcher.min.js') }}"></script>
<script src="{{ asset('assets/js/spectragram.min.js') }}"></script>
<script src="{{ asset('assets/js/smooth-scroll.min.js') }}"></script>
<script src="{{ asset('assets/js/scripts.js') }}"></script>
</body>

</html>