@extends('layouts.master')

@section('title','Learning Driving Tool')

@section('banner')
    <section class="">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-8">
                    <h1>Learning Driving tool</h1>
                    <p class="lead">
                        We want to ensure that more people can become drivers, whether for the present or for future purposes.
                        It would be a delight if everyone becomes a well trained professional driver, who understands the very basic and principles of living. Hence, We have built a chatbot that you can engage with that helps you through your learning hour to drive. Youcan also the link with friends on twitter, facebook, instagram etc so they could join you in learning
                    </p>
                
                </div>
            </div>
        </div>
    </section>
@endsection
