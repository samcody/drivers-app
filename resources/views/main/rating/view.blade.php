@extends('layouts.master')

@section('title','Rate Driver')

@section('banner')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-8">
                    <h1>Rate {{ ucfirst($driver->firstname) }} {{ ucfirst($driver->lastname) }}</h1>
                    {{--<p class="lead">--}}
                    {{--Choose Full-Time Package--}}
                    {{--</p>--}}
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section class=" bg--secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-12 ">
                    <div class="row">
                        <div class="boxed boxed--border">
                            <form method="post" action="{{ url('driver/rate',['id'=>$driver->id]) }}" class="text-left">
                                {{ csrf_field() }}
                                <input type="hidden" name="driver_id" value="{{ $driver->id }}">
                                <div class="col-sm-4">
                                    <span>NEATNESS</span>
                                    <select name="neatness">
                                        <option value="">Choose One</option>
                                        <option value="Excellent">Excellent</option>
                                        <option value="Good">Good</option>
                                        <option value="Fair">Fair</option>
                                        <option value="Poor">Poor</option>
                                    </select>
                                </div>

                                <div class="col-sm-4">
                                    <span>DRESSING</span>
                                    <select name="dressing">
                                        <option value="">Choose One</option>
                                        <option value="Excellent">Excellent</option>
                                        <option value="Good">Good</option>
                                        <option value="Fair">Fair</option>
                                        <option value="Poor">Poor</option>
                                    </select>
                                </div>

                                <div class="col-sm-4">
                                    <span>SERVICE QUALITY</span>
                                    <select name="service_quality">
                                        <option value="">Choose One</option>
                                        <option value="Excellent">Excellent</option>
                                        <option value="Good">Good</option>
                                        <option value="Fair">Fair</option>
                                        <option value="Poor">Poor</option>
                                    </select>
                                </div>

                                <div class="col-sm-4">
                                    <span>PUNCTUALITY</span>
                                    <select name="punctuality">
                                        <option value="">Choose One</option>
                                        <option value="Excellent">Excellent</option>
                                        <option value="Good">Good</option>
                                        <option value="Fair">Fair</option>
                                        <option value="Poor">Poor</option>
                                    </select>
                                </div>

                                <div class="col-sm-4">
                                    <span>INTEGRITY</span>
                                    <select name="integrity">
                                        <option value="">Choose One</option>
                                        <option value="Excellent">Excellent</option>
                                        <option value="Good">Good</option>
                                        <option value="Fair">Fair</option>
                                        <option value="Poor">Poor</option>
                                    </select>
                                </div>
                                <div class="col-sm-12">
                                    <span>COMMENT</span>
                                    <textarea name="comment" cols="7"></textarea>
                                </div>

                                <div class="col-sm-12 boxed">
                                    <button type="submit" class="btn btn--primary type--uppercase">Rate Driver</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection