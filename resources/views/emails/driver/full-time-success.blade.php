<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <style type="text/css" rel="stylesheet" media="all">
        /* Media Queries */
        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>
</head>

<?php

$style = [
    /* Layout ------------------------------ */

    'body' => 'margin: 0; padding: 0; width: 100%; background-color: #F2F4F6;',
    'email-wrapper' => 'width: 100%; margin: 0; padding: 0; background-color: #F2F4F6;',

    /* Masthead ----------------------- */

    'email-masthead' => 'padding: 25px 0; text-align: center;',
    'email-masthead_name' => 'font-size: 16px; font-weight: bold; color: #2F3133; text-decoration: none; text-shadow: 0 1px 0 white;',

    'email-body' => 'width: 100%; margin: 0; padding: 0; border-top: 1px solid #EDEFF2; border-bottom: 1px solid #EDEFF2; background-color: #FFF;',
    'email-body_inner' => 'width: auto; max-width: 570px; margin: 0 auto; padding: 0;',
    'email-body_cell' => 'padding: 35px;',

    'email-footer' => 'width: auto; max-width: 570px; margin: 0 auto; padding: 0; text-align: center;',
    'email-footer_cell' => 'color: #AEAEAE; padding: 35px; text-align: center;',

    /* Body ------------------------------ */

    'body_action' => 'width: 100%; margin: 30px auto; padding: 0; text-align: center;',
    'body_sub' => 'margin-top: 25px; padding-top: 25px; border-top: 1px solid #EDEFF2;',

    /* Type ------------------------------ */

    'anchor' => 'color: #3869D4;',
    'header-1' => 'margin-top: 0; color: #2F3133; font-size: 19px; font-weight: bold; text-align: left;',
    'paragraph' => 'margin-top: 0; color: #74787E; font-size: 16px; line-height: 1.5em;',
    'paragraph-sub' => 'margin-top: 0; color: #74787E; font-size: 12px; line-height: 1.5em;',
    'paragraph-center' => 'text-align: center;',

    /* Buttons ------------------------------ */

    'button' => 'display: block; display: inline-block; width: 200px; min-height: 20px; padding: 10px;
                 background-color: #3869D4; border-radius: 3px; color: #ffffff; font-size: 15px; line-height: 25px;
                 text-align: center; text-decoration: none; -webkit-text-size-adjust: none;',

    'button--green' => 'background-color: #22BC66;',
    'button--red' => 'background-color: #dc4d2f;',
    'button--blue' => 'background-color: #3869D4;',
];
?>

<?php $fontFamily = 'font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;'; ?>

<body style="{{ $style['body'] }}">
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td style="{{ $style['email-wrapper'] }}" align="center">
            <table width="100%" cellpadding="0" cellspacing="0">
                <!-- Logo -->
                <tr>
                    <td style="{{ $style['email-masthead'] }}">
                        <a style="{{ $fontFamily }} {{ $style['email-masthead_name'] }}" href="{{ url('/') }}" target="_blank">
                            {{ config('app.name') }}
                        </a>
                    </td>
                </tr>

                <!-- Email Body -->
                <tr>
                    <td style="{{ $style['email-body'] }}" width="100%">
                        <table style="{{ $style['email-body_inner'] }}" align="center" width="570" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="{{ $fontFamily }} {{ $style['email-body_cell'] }}">
                                    <!-- Greeting -->
                                    <h1 style="{{ $style['header-1'] }}">
                                        Hello {{ Auth::User()->full_name }},
                                    </h1>

                                    <!-- Intro -->

                                    <p style="{{ $style['paragraph'] }}">
                                            With regards to your application, you have been invited for an appointment/training at DriversNG Lagos Office; 22, Ayodele street, off ikorodu road, Fadeyi, Yaba Lagos.
                                            <br />
                                            Appointment Time: 10am
                                            <br />
                                            Appointment Date:  Monday, 
                                            @php 
                                            echo date("F j, Y",strtotime('next Monday'));
                                            @endphp

                                    </p>
                                    <h3 style="{{ $style['paragraph'] }}">KINDLY FOLLOW THE INSTRUCTIONS BELOW:</h3>
                                            <li style="{{ $style['paragraph'] }}">    Ensure you must have read what it takes to be a professional driver when applying.
                
                                            <li style="{{ $style['paragraph'] }}">    Ensure you adhere to instructions given on each of the attached forms you are to download.
                                                
                                            <li style="{{ $style['paragraph'] }}">    Ensure you must have downloaded and read the Guarantor’s Requirements before filling.
                                                
                                            <li style="{{ $style['paragraph'] }}">    Ensure you come along with a copy of your CV, a passport photograph and a valid driver’s license.
                                                
                                            <li style="{{ $style['paragraph'] }}">    Ensure you are corporately dressed.
                                                
                                            <li style="{{ $style['paragraph'] }}">    Ensure you come along with a copy of your Nepa Bill and Valid ID Card.
                                                
                                            <li style="{{ $style['paragraph'] }}">    Please note that you are to come along with all necessary documents to avoid disqualification.
                                                

                                            <h3 style="{{ $style['paragraph'] }}">DOCUMENTS TO BE DOWNLOADED AND COME ALONG WITH.</h3>

                                                    <li style="{{ $style['paragraph'] }}"> Two (2) Filled Guarantor’s Form attached with Guarantor’s Valid ID card and Nepa Bill and One (1) Filled Security Form.
                
                                                    <li style="{{ $style['paragraph'] }}"> Two (2) Filled Confidential Guaranteed and Referee Cover Letter Form.
                                                     
                                                    <li style="{{ $style['paragraph'] }}"> One (1) Filled Service Contract Form.


                                    <table style="{{ $style['body_action'] }}" align="center" width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="center">
                                                <?php
                                                $actionColor = 'button--blue';
                                                ?>

                                                <a href="{{ url('drive/home') }}"
                                                   style="{{ $fontFamily }} {{ $style['button'] }} {{ $style[$actionColor] }}"
                                                   class="button"
                                                   target="_blank">
                                                   Go to Dashboard
                                                </a>
                                                <a href="{{ asset('driver-pdf/FULL-TIME-DRIVER.pdf') }}"
                                                   download="{{ 'FULL-TIME-DRIVER.pdf' }}"
                                                   style="{{ $fontFamily }} {{ $style['button'] }} {{ $style[$actionColor] }}"
                                                   class="button"
                                                   target="_blank">
                                                    Download Guarantor’s Form
                                                </a>
                                            </td>
                                        </tr>
                                    </table>

                                    <p style="{{ $style['paragraph'] }}">
                                        Thank you
                                    </p>


                                    <!-- Salutation -->
                                    <p style="{{ $style['paragraph'] }}">
                                        Cheers,<br>{{ config('app.name') }}
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <!-- Footer -->
                <tr>
                    <td>
                        <table style="{{ $style['email-footer'] }}" align="center" width="570" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="{{ $fontFamily }} {{ $style['email-footer_cell'] }}">
                                    <p style="{{ $style['paragraph-sub'] }}">
                                        &copy; {{ date('Y') }}
                                        <a style="{{ $style['anchor'] }}" href="{{ url('/') }}" target="_blank">{{ config('app.name') }}</a>.
                                        All rights reserved.
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>