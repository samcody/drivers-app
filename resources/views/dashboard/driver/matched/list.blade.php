@extends('layouts.dashboard')

@section('title','Matched Drivers')

@section('content')
    <section class="bg--secondary space--sm">
        <div class="container">
            <div class="row">
                @include('layouts.dashboard.side-nav')
                <div class="col-md-8">
                    <div class="boxed boxed--lg boxed--border">
                        <div id="account-billing" class="account-tab">
                            <h4>List of Drivers For Booking</h4>
                            @include('partial.alert')
                            @if(count($drivers) < 1)
                                <div class="boxed boxed--border bg--secondary text-center">
                                    <h5>Hi {{ Auth::User()->full_name }}, No Driver has been matched to you</h5>
                                    <hr>
                                    <a href="{{ url('package') }}" class="btn">Hire Driver</a>
                                </div>
                            @else
                            @foreach($drivers as $driver)
                                <div class="boxed boxed--border bg--secondary">
                                    <h5>{{ $driver->firstname }} {{ $driver->lastname }} <span class="pull-right">{{ $driver->package }}</span></h5>
                                    <hr>
                                    <div class="row">
                                    </div>
                                    <a class="btn bg--primary-2 btn-block" href="{{ url('dashboard/matched/driver',['id' => $driver->id]) }}"><span>View Driver Profile</span></a>
                                </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
