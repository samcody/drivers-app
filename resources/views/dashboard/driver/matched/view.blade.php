@extends('layouts.master')

@section('title','View Driver')

@section('banner')

@endsection

@section('content')
    <section class="bg--secondary space--sm">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="boxed boxed--lg boxed--border">
                        <div class="text-block text-center">
                            <img alt="avatar" src="{{ asset('avatar/'.$driver->profilepicture) }}" class="image--md" />
                            <span class="h5">{{ ucfirst($driver->firstname) }} {{ ucfirst($driver->lastname) }}</span>
                            <span>{{ $driver->location }}, Nigeria</span>
                            <span class="label">Driver</span>
                        </div>
                        <div class="text-block clearfix text-center">
                            <a class="btn btn--primary type--uppercase" href="{{ url('short-term/book',['id' => $driver->id]) }}">
                                <span class="btn__text">
                                    Hire Driver
                                </span>
                            </a>
                            <a class="btn btn--success type--uppercase" href="{{ url('driver/rate',['id' => $driver->id]) }}">
                                <span class="btn__text">
                                    Rate Driver
                                </span>
                            </a>
                        </div>

                    </div>
                    <div class="boxed boxed--border">
                        <ul class="row row--list clearfix text-center">
                            <li class="col-sm-3 col-xs-6">
                                <span class="h6 type--uppercase type--fade">Age</span>
                                <span class="h3">{{ $driver->age }}</span>
                            </li>
                            <li class="col-sm-3 col-xs-6">
                                <span class="h6 type--uppercase type--fade">Marital Status</span>
                                <span class="h3">{{ $driver->marital_status }}</span>
                            </li>
                            <li class="col-sm-3 col-xs-6">
                                <span class="h6 type--uppercase type--fade">State</span>
                                <span class="h3">{{ $driver->location }}</span>
                            </li>
                            <li class="col-sm-3 col-xs-6">
                                <span class="h6 type--uppercase type--fade">Religion</span>
                                <span class="h3">{{ $driver->religion }}</span>
                            </li>
                        </ul>
                    </div>

                    <div class="boxed boxed--border">
                        <h4>Ratings</h4>
                        @if(count($ratings) < 1)
                            <div class="text-center">
                                <h5>No Rating For This Driver Yet!</h5>
                            </div>
                        @else
                            <ul>
                                @foreach($ratings as $rating)
                                    <li class="clearfix">
                                        <div class="row">
                                            <div class="col-md-2 col-xs-3 text-center">
                                                <div class="icon-circle">
                                                    <i class="icon icon--lg material-icons">comment</i>
                                                </div>
                                            </div>
                                            <div class="col-md-8 col-xs-7">
                                                <span class="type--fine-print">{{ date_format($rating->created_at,'d-M-Y') }}</span>
                                                <a href="#" class="block color--primary">
                                                    <span class="color--dark">Neatness:</span> {{ $rating->neatness }},
                                                    <span class="color--dark">Dressing:</span> {{ $rating->dressing }}
                                                    <span class="color--dark">Service Quality: </span> {{ $rating->service_quality }}
                                                    <span class="color--dark">Punctuality: </span> {{ $rating->punctuality }}
                                                    <span class="color--dark">Integrity:</span> {{ $rating->integrity }}</a>
                                                <p>
                                                    {{ $rating->comment }}
                                                </p>
                                            </div>
                                        </div>
                                        <hr>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection