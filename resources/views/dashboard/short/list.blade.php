@extends('layouts.dashboard')

@section('title','Short-Time Bookings')

@section('content')
    <section class="bg--secondary space--sm">
        <div class="container">
            <div class="row">
                @include('layouts.dashboard.side-nav')
                <div class="col-md-8">
                    <div class="boxed boxed--lg boxed--border">
                        <div id="account-billing" class="account-tab">
                            <h4>Short-Term Driver Bookings</h4>
                            @include('partial.alert')
                            @if(count($bookings) < 1)
                                <div class="boxed boxed--border bg--secondary text-center">
                                    <h5>Hi {{ Auth::User()->full_name }}, No Booking has been made by you</h5>
                                    <hr>
                                    <a href="{{ url('package') }}" class="btn">Hire Driver</a>
                                </div>
                            @else
                            @foreach($bookings as $booking)
                                <div class="boxed boxed--border bg--secondary">
                                    <h5>Short-Term Booking Made On {{ date_format($booking->created_at,'D-M-Y') }} <span class="pull-right">&#8358;{{ $booking->package }}</span></h5>
                                    <hr>
                                    <div class="row">
                                        <ul>
                                            <li>
                                                <div class="col-sm-6">
                                                    <p>
                                                    </p>
                                                </div>
                                                <div class="col-sm-3 text-right text-left-xs">
                                                </div>
                                                <div class="col-sm-3 text-right text-left-xs">
                                                    @if($booking->has_paid == true)
                                                        <button type="submit" class="btn bg--primary"><span>Paid</span></button>
                                                    @else
                                                        <button type="submit" class="btn bg--warning"><span>Not Paid</span></button>
                                                    @endif
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <hr>
                                    @if($booking->is_matched == true && $booking->has_paid == true)
                                        <a href="{{ url('dashboard/matched/driver',['id' => $booking->driver_id]) }}" class="btn bg--primary btn-block"><span>View Matched Driver</span></a><br />
                                        <a href=" {{ url('full-time/profile',['id' => $booking->driver_id]) }}" class="btn bg--primary btn-block"><span>View Full Profile</span></a>
                                    @else
                                        <button type="submit" class="btn bg--warning"><span>Matching In Progress</span></button>
                                    @endif
                                    <br><br>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a class="btn bg--primary-1 " href="{{ asset('pdf/short-term-form.pdf') }}" download="short-term-form.pdf"><span>Download Agreement Form</span></a>
                                        </div>
                                        <div class="col-md-6">
                                            <a class="btn bg--primary-1 " href="{{ asset('files/'.$booking->guarantor_form) }}" download="{{ $booking->guarantor_form }}"><span>Download Guarantor's Form</span></a>
                                        </div>
                                    </div>
                                    <br>
                                    <a class="btn bg--primary-1 btn-block" href="{{ asset('pdf/short-term-condition.pdf') }}" download="short-term-condition.pdf"><span>Download Terms & Condition</span></a>
                                    <br>
                                    <a class="btn bg--primary-2 btn-block" href="{{ url('dashboard/short-time/upload',['id' => $booking->id]) }}"><span>Upload Signed Document</span></a>
                                </div>
                            @endforeach
                            @endif
                        </div>
                        {{ $bookings->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
