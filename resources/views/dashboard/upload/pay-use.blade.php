@extends('layouts.dashboard')

@section('title','Upload Document')

@section('content')
    <section class="bg--secondary space--sm">
        <div class="container">
            <div class="row">
                @include('layouts.dashboard.side-nav')
                <div class="col-md-8">
                    <div class="boxed boxed--lg boxed--border">
                        <div id="account-profile" class="account-tab">
                            <h4>Upload Document</h4>
                            @include('partial.alert')
                            <form action="{{ url('dashboard/pay-use/upload',['id' => $booking->id]) }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Select Signed Service Agreement Document</label>
                                        <input type="file" name="sla_document" />
                                    </div>
                                    <div class="col-md-3 col-sm-4">
                                        <button type="submit" class="btn btn--primary type--uppercase">Upload</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
