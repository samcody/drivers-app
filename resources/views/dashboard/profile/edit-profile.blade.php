@extends('layouts.dashboard')

@section('title','Edit Profile')

@section('content')
    <section class="bg--secondary space--sm">
        <div class="container">
            <div class="row">
                @include('layouts.dashboard.side-nav')
                <div class="col-md-8">
                    <div class="boxed boxed--lg boxed--border">
                        <div id="account-profile" class="account-tab">
                            <h4>Profile</h4>
                            @include('partial.alert')
                            <form action="{{ url('edit-profile') }}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Full Name:</label>
                                        <input type="text" name="full_name" value="{{ Auth::User()->full_name }}" />
                                    </div>
                                    <div class="col-sm-12">
                                        <label>Email Address:</label>
                                        <input type="email" name="email" value="{{ Auth::User()->email }}" />
                                    </div>
                                    <div class="col-sm-12">
                                        <label>Mobile Number</label>
                                        <input type="tel" name="mobile_number" value="{{ Auth::User()->mobile_number }}" />
                                    </div>
                                    <div class="col-md-3 col-sm-4">
                                        <button type="submit" class="btn btn--primary type--uppercase">Save Profile</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
