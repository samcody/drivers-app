@extends('layouts.dashboard')

@section('title','Dashboard')

@section('content')
    <section class="bg--secondary space--sm">
        <div class="container">
            <div class="row">
                @include('layouts.dashboard.side-nav')
                <div class="col-md-8">
                    <div class="boxed boxed--lg boxed--border">
                        @include('partial.alert')
                            <div class="col-md-6 text-center">
                                <div class="boxed boxed--border  bg--secondary boxed--lg box-shadow">
                                    <h5>Hiring Driver</h5>
                                    <p>

                                    </p>
                                    <a class="btn btn--primary" href="{{ url('package') }}">
                                    <span class="btn__text">
                                        Hire Driver
                                    </span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6 text-center">
                                <div class="boxed boxed--border  bg--secondary boxed--lg box-shadow">
                                    <h5>My Account</h5>
                                    <p>

                                    </p>
                                    <a class="btn btn--primary" href="{{ url('edit-profile') }}">
                                        <span class="btn__text">
                                            Update Profile
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6 text-center">
                                <div class="boxed boxed--border  bg--secondary boxed--lg box-shadow">
                                    <h5>Full-Time Bookings</h5>
                                    <p>

                                    </p>
                                    <a class="btn btn--primary" href="{{ url('dashboard/full-time') }}">
                                        <span class="btn__text">
                                           View
                                        </span>
                                    </a>
                                </div>
                            </div>
                        <div class="col-md-6 text-center">
                            <div class="boxed boxed--border  bg--secondary boxed--lg box-shadow">
                                <h5>Short-Term Bookings</h5>
                                <p>

                                </p>
                                <a class="btn btn--primary" href="{{ url('dashboard/short-time') }}">
                                <span class="btn__text">
                                   View
                                </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
