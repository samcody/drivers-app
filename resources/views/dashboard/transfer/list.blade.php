@extends('layouts.dashboard')

@section('title','Transfer Bookings')

@section('content')
    <section class="bg--secondary space--sm">
        <div class="container">
            <div class="row">
                @include('layouts.dashboard.side-nav')
                <div class="col-md-8">
                    <div class="boxed boxed--lg boxed--border">
                        <div id="account-billing" class="account-tab">
                            <h4>Transfer Driver Bookings</h4>
                            @include('partial.alert')
                            @if(count($bookings) < 1)
                                <div class="boxed boxed--border bg--secondary text-center">
                                    <h5>Hi {{ Auth::User()->full_name }}, No Booking has been made by you</h5>
                                    <hr>
                                    <a href="{{ url('package') }}" class="btn">Hire Driver</a>
                                </div>
                            @else
                            @foreach($bookings as $booking)
                                <div class="boxed boxed--border bg--secondary">
                                    <h5>Transfer Booking Made On {{ date_format($booking->created_at,'D-M-Y') }} <span class="pull-right">{{ $booking->transfer_type }}</span></h5>
                                    <hr>
                                    <div class="row">
                                        <ul>
                                            <li>
                                                <div class="col-sm-6">
                                                    <p>
                                                        {{--<i class="material-icons">credit_card</i>--}}
                                                        @if($booking->amount != null)
                                                            <span>
                                                        <strong>&#8358;{{ number_format($booking->amount) }}</strong>
                                                        </span>
                                                        @endif
                                                    </p>
                                                </div>
                                                <div class="col-sm-3 text-right text-left-xs">
                                                </div>
                                                <div class="col-sm-3 text-right text-left-xs">
                                                    @if($booking->has_paid == true)
                                                        <button type="submit" class="btn bg--primary"><span>Paid</span></button>
                                                    @else
                                                        <button type="submit" class="btn bg--warning"><span>Not Paid</span></button>
                                                    @endif
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <hr>
                                    @if($booking->is_matched == true && $booking->number_of_driver == 1)
                                        <a href="{{ url('dashboard/matched/driver',['id' => $booking->driver_id]) }}" class="btn bg--primary btn-block"><span>View Matched Driver</span></a>
                                    @elseif($booking->number_of_driver > 1 && $booking->is_matched == true)
                                        <a href="{{ url('dashboard/matched/drivers',['id' => $booking->id]) }}" class="btn bg--primary btn-block"><span>View Matched Drivers</span></a>
                                    @else
                                        <button type="submit" class="btn bg--warning"><span>Matching In Progress</span></button>
                                        <br>
                                    @endif
                                    <br>
                                    <div class="row">
                                        {{--<div class="col-md-6">--}}
                                            {{--<a class="btn bg--primary-1 " href="{{ asset('pdf/full-time-form.pdf') }}" download="full-time-form.pdf"><span>Download Agreement Form</span></a>--}}
                                        {{--</div>--}}
                                        <div class="col-md-12">
                                            <a class="btn bg--primary-1 btn-block" href="{{ asset('files/'.$booking->transfer_document) }}" download="{{ $booking->transfer_document }}"><span>Download Invoice</span></a>
                                        </div>
                                    </div>
                                    <br>
                                    {{--<a class="btn bg--primary-2 btn-block" href="{{ url('dashboard/transfer/upload',['id' => $booking->id]) }}"><span>Upload Signed Document</span></a>--}}
                                </div>
                            @endforeach
                            @endif
                        </div>
                        {{ $bookings->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
