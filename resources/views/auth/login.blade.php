@extends('layouts.auth')

@section('title','Login To Continue')

@section('content')
        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <div class="col-md-12">
                    <input id="email" type="email" name="email" placeholder="Email Address" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="col-md-12">
                    <input id="password" type="password" name="password" placeholder="Password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    <button type="submit" class="btn btn--primary type--uppercase">
                        Login
                    </button>
                </div>
            </div>
        </form>
        <span class="type--fine-print block">
        Don't have an account?
        <a href="{{ url('register') }}"> Register</a>
        </span>
        <span class="type--fine-print block">Forgot your username or password?
        <a href="{{ route('password.request') }}"> Recover account</a>
        </span>
@endsection
