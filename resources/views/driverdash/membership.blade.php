@extends('layouts.driver-dashboard')

@section('title','Membership Status')


@section('content')
    <section class="">
        <div class="container">
                @foreach($drivers as $driver)
            @if($driver->card_exp == 0000-00-00)
            <div class="row">
                    <div class="col-sm-6 col-sm-offset-1 col-md-6 col-md-offset-1">
                        <h2>Dear {{ Auth::User()->full_name }} </h2>
                        <p class="lead">
                            You have not enrolled for the membership card yet.
                            <br />
                            Kindly visit the office to get one.
                        </p>
                        <p class="lead">
                            Take a look at membership card benefits
                        </p>
                        <li>Professional identification</li>
                            <li>Profile Management</li>
                            <li>Administration, verification and documentation</li>
                            <li>Access to Health Management</li>
                            <li>Access to Loan facilities</li>
                            <li>Access to all year round job matches privilege</li>
                            <li>Access to retraining programs</li>
                            <li>Membership for a year</li>
                    </div>
                </div>
            @else
            <div class="row">
                    <div class="col-sm-6 col-sm-offset-1 col-md-6 col-md-offset-2">
                        <h2>Dear {{ Auth::User()->full_name }} </h2>
                        <p class="lead">
                            You have successfully enrolled for the membership card.
                            <br />
                            Your Membership Card Expires {{$driver->card_exp}}
                        </p>
                        <p class="lead">
                            Take a look at membership card benefits
                        </p>
                        <li>Professional identification</li>
                            <li>Profile Management</li>
                            <li>Administration, verification and documentation</li>
                            <li>Access to Health Management</li>
                            <li>Access to Loan facilities</li>
                            <li>Access to all year round job matches privilege</li>
                            <li>Access to retraining programs</li>
                            <li>Membership for a year</li>
                    </div>
                </div>
            @endif
            @endforeach
        </div>
    </section>
@endsection