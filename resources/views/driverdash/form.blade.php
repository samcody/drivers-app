@extends('layouts.driver-dashboard')

@section('title','Update Your Profile')

@section('banner')
<br />
    <section class="text-center imagebg space--lg" data-overlay="3">
        <div class="background-image-holder">
            <img alt="background" src="{{ asset('assets/img/bg-3.jpeg') }}" />
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-10 color--primary-1">
                    <h1>Welcome! You are in the right place</h1>
                    <p class="lead">
                            DriversNg is the new HOME FOR DRIVERS. This is where drivers can freely come to get more trainings, reformation and transformation and then are  placed in a job within Their locations and convenience. On this platform we work With experienced drivers, who have quality skills and passion For driving. We welcome all kinds of vehicle drivers, from cars, buses, Trucks, vans, to cab hailing services and ride sharing platforms. We are Extremely open to all and there are no favorites. We are not AGENTS, we Are a platform that completely understands how driver’s lifestyle should Be spelt out. We are DRIVERSNG
                    </p>
                    <a class="btn btn--lg btn--primary type--uppercase" href="{{ url('drive/full-time') }}">
                            <span class="btn__text">
                                Become A Full-Time Driver 
                            </span>
                    </a>
                    <a class="btn btn--lg btn--primary type--uppercase" href="{{ url('drive/short-term') }}">
                            <span class="btn__text">
                                Become A Short-Term Driver
                            </span>
                    </a>
                    <br /><br />
                    <a class="btn btn--lg btn--primary type--uppercase" href="{{ url('drive/uber') }}">
                            <span class="btn__text">
                                Get a Car On Uber         
                            </span>
                    </a>
                    <a class="btn btn--lg btn--primary type--uppercase" href="{{ url('drive/vehicle') }}">
                            <span class="btn__text">
                                GET A VEHICLE ON DRIVERSNG
                            </span>
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection
