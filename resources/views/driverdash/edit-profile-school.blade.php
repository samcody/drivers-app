@extends('layouts.driver-dashboard')

@section('title','Update your profile')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">Create New Driver</h4>
                            <div class="row">
@include('partial.alert')
                                    <form class="form-horizontal" action="{{ url('drive/edit-profile') }}" method="post" enctype="multipart/form-data" role="form">
                                        {{ csrf_field() }}
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Profile Picture
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <label class="btn btn-primary">
                                                    <input type="file" name="profilepicture" accept="image/*" class="form-control" required>
                                                    <i class="fa fa-photo"></i> Add Photo
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Full name
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="text" name="firstname" value="{{ Auth::User()->full_name }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Full Name Of Driving School.
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="text" name="lastname" placeholder="Last name" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Year Of Establishment
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input name="age" type="date" min="01-01-1953" max="01-01-2000" class="form-control" required>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Phone number
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="tel" name="phonenumber" value="{{ Auth::User()->mobile_number }}" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Mobile number
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="tel" name="mobilenumber" value="{{ Auth::User()->mobile_number }}" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Email
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="email" name="email" placeholder="{{ Auth::User()->email }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Location of Training School
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="text" name="homeaddress" placeholder="13, Mokola Street" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Objective
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <textarea name="experience" rows="4" placeholder="Objective" class="form-control" required></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Location
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <select name="location" class="form-control" required>
                                                    <option value="">Choose A State</option>
                                                    @foreach($states as $state)
                                                        <option value="{{ $state }}">{{ $state }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                                <div class="col-md-10">
                                                    <button class="pull-right"><a class="btn btn-primary pull-right"><span style="color:antiquewhite" class="btn__text">
                                                            Submit
                                                        </span></a>
                                                    </button>
                                                </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection