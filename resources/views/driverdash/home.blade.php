@extends('layouts.driver-dashboard')

@section('title','Dashboard')

@section('content')
    <section class="bg--secondary space--sm">
        <div class="container">
            <div class="row">
                @include('layouts.dashboard.driver-side-nav')
                <div class="col-md-10 ">
                    <div class="">
                        @include('partial.alert')
                        <div class="alert-warning" style="margin: 10px auto;">
                    </div>
                            <div class="col-md-4 text-center">
                                <div class="boxed boxed--border  bg--secondary boxed--lg box-shadow">
                                    <h5>My Account</h5>
                                    <p>

                                    </p>
                                    <a class="btn btn--primary" href="{{ url('drive/list') }}">
                                        <span class="btn__text">
                                            Update Profile
                                        </span>
                                    </a>
                                </div>
                            </div>
                            
                            <div class="col-md-4 text-center">
                                <div class="boxed boxed--border  bg--secondary boxed--lg box-shadow">
                                    <h5>Current Status</h5>
                                    <p>

                                    </p>
                                    <a onclick="myStatus()" class="btn btn--primary">
                                    <span class="btn__text">
                                        View
                                    </span>
                                    </a>
                                </div>
                            </div>
                            @foreach($drivers as $driver)
                            @if($driver->is_matched == true)
                            <script>
                                function myStatus() {
                                    alert("You Are Presently Engaged On a DriversNG Job");
                                }
                                </script>
                                @else
                                <script>
                                    function myStatus() {
                                        alert("You Are Not Presently Engaged On a DriversNG Job");
                                    }
                                    </script>
                                    @endif
                                
                                @if($driver->driver_type == 'SHORT-TIME' && $driver->is_matched == true)
                                <div class="col-md-4 text-center">
                                    <div class="boxed boxed--border  bg--secondary boxed--lg box-shadow">
                                        <h5>Short Term Alerts</h5>
                                        <p>
    
                                        </p>
                                        <a onclick="shortStatus()" class="btn btn--primary">
                                        <span class="btn__text">
                                            View
                                        </span>
                                        </a>
                                    </div>
                                </div>
                                @endif

                            <script>
                                function shortStatus() {
                                    alert("You Have Been Booked For A Short Term Service");
                                }
                                </script>
                               
                                    @endforeach

                            
                            <div class="col-md-4 text-center">
                                <div class="boxed boxed--border  bg--secondary boxed--lg box-shadow">
                                    <h5>EDM Alerts</h5>
                                    <p>

                                    </p>
                                    <a class="btn btn--primary" href="{{ url('drive/home') }}">
                                    <span class="btn__text">
                                        View
                                    </span>
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-4 text-center">
                                <div class="boxed boxed--border  bg--secondary boxed--lg box-shadow">
                                    <h5>Training Simulation</h5>
                                    <p>

                                    </p>
                                    <a class="btn btn--primary" href="{{ url('drive/training') }}">
                                    <span class="btn__text">
                                        View
                                    </span>
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-4 text-center">
                                <div class="boxed boxed--border  bg--secondary boxed--lg box-shadow">
                                    <h5>Refer Driver</h5>
                                    <p>

                                    </p>
                                    <a class="btn btn--primary" href="{{ url('drive/refer') }}">
                                    <span class="btn__text">
                                        Refer
                                    </span>
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-4 text-center">
                                <div class="boxed boxed--border  bg--secondary boxed--lg box-shadow">
                                    <h5>Collect Cash</h5>
                                    <p>

                                    </p>
                                    <a class="btn btn--primary" href="{{ url('drive/refer') }}">
                                    <span class="btn__text">
                                        Get Cash
                                    </span>
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-4 text-center">
                                <div class="boxed boxed--border  bg--secondary boxed--lg box-shadow">
                                    <h5>Driver's Shop</h5>
                                    <p>

                                    </p>
                                    <a class="btn btn--primary" href="{{ url('drive/home') }}">
                                    <span class="btn__text">
                                        View
                                    </span>
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-4 text-center">
                                <div class="boxed boxed--border  bg--secondary boxed--lg box-shadow">
                                    <h5>Drivers Rating</h5>
                                    <p>

                                    </p>
                                    <a class="btn btn--primary" href="{{ url('drive/home') }}">
                                    <span class="btn__text">
                                        View
                                    </span>
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-4 text-center">
                                <div class="boxed boxed--border  bg--secondary boxed--lg box-shadow">
                                    <h5>Subscription &amp; Membership Status</h5>
                                    <p>

                                    </p>
                                    <a class="btn btn--primary" href="{{ url('drive/membership') }}">
                                    <span class="btn__text">
                                        View
                                    </span>
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-4 text-center">
                                <div class="boxed boxed--border  bg--secondary boxed--lg box-shadow">
                                    <h5>Navigation / Mapping</h5>
                                    <p>

                                    </p>
                                    
                                    <a class="btn btn--primary" href="{{ url('drive/navigator') }}">
                                    <span class="btn__text">
                                        View
                                    </span>
                                    </a>
                                </div>
                            </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
