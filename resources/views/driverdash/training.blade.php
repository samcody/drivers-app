@extends('layouts.driver-dashboard')

@section('title','Training & Simulation')

@section('content')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                        <div class="boxed boxed--lg boxed--border">
                                <div align ="center" class="container" style ="margin:20px auto">
                                        <a class="btn btn-primary" href="{{ url('package') }}">

                                                    Paid <br /> Contents
                                                
                                        </a>
                                    </div>

                                    <div align ="center" class="container"style ="margin:20px auto">
                                            <a class="btn btn-primary" href="{{ url('package') }}">

                                                        Drivers <br /> Simulation
                                                    
                                            </a>
                                        </div>

                                        <div align ="center" class="container" style ="margin:20px auto">
                                                <a class="btn btn-primary" href="{{ url('drive/smartbook') }}">
    
                                                            View <br /> Smart book
                                                        
                                                </a>
                                            </div>

                        </div>
                </div>

                <div class="col-md-6">
                        <div class="boxed boxed--lg boxed--border">
                                <iframe width="420" height="345" src="https://www.youtube.com/embed/Aa6C_OZ-kmU">
                                </iframe>
                        </div>
                </div>

                <div class="col-md-3">
                        <div class="boxed boxed--lg boxed--border">
                                <div align ="center" class="container" style ="margin:20px auto">
                                        <a class="btn btn-primary" href="{{ url('package') }}">

                                                    Driving <br /> Videos
                                                
                                        </a>
                                    </div>

                                <div align ="center" class="container" style ="margin:20px auto">
                                        <a class="btn btn-primary" href="{{ url('package') }}">

                                                    Courses<br />
                                                
                                        </a>
                                    </div>

                                        <div align ="center" class="container" style ="margin:20px auto">
                                                <a class="btn btn-primary" href="{{ url('package') }}">
    
                                                            Testing &amp; <br />  Certification
                                                        
                                                </a>
                                            </div>

                        </div>
                </div>

            </div>
        </div>
    </section>
@endsection