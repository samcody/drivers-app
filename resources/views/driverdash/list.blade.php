@extends('layouts.driver-dashboard')

@section('title','Full-Time Drivers')

@section('content')
    <div class="content-page">
        <div class="content">
            <br /><br />
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="table-rep-plugin">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    @if(count($drivers) < 1)
                                        <br><br>
                                        <div class="alert alert-info text-center">
                                            <p>You do not have an active profile</p>
                                            <a href="{{ url('drive/new') }}" class="btn btn-info">Become a Full-Time Driver</a>
                                            <a href="{{ url('drive/new-short') }}" class="btn btn-info">Become a Short-Term Driver</a>
                                            <a href="{{ url('drive/new-uber') }}" class="btn btn-info">Become an Uber Driver</a><br><br>
                                            {{-- <a href="{{ url('drive/new-vehicle') }}" class="btn btn-info">Register as a vehicle owner driver</a>
                                            <a href="{{ url('drive/new-tutor') }}" class="btn btn-info">Register as a Driving Tutor</a>
                                            <a href="{{ url('drive/new-school') }}" class="btn btn-info">Register as a Driving school</a><br><br>
                                            <a href="{{ url('drive/new-graduate') }}" class="btn btn-info">Register as a Graduate Driver</a> --}}
                                        </div>
                                    @else
                                        
                                                <div class="boxed boxed--lg boxed--border">
                                                    <div class="row">
                                                        <div class="col-sm-2">
                                                                
                                                        </div>
                                                        <div class="col-md-8">
                                                        @foreach($drivers as $driver)
                                                        <img style="display: block; margin-left: auto; margin-right: auto " alt="Image" width="30%" src="{{ asset('avatar/') }}{{'/'.$driver->profilepicture}}" />
                                                            <table id="tech-companies-1" class="table  table-striped">
                                                                <thead>
                                                                <tr>
                                                                    <th></th>
                                                                    <th></th>
                                                                    
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                
                                                                    <tr>

                                                                        <td><h4>{{'Full Name'}}</h4></td>
                                                                        <td><h4>{{ $driver->firstname.' '. $driver->lastname }}</h4></td>
                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                        
                                                                        <td><h4>{{'Email Address'}}</h4></td>
                                                                        <td><h4>{{ $driver->email }}</h4></td>
                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                        
                                                                        <td><h4>{{'Location'}}</h4></td>
                                                                        <td><h4>{{ $driver->location }}</h4></td>
                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                        
                                                                            <td><h4>{{'DNG Code'}}</h4></td>
                                                                            <td><h4>{{ $driver->serial_number }}</h4></td>
                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                        
                                                                            <td><h4>{{'Driver Type'}}</h4></td>
                                                                            <td><h4>{{ $driver->driver_type }}</h4></td>
                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                        
                                                                            <td><h4>{{'Phone Number'}}</h4></td>
                                                                            <td><h4>{{ $driver->phonenumber }}</h4></td>
                                                                        
                                                                    </tr>
                                                                    
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="col-sm-2">
                                                                
                                                        </div>
                                                        </div>
                                                    </div>
                                    @endif
                                </div>
                                {{ $drivers->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection