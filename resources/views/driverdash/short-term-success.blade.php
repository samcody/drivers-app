@extends('layouts.driver-dashboard')

@section('title','Dashboard')

@section('content')

<div class="col-md-4 text-center">
        <br /><br /><br /><br />

    <div class="boxed boxed--border  bg--secondary boxed--lg box-shadow">
        <h1>Profile Successfully Created</h1>
        <p>
            Download Guarantor's Form 
        </p>
        <a class="btn btn--primary" download="{{ 'SHORT-TERM-DRIVER.pdf' }}" href="{{ asset('driver-pdf/SHORT-TERM-DRIVER.pdf') }}">
            <span class="btn__text">
                Download 
            </span>
        </a>
    </div>
</div>
<div class="col-md-8">
        <br /><br /><br /><br />

    <div class="boxed boxed--border  bg--secondary boxed--lg box-shadow">
        <h1>DriversNG drivers application</h1>
        <p>Hello,</p>
        <p>
            With regards to your application, you have been invited for an appointment/training at DriversNG .
    </p>
    <p>
        Lagos Office; 22, Ayodele street, off ikorodu road, Fadeyi, Yaba Lagos.
    </p>
    <p>
        P.H Address: 1, Khana Street, Off Worji Street, D-line Port-Harcourt, Rivers State
    </p>
    <p>
        ABUJA Address: 29, Mambilla street, Maitama, Abuja
    </p>
        <p>        Appointment Time: 10am </p>
                
        <p>        Appointment Date:  Monday, 
                @php 
                echo date("F j, Y",strtotime('next Monday'));
                @endphp
            </p>
                
         <h3>       KINDLY FOLLOW THE INSTRUCTIONS BELOW:</h3>
                
            <li>    Ensure you must have read what it takes to be a professional driver when applying.
                
            <li>    Ensure you adhere to instructions given on each of the attached forms you are to download.
                
            <li>    Ensure you must have downloaded and read the Guarantor’s Requirements before filling.
                
            <li>    Ensure you come along with a copy of your CV, a passport photograph and a valid driver’s license.
                
            <li>    Ensure you are corporately dressed.
                
            <li>    Ensure you come along with a copy of your Nepa Bill and Valid ID Card.
                
            <li>    Please note that you are to come along with all necessary documents to avoid disqualification.
                
                
                 
                
               <h3> DOCUMENTS TO BE DOWNLOADED AND COME ALONG WITH.</h3>
                
               <li> Two (2) Filled Guarantor’s Form attached with Guarantor’s Valid ID card and Nepa Bill and One (1) Filled Security Form.
                
               <li> Two (2) Filled Confidential Guaranteed and Referee Cover Letter Form.
                
               <li> One (1) Filled Service Contract Form.
        </p>
        <a class="btn btn--primary"href="{{ url('drive/home') }}">
            <span class="btn__text">
                Go to Dashboard 
            </span>
        </a>
        
    </div>
</div>
