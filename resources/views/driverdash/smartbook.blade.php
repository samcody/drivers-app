@extends('layouts.driver-dashboard')

@section('title','Training & Simulation')

@section('content')
    <section class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                        <div class="boxed boxed--lg boxed--border">
                                <div align ="center" class="container" style ="margin:20px auto">
                                        <a class="btn btn-primary" href="{{ url('drive/training') }}">

                                                    Paid <br /> Contents
                                                
                                        </a>
                                    </div>

                                    <div align ="center" class="container"style ="margin:20px auto">
                                            <a class="btn btn-primary" href="{{ url('drive/training') }}">

                                                        Drivers <br /> Simulation
                                                    
                                            </a>
                                        </div>

                                        <div align ="center" class="container" style ="margin:20px auto">
                                                <a class="btn btn-primary" href="{{ url('drive/smartbook') }}">
    
                                                            View <br /> Smart book
                                                        
                                                </a>
                                            </div>

                        </div>
                </div>

                <div class="col-md-6">
                        <div class="boxed boxed--lg boxed--border">
                                <iframe width="420" height="345" src="https://drive.google.com/file/d/1z8psAx_tvlq_XnIW2jUpbwh57y4GqqfY/preview">
                                </iframe>
                        </div>
                        
                </div>

                <div class="col-md-3">
                        <div class="boxed boxed--lg boxed--border">
                                <div align ="center" class="container" style ="margin:20px auto">
                                        <a class="btn btn-primary" href="{{ url('drive/training') }}">

                                                    Driving <br /> Videos
                                                
                                        </a>
                                    </div>

                                <div align ="center" class="container" style ="margin:20px auto">
                                        <a class="btn btn-primary" href="{{ url('drive/training') }}">

                                                    Courses<br />
                                                
                                        </a>
                                    </div>

                                        <div align ="center" class="container" style ="margin:20px auto">
                                                <a class="btn btn-primary" href="{{ url('drive/traininga') }}">
    
                                                            Testing &amp; <br />  Certification
                                                        
                                                </a>
                                            </div>

                        </div>
                </div>

            </div>
        </div>
    </section>
@endsection