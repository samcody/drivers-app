@extends('layouts.driver-dashboard')

@section('title','Update your profile')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">Create New Driver</h4>
                            <div class="row">
@include('partial.alert')
                                    <form class="form-horizontal" action="{{ url('drive/edit-profile-vehicle') }}" method="post" enctype="multipart/form-data" role="form">
                                        {{ csrf_field() }}
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Profile Picture
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <label class="btn btn-primary">
                                                    <input type="file" name="profilepicture" accept="image/*" class="form-control" required>
                                                    <i class="fa fa-photo"></i> Add Photo
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                First name
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="text" name="firstname" value="{{ Auth::User()->full_name }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Last name
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="text" name="lastname" placeholder="Last name" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Age
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input name="age" type="date" min="01-01-1953" max="01-01-2000" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Marital Status
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <select name="marital_status" class="form-control" required>
                                                    <option value="">Choose a value</option>
                                                    <option value="Married">Married</option>
                                                    <option value="Single">Single</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Religion
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <select name="religion" class="form-control" required>
                                                    <option value="">Choose a value</option>
                                                    <option value="Christianity">Christianity</option>
                                                    <option value="Islam">Islam</option>
                                                    <option value="None">None</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Phone number
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="tel" name="phonenumber" value="{{ Auth::User()->mobile_number }}" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Mobile number
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="tel" name="mobilenumber" value="{{ Auth::User()->mobile_number }}" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Email
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="email" name="email" placeholder="{{ Auth::User()->email }}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Home Address
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="text" name="homeaddress" placeholder="13, Mokola Street" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Experience
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <textarea name="experience" rows="5" placeholder="Experience" class="form-control" required></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Location
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <select name="location" class="form-control" required>
                                                    <option value="">Choose A State</option>
                                                    @foreach($states as $state)
                                                        <option value="{{ $state }}">{{ $state }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Residence
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="text" name="residence" placeholder="Ikeja" class="form-control" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Account number
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="text" name="accountnumber" placeholder="0023381833" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Bank Name
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="text" name="bank_name" placeholder="E.g First Bank" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                What Can You Drive?
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <select name="can_drive" class="form-control" required>
                                                    <option>Cars</option>
                                                    <option>Trucks</option>
                                                    <option>Vans</option>
                                                    <option>Cars and Trucks</option>
                                                    <option>Cars and Vans</option>
                                                    <option>Cars, Trucks and Vans</option>
                                                </select>
                                            </div>  
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Drivers license number
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="text" name="license" placeholder="Drivers license" class="form-control" required>

                                                </select>
                                            </div>  
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Lasdri number
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="text" name="lasdri" placeholder="Drivers license" class="form-control">
                                                
                                            </div>  
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                License Expiry Date
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="date" name="license_date" placeholder="Drivers license" class="form-control" required>   
                                            </div>  
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Lasdri Expiry Date
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="date" name="lasdri_date" placeholder="Drivers license" class="form-control">   
                                            </div>  
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                    
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <h3>Vehicle Registration</h3>   
                                            </div>  
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Vehicle Type
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <select name="vehicle_type" class="form-control">
                                                    <option>Private Car</option>
                                                    <option>Private Bus</option>
                                                    <option>Private Truck</option>
                                                    <option>Private Van</option>
                                                    <option>Delivery Van</option>
                                                    <option>Uber</option>
                                                    <option>Taxify</option>
                                                    <option>Commercial Car</option>
                                                    <option>commercial Bus</option>
                                                    <option>Commercial Truck</option>
                                                    <option>Commercial Van</option>
                                                    
                                                </select>   
                                            </div>  
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Vehicle Brand
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                    <input type="text" name="vehicle_brand" placeholder="Vehicle brand" class="form-control"> 
                                            </div>  
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Vehicle Model
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="text" name="vehicle_model" placeholder="Vehicle Model" class="form-control">  
                                            </div>  
                                        </div>

                                        <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Vehicle Year
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <input type="number" name="vehicle_year" placeholder="Vehicle Year" class="form-control">   
                                                </div>  
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Vehicle Identification (Engine Number)
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="text" name="engine_number" placeholder="Engine Number" class="form-control">   
                                            </div>  
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Vehicle Registration (Plate Number)
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <input type="text" name="plate_number" placeholder="Plate NUmber" class="form-control">   
                                                </div>  
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Vehicle Transmission
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="text" name="vehicle_transmission" placeholder="Transmission" class="form-control">   
                                            </div>  
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Insurance Type
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <select name="insurance_type" class="form-control">
                                                    <option>Third Party Insurance</option>
                                                    <option>Comprehensive Insurance</option>
                                                    
                                                </select>
                                            </div>  
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                    
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <h3>Vehicle Condition</h3>   
                                            </div>  
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Air condition
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <select name="air_condition" class="form-control">
                                                    <option>Very good</option>
                                                    <option>Good</option>
                                                    <option>Okay</option>
                                                    <option>No A/C</option>
                                                    
                                                </select>   
                                            </div>  
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Neatness
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <select name="neatness" class="form-control">
                                                    <option>Very good</option>
                                                    <option>Good</option>
                                                    <option>Okay</option>
                                                    <option>No neatness</option>
                                                    
                                                </select>  
                                            </div>  
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Vehicle Beauty
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <select name="vehicle_beauty" class="form-control">
                                                    <option>Very good</option>
                                                    <option>Good</option>
                                                    <option>Okay</option>
                                                    <option>Not beautiful</option>
                                                    
                                                </select>
                                            </div>  
                                        </div>

                                        <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Scratches On Vehicle
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <select name="vehicle_scratches" class="form-control">
                                                        <option>High</option>
                                                        <option>Medium</option>
                                                        <option>Low</option>
                                                        <option>Not at all</option>
                                                        
                                                    </select>
                                                </div>  
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Vehicle Tracker
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <select name="vehicle_tracker" class="form-control">
                                                    <option>Yes</option>
                                                    <option>No</option>
                                                    
                                                </select>
                                            </div>  
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <h3>Set Routes</h3>   
                                            </div>  
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Location
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <div class="col-md-4 col-sm-4">
                                                <input type="text" id="from" size="10px" placeholder="from...">
                                                </div>
                                                <div class="col-md-4 col-sm-4">
                                                <input type="text" id="to" size="10px" placeholder="to..." > 
                                                </div>
                                                <div class="col-md-4 col-sm-4">
                                                <input type="number" id="rate" size="10px" placeholder="rate..."> 
                                                </div>
                                                <button type="button" onclick="myFunction()">Add Location</button>
                                                <select id="mySelect" name="location_route" class="form-control" multiple>
    
                                                </select>

                                                <script>
                                                        function myFunction() {
                                                            var x = document.getElementById("mySelect");
                                                            var from = document.getElementById("from");
                                                            var to = document.getElementById("to");
                                                            var rate = document.getElementById("rate");
                                                            var option = document.createElement("option");
                                                            if(to.value !== "" && from.value !== "" && rate.value !== ""){
                                                                option.setAttribute("selected", "true");
                                                                option.text = "from "+from.value+" to "+to.value+"  (#"+rate.value+")";
                                                                x.add(option, x[0]);
                                                            }
                                                            
                                                        }
                                                </script>
                                                
                                            </div>  
                                        </div>

                                        <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Driving hours
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <input type="text" name="driving_hours" placeholder="Driving Hours" class="form-control">   
                                                </div>  
                                            </div>
                                        
                                        <div class="form-group">
                                            <div class="col-md-10">
                                                <button class="pull-right"><a class="btn btn-primary pull-right"><span style="color:antiquewhite" class="btn__text">
                                                        Submit
                                                    </span></a>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">function add_chatinline(){var hccid=90239265;var nt=document.createElement("script");nt.async=true;nt.src="https://mylivechat.com/chatinline.aspx?hccid="+hccid;var ct=document.getElementsByTagName("script")[0];ct.parentNode.insertBefore(nt,ct);}
            add_chatinline(); </script>
@endsection