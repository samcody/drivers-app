@extends('layouts.driver-dashboard')

@section('title','Short-Term Drivers')

@section('content')
    <div class="content-page">
        <div class="content">
                <br /><br />
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="table-rep-plugin">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    @if(count($drivers) < 1)
                                        <br><br>
                                        <div class="alert alert-info text-center">
                                            <p>You do not have an active profile</p>
                                            <a href="{{ url('drive/new') }}" class="btn btn-info">Create Full-Time Profile</a>
                                            <a href="{{ url('drive/new-short') }}" class="btn btn-info">Create Short-Term  Profile</a>
                                            <a href="{{ url('drive/new-uber') }}" class="btn btn-info">Create Uber Profile</a>
                                        </div>
                                    @else
                                        <table id="tech-companies-1" class="table  table-striped">
                                            <thead>
                                            <tr>
                                                <th>Full Name</th>
                                                <th>Email Address</th>
                                                <th>Location</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($drivers as $driver)
                                                <tr>
                                                    <td>{{ $driver->firstname.' '. $driver->lastname }}</td>
                                                    <td>{{ $driver->email }}</td>
                                                    <td>{{ $driver->location }}</td>
                                                    <td>
                                                        @if ($driver->marital_status == "Married")
                                                        <a href="{{ url('drive/edit',['id' => $driver->id]) }}" class="btn btn-info">Update Profile</a>
                                                        @endif
                                                        <a href="{{ url('drive/update',['id' => $driver->id]) }}" class="btn btn-info">Update Profile</a>
                                                    </td>
                                                </tr>
                                                
                                                </div>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                                {{ $drivers->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection