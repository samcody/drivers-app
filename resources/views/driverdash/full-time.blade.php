@extends('layouts.driver-dashboard')

@section('title','Earn Extra Cash')

@section('content')
    <section class="r">
        <div class="container">
            <div class="boxed boxed--lg boxed--border">
                <div class="container">
                    <div class="row">
                        <div class = "container">
                            @if (Session::has('flash_message'))
                            <div class= "alert alert-success">{{Session::get('flash_message') }}

                            @endif
                            <form action ="{{ url('drive/full-time')}}" method ="post">
                                {{ csrf_field() }}
                                <input id="name" name="name" type="text" class="span6" placeholder="Name" required> <br /><br/>
                        </div>   
                    
            

                        <div class = "col-md-6">
                            <input id="age" name="age" type="text" class="span3" placeholder="Age" required><br /><br />
                            <input id="address" name="address" type="text" class="span3" placeholder="Home Address" required><br /><br />
                          
                                   <select class="span3" name="Religion" id="select" required>
                                      <option>Religion</option>
                                      <option>Christianity</option>
                                      <option>Islam</option>
                                    </select><br/><br/>


                                     <select class="span3" name="Marital_Status" id="select" required>
                                      <option>Marital Status</option>
                                      <option>Single</option>
                                      <option>Married</option>
                                      <option>Divorced</option>
                                     </select><br/><br/>

                            <input id="phone" name="Phone" type="number" class="span3" placeholder="Phone Number" required><br /><br />
                            <input id="phone2" name="Phone2" type="number" class="span3" placeholder="Alternative Phone Number" required><br /><br />
                            <input id="rel" name="Relationship" type="text" class="span3" placeholder="Relationship Of Next Of Kin" required><br /><br />
                                   
                         
                                
                                  
                        </div>
                        <div class ="col-md-6">
                                <input id="email" name="email" type="email" class="span3" placeholder="Email address" required><br /><br />
                                <select name="SOO" id="select" required>
                                    <option>State Of Origin</option>
                                    <option>ABUJA FCT</option>
                                    <option>ABIA</option>
                                    <option>ADAMAWA</option>
                                    <option>AKWA IBOM</option>
                                    <option>ANAMBRA</option>
                                    <option>BAUCHI</option>
                                    <option>BAYELSA</option>
                                    <option>BENUE</option>
                                    <option>BORNO</option>
                                    <option>CROSS RIVER</option>
                                    <option>DELTA</option>
                                    <option>EBONYI</option>
                                    <option>EDO</option>
                                    <option>EKITI</option>
                                    <option>ENUGU</option>
                                    <option>GOMBE</option>
                                    <option>IMO</option>
                                    <option>JIGAWA</option>
                                    <option>KADUNA</option>
                                    <option>KANO</option>
                                    <option>KATSINA</option>
                                    <option>KEBBI</option>
                                    <option>KOGI</option>
                                    <option>KWARA</option>
                                    <option>LAGOS</option>
                                    <option>NASSARAWA</option>
                                    <option>NIGER</option>
                                    <option>OGUN</option>
                                    <option>ONDO</option>
                                    <option>OSUN</option>
                                    <option>OYO</option>
                                    <option>PLATEAU</option>
                                    <option>RIVERS</option>
                                    <option>SOKOTO</option>
                                    <option>TARABA</option>
                                    <option>YOBE</option>
                                    <option>ZAMFARA</option>
                                </select><br/><br/>


                                 <select name="SOR" id="select" required>
                                    <option>State Of Residence</option>
                                    <option>ABUJA FCT</option>
                                    <option>ABIA</option>
                                    <option>ADAMAWA</option>
                                    <option>AKWA IBOM</option>
                                    <option>ANAMBRA</option>
                                    <option>BAUCHI</option>
                                    <option>BAYELSA</option>
                                    <option>BENUE</option>
                                    <option>BORNO</option>
                                    <option>CROSS RIVER</option>
                                    <option>DELTA</option>
                                    <option>EBONYI</option>
                                    <option>EDO</option>
                                    <option>EKITI</option>
                                    <option>ENUGU</option>
                                    <option>GOMBE</option>
                                    <option>IMO</option>
                                    <option>JIGAWA</option>
                                    <option>KADUNA</option>
                                    <option>KANO</option>
                                    <option>KATSINA</option>
                                    <option>KEBBI</option>
                                    <option>KOGI</option>
                                    <option>KWARA</option>
                                    <option>LAGOS</option>
                                    <option>NASSARAWA</option>
                                    <option>NIGER</option>
                                    <option>OGUN</option>
                                    <option>ONDO</option>
                                    <option>OSUN</option>
                                    <option>OYO</option>
                                    <option>PLATEAU</option>
                                    <option>RIVERS</option>
                                    <option>SOKOTO</option>
                                    <option>TARABA</option>
                                    <option>YOBE</option>
                                    <option>ZAMFARA</option>
                                </select><br/><br/>


                                        <select name="Location" id="select" required>
                                            <option>Location</option>
                                            <option value='Abadam' >Abadam</option>
                                            <option value='Abaji' >Abaji</option>
                                            <option value='Abak' >Abak</option>
                                            <option value='Abakaliki' >Abakaliki</option>
                                            <option value='Aba North' >Aba North</option>
                                            <option value='Aba South' >Aba South</option>
                                            <option value='Abeokuta North' >Abeokuta North</option>
                                            <option value='Abeokuta South' >Abeokuta South</option>
                                            <option value='Abi' >Abi</option>
                                            <option value='Aboh Mbaise' >Aboh Mbaise</option>
                                            <option value='Abua/Odual' >Abua/Odual</option>
                                            <option value='Adavi' >Adavi</option>
                                            <option value='Ado Ekiti' >Ado Ekiti</option>
                                            <option value='Ado-Odo/Ota' >Ado-Odo/Ota</option>
                                            <option value='Afijio' >Afijio</option>
                                            <option value='Afikpo North' >Afikpo North</option>
                                            <option value='Afikpo South (Edda)' >Afikpo South (Edda)</option>
                                            <option value='Agaie' >Agaie</option>
                                            <option value='Agatu' >Agatu</option>
                                            <option value='Agwara' >Agwara</option>
                                            <option value='Agege' >Agege</option>
                                            <option value='Aguata' >Aguata</option>
                                            <option value='Ahiazu Mbaise' >Ahiazu Mbaise</option>
                                            <option value='Ahoada East' >Ahoada East</option>
                                            <option value='Ahoada West' >Ahoada West</option>
                                            <option value='Ajaokuta' >Ajaokuta</option>
                                            <option value='Ajeromi-Ifelodun' >Ajeromi-Ifelodun</option>
                                            <option value='Ajingi' >Ajingi</option>
                                            <option value='Akamkpa' >Akamkpa</option>
                                            <option value='Akinyele' >Akinyele</option>
                                            <option value='Akko' >Akko</option>
                                            <option value='Akoko-Edo' >Akoko-Edo</option>
                                            <option value='Akoko North-East' >Akoko North-East</option>
                                            <option value='Akoko North-West' >Akoko North-West</option>
                                            <option value='Akoko South-West' >Akoko South-West</option>
                                            <option value='Akoko South-East' >Akoko South-East</option>
                                            <option value='Akpabuyo' >Akpabuyo</option>
                                            <option value='Akuku-Toru' >Akuku-Toru</option>
                                            <option value='Akure North' >Akure North</option>
                                            <option value='Akure South' >Akure South</option>
                                            <option value='Akwanga' >Akwanga</option>
                                            <option value='Albasu' >Albasu</option>
                                            <option value='Aleiro' >Aleiro</option>
                                            <option value='Alimosho' >Alimosho</option>
                                            <option value='Alkaleri' >Alkaleri</option>
                                            <option value='Amuwo-Odofin' >Amuwo-Odofin</option>
                                            <option value='Anambra East' >Anambra East</option>
                                            <option value='Anambra West' >Anambra West</option>
                                            <option value='Anaocha' >Anaocha</option>
                                            <option value='Andoni' >Andoni</option>
                                            <option value='Aninri' >Aninri</option>
                                            <option value='Aniocha North' >Aniocha North</option>
                                            <option value='Aniocha South' >Aniocha South</option>
                                            <option value='Anka' >Anka</option>
                                            <option value='Ankpa' >Ankpa</option>
                                            <option value='Apa' >Apa</option>
                                            <option value='Apapa' >Apapa</option>
                                            <option value='Ado' >Ado</option>
                                            <option value='Ardo Kola' >Ardo Kola</option>
                                            <option value='Arewa Dandi' >Arewa Dandi</option>
                                            <option value='Argungu' >Argungu</option>
                                            <option value='Arochukwu' >Arochukwu</option>
                                            <option value='Asa' >Asa</option>
                                            <option value='Asari-Toru' >Asari-Toru</option>
                                            <option value='Askira/Uba' >Askira/Uba</option>
                                            <option value='Atakunmosa East' >Atakunmosa East</option>
                                            <option value='Atakunmosa West' >Atakunmosa West</option>
                                            <option value='Atiba' >Atiba</option>
                                            <option value='Atisbo' >Atisbo</option>
                                            <option value='Augie' >Augie</option>
                                            <option value='Auyo' >Auyo</option>
                                            <option value='Awe' >Awe</option>
                                            <option value='Awgu' >Awgu</option>
                                            <option value='Awka North' >Awka North</option>
                                            <option value='Awka South' >Awka South</option>
                                            <option value='Ayamelum' >Ayamelum</option>
                                            <option value='Aiyedaade' >Aiyedaade</option>
                                            <option value='Aiyedire' >Aiyedire</option>
                                            <option value='Ajamgbadi' >Ajamgbadi</option>
                                            <option value='Badagry' >Badagry</option>
                                            <option value='Bagudo' >Bagudo</option>
                                            <option value='Bagwai' >Bagwai</option>
                                            <option value='Bakassi' >Bakassi</option>
                                            <option value='Bokkos' >Bokkos</option>
                                            <option value='Bakori' >Bakori</option>
                                            <option value='Bakura' >Bakura</option>
                                            <option value='Balanga' >Balanga</option>
                                            <option value='Bali' >Bali</option>
                                            <option value='Bama' >Bama</option>
                                            <option value='Bade' >Bade</option>
                                            <option value='Barkin Ladi' >Barkin Ladi</option>
                                            <option value='Baruten' >Baruten</option>
                                            <option value='Bassa' >Bassa</option>
                                            <option value='Bassa' >Bassa</option>
                                            <option value='Batagarawa' >Batagarawa</option>
                                            <option value='Batsari' >Batsari</option>
                                            <option value='Bauchi' >Bauchi</option>
                                            <option value='Baure' >Baure</option>
                                            <option value='Bayo' >Bayo</option>
                                            <option value='Bebeji' >Bebeji</option>
                                            <option value='Bekwarra' >Bekwarra</option>
                                            <option value='Bende' >Bende</option>
                                            <option value='Biase' >Biase</option>
                                            <option value='Bichi' >Bichi</option>
                                            <option value='Bida' >Bida</option>
                                            <option value='Billiri' >Billiri</option>
                                            <option value='Bindawa' >Bindawa</option>
                                            <option value='Binji' >Binji</option>
                                            <option value='Biriniwa' >Biriniwa</option>
                                            <option value='Birnin Gwari' >Birnin Gwari</option>
                                            <option value='Birnin Kebbi' >Birnin Kebbi</option>
                                            <option value='Birnin Kudu' >Birnin Kudu</option>
                                            <option value='Birnin Magaji/Kiyaw' >Birnin Magaji/Kiyaw</option>
                                            <option value='Biu' >Biu</option>
                                            <option value='Bodinga' >Bodinga</option>
                                            <option value='Bogoro' >Bogoro</option>
                                            <option value='Boki' >Boki</option>
                                            <option value='Boluwaduro' >Boluwaduro</option>
                                            <option value='Bomadi' >Bomadi</option>
                                            <option value='Bonny' >Bonny</option>
                                            <option value='Borgu' >Borgu</option>
                                            <option value='Boripe' >Boripe</option>
                                            <option value='Bursari' >Bursari</option>
                                            <option value='Bosso' >Bosso</option>
                                            <option value='Brass' >Brass</option>
                                            <option value='Buji' >Buji</option>
                                            <option value='Bukkuyum' >Bukkuyum</option>
                                            <option value='Buruku' >Buruku</option>
                                            <option value='Bungudu' >Bungudu</option>
                                            <option value='Bunkure' >Bunkure</option>
                                            <option value='Bunza' >Bunza</option>
                                            <option value='Burutu' >Burutu</option>
                                            <option value='Bwari' >Bwari</option>
                                            <option value='Calabar Municipal' >Calabar Municipal</option>
                                            <option value='Calabar South' >Calabar South</option>
                                            <option value='Chanchaga' >Chanchaga</option>
                                            <option value='Charanchi' >Charanchi</option>
                                            <option value='Chibok' >Chibok</option>
                                            <option value='Chikun' >Chikun</option>
                                            <option value='Dala' >Dala</option>
                                            <option value='Damaturu' >Damaturu</option>
                                            <option value='Damban' >Damban</option>
                                            <option value='Dambatta' >Dambatta</option>
                                            <option value='Damboa' >Damboa</option>
                                            <option value='Dandi' >Dandi</option>
                                            <option value='Dandume' >Dandume</option>
                                            <option value='Dange Shuni' >Dange Shuni</option>
                                            <option value='Danja' >Danja</option>
                                            <option value='Dan Musa' >Dan Musa</option>
                                            <option value='Darazo' >Darazo</option>
                                            <option value='Dass' >Dass</option>
                                            <option value='Daura' >Daura</option>
                                            <option value='Dawakin Kudu' >Dawakin Kudu</option>
                                            <option value='Dawakin Tofa' >Dawakin Tofa</option>
                                            <option value='Degema' >Degema</option>
                                            <option value='Dekina' >Dekina</option>
                                            <option value='Demsa' >Demsa</option>
                                            <option value='Dikwa' >Dikwa</option>
                                            <option value='Doguwa' >Doguwa</option>
                                            <option value='Doma' >Doma</option>
                                            <option value='Donga' >Donga</option>
                                            <option value='Dukku' >Dukku</option>
                                            <option value='Dunukofia' >Dunukofia</option>
                                            <option value='Dutse' >Dutse</option>
                                            <option value='Dutsi' >Dutsi</option>
                                            <option value='Dutsin Ma' >Dutsin Ma</option>
                                            <option value='Eastern Obolo' >Eastern Obolo</option>
                                            <option value='Ebonyi' >Ebonyi</option>
                                            <option value='Edati' >Edati</option>
                                            <option value='Ede North' >Ede North</option>
                                            <option value='Ede South' >Ede South</option>
                                            <option value='Edu' >Edu</option>
                                            <option value='Ife Central' >Ife Central</option>
                                            <option value='Ife East' >Ife East</option>
                                            <option value='Ife North' >Ife North</option>
                                            <option value='Ife South' >Ife South</option>
                                            <option value='Efon' >Efon</option>
                                            <option value='Yewa North' >Yewa North</option>
                                            <option value='Yewa South' >Yewa South</option>
                                            <option value='Egbeda' >Egbeda</option>
                                            <option value='Egbedore' >Egbedore</option>
                                            <option value='Egor' >Egor</option>
                                            <option value='Ehime Mbano' >Ehime Mbano</option>
                                            <option value='Ejigbo' >Ejigbo</option>
                                            <option value='Ekeremor' >Ekeremor</option>
                                            <option value='Eket' >Eket</option>
                                            <option value='Ekiti' >Ekiti</option>
                                            <option value='Ekiti East' >Ekiti East</option>
                                            <option value='Ekiti South-West' >Ekiti South-West</option>
                                            <option value='Ekiti West' >Ekiti West</option>
                                            <option value='Ekwusigo' >Ekwusigo</option>
                                            <option value='Eleme' >Eleme</option>
                                            <option value='Emuoha' >Emuoha</option>
                                            <option value='Emure' >Emure</option>
                                            <option value='Enugu East' >Enugu East</option>
                                            <option value='Enugu North' >Enugu North</option>
                                            <option value='Enugu South' >Enugu South</option>
                                            <option value='Epe' >Epe</option>
                                            <option value='Esan Central' >Esan Central</option>
                                            <option value='Esan North-East' >Esan North-East</option>
                                            <option value='Esan South-East' >Esan South-East</option>
                                            <option value='Esan West' >Esan West</option>
                                            <option value='Ese Odo' >Ese Odo</option>
                                            <option value='Esit Eket' >Esit Eket</option>
                                            <option value='Essien Udim' >Essien Udim</option>
                                            <option value='Etche' >Etche</option>
                                            <option value='Ethiope East' >Ethiope East</option>
                                            <option value='Ethiope West' >Ethiope West</option>
                                            <option value='Etim Ekpo' >Etim Ekpo</option>
                                            <option value='Etinan' >Etinan</option>
                                            <option value='Eti Osa' >Eti Osa</option>
                                            <option value='Etsako Central' >Etsako Central</option>
                                            <option value='Etsako East' >Etsako East</option>
                                            <option value='Etsako West' >Etsako West</option>
                                            <option value='Etung' >Etung</option>
                                            <option value='Ewekoro' >Ewekoro</option>
                                            <option value='Ezeagu' >Ezeagu</option>
                                            <option value='Ezinihitte' >Ezinihitte</option>
                                            <option value='Ezza North' >Ezza North</option>
                                            <option value='Ezza South' >Ezza South</option>
                                            <option value='Fagge' >Fagge</option>
                                            <option value='Fakai' >Fakai</option>
                                            <option value='Faskari' >Faskari</option>
                                            <option value='Fika' >Fika</option>
                                            <option value='Fufure' >Fufure</option>
                                            <option value='Funakaye' >Funakaye</option>
                                            <option value='Fune' >Fune</option>
                                            <option value='Funtua' >Funtua</option>
                                            <option value='Gabasawa' >Gabasawa</option>
                                            <option value='Gada' >Gada</option>
                                            <option value='Gagarawa' >Gagarawa</option>
                                            <option value='Gamawa' >Gamawa</option>
                                            <option value='Ganjuwa' >Ganjuwa</option>
                                            <option value='Ganye' >Ganye</option>
                                            <option value='Garki' >Garki</option>
                                            <option value='Garko' >Garko</option>
                                            <option value='Garun Mallam' >Garun Mallam</option>
                                            <option value='Gashaka' >Gashaka</option>
                                            <option value='Gassol' >Gassol</option>
                                            <option value='Gaya' >Gaya</option>
                                            <option value='Gayuk' >Gayuk</option>
                                            <option value='Gezawa' >Gezawa</option>
                                            <option value='Gbako' >Gbako</option>
                                            <option value='Gboko' >Gboko</option>
                                            <option value='Gbonyin' >Gbonyin</option>
                                            <option value='Geidam' >Geidam</option>
                                            <option value='Giade' >Giade</option>
                                            <option value='Giwa' >Giwa</option>
                                            <option value='Gokana' >Gokana</option>
                                            <option value='Gombe' >Gombe</option>
                                            <option value='Gombi' >Gombi</option>
                                            <option value='Goronyo' >Goronyo</option>
                                            <option value='Grie' >Grie</option>
                                            <option value='Gubio' >Gubio</option>
                                            <option value='Gudu' >Gudu</option>
                                            <option value='Gujba' >Gujba</option>
                                            <option value='Gulani' >Gulani</option>
                                            <option value='Guma' >Guma</option>
                                            <option value='Gumel' >Gumel</option>
                                            <option value='Gummi' >Gummi</option>
                                            <option value='Gurara' >Gurara</option>
                                            <option value='Guri' >Guri</option>
                                            <option value='Gusau' >Gusau</option>
                                            <option value='Guzamala' >Guzamala</option>
                                            <option value='Gwadabawa' >Gwadabawa</option>
                                            <option value='Gwagwalada' >Gwagwalada</option>
                                            <option value='Gwale' >Gwale</option>
                                            <option value='Gwandu' >Gwandu</option>
                                            <option value='Gwaram' >Gwaram</option>
                                            <option value='Gwarzo' >Gwarzo</option>
                                            <option value='Gwer East' >Gwer East</option>
                                            <option value='Gwer West' >Gwer West</option>
                                            <option value='Gwiwa' >Gwiwa</option>
                                            <option value='Gwoza' >Gwoza</option>
                                            <option value='Hadejia' >Hadejia</option>
                                            <option value='Hawul' >Hawul</option>
                                            <option value='Hong' >Hong</option>
                                            <option value='Ibadan North' >Ibadan North</option>
                                            <option value='Ibadan North-East' >Ibadan North-East</option>
                                            <option value='Ibadan North-West' >Ibadan North-West</option>
                                            <option value='Ibadan South-East' >Ibadan South-East</option>
                                            <option value='Ibadan South-West' >Ibadan South-West</option>
                                            <option value='Ibaji' >Ibaji</option>
                                            <option value='Ibarapa Central' >Ibarapa Central</option>
                                            <option value='Ibarapa East' >Ibarapa East</option>
                                            <option value='Ibarapa North' >Ibarapa North</option>
                                            <option value='Ibeju-Lekki' >Ibeju-Lekki</option>
                                            <option value='Ibeno' >Ibeno</option>
                                            <option value='Ibesikpo Asutan' >Ibesikpo Asutan</option>
                                            <option value='Ibi' >Ibi</option>
                                            <option value='Ibiono-Ibom' >Ibiono-Ibom</option>
                                            <option value='Idah' >Idah</option>
                                            <option value='Idanre' >Idanre</option>
                                            <option value='Ideato North' >Ideato North</option>
                                            <option value='Ideato South' >Ideato South</option>
                                            <option value='Idemili North' >Idemili North</option>
                                            <option value='Idemili South' >Idemili South</option>
                                            <option value='Ido' >Ido</option>
                                            <option value='Ido Osi' >Ido Osi</option>
                                            <option value='Ifako-Ijaiye' >Ifako-Ijaiye</option>
                                            <option value='Ifedayo' >Ifedayo</option>
                                            <option value='Ifedore' >Ifedore</option>
                                            <option value='Ifelodun' >Ifelodun</option>
                                            <option value='Ifelodun' >Ifelodun</option>
                                            <option value='Ifo' >Ifo</option>
                                            <option value='Igabi' >Igabi</option>
                                            <option value='Igalamela Odolu' >Igalamela Odolu</option>
                                            <option value='Igbo Etiti' >Igbo Etiti</option>
                                            <option value='Igbo Eze North' >Igbo Eze North</option>
                                            <option value='Igbo Eze South' >Igbo Eze South</option>
                                            <option value='Igueben' >Igueben</option>
                                            <option value='Ihiala' >Ihiala</option>
                                            <option value='Ihitte/Uboma' >Ihitte/Uboma</option>
                                            <option value='Ilaje' >Ilaje</option>
                                            <option value='Ijebu East' >Ijebu East</option>
                                            <option value='Ijebu North' >Ijebu North</option>
                                            <option value='Ijebu North East' >Ijebu North East</option>
                                            <option value='Ijebu Ode' >Ijebu Ode</option>
                                            <option value='Ijero' >Ijero</option>
                                            <option value='Ijumu' >Ijumu</option>
                                            <option value='Ika' >Ika</option>
                                            <option value='Ika North East' >Ika North East</option>
                                            <option value='Ikara' >Ikara</option>
                                            <option value='Ika South' >Ika South</option>
                                            <option value='Ikeduru' >Ikeduru</option>
                                            <option value='Ikeja' >Ikeja</option>
                                            <option value='Ikenne' >Ikenne</option>
                                            <option value='Ikere' >Ikere</option>
                                            <option value='Ikole' >Ikole</option>
                                            <option value='Ikom' >Ikom</option>
                                            <option value='Ikono' >Ikono</option>
                                            <option value='Ikorodu' >Ikorodu</option>
                                            <option value='Ikot Abasi' >Ikot Abasi</option>
                                            <option value='Ikot Ekpene' >Ikot Ekpene</option>
                                            <option value='Ikpoba Okha' >Ikpoba Okha</option>
                                            <option value='Ikwerre' >Ikwerre</option>
                                            <option value='Ikwo' >Ikwo</option>
                                            <option value='Ikwuano' >Ikwuano</option>
                                            <option value='Ila' >Ila</option>
                                            <option value='Ilejemeje' >Ilejemeje</option>
                                            <option value='Ile Oluji/Okeigbo' >Ile Oluji/Okeigbo</option>
                                            <option value='Ilesa East' >Ilesa East</option>
                                            <option value='Ilesa West' >Ilesa West</option>
                                            <option value='Illela' >Illela</option>
                                            <option value='Ilorin East' >Ilorin East</option>
                                            <option value='Ilorin South' >Ilorin South</option>
                                            <option value='Ilorin West' >Ilorin West</option>
                                            <option value='Imeko Afon' >Imeko Afon</option>
                                            <option value='Ingawa' >Ingawa</option>
                                            <option value='Ini' >Ini</option>
                                            <option value='Ipokia' >Ipokia</option>
                                            <option value='Irele' >Irele</option>
                                            <option value='Irepo' >Irepo</option>
                                            <option value='Irepodun' >Irepodun</option>
                                            <option value='Irepodun' >Irepodun</option>
                                            <option value='Irepodun/Ifelodun' >Irepodun/Ifelodun</option>
                                            <option value='Irewole' >Irewole</option>
                                            <option value='Isa' >Isa</option>
                                            <option value='Ise/Orun' >Ise/Orun</option>
                                            <option value='Iseyin' >Iseyin</option>
                                            <option value='Ishielu' >Ishielu</option>
                                            <option value='Isiala Mbano' >Isiala Mbano</option>
                                            <option value='Isiala Ngwa North' >Isiala Ngwa North</option>
                                            <option value='Isiala Ngwa South' >Isiala Ngwa South</option>
                                            <option value='Isin' >Isin</option>
                                            <option value='Isi Uzo' >Isi Uzo</option>
                                            <option value='Isokan' >Isokan</option>
                                            <option value='Isoko North' >Isoko North</option>
                                            <option value='Isoko South' >Isoko South</option>
                                            <option value='Isu' >Isu</option>
                                            <option value='Isuikwuato' >Isuikwuato</option>
                                            <option value='Itas/Gadau' >Itas/Gadau</option>
                                            <option value='Itesiwaju' >Itesiwaju</option>
                                            <option value='Itu' >Itu</option>
                                            <option value='Ivo' >Ivo</option>
                                            <option value='Iwajowa' >Iwajowa</option>
                                            <option value='Iwo' >Iwo</option>
                                            <option value='Izzi' >Izzi</option>
                                            <option value='Jaba' >Jaba</option>
                                            <option value='Jada' >Jada</option>
                                            <option value='Jahun' >Jahun</option>
                                            <option value='Jakusko' >Jakusko</option>
                                            <option value='Jalingo' >Jalingo</option>
                                            <option value='Jama&#039;are' >Jama&#039;are</option>
                                            <option value='Jega' >Jega</option>
                                            <option value='Jema&#039;a' >Jema&#039;a</option>
                                            <option value='Jere' >Jere</option>
                                            <option value='Jibia' >Jibia</option>
                                            <option value='Jos East' >Jos East</option>
                                            <option value='Jos North' >Jos North</option>
                                            <option value='Jos South' >Jos South</option>
                                            <option value='Kabba/Bunu' >Kabba/Bunu</option>
                                            <option value='Kabo' >Kabo</option>
                                            <option value='Kachia' >Kachia</option>
                                            <option value='Kaduna North' >Kaduna North</option>
                                            <option value='Kaduna South' >Kaduna South</option>
                                            <option value='Kafin Hausa' >Kafin Hausa</option>
                                            <option value='Kafur' >Kafur</option>
                                            <option value='Kaga' >Kaga</option>
                                            <option value='Kagarko' >Kagarko</option>
                                            <option value='Kaiama' >Kaiama</option>
                                            <option value='Kaita' >Kaita</option>
                                            <option value='Kajola' >Kajola</option>
                                            <option value='Kajuru' >Kajuru</option>
                                            <option value='Kala/Balge' >Kala/Balge</option>
                                            <option value='Kalgo' >Kalgo</option>
                                            <option value='Kaltungo' >Kaltungo</option>
                                            <option value='Kanam' >Kanam</option>
                                            <option value='Kankara' >Kankara</option>
                                            <option value='Kanke' >Kanke</option>
                                            <option value='Kankia' >Kankia</option>
                                            <option value='Kano Municipal' >Kano Municipal</option>
                                            <option value='Karasuwa' >Karasuwa</option>
                                            <option value='Karaye' >Karaye</option>
                                            <option value='Karim Lamido' >Karim Lamido</option>
                                            <option value='Karu' >Karu</option>
                                            <option value='Katagum' >Katagum</option>
                                            <option value='Katcha' >Katcha</option>
                                            <option value='Katsina' >Katsina</option>
                                            <option value='Katsina-Ala' >Katsina-Ala</option>
                                            <option value='Kaura' >Kaura</option>
                                            <option value='Kaura Namoda' >Kaura Namoda</option>
                                            <option value='Kauru' >Kauru</option>
                                            <option value='Kazaure' >Kazaure</option>
                                            <option value='Keana' >Keana</option>
                                            <option value='Kebbe' >Kebbe</option>
                                            <option value='Keffi' >Keffi</option>
                                            <option value='Khana' >Khana</option>
                                            <option value='Kibiya' >Kibiya</option>
                                            <option value='Kirfi' >Kirfi</option>
                                            <option value='Kiri Kasama' >Kiri Kasama</option>
                                            <option value='Kiru' >Kiru</option>
                                            <option value='Kiyawa' >Kiyawa</option>
                                            <option value='Kogi' >Kogi</option>
                                            <option value='Koko/Besse' >Koko/Besse</option>
                                            <option value='Kokona' >Kokona</option>
                                            <option value='Kolokuma/Opokuma' >Kolokuma/Opokuma</option>
                                            <option value='Konduga' >Konduga</option>
                                            <option value='Konshisha' >Konshisha</option>
                                            <option value='Kontagora' >Kontagora</option>
                                            <option value='Kosofe' >Kosofe</option>
                                            <option value='Kaugama' >Kaugama</option>
                                            <option value='Kubau' >Kubau</option>
                                            <option value='Kudan' >Kudan</option>
                                            <option value='Kuje' >Kuje</option>
                                            <option value='Kukawa' >Kukawa</option>
                                            <option value='Kumbotso' >Kumbotso</option>
                                            <option value='Kumi' >Kumi</option>
                                            <option value='Kunchi' >Kunchi</option>
                                            <option value='Kura' >Kura</option>
                                            <option value='Kurfi' >Kurfi</option>
                                            <option value='Kusada' >Kusada</option>
                                            <option value='Kwali' >Kwali</option>
                                            <option value='Kwande' >Kwande</option>
                                            <option value='Kwami' >Kwami</option>
                                            <option value='Kware' >Kware</option>
                                            <option value='Kwaya Kusar' >Kwaya Kusar</option>
                                            <option value='Lafia' >Lafia</option>
                                            <option value='Lagelu' >Lagelu</option>
                                            <option value='Lagos Island' >Lagos Island</option>
                                            <option value='Lagos Mainland' >Lagos Mainland</option>
                                            <option value='Langtang South' >Langtang South</option>
                                            <option value='Langtang North' >Langtang North</option>
                                            <option value='Lapai' >Lapai</option>
                                            <option value='Lamurde' >Lamurde</option>
                                            <option value='Lau' >Lau</option>
                                            <option value='Lavun' >Lavun</option>
                                            <option value='Lere' >Lere</option>
                                            <option value='Logo' >Logo</option>
                                            <option value='Lokoja' >Lokoja</option>
                                            <option value='Machina' >Machina</option>
                                            <option value='Madagali' >Madagali</option>
                                            <option value='Madobi' >Madobi</option>
                                            <option value='Mafa' >Mafa</option>
                                            <option value='Magama' >Magama</option>
                                            <option value='Magumeri' >Magumeri</option>
                                            <option value='Mai&#039;Adua' >Mai&#039;Adua</option>
                                            <option value='Maiduguri' >Maiduguri</option>
                                            <option value='Maigatari' >Maigatari</option>
                                            <option value='Maiha' >Maiha</option>
                                            <option value='Maiyama' >Maiyama</option>
                                            <option value='Makarfi' >Makarfi</option>
                                            <option value='Makoda' >Makoda</option>
                                            <option value='Malam Madori' >Malam Madori</option>
                                            <option value='Malumfashi' >Malumfashi</option>
                                            <option value='Mangu' >Mangu</option>
                                            <option value='Mani' >Mani</option>
                                            <option value='Maradun' >Maradun</option>
                                            <option value='Mariga' >Mariga</option>
                                            <option value='Makurdi' >Makurdi</option>
                                            <option value='Marte' >Marte</option>
                                            <option value='Maru' >Maru</option>
                                            <option value='Mashegu' >Mashegu</option>
                                            <option value='Mashi' >Mashi</option>
                                            <option value='Matazu' >Matazu</option>
                                            <option value='Mayo Belwa' >Mayo Belwa</option>
                                            <option value='Mbaitoli' >Mbaitoli</option>
                                            <option value='Mbo' >Mbo</option>
                                            <option value='Michika' >Michika</option>
                                            <option value='Miga' >Miga</option>
                                            <option value='Mikang' >Mikang</option>
                                            <option value='Minjibir' >Minjibir</option>
                                            <option value='Misau' >Misau</option>
                                            <option value='Moba' >Moba</option>
                                            <option value='Mobbar' >Mobbar</option>
                                            <option value='Mubi North' >Mubi North</option>
                                            <option value='Mubi South' >Mubi South</option>
                                            <option value='Mokwa' >Mokwa</option>
                                            <option value='Monguno' >Monguno</option>
                                            <option value='Mopa Muro' >Mopa Muro</option>
                                            <option value='Moro' >Moro</option>
                                            <option value='Moya' >Moya</option>
                                            <option value='Mkpat-Enin' >Mkpat-Enin</option>
                                            <option value='Municipal Area Council' >Municipal Area Council</option>
                                            <option value='Musawa' >Musawa</option>
                                            <option value='Mushin' >Mushin</option>
                                            <option value='Nafada' >Nafada</option>
                                            <option value='Nangere' >Nangere</option>
                                            <option value='Nasarawa' >Nasarawa</option>
                                            <option value='Nasarawa' >Nasarawa</option>
                                            <option value='Nasarawa Egon' >Nasarawa Egon</option>
                                            <option value='Ndokwa East' >Ndokwa East</option>
                                            <option value='Ndokwa West' >Ndokwa West</option>
                                            <option value='Nembe' >Nembe</option>
                                            <option value='Ngala' >Ngala</option>
                                            <option value='Nganzai' >Nganzai</option>
                                            <option value='Ngaski' >Ngaski</option>
                                            <option value='Ngor Okpala' >Ngor Okpala</option>
                                            <option value='Nguru' >Nguru</option>
                                            <option value='Ningi' >Ningi</option>
                                            <option value='Njaba' >Njaba</option>
                                            <option value='Njikoka' >Njikoka</option>
                                            <option value='Nkanu East' >Nkanu East</option>
                                            <option value='Nkanu West' >Nkanu West</option>
                                            <option value='Nkwerre' >Nkwerre</option>
                                            <option value='Nnewi North' >Nnewi North</option>
                                            <option value='Nnewi South' >Nnewi South</option>
                                            <option value='Nsit-Atai' >Nsit-Atai</option>
                                            <option value='Nsit-Ibom' >Nsit-Ibom</option>
                                            <option value='Nsit-Ubium' >Nsit-Ubium</option>
                                            <option value='Nsukka' >Nsukka</option>
                                            <option value='Numan' >Numan</option>
                                            <option value='Nwangele' >Nwangele</option>
                                            <option value='Obafemi Owode' >Obafemi Owode</option>
                                            <option value='Obanliku' >Obanliku</option>
                                            <option value='Obi' >Obi</option>
                                            <option value='Obi' >Obi</option>
                                            <option value='Obi Ngwa' >Obi Ngwa</option>
                                            <option value='Obio/Akpor' >Obio/Akpor</option>
                                            <option value='Obokun' >Obokun</option>
                                            <option value='Obot Akara' >Obot Akara</option>
                                            <option value='Obowo' >Obowo</option>
                                            <option value='Obubra' >Obubra</option>
                                            <option value='Obudu' >Obudu</option>
                                            <option value='Odeda' >Odeda</option>
                                            <option value='Odigbo' >Odigbo</option>
                                            <option value='Odogbolu' >Odogbolu</option>
                                            <option value='Odo Otin' >Odo Otin</option>
                                            <option value='Odukpani' >Odukpani</option>
                                            <option value='Offa' >Offa</option>
                                            <option value='Ofu' >Ofu</option>
                                            <option value='Ogba/Egbema/Ndoni' >Ogba/Egbema/Ndoni</option>
                                            <option value='Ogbadibo' >Ogbadibo</option>
                                            <option value='Ogbaru' >Ogbaru</option>
                                            <option value='Ogbia' >Ogbia</option>
                                            <option value='Ogbomosho North' >Ogbomosho North</option>
                                            <option value='Ogbomosho South' >Ogbomosho South</option>
                                            <option value='Ogu/Bolo' >Ogu/Bolo</option>
                                            <option value='Ogoja' >Ogoja</option>
                                            <option value='Ogo Oluwa' >Ogo Oluwa</option>
                                            <option value='Ogori/Magongo' >Ogori/Magongo</option>
                                            <option value='Ogun Waterside' >Ogun Waterside</option>
                                            <option value='Oguta' >Oguta</option>
                                            <option value='Ohafia' >Ohafia</option>
                                            <option value='Ohaji/Egbema' >Ohaji/Egbema</option>
                                            <option value='Ohaozara' >Ohaozara</option>
                                            <option value='Ohaukwu' >Ohaukwu</option>
                                            <option value='Ohimini' >Ohimini</option>
                                            <option value='Orhionmwon' >Orhionmwon</option>
                                            <option value='Oji River' >Oji River</option>
                                            <option value='Ojo' >Ojo</option>
                                            <option value='Oju' >Oju</option>
                                            <option value='Okehi' >Okehi</option>
                                            <option value='Okene' >Okene</option>
                                            <option value='Oke Ero' >Oke Ero</option>
                                            <option value='Okigwe' >Okigwe</option>
                                            <option value='Okitipupa' >Okitipupa</option>
                                            <option value='Okobo' >Okobo</option>
                                            <option value='Okpe' >Okpe</option>
                                            <option value='Okrika' >Okrika</option>
                                            <option value='Olamaboro' >Olamaboro</option>
                                            <option value='Ola Oluwa' >Ola Oluwa</option>
                                            <option value='Olorunda' >Olorunda</option>
                                            <option value='Olorunsogo' >Olorunsogo</option>
                                            <option value='Oluyole' >Oluyole</option>
                                            <option value='Omala' >Omala</option>
                                            <option value='Omuma' >Omuma</option>
                                            <option value='Ona Ara' >Ona Ara</option>
                                            <option value='Ondo East' >Ondo East</option>
                                            <option value='Ondo West' >Ondo West</option>
                                            <option value='Onicha' >Onicha</option>
                                            <option value='Onitsha North' >Onitsha North</option>
                                            <option value='Onitsha South' >Onitsha South</option>
                                            <option value='Onna' >Onna</option>
                                            <option value='Okpokwu' >Okpokwu</option>
                                            <option value='Opobo/Nkoro' >Opobo/Nkoro</option>
                                            <option value='Oredo' >Oredo</option>
                                            <option value='Orelope' >Orelope</option>
                                            <option value='Oriade' >Oriade</option>
                                            <option value='Ori Ire' >Ori Ire</option>
                                            <option value='Orlu' >Orlu</option>
                                            <option value='Orolu' >Orolu</option>
                                            <option value='Oron' >Oron</option>
                                            <option value='Orsu' >Orsu</option>
                                            <option value='Oru East' >Oru East</option>
                                            <option value='Oruk Anam' >Oruk Anam</option>
                                            <option value='Orumba North' >Orumba North</option>
                                            <option value='Orumba South' >Orumba South</option>
                                            <option value='Oru West' >Oru West</option>
                                            <option value='Ose' >Ose</option>
                                            <option value='Oshimili North' >Oshimili North</option>
                                            <option value='Oshimili South' >Oshimili South</option>
                                            <option value='Oshodi-Isolo' >Oshodi-Isolo</option>
                                            <option value='Osisioma' >Osisioma</option>
                                            <option value='Osogbo' >Osogbo</option>
                                            <option value='Oturkpo' >Oturkpo</option>
                                            <option value='Ovia North-East' >Ovia North-East</option>
                                            <option value='Ovia South-West' >Ovia South-West</option>
                                            <option value='Owan East' >Owan East</option>
                                            <option value='Owan West' >Owan West</option>
                                            <option value='Owerri Municipal' >Owerri Municipal</option>
                                            <option value='Owerri North' >Owerri North</option>
                                            <option value='Owerri West' >Owerri West</option>
                                            <option value='Owo' >Owo</option>
                                            <option value='Oye' >Oye</option>
                                            <option value='Oyi' >Oyi</option>
                                            <option value='Oyigbo' >Oyigbo</option>
                                            <option value='Oyo' >Oyo</option>
                                            <option value='Oyo East' >Oyo East</option>
                                            <option value='Oyun' >Oyun</option>
                                            <option value='Paikoro' >Paikoro</option>
                                            <option value='Pankshin' >Pankshin</option>
                                            <option value='Patani' >Patani</option>
                                            <option value='Pategi' >Pategi</option>
                                            <option value='Port Harcourt' >Port Harcourt</option>
                                            <option value='Potiskum' >Potiskum</option>
                                            <option value='Qua&#039;an Pan' >Qua&#039;an Pan</option>
                                            <option value='Rabah' >Rabah</option>
                                            <option value='Rafi' >Rafi</option>
                                            <option value='Rano' >Rano</option>
                                            <option value='Remo North' >Remo North</option>
                                            <option value='Rijau' >Rijau</option>
                                            <option value='Rimi' >Rimi</option>
                                            <option value='Rimin Gado' >Rimin Gado</option>
                                            <option value='Ringim' >Ringim</option>
                                            <option value='Riyom' >Riyom</option>
                                            <option value='Rogo' >Rogo</option>
                                            <option value='Roni' >Roni</option>
                                            <option value='Sabon Birni' >Sabon Birni</option>
                                            <option value='Sabon Gari' >Sabon Gari</option>
                                            <option value='Sabuwa' >Sabuwa</option>
                                            <option value='Safana' >Safana</option>
                                            <option value='Sagbama' >Sagbama</option>
                                            <option value='Sakaba' >Sakaba</option>
                                            <option value='Saki East' >Saki East</option>
                                            <option value='Saki West' >Saki West</option>
                                            <option value='Sandamu' >Sandamu</option>
                                            <option value='Sanga' >Sanga</option>
                                            <option value='Sapele' >Sapele</option>
                                            <option value='Sardauna' >Sardauna</option>
                                            <option value='Shagamu' >Shagamu</option>
                                            <option value='Shagari' >Shagari</option>
                                            <option value='Shanga' >Shanga</option>
                                            <option value='Shani' >Shani</option>
                                            <option value='Shanono' >Shanono</option>
                                            <option value='Shelleng' >Shelleng</option>
                                            <option value='Shendam' >Shendam</option>
                                            <option value='Shinkafi' >Shinkafi</option>
                                            <option value='Shira' >Shira</option>
                                            <option value='Shiroro' >Shiroro</option>
                                            <option value='Shongom' >Shongom</option>
                                            <option value='Shomolu' >Shomolu</option>
                                            <option value='Silame' >Silame</option>
                                            <option value='Soba' >Soba</option>
                                            <option value='Sokoto North' >Sokoto North</option>
                                            <option value='Sokoto South' >Sokoto South</option>
                                            <option value='Song' >Song</option>
                                            <option value='Southern Ijaw' >Southern Ijaw</option>
                                            <option value='Suleja' >Suleja</option>
                                            <option value='Sule Tankarkar' >Sule Tankarkar</option>
                                            <option value='Sumaila' >Sumaila</option>
                                            <option value='Suru' >Suru</option>
                                            <option value='Surulere' >Surulere</option>
                                            <option value='Surulere' >Surulere</option>
                                            <option value='Tafa' >Tafa</option>
                                            <option value='Tafawa Balewa' >Tafawa Balewa</option>
                                            <option value='Tai' >Tai</option>
                                            <option value='Takai' >Takai</option>
                                            <option value='Takum' >Takum</option>
                                            <option value='Talata Mafara' >Talata Mafara</option>
                                            <option value='Tambuwal' >Tambuwal</option>
                                            <option value='Tangaza' >Tangaza</option>
                                            <option value='Tarauni' >Tarauni</option>
                                            <option value='Tarka' >Tarka</option>
                                            <option value='Tarmuwa' >Tarmuwa</option>
                                            <option value='Taura' >Taura</option>
                                            <option value='Toungo' >Toungo</option>
                                            <option value='Tofa' >Tofa</option>
                                            <option value='Toro' >Toro</option>
                                            <option value='Toto' >Toto</option>
                                            <option value='Chafe' >Chafe</option>
                                            <option value='Tsanyawa' >Tsanyawa</option>
                                            <option value='Tudun Wada' >Tudun Wada</option>
                                            <option value='Tureta' >Tureta</option>
                                            <option value='Udenu' >Udenu</option>
                                            <option value='Udi' >Udi</option>
                                            <option value='Udu' >Udu</option>
                                            <option value='Udung-Uko' >Udung-Uko</option>
                                            <option value='Ughelli North' >Ughelli North</option>
                                            <option value='Ughelli South' >Ughelli South</option>
                                            <option value='Ugwunagbo' >Ugwunagbo</option>
                                            <option value='Uhunmwonde' >Uhunmwonde</option>
                                            <option value='Ukanafun' >Ukanafun</option>
                                            <option value='Ukum' >Ukum</option>
                                            <option value='Ukwa East' >Ukwa East</option>
                                            <option value='Ukwa West' >Ukwa West</option>
                                            <option value='Ukwuani' >Ukwuani</option>
                                            <option value='Umuahia North' >Umuahia North</option>
                                            <option value='Umuahia South' >Umuahia South</option>
                                            <option value='Umu Nneochi' >Umu Nneochi</option>
                                            <option value='Ungogo' >Ungogo</option>
                                            <option value='Unuimo' >Unuimo</option>
                                            <option value='Uruan' >Uruan</option>
                                            <option value='Urue-Offong/Oruko' >Urue-Offong/Oruko</option>
                                            <option value='Ushongo' >Ushongo</option>
                                            <option value='Ussa' >Ussa</option>
                                            <option value='Uvwie' >Uvwie</option>
                                            <option value='Uyo' >Uyo</option>
                                            <option value='Uzo-Uwani' >Uzo-Uwani</option>
                                            <option value='Vandeikya' >Vandeikya</option>
                                            <option value='Wamako' >Wamako</option>
                                            <option value='Wamba' >Wamba</option>
                                            <option value='Warawa' >Warawa</option>
                                            <option value='Warji' >Warji</option>
                                            <option value='Warri North' >Warri North</option>
                                            <option value='Warri South' >Warri South</option>
                                            <option value='Warri South West' >Warri South West</option>
                                            <option value='Wasagu/Danko' >Wasagu/Danko</option>
                                            <option value='Wase' >Wase</option>
                                            <option value='Wudil' >Wudil</option>
                                            <option value='Wukari' >Wukari</option>
                                            <option value='Wurno' >Wurno</option>
                                            <option value='Wushishi' >Wushishi</option>
                                            <option value='Yabo' >Yabo</option>
                                            <option value='Yagba East' >Yagba East</option>
                                            <option value='Yagba West' >Yagba West</option>
                                            <option value='Yakuur' >Yakuur</option>
                                            <option value='Obi' >Obi</option>
                                            <option value='Obi' >Obi</option>
                                            <option value='Obi Ngwa' >Obi Ngwa</option>
                                            <option value='Obio/Akpor' >Obio/Akpor</option>
                                            <option value='Obokun' >Obokun</option>
                                            <option value='Obot Akara' >Obot Akara</option>
                                            <option value='Obowo' >Obowo</option>
                                            <option value='Obubra' >Obubra</option>
                                            <option value='Yala' >Yala</option>
                                            <option value='Yamaltu/Deba' >Yamaltu/Deba</option>
                                            <option value='Yankwashi' >Yankwashi</option>
                                            <option value='Yauri' >Yauri</option>
                                            <option value='Yenagoa' >Yenagoa</option>
                                            <option value='Yola North' >Yola North</option>
                                            <option value='Yola South' >Yola South</option>
                                            <option value='Yorro' >Yorro</option>
                                            <option value='Yunusari' >Yunusari</option>
                                            <option value='Yusufari' >Yusufari</option>
                                            <option value='Zaki' >Zaki</option>
                                            <option value='Zango' >Zango</option>
                                            <option value='Zangon Kataf' >Zangon Kataf</option>
                                            <option value='Zaria' >Zaria</option>
                                            <option value='Zing' >Zing</option>
                                            <option value='Zurmi' >Zurmi</option>
                                            <option value='Zuru' >Zuru</option>
                                        </select><br/><br/>
                            <input id="nameOfKin" name="nameOfKin" type="text" class="span3" placeholder="Name Of Next Of Kin" required><br /><br />
                            <input id="noOfKin" name="noOfKin" type="number" class="span3" placeholder="Phone Number Of Next Of Kin" required><br /><br />
                                        
                                 
                        </div>
                            




                    </div>
                </div> 
                <hr>
                <h2 style="text-align: center;">Experience And Achievements</h2>
                <hr>
                <div class="row">
                    <div class="col-md-6">    
                        <input id="years" name="years" type="text" class="span3" placeholder="Years of Driving Experience" required><br /><br />
                        <select class="span3" name="vehicle" id="select" required>
                                      <option>What Can You Drive?</option>
                                      <option>Cars</option>
                                      <option>Trucks</option>
                                      <option>Vans</option>
                                      <option>Cars and Trucks</option>
                                      <option>Cars and Vans</option>
                                      <option>Cars, Trucks and Vans</option>
                        </select><br/><br/>

                        <select class="span3" name="passion" id="select" required>
                                      <option>Are You Passionate About Driving?</option>
                                      <option>Yes</option>
                                      <option>No</option>
                                      
                        </select><br/><br/>

                        <input id="years" name="licence-number" type="number" class="span3" placeholder="Drivers License Number" required><br /><br />
                        
                        <input id="years" name="lasdri-number" type="number" class="span3" placeholder="Lasdri Number" required><br /><br />
                        
                    </div> 
                    <div class="col-md-6">
                        <input id="past_employer" name="past_employer" type="text" class="span3" placeholder="Past/Current Employer" required><br /><br />
                        <input id="years" name="past_employer_number" type="number" class="span3" placeholder="Phone Number of Past/Current Employer" required><br /><br />
                        <select class="span3" name="license_boolean" id="select" required>
                                      <option>Do You Have A Valid Drivers Licence?</option>
                                      <option>Yes</option>
                                      <option>No</option>
                                      
                        </select><br/><br/>

                        <select class="span3" name="lasdri-boolean" id="select" required>
                                      <option>Do You Have A Valid Lasdri Licence?</option>
                                      <option>Yes</option>
                                      <option>No</option>
                                      
                        </select><br/><br/>
                           </div>
                </div>

                    <hr>
                <h2 style="text-align: center;">Job Offer &amp; SDTM Program</h2>
                <hr>
                <div class="row">
                    <div class="col-md-8">    
                        <select class="span3" name="TOC" id="select" required>
                                      <option>Choose time of contract (Caution!: Note that you would sign a contract with DRIVERSNG to support this)?</option>
                                      <option>3 months duration (RENEWABLE)</option>
                                      <option>6 months duration (RENEWABLE)</option>
                                      <option>12 months duration (RENEWABLE)</option>
                                      
                        </select><br/><br/>

                        <select class="span3" name="SDTM-session" id="select" required>
                                      <option>Select SDTM Session (NB: You must Complete the Program when you start so you can get posted to a job)?</option>
                                      <option>Morning Session (9:30am - 10:30am (Starts Monday - Ends Friday))</option>
                        </select><br/><br/>

                        <select class="span3" name="time" id="select" required>
                                      <option>What full time salary job are you interested in??</option>
                                      <option value='Verified driver (Mon – Fri (6am-8pm) NGN 45,000)' >Verified driver (Mon – Fri (6am-8pm) NGN 45,000)</option>
                                      <option value='Verified driver (Mon – Sat (6am-8pm) NGN 50,000)' >Verified driver (Mon – Sat (6am-8pm) NGN 50,000)</option>
                                      <option value='Verified driver (Mon – Sun (6am-8pm) NGN 75,000)' >Verified driver (Mon – Sun (6am-8pm) NGN 75,000)</option>
                                      <option value='Special Drivers' >Special Drivers</option>
                                      
                        </select><br/><br/>

                        <select class="span3" name="Uniform" id="select" required>
                                      <option>Select Uniform Size</option>
                                      <option>Medium</option>
                                      <option>Large</option>
                                      <option>Extra Large</option>

                        </select><br/><br/>

                        <select class="span3" name="relocate" id="select" required>
                                      <option>Can you relocate for a job?</option>
                                      <option>Yes</option>
                                      <option>No</option>
                                      
                        </select><br/><br/>

                    </div> 
                    
                </div>
                             
                        <div>
                            <button id="contact-submit" type="submit" class="btn btn-primary input-medium pull-right">Send</button>
                        </div>
        </form>
            </div>
        </div>
    </section>
@endsection