@extends('layouts.driver-dashboard')

@section('title','Refer a driver')

@section('content')
    <section class="r">
            <div class="container">
                    <div class="row">
                        <div class="boxed boxed--lg boxed--border">
                        <div class="col-sm-4">
                                <img alt="Image" src="{{ asset('assets/img/bgg-2.png') }}" />
                                <br>
                        </div>
                        <div class="col-sm-8 col-md-6 color--primary-1 text-center">
                                @include('partial.alert')
                            <h2>Invite friends
                                &amp; get &#8358;1000</h2>
                            <h4>
                                    Earn &#8358;1000 for every driver
                                    that gets a card
                            </h4>
                            <div class="form-group">
                                    <form action ="{{ url('drive/refer')}}" method ="post">
                                        {{ csrf_field() }}
        
                                        <div class="col-md-12 col-sm-12">
                                            <input type="email" name="email" placeholder="Add Email" class="form-control">
                                        </div>
                                    </form>
                            </div>
                            
                            <h1>Or</h1>
        
                            <div class="form-group">
                                @foreach($drivers as $driver)
                                <div class="col-md-12 col-sm-12">
                                        <h4>
                                                Invite a friend by using this link
                                        </h4>
                                    <input type="url" value="{{ url('drive/register')}}{{'/'}}{{$driver->id }}" class="form-control">
                                </div>
                                    <a style="margin-top:50px;" class="btn btn--primary" href="{{ url('drive/refer') }}">
                                    <span class="btn__text">
                                        Get Cash <i class="glyphicon glypicon-money"></i>
                                    </span>
                                    </a>
                                
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                </div>
                </div>
            </div>
        </div>
    </section>
    @foreach($refers as $refer)
    @if(count($refers) >= 1)
    <section class="text-center">
                    <div class="col-sm-12 col-md-12">
                        <h1>Track Your Referrals</h1>
                        <p class="lead">
                            <span>Invite Friends
                                    &amp; Get &#8358;1000</span><br>
                                    Earn &#8358;1000 for every driver
                                    that gets a card
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="container">
                <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Email</th>
                          <th>Name</th>
                          <th>Date of Registration</th>
                          <th>Status</th>
                          <th>Phone Number</th>
                        </tr>
                      </thead>
                      @endif
                      @break
                      @endforeach
                      <tbody>
                            @foreach($refers as $refer)
                            
                            @if(count($refers) <= 1)
                            <tr>
                                <td align ="left">{{ $refer->full_name }}</td>
                                <td align ="left">{{ $refer->email }}</td>
                                
                                
                                <td align ="left">{{ $refer->updated_at }}</td>
                                
                                @if($refer->has_card == false)
                                <td align ="left">Registration Incomplete</td>
                                @else
                                <td align ="left">Completed Registration</td>
                                @endif

                                <td align ="left">{{ $refer->mobile_number }}</td>
    
                                
                                
                        </tr>
    
                            @endif
                            @endforeach
                      </tbody>
                    </table>
                  </div>
    
                  <div class="col-md-12 text-center">
                        
                    </section>
@endsection