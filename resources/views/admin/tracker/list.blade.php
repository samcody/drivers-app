@extends('layouts.admin')

@section('title','Car Tracker')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="table-rep-plugin">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    @if(count($trackers) < 1)
                                        <br><br>
                                        <div class="alert alert-info text-center">
                                            <p>No Tracker request yet!</p>
                                        </div>
                                    @else
                                        <table id="tech-companies-1" class="table  table-striped">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Name</th>
                                                <th>Address</th>
                                                <th>Vehicle Type</th>
                                                <th>Email</th>
                                                <th>Date</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($trackers as $tracker)
                                                <tr>
                                                    <td>#</td>
                                                    <td>{{ $tracker->name }}</td>
                                                    <td>{{ $tracker->address }}</td>
                                                    <td>{{ $tracker->vehicle_type }}</td>
                                                    <td>{{ $tracker->email}}</td>
                                                    <td>{{ date_format($tracker->created_at,'d-m-Y') }}</td>
                                                    <td><a href="{{ url('tracker',['id' => $tracker->id]) }}" class="btn btn-primary">View</a></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                                {{ $trackers->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection