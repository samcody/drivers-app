@extends('layouts.admin')

@section('title','Car Tracker')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">View Tracker Request By {{ $tracker->name }}</h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>Client: {{ $tracker->name }}</p>
                                    <p>Client's Mobile Number: {{ $tracker->phone }}</p>
                                    <p>Client's Address: {{ $tracker->address }}</p>
                                    <p>Client's Vehicle Type: {{ $tracker->vehicle_type }}</p>
                                    <p>Client's Vehicle Transmission: {{ $tracker->vehicle_transmission }}</p>
                                    <p>Client's Email : {{ $tracker->email }}</p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection