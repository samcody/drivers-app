@extends('layouts.admin')

@section('title','New Full-Time Driver')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">Create New Driver</h4>
                            <div class="row">
@include('partial.alert')
                                    <form class="form-horizontal" action="{{ url('full-time/new') }}" method="post" enctype="multipart/form-data" role="form">
                                        {{ csrf_field() }}
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Profile Picture
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <label class="btn btn-primary">
                                                        <input type="file" name="profilepicture" accept="image/*" class="form-control" required>
                                                        <i class="fa fa-photo"></i> Add Photo
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    First name
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <input type="text" name="firstname" value="{{ Auth::User()->full_name }}" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Last name
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <input type="text" name="lastname" placeholder="Last name" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Age
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <input name="age" type="date" min="01-01-1953" max="01-01-2000" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Marital Status
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <select name="marital_status" class="form-control" required>
                                                        <option value="">Choose a value</option>
                                                        <option value="Married">Married</option>
                                                        <option value="Single">Single</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Religion
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <select name="religion" class="form-control" required>
                                                        <option value="">Choose a value</option>
                                                        <option value="Christianity">Christianity</option>
                                                        <option value="Islam">Islam</option>
                                                        <option value="None">None</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Phone number
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <input type="tel" name="phonenumber" value="{{ Auth::User()->mobile_number }}" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Mobile number
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <input type="tel" name="mobilenumber" value="{{ Auth::User()->mobile_number }}" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Email
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <input type="email" name="email" class="form-control" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Home Address
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <input type="text" name="homeaddress" placeholder="13, Mokola Street" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Experience
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <textarea name="experience" rows="5" placeholder="Experience" class="form-control" required></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Location
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <select name="location" class="form-control" required>
                                                        <option value="">Choose A State</option>
                                                        @foreach($states as $state)
                                                            <option value="{{ $state }}">{{ $state }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
    
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Residence
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <input type="text" name="residence" placeholder="Ikeja" class="form-control" required>
                                                </div>
                                            </div>
    
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Account number
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <input type="text" name="accountnumber" placeholder="0023381833" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Bank Name
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <input type="text" name="bank_name" placeholder="E.g First Bank" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    What Can You Drive?
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <select name="can_drive" class="form-control" required>
                                                        <option>Cars</option>
                                                        <option>Trucks</option>
                                                        <option>Vans</option>
                                                        <option>Cars and Trucks</option>
                                                        <option>Cars and Vans</option>
                                                        <option>Cars, Trucks and Vans</option>
                                                    </select>
                                                </div>  
                                            </div>
    
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Drivers license number
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <input type="text" name="license" placeholder="Drivers license" class="form-control" required>
    
                                                    </select>
                                                </div>  
                                            </div>
    
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Lasdri number
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <input type="text" name="lasdri" placeholder="Drivers license" class="form-control">
                                                    
                                                </div>  
                                            </div>
    
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    License Expiry Date
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <input type="date" name="license_date" placeholder="Drivers license" class="form-control" required>   
                                                </div>  
                                            </div>
    
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Lasdri Expiry Date
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <input type="date" name="lasdri_date" placeholder="Drivers license" class="form-control">   
                                                </div>  
                                            </div>
    
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                        
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <h3>Job Offer &amp; SDTM Program</h3>   
                                                </div>  
                                            </div>
    
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Choose time of contract (Caution!: Note that you would sign a contract with DRIVERSNG to support this)
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <select name="time_of_contract" class="form-control">
                                                        <option>3 months duration (RENEWABLE)</option>
                                                        <option>6 months duration (RENEWABLE)</option>
                                                        <option>9 months duration (RENEWABLE)</option>
                                                        
                                                    </select>   
                                                </div>  
                                            </div>
    
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Select SDTM Session (NB: You must Complete the Program when you start so you can get posted to a job)
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <select name="sdtm" class="form-control">
                                                        <option>Morning Session(9:30-10:30am (Starts Monday-Ends Friday))</option>
                                                        
                                                    </select>   
                                                </div>  
                                            </div>
    
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    What full time salary job are you interested in?
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <select name="job_interest" class="form-control" required>
                                                        <option>Verified Driver (Mon-Fri(6am-8pm) &#8358;45,000)</option>
                                                        <option>Verified Driver (Mon-Sat(6am-8pm) &#8358;50,000)</option>
                                                        <option>Verified Driver (Mon-Fri(6am-8pm) &#8358;75,000)</option>
                                                        <option>Special Drivers</option>
                                                    </select>   
                                                </div>  
                                            </div>
    
                                            <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3">
                                                            Can You Relocate for a Job
                                                    </label>
                                                    <div class="col-md-9 col-sm-9">
                                                        <select name="relocate" class="form-control" required>
                                                            <option>Yes</option>
                                                            <option>No</option>
                                                            
                                                        </select>   
                                                    </div>  
                                                </div>

                                            
                                            
                                            <div class="form-group">
                                                <div class="col-md-10">
                                                    <button class="btn btn-primary pull-right">Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection