@extends('layouts.admin')

@section('title','Edit Full-Time Driver')

@section('content')
<div class="content-page">
    <div class="content">
        <div class="container">

            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30">Edit {{ ucfirst($driver->firstname.' '. $driver->lastname) }} Driver's Profile</h4>
                        <div class="row">
                            @include('partial.alert')
                            <form class="form-horizontal" action="{{ url('full-time/edit',['id' => $driver->id]) }}" method="post" enctype="multipart/form-data" role="form">
                                {{ csrf_field() }}
                                <div class="col-lg-6">
                                    <img src="{{ asset('avatar/'.$driver->profilepicture) }}" style="width: 30%" class="img-responsive center-block" alt=""><br>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Profile Picture
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <label class="btn btn-primary">
                                                <input type="file" name="profilepicture" accept="image/*" class="form-control">
                                                <i class="fa fa-photo"></i> Add Photo
                                            </label>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            First name
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="firstname" placeholder="First name" value="{{ $driver->firstname }}" class="form-control"  >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Last name
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="lastname" placeholder="Last name" value="{{ $driver->lastname }}"  class="form-control"  >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Age
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input name="age" type="date" min="18" max="65" value="{{ $driver->age }}"  class="form-control" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Marital Status
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <select name="marital_status" class="form-control" >
                                                <option value="{{ $driver->marital_status }}" >{{ $driver->marital_status }}</option>
                                                <option value="Married">Married</option>
                                                <option value="Single">Single</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Religion
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <select name="religion" class="form-control">
                                                <option value="{{ $driver->religion }}" >{{ $driver->religion }}</option>
                                                <option value="Christianity">Christianity</option>
                                                <option value="Islam">Islam</option>
                                                <option value="None">None</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Years of Experience
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <textarea name="experience" rows="5" placeholder="Experience" class="form-control">
                                                {{ $driver->experience }}
                                            </textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Phone number
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="tel" name="phonenumber" value="{{ $driver->phonenumber }}"  placeholder="09082818382" class="form-control"  >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Mobile number
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="tel" name="mobilenumber" value="{{ $driver->mobilenumber }}"  placeholder="09082818382" class="form-control"  >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Email
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="email" name="email" value="{{ $driver->email }}"  placeholder="Email Address" class="form-control"  >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Home Address
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="homeaddress" value="{{ $driver->homeaddress }}"  placeholder="13, Mokola Street" class="form-control"  >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Location
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <select name="location" class="form-control">
                                                <option value="{{ $driver->location }}" >Choose A State</option>
                                                @foreach($states as $state)
                                                    <option value="{{ $state }}">{{ $state }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Residence
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" value="{{ $driver->residence }}"  name="residence" placeholder="Ikeja" class="form-control" >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Account number
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="accountnumber" value="{{ $driver->accountnumber }}"  placeholder="0023381833" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Bank Name
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="bank_name" value="{{ $driver->bank_name }}"  placeholder="E.g First Bank" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            What Can You Drive?
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <select name="can_drive" class="form-control">
                                                <option value="{{ $driver->can_drive }}" >{{ $driver->can_drive }}</option>
                                                <option>Cars</option>
                                                <option>Trucks</option>
                                                <option>Vans</option>
                                                <option>Cars and Trucks</option>
                                                <option>Cars and Vans</option>
                                                <option>Cars, Trucks and Vans</option>
                                            </select>
                                        </div>  
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Drivers license number
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="license" value="{{ $driver->license }}" class="form-control">

                                            </select>
                                        </div>  
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Lasdri number
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="lasdri" value="{{ $driver->lasdri }}" class="form-control">
                                            
                                        </div>  
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            License Expiry Date
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="date" name="license_date" value="{{ $driver->license_date }}" class="form-control">   
                                        </div>  
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Lasdri Expiry Date
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="date" name="lasdri_date" value="{{ $driver->lasdri_date }}" class="form-control">   
                                        </div>  
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Gender
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <select name="gender" class="form-control" >
                                               <option name = "male" value="Male">Male</option>
                                                <option name = "female" value="Female">Female</option>
                                            </select>
                                        </div>
                                        </div>
                                        <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Hair color
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="hair_color" value="{{ $driver->hair_color }}" class="form-control">   
                                        </div> </div>
                                         <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Height
                                            </label>

                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="height" value="{{ $driver->height }}" class="form-control">   
                                        </div>  
                                        </div>
                                         <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Width
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="width" value="{{ $driver->width }}" class="form-control">   
                                        </div>  
                                        </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Uniform Size
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <select name="uniform_size" class="form-control" >
                                               <option name = "medium" value="Medium">Medium</option>
                                               <option name = "large" value="Large">Large</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Educational Profile
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="educational_profile" value="{{ $driver->educational_profile }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Father's Name
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="father_name" value="{{ $driver->father_name }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Father's Phone Number
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="tel" name="father_number" value="{{ $driver->father_number }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Mother's Name
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="mother_name" value="{{ $driver->mother_name }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Mother's Phone Number
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="tel" name="mother_number" value="{{ $driver->mother_number }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Wife's Name
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="wife_name" value="{{ $driver->wife_name }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Wife's Phone Number
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="tel" name="wife_number" value="{{ $driver->wife_number }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Sister-in-law's Name
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="sisterInLaw_name" value="{{ $driver->sisterInLaw_name }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Sister-in-law's Phone Number
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="tel" name="sisterInLaw_number" value="{{ $driver->sisterInLaw_number }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Father-in-law's Name
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="fatherInLaw_name" value="{{ $driver->fatherInLaw_name }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Father-in-law's Phone Number
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="tel" name="fatherInLaw_number" value="{{ $driver->fatherInLaw_number }}" class="form-control">   
                                        </div>  
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Uncle-in-law's Name
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="uncleInLaw_name" value="{{ $driver->uncleInLaw_name }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Uncle-in-law's Phone Number
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="tel" name="uncleInLaw_number" value="{{ $driver->uncleInLaw_number }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Aunty-in-law's Name
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="auntyInLaw_name" value="{{ $driver->auntyInLaw_name }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Aunty-in-law's Phone Number
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="tel" name="auntyInLaw_number" value="{{ $driver->auntyInLaw_number }}" class="form-control">   
                                        </div>  
                                    </div>  
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Brother-in-law's Name
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="brotherInLaw_name" value="{{ $driver->brotherInLaw_name }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Brother-in-law's Phone Number
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="tel" name="brotherInLaw_number" value="{{ $driver->brotherInLaw_number }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Uncle's Name
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="uncle_name" value="{{ $driver->uncle_name }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Uncle's Phone Number
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="tel" name="uncle_number" value="{{ $driver->uncle_number }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Neighbour's Name
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="neighbour_name" value="{{ $driver->neighbour_name }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Neighbour's Phone Number
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="tel" name="neighbour_number" value="{{ $driver->neighbour_number }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Brother's Name
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="brother_name" value="{{ $driver->brother_name }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Brother's Phone Number
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="tel" name="brother_number" value="{{ $driver->brother_number }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Sister's Name
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="sister_name" value="{{ $driver->sister_name }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Sister's Phone Number
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="tel" name="sister_number" value="{{ $driver->sister_number }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Uniform Size
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <select name="uniform_size" class="form-control" >
                                                   <option name = "" value="{{ $driver->uniform_size }}">{{ $driver->uniform_size }}</option>
                                                   <option name = "medium" value="Medium">Medium</option>
                                                   <option name = "large" value="Large">Large</option>
                                                </select>
                                            </div>
                                        </div>
                                        <h4 align="center">Communication skills</h4>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                English
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <select name="english" class="form-control" >
                                                   <option name = "" value="{{ $driver->english }}">{{ $driver->english }}</option>
                                                   <option name = "Nil" value="Fair">Nil</option>
                                                   <option name = "good" value="Good">Good</option>
                                                   <option name = "Proficient" value="Proficient">Proficient</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Yoruba
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <select name="yoruba" class="form-control" >
                                                   <option name = "" value="{{ $driver->yoruba }}">{{ $driver->yoruba }}</option>
                                                   <option name = "Nil" value="Nil">Nil</option>
                                                   <option name = "good" value="Good">Good</option>
                                                   <option name = "Proficient" value="Proficient">Proficient</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Igbo
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <select name="igbo" class="form-control" >
                                                        <option name = "" value="{{ $driver->igbo }}">{{ $driver->igbo }}</option>
                                                   <option name = "Nil" value="Nil">Nil</option>
                                                   <option name = "good" value="Good">Good</option>
                                                   <option name = "Proficient" value="Proficient">Proficient</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Hausa
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <select name="hausa" class="form-control" >
                                                   <option name = "" value="{{ $driver->hausa }}">{{ $driver->hausa }}</option>
                                                   <option name = "Nil" value="Nil">Nil</option>
                                                   <option name = "good" value="Good">Good</option>
                                                   <option name = "Proficient" value="Proficient">Proficient</option>
                                                </select>
                                            </div>
                                        </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Past/Current Employer's name
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="employer_name" value="{{ $driver->employer_name }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3">
                                            Past/Current Employer's Phone Number
                                        </label>
                                        <div class="col-md-9 col-sm-9">
                                            <input type="text" name="employer_number" value="{{ $driver->employer_number }}" class="form-control">   
                                        </div>  
                                    </div> 
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <button class="btn btn-primary pull-right">Update</button>
                                        </div>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
@endsection