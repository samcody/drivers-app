@extends('layouts.admin')

@section('title','MatchMail')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">Match Driver To Client</h4>
                            <div class="row">
                                @include('partial.alert')
                                <h3 class="text-center">Number Of Drivers To Match is {{ $booking->number_of_driver_unmatched }}</h3>
                                <br>
                                <form class="form-horizontal" action="{{ url('booking/uber/match',['id' => $booking->id]) }}" method="post" role="form">
                                    {{ csrf_field() }}
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                               Select Uber Driver
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <select name="driver_id[]" class="form-control" multiple>
                                                    <option value="">Choose A Driver</option>
                                                    @foreach($drivers as $driver)
                                                        <option value="{{ $driver->id }}">{{ ucfirst($driver->firstname) }} {{ ucfirst($driver->lastname) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-10">
                                                <button class="btn btn-primary pull-right">Submit</button>
                                            </div>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection