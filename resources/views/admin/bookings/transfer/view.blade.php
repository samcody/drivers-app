@extends('layouts.admin')

@section('title',' Uber Booking')

@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-8">
                        <div class="card-box">

                            {{-- first table--}}
                            <div class="table-rep-plugin">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table  table-striped">
                                        <thead>
                                        <tr>
                                            <th>Booking ID</th>
                                            <th>Package</th>
                                            <th>Request Date</th>
                                            <th>Payment Status</th>
                                            <th>Matched Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ $booking->booking_id }}</td>
                                            <td>{{ $booking->transfer_type }}</td>
                                            <td>{{ date_format($booking->created_at,'d-m-Y') }}</td>
                                            @if($booking->has_paid == true)
                                                <td><button class="btn btn-success">Paid</button></td>
                                            @else
                                                <td><button class="btn btn-warning">Not Paid</button></td>
                                            @endif
                                            @if($booking->is_matched == true)
                                                <td><button class="btn btn-success">Matched</button></td>
                                            @else
                                                <td><button class="btn btn-primary">Not Matched</button></td>
                                            @endif
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <br>
                            {{-- second--}}
                            <div class="table-rep-plugin">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table  table-striped">
                                        <thead>
                                        <tr>
                                            <th>No of Persons</th>
                                            <th>No of Luggage</th>
                                            <th>Resumption Time</th>
                                            <th>Vehicle Type</th>
                                            <th>Number Of Vehicles</th>
                                            <th>Transfer Type</th>
                                            <th>Transfer Mode</th>
                                            <th>Start Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ $booking->no_of_person }}</td>
                                            <td>{{ $booking->no_of_luggage }}</td>
                                            <td>{{ $booking->resumption_time }}</td>
                                            <td>{{ $booking->vehicle_type }}</td>
                                            <td>{{ $booking->no_of_vehicle }}</td>
                                            <td>{{ $booking->transfer_type }}</td>
                                            <td>{{ $booking->transfer_mode }}</td>
                                            <td>{{ $booking->start_date }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <br>

                            <div class="table-rep-plugin">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table  table-striped">
                                        <thead>
                                        <tr>
                                            <th>End Date</th>
                                            <th>Start State</th>
                                            <th>End State</th>
                                            <th>Pick Up Point</th>
                                            <th>Drop Off Point</th>
                                            <th>Govt. ID No.</th>
                                            <th>Amount</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ $booking->end_date }}</td>
                                            <td>{{ $booking->start_state }}</td>
                                            <td>{{ $booking->end_state }}</td>
                                            <td>{{ $booking->pick_up_point }}</td>
                                            <td>{{ $booking->drop_off_point }}</td>
                                            <td>{{ $booking->govt_id_no }}</td>
                                            <td>{{ $booking->amount }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <br>

                            <div class="table-rep-plugin">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table  table-striped">
                                        <thead>
                                        <tr>
                                            <th>Document Name</th>
                                            <th>Download</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>SLA Document</td>
                                            <td><a class="btn btn-primary" href="{{ asset('files/'.$booking->sla_document) }}" download="{{ $booking->sla_document }}">Download Service Agreement Doc</a></td>
                                        </tr>
                                        <tr>
                                            <td>Guarantor's Form</td>
                                            <td><a class="btn btn-primary" href="{{ asset('files/'.$booking->guarantor_form) }}" download="{{ $booking->guarantor_form }}">Download Guarantor's Form</a></td>
                                        </tr>
                                        <tr>
                                            <td>Client Invoice</td>
                                            <td><a class="btn btn-primary" href="{{ asset('files/'.$booking->transfer_document) }}" download="{{ $booking->transfer_document }}">Download Transfer Document</a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            {{-- fourth --}}
                            @if($booking->vehicle_documents == null)
                            @else
                            <div class="table-rep-plugin">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table  table-striped">
                                        <thead>
                                        <tr>
                                            <th>Document Name</th>
                                            <th>Download</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach(json_decode($booking->vehicle_documents) as $doc)
                                        <tr>
                                            <td>{{ $doc }}</td>
                                            <td><a href="{{ asset('vehicle-docs/'.$doc) }}" download="{{ $doc }}" class="btn btn-primary">Download</a></td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            @endif

                            {{-- fifth --}}
                            @if($booking->luggage_image == null)
                            @else
                                <div class="table-rep-plugin">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <table id="tech-companies-1" class="table  table-striped">
                                            <thead>
                                            <tr>
                                                <th>Picture</th>
                                                <th>Download</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Luggage Image</td>
                                                    <td><a href="{{ asset('transfer/'.$booking->luggage_image) }}" download="{{ $booking->luggage_image }}" class="btn btn-primary">Download</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">Driver Requester</h4>

                            <ul class="list-group m-b-0 user-list">
                                <li class="list-group-item">
                                    <div class="u">
                                        <p>{{ $booking->user->full_name }}</p>
                                        <p>{{ $booking->user->email }}</p>
                                        <p>{{ $booking->user->mobile_number }}</p>
                                    </div>
                                    <div>
                                        @if($booking->is_matched == true)
                                            <a href="#" class="btn btn-primary" disabled="">Already Matched</a>
                                        @else
                                            <a href="{{ url('booking/transfer/match',['id' => $booking->id]) }}" class="btn btn-primary">Match Driver</a>
                                        @endif
                                    </div>
                                </li>
                            </ul>
                        </div>

                        {{--<div class="card-box">--}}
                            {{--<h4 class="header-title m-t-0 m-b-30"><i class="zmdi zmdi-notifications-none m-r-5"></i>  Matched Driver</h4>--}}

                            {{--@if($booking->is_matched == true)--}}
                                {{--<ul class="list-group m-b-0 user-list">--}}

                                    {{--<li class="list-group-item">--}}
                                        {{--<a href="#" class="user-list-item">--}}
                                            {{--<div class="avatar text-center">--}}
                                                {{--<i class="zmdi zmdi-circle text-primary"></i>--}}
                                            {{--</div>--}}
                                            {{--<div class="user-desc">--}}
                                                {{--<span class="name">{{ $booking->driver->firstname }} {{ $booking->driver->lastname }}</span>--}}
                                                {{--<span class="desc">{{ $booking->driver->email }}</span>--}}
                                                {{--<span class="desc">{{ $booking->driver->phonenumber }}</span>--}}
                                            {{--</div>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}

                                {{--</ul>--}}
                            {{--@else--}}
                                {{--<div>--}}
                                    {{--<p class="text-center">No Driver Matched Yet</p>--}}
                                {{--</div>--}}
                            {{--@endif--}}
                        {{--</div>--}}

                    </div>
                </div>
            </div>
        </div>
@endsection