@extends('layouts.admin')

@section('title','Pay Per Use Drivers Bookings')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="table-rep-plugin">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    @include('partial.alert')
                                    @if(count($bookings) < 1)
                                        <br><br>
                                        <div class="alert alert-info text-center">
                                            <p>No Booking has been made</p>
                                        </div>
                                    @else
                                        <table id="tech-companies-1" class="table  table-striped">
                                            <thead>
                                            <tr>
                                                <th>Booking ID</th>
                                                <th>Package</th>
                                                <th>Request Date</th>
                                                <th>Payment Status</th>
                                                <th>Matched Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($bookings as $booking)
                                                <tr>
                                                    <td>{{ $booking->booking_id }}</td>
                                                    <td>{{ $booking->transfer_type }}</td>
                                                    <td>{{ date_format($booking->created_at,'d-m-Y') }}</td>
                                                    @if($booking->has_paid == true)
                                                        <td><button class="btn btn-success">Paid</button></td>
                                                    @else
                                                        <td><button class="btn btn-warning">Not Paid</button></td>
                                                    @endif
                                                    @if($booking->is_matched == true)
                                                        <td><button class="btn btn-success">Matched</button></td>
                                                    @else
                                                        <td><button class="btn btn-primary">Not Matched</button></td>
                                                    @endif
                                                    <td>
                                                        @if($booking->is_matched == false)
                                                        <a href="{{ url('booking/pay-use/match',['id' => $booking->id]) }}" class="btn btn-primary">Match Driver</a>
                                                        @elseif($booking->number_of_driver_unmatched != 0)
                                                            <a href="{{ url('booking/pay-use/match',['id' => $booking->id]) }}" class="btn btn-primary">Match Drivers({{ $booking->number_of_driver_unmatched }})</a>
                                                        @else
                                                            <a href="#" class="btn btn-success"> Driver Matched</a>
                                                        @endif
                                                        @if($booking->has_paid == false)
                                                        <a data-toggle="modal" data-target="#mod-success{{$booking->id}}" href="#" class="btn btn-warning">Confirm Client Payment</a>
                                                        @else
                                                        <a data-toggle="modal" data-target="#mod-warning{{$booking->id}}" href="#" class="btn btn-danger">UnConfirm Client Payment</a>
                                                        @endif
                                                        <a href="{{ url('booking/pay-use/view',['id' => $booking->id]) }}" class="btn btn-info">View</a>
                                                        <a href="{{ url('booking/pay-use/upload',['id' => $booking->id]) }}" class="btn btn-primary">Upload Client Docs</a>
                                                        <a data-toggle="modal" data-target="#mod-danger{{$booking->id}}" href="#" class="btn btn-danger">Delete</a>
                                                    </td>
                                                </tr>
                                                <div id="mod-success{{$booking->id}}" tabindex="-1" role="dialog" style="" class="modal fade">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="text-center">
                                                                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                                                                    <h3>Warning!</h3>
                                                                    <p>You are about to confirm payment of {{ $booking->user->full_name }} for Pay Per Use driver request.</p>
                                                                    <div class="xs-mt-50">
                                                                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Cancel</button>
                                                                        <a href="{{ url('booking/pay-use/payment',['id' => $booking->id]) }}" class="btn btn-space btn-danger">Confirm Payment</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="mod-warning{{$booking->id}}" tabindex="-1" role="dialog" style="" class="modal fade">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="text-center">
                                                                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                                                                    <h3>Warning!</h3>
                                                                    <p>You are about to unconfirm payment of {{ $booking->user->full_name }} for Pay Per Use driver request.</p>
                                                                    <div class="xs-mt-50">
                                                                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Cancel</button>
                                                                        <a href="{{ url('booking/pay-use/payment/unconfirm',['id' => $booking->id]) }}" class="btn btn-space btn-danger">UnConfirm Payment</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="mod-danger{{$booking->id}}" tabindex="-1" role="dialog" style="" class="modal fade">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="text-center">
                                                                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                                                                    <h3>Warning!</h3>
                                                                    <p>You are about to delete a Pay Per Use driver request.</p>
                                                                    <div class="xs-mt-50">
                                                                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Cancel</button>
                                                                        <a href="{{ url('booking/pay-use/delete',['id' => $booking->id]) }}" class="btn btn-space btn-danger">Delete</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                                {{ $bookings->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection