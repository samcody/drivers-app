@extends('layouts.admin')

@section('title',' Pay Per Use Booking')

@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-8">
                        <div class="card-box">

                            {{-- first table--}}
                            <div class="table-rep-plugin">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table  table-striped">
                                        <thead>
                                        <tr>
                                            <th>Booking ID</th>
                                            <th>Package</th>
                                            <th>Request Date</th>
                                            <th>Payment Status</th>
                                            <th>Matched Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ $booking->booking_id }}</td>
                                            <td>{{ $booking->transfer_type }}</td>
                                            <td>{{ date_format($booking->created_at,'d-m-Y') }}</td>
                                            @if($booking->has_paid == true)
                                                <td><button class="btn btn-success">Paid</button></td>
                                            @else
                                                <td><button class="btn btn-warning">Not Paid</button></td>
                                            @endif
                                            @if($booking->is_matched == true)
                                                <td><button class="btn btn-success">Matched</button></td>
                                            @else
                                                <td><button class="btn btn-primary">Not Matched</button></td>
                                            @endif
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <br>
                            {{-- second--}}
                            <div class="table-rep-plugin">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table  table-striped">
                                        <thead>
                                        <tr>
                                            <th>Number Of Drivers</th>
                                            <th>Resumption Date</th>
                                            <th>Use Type</th>
                                            <th>State Of Residence</th>
                                            <th>Vehicle Type</th>
                                            <th>Closing Time</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ $booking->number_of_driver }}</td>
                                            <td>{{ $booking->resumption_date }}</td>
                                            <td>{{ $booking->use_type }}</td>
                                            <td>{{ $booking->state_of_residence }}</td>
                                            <td>{{ $booking->vehicle_type }}</td>
                                            <td>{{ $booking->closing_time }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <br>
                            {{-- third--}}
                            <div class="table-rep-plugin">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table  table-striped">
                                        <thead>
                                        <tr>
                                            <th>Nearest Location</th>
                                            <th>Driver Gender</th>
                                            <th>Transmission</th>
                                            <th>Salary Package</th>
                                            <th>Salary Frequency</th>
                                            <th>Driver Accommodation</th>
                                            <th>Insurance Type</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ $booking->nearest_location }}</td>
                                            <td>{{ $booking->driver_gender }}</td>
                                            <td>{{ $booking->transmission }}</td>
                                            <td>{{ number_format($booking->salary_package) }}</td>
                                            <td>{{ $booking->salary_frequency }}</td>
                                            <td>{{ $booking->provide_driver_accommodation }}</td>
                                            <td>{{ $booking->insurance_type }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <br>

                            <div class="table-rep-plugin">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table  table-striped">
                                        <thead>
                                        <tr>
                                            <th>Document Name</th>
                                            <th>Download</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>SLA Document</td>
                                            <td><a class="btn btn-primary" href="{{ asset('files/'.$booking->sla_document) }}" download="{{ $booking->sla_document }}">Download Service Agreement Doc</a></td>
                                        </tr>
                                        <tr>
                                            <td>Guarantor's Form</td>
                                            <td><a class="btn btn-primary" href="{{ asset('files/'.$booking->guarantor_form) }}" download="{{ $booking->guarantor_form }}">Download Guarantor's Form</a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">Driver Requester</h4>

                            <ul class="list-group m-b-0 user-list">
                                <li class="list-group-item">
                                    <div class="u">
                                        <p>{{ $booking->user->full_name }}</p>
                                        <p>{{ $booking->user->email }}</p>
                                        <p>{{ $booking->user->mobile_number }}</p>
                                    </div>
                                    <div>
                                        @if($booking->is_matched == true)
                                            <a href="#" class="btn btn-primary" disabled="">Already Matched</a>
                                        @else
                                            <a href="{{ url('booking/transfer/match',['id' => $booking->id]) }}" class="btn btn-primary">Match Driver</a>
                                        @endif
                                    </div>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection