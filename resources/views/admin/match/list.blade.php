@extends('layouts.admin')

@section('title','Matches')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="table-rep-plugin">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    @if(count($matches) < 1)
                                        <br><br>
                                        <div class="alert alert-info text-center">
                                            <p>No Match has been made</p>
                                        </div>
                                    @else
                                        <table id="tech-companies-1" class="table  table-striped">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Booking Type</th>
                                                <th>Client</th>
                                                <th>Client Mobile</th>
                                                {{--<th>Assigned Driver</th>--}}
                                                {{--<th>Driver Mobile</th>--}}
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($matches as $match)
                                                <tr>
                                                    <td>#</td>
                                                    <td>{{ $match->booking_type }}</td>
                                                    <td>{{ $match->user->full_name }}</td>
                                                    <td>{{ $match->user->mobile_number }}</td>
{{--                                                    <td>{{ $match->driver->phonenumber }}</td>--}}
                                                    @if($match->status == true)
                                                        <td><button class="btn btn-success">Active</button></td>
                                                    @else
                                                        <td><button class="btn btn-danger">InActive</button></td>
                                                    @endif
                                                    <td>
                                                        <a data-toggle="modal" data-target="#mod-danger{{$match->id}}" href="#" class="btn btn-danger">Delete</a>
                                                    </td>
                                                </tr>
                                                <div id="mod-danger{{$match->id}}" tabindex="-1" role="dialog" style="" class="modal fade">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="text-center">
                                                                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                                                                    <h3>Warning!</h3>
                                                                    <p>You are about to delete a match.</p>
                                                                    <div class="xs-mt-50">
                                                                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Cancel</button>
                                                                        <a href="{{ url('match/delete',['id' => $match->id]) }}" class="btn btn-space btn-danger">Delete</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                                {{ $matches->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection