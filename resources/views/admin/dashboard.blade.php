@extends('layouts.admin')

@section('title','Dashboard')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="card-box text-center">
                            <h4 class="header-title m-t-0 m-b-30">Users</h4>
                            <div class="widget-chart-3">
                                <div class="widget-detail-1 ">
                                    <h2 class="p-t-10 m-b-0"><a href="{{ url('users/list') }}">{{ count($users) }}</a> </h2>
                                    <p class="text-muted">Registered</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="card-box text-center">
                            <h4 class="header-title m-t-0 m-b-30">Short Time Drivers</h4>
                            <div class="widget-chart-3">
                                <div class="widget-detail-1 ">
                                    <h2 class="p-t-10 m-b-0"><a href="{{ url('short-time/list') }}">{{ count($shortTimeDrivers) }}</a> </h2>
                                    <p class="text-muted">Registered</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="card-box text-center">
                            <h4 class="header-title m-t-0 m-b-30">Full Time Drivers</h4>
                            <div class="widget-chart-3">
                                <div class="widget-detail-1 ">
                                    <h2 class="p-t-10 m-b-0"><a href="{{ url('full-time/list') }}">{{ count($fullTimeDrivers) }}</a> </h2>
                                    <p class="text-muted">Registered</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="card-box text-center">
                            <h4 class="header-title m-t-0 m-b-30">Uber Drivers</h4>
                            <div class="widget-chart-3">
                                <div class="widget-detail-1 ">
                                    <h2 class="p-t-10 m-b-0"><a href="{{ url('uber/list') }}">{{ count($uberDrivers) }}</a> </h2>
                                    <p class="text-muted">Registered</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="card-box text-center">
                            <h4 class="header-title m-t-0 m-b-30">Short Time Bookings</h4>
                            <div class="widget-chart-3">
                                <div class="widget-detail-1 ">
                                    <h2 class="p-t-10 m-b-0"><a href="{{ url('bookings/short-time') }}">{{ count($shortTimeBookings) }}</a> </h2>
                                    <p class="text-muted">Made</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="card-box text-center">
                            <h4 class="header-title m-t-0 m-b-30">Full Time Bookings</h4>
                            <div class="widget-chart-3">
                                <div class="widget-detail-1 ">
                                    <h2 class="p-t-10 m-b-0"><a href="{{ url('bookings/full-time') }}">{{ count($fullTimeBookings) }}</a> </h2>
                                    <p class="text-muted">Made</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="card-box text-center">
                            <h4 class="header-title m-t-0 m-b-30">Uber Bookings</h4>
                            <div class="widget-chart-3">
                                <div class="widget-detail-1 ">
                                    <h2 class="p-t-10 m-b-0"><a href="{{ url('bookings/uber') }}">{{ count($uberBookings) }}</a> </h2>
                                    <p class="text-muted">Made</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
@endsection