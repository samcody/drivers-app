@extends('layouts.admin')

@section('title','Payments')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="table-rep-plugin">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    @if(count($payments) < 1)
                                        <br><br>
                                        <div class="alert alert-info text-center">
                                            <p>No Payments made via Paystack</p>
                                        </div>
                                    @else
                                        <table id="tech-companies-1" class="table  table-striped">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Booking Type</th>
                                                <th>Client</th>
                                                <th>Client Mobile</th>
                                                <th>Ammount</th>
                                                <th>Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($payments as $payment)
                                                <tr>
                                                    <td>#</td>
                                                    <td>{{ $payment->hire_type }}</td>
                                                    <td>{{ $payment->user->full_name }}</td>
                                                    <td>{{ $payment->user->mobile_number }}</td>
                                                    <td>{{ $payment->amount }}</td>
                                                    <td>{{ date_format($payment->created_at,'d-m-Y') }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                                {{ $payments->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection