@extends('layouts.admin')

@section('title','Feedback')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">View Feedback By {{ $feedback->name }}</h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>Client: {{ $feedback->name }}</p>
                                    <p>Client's Mobile Number: {{ $feedback->phone }}</p>
                                    <p>Client's Email: {{ $feedback->email }}</p>
                                    <p>Client's Dissatisfaction: {{ $feedback->dissatisfied }}</p>
                                    <p>Waus our service has been helpful: {{ $feedback->helped }}</p>
                                    <p>Client's Support : {{ $feedback->support }}</p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection