@extends('layouts.admin')

@section('title','Feedback')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="table-rep-plugin">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    @if(count($feedbacks) < 1)
                                        <br><br>
                                        <div class="alert alert-info text-center">
                                            <p>No Feedback yet!</p>
                                        </div>
                                    @else
                                        <table id="tech-companies-1" class="table  table-striped">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Date</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($feedbacks as $feedback)
                                                <tr>
                                                    <td>#</td>
                                                    <td>{{ $feedback->name }}</td>
                                                    <td>{{ $feedback->email }}</td>
                                                    <td>{{ $feedback->phone}}</td>
                                                    <td>{{ date_format($feedback->created_at,'d-m-Y') }}</td>
                                                    <td><a href="{{ url('feedback',['id' => $feedback->id]) }}" class="btn btn-primary">View</a></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                                {{ $feedbacks->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection