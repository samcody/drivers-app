@extends('layouts.admin')

@section('title','Partnership')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="table-rep-plugin">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    @if(count($partners) < 1)
                                        <br><br>
                                        <div class="alert alert-info text-center">
                                            <p>No Partnership yet!</p>
                                        </div>
                                    @else
                                        <table id="tech-companies-1" class="table  table-striped">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Partner Name</th>
                                                <th>Partner City</th>
                                                <th>Partnership Purpose</th>
                                                <th>Investment Price</th>
                                                <th>Date</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($partners as $partner)
                                                <tr>
                                                    <td>#</td>
                                                    <td>{{ $partner->name }}</td>
                                                    <td>{{ $partner->city }}</td>
                                                    <td>{{ $partner->partnership_purpose }}</td>
                                                    <td>{{ $partner->investment_price }}</td>
                                                    <td>{{ date_format($partner->created_at,'d-m-Y') }}</td>
                                                    <td><a href="{{ url('partner',['id' => $partner->id]) }}" class="btn btn-primary">View</a></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                                {{ $partners->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection