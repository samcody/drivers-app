@extends('layouts.admin')

@section('title','Partnership')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">View Partnership By {{ $partner->name }}</h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>Partner: {{ $partner->name }}</p>
                                    <p>Partner's Mobile Number: {{ $partner->phone }}</p>
                                    <p>Partner's City: {{ $partner->city }}</p>
                                    <p>Partner's Nationality: {{ $partner->nationality }}</p>
                                    <p>Partner's Country: {{ $partner->country }}</p>
                                    <p>Partnership purpose : {{ $partner->partnership_purpose }}</p>
                                    <p>Partner's Investment Price: {{ $partner->investment_price }}</p>
                                    <p>Partner's Investor Background: {{ $partner->investor_background }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection