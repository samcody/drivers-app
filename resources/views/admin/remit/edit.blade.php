@extends('layouts.admin')

@section('title','New ')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">Create New </h4>
                            <div class="row">
@include('partial.alert')
                                    <form class="form-horizontal" action="{{ url('remit/edit',['id' => $remit->id]) }}" method="post" enctype="multipart/form-data" role="form">
                                        {{ csrf_field() }}
                                       
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    First name
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <input type="text" name="firstname" value="{{ $remit->first_name }}" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Last name
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <input type="text" name="lastname" value="{{ $remit->last_name }}" class="form-control" required>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Phone number
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <input type="tel" name="phonenumber" value="{{ $remit->mobile_number }}" class="form-control" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    email
                                                </label>
                                                <div class="col-md-9 col-sm-9">
                                                    <input type="email" name="email" placeholder="{{ $remit->email }}" class="form-control" disabled>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <div class="col-md-10">
                                                    <button class="btn btn-primary pull-right">Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection