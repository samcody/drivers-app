@extends('layouts.admin')

@section('title','Remitance')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="table-rep-plugin">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    @include('partial.alert')
                                    @if(count($report) < 1)
                                        <br><br>
                                        <div class="alert alert-info text-center">
                                            <p>No report has been created</p>
                                        </div>
                                    @else
                                        <table id="tech-companies-1" class="table  table-striped">
                                            <thead>
                                            <tr>
                                                <th>User</th>
                                                <th>Driver</th>
                                                <th>Week</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($report as $rem)
                                                <tr>
                                    
                                                    <td>{{ $user->first_name.' '. $user->last_name }}</td>
                                                    <td>{{ $rem->drivers_name }}</td>
                                                    <td>{{ $rem->week }}</td>
                                                    <td>
                                                        <a href="{{ url('report/edit',['id' => $rem->id]) }}" class="btn btn-info">Edit</a>
                                                        <a href="{{ url('report/pdf',['id' => $rem->id]) }}" class="btn btn-primary">Download Report</a>
                                                        <a href="{{ url('report/sendmail',['id' => $rem->id]) }}" class="btn btn-primary">Send Report</a>
                                                        <a data-toggle="modal" data-target="#mod-danger{{$rem->id}}" href="#" class="btn btn-danger">Delete</a>
                                                    </td>
                                                </tr>
                                                <div id="mod-danger{{$rem->id}}" tabindex="-1" role="dialog" style="" class="modal fade">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="text-center">
                                                                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                                                                    <h3>Warning!</h3>
                                                                    <p>You are about to delete a user.</p>
                                                                    <div class="xs-mt-50">
                                                                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Cancel</button>
                                                                        <a href="{{ url('report/delete',['id' => $rem->id]) }}" class="btn btn-space btn-danger">Delete</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                                {{ $report->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection