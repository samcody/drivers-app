@extends('layouts.admin')

@section('title','Diamond Report')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <a href="{{ url('remit/new') }}" class="btn btn-primary">Add User</a>
                            <div class="table-rep-plugin">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    @include('partial.alert')
                                    @if(count($remit) < 1)
                                        <br><br>
                                        <div class="alert alert-info text-center">
                                            <p>No User has been added</p>
                                        </div>
                                    @else
                                        <table id="tech-companies-1" class="table  table-striped">
                                            <thead>
                                            <tr>
                                                <th>Full Name</th>
                                                <th>Email Address</th>
                                                <th>Phone</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($remit as $rem)
                                                <tr>
                                    
                                                    <td>{{ $rem->first_name.' '. $rem->last_name }}</td>
                                                    <td>{{ $rem->email }}</td>
                                                    <td>{{ $rem->mobile_number }}</td>
                                                    <td>
                                                        <a href="{{ url('remit/edit',['id' => $rem->id]) }}" class="btn btn-info">Edit</a>
                                                        <a href="{{ url('report/new',['id' => $rem->id]) }}" class="btn btn-primary">Create Report</a>
                                                        <a href="{{ url('report/list',['id' => $rem->id]) }}" class="btn btn-primary">View Reports</a>
                                                        <a data-toggle="modal" data-target="#mod-danger{{$rem->id}}" href="#" class="btn btn-danger">Delete</a>
                                                    </td>
                                                </tr>
                                                <div id="mod-danger{{$rem->id}}" tabindex="-1" role="dialog" style="" class="modal fade">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="text-center">
                                                                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                                                                    <h3>Warning!</h3>
                                                                    <p>You are about to delete a user.</p>
                                                                    <div class="xs-mt-50">
                                                                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Cancel</button>
                                                                        <a href="{{ url('remit/delete',['id' => $rem->id]) }}" class="btn btn-space btn-danger">Delete</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                                {{ $remit->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection