@extends('layouts.admin')

@section('title','New ')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">Create New report </h4>
                            <div class="row">
@include('partial.alert')
                            <form class="form-horizontal" action="{{ url('report/new/') }}{{'/'}}{{$remit->id }}" method="post" enctype="multipart/form-data" role="form">
                                        {{ csrf_field() }}
                                       
                                            <div class="form-group">
                                                <label class="control-label col-md-1 col-sm-1">
                                                    Week
                                                </label>
                                                <div class="col-md-2 col-sm-2">
                                                    <input type="number" name="week" class="form-control">
                                                </div>
                                                <label class="control-label col-md-1 col-sm-1">
                                                    Month
                                                </label>
                                                <div class="col-md-2 col-sm-2">
                                                    <input type="text" name="month" class="form-control">
                                                </div>
                                                <label class="control-label col-md-1 col-sm-1">
                                                    Week days
                                                </label>
                                                <div class="col-md-2 col-sm-2">
                                                    <input type="text" name="week_days" class="form-control">
                                                </div>
                                                <label class="control-label col-md-1 col-sm-1">
                                                    Number of Days Worked
                                                </label>
                                                <div class="col-md-2 col-sm-2">
                                                    <input type="number" name="days_worked" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Driver's Name
                                                </label>
                                                <div class="col-md-3 col-sm-3">
                                                    <input type="text" name="drivers_name" placeholder="Driver's Name" class="form-control" required>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Car Registration Number
                                                </label>
                                                <div class="col-md-3 col-sm-3">
                                                    <input type="text" name="reg_num" class="form-control" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Car Model
                                                </label>
                                                <div class="col-md-3 col-sm-3">
                                                    <input type="text" name="car_model" class="form-control" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Expected Returns
                                                </label>
                                                <div class="col-md-3 col-sm-3">
                                                    <input type="number" name="expected_returns" class="form-control" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Uber Returns
                                                </label>
                                                <div class="col-md-3 col-sm-3">
                                                    <input type="number" name="uber_returns" class="form-control" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Management fee
                                                </label>
                                                <div class="col-md-3 col-sm-3">
                                                    <input type="number" name="management_fee" class="form-control" required>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3">
                                                    Maintenance
                                                </label>
                                                <div class="col-md-3 col-sm-3">
                                                    <input type="number" name="maintenance" class="form-control" required>
                                                    <input type="hidden" name="remit_id" value="{{ $remit->id }}" required>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <div class="col-md-10">
                                                    <button class="btn btn-primary pull-right">Submit</button>
                                                </div>
                                            </div>

                                        </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection