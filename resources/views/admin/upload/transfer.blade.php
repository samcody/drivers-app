@extends('layouts.admin')

@section('title','Upload Guarantor Form For Uber Booking')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">Upload Guarantor Form To Client</h4>
                            <div class="row">
                                @include('partial.alert')
                                @if(!empty($booking->transfer_document))
                                    <p class="text-center alert alert-danger">Warning!!! You have uploaded invoice file to client already. Uploading another one will erase previous.</p>
                                @endif
                                <br>
                                <form class="form-horizontal" action="{{ url('booking/transfer/upload',['id' => $booking->id]) }}" enctype="multipart/form-data" method="post" role="form">
                                    {{ csrf_field() }}
                                    <div class="col-lg-6">

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Select Client's Price Breakdown file
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="file" name="transfer_document">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Amount
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="text" name="amount" value="{{ $booking->amount }}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-10">
                                                <button class="btn btn-primary pull-right">Submit</button>
                                            </div>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection