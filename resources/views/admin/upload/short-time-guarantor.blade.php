@extends('layouts.admin')

@section('title','Upload Guarantor Form For Full-Time Driver')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">Upload Guarantor Form To Client</h4>
                            <div class="row">
                                @include('partial.alert')
                                @if(!empty($driver->guarantors_form))
                                    <p class="text-center alert alert-danger">Warning!!! You have uploaded file to client already. Uploading another one will erase previous.</p>
                                @endif
                                <br>
                                <form class="form-horizontal" action="{{ url('short-time/guarantor',['id' => $driver->id]) }}" enctype="multipart/form-data" method="post" role="form">
                                    {{ csrf_field() }}
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Select Guarantor Form
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="file" name="guarantors_form">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-10">
                                                <button class="btn btn-primary pull-right">Submit</button>
                                            </div>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection