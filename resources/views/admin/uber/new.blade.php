@extends('layouts.admin')

@section('title','New Uber Driver')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">Create New Driver</h4>
                            <div class="row">
@include('partial.alert')
                                    <form class="form-horizontal" action="{{ url('uber/new') }}" method="post" enctype="multipart/form-data" role="form">
                                        {{ csrf_field() }}
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Profile Picture
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <label class="btn btn-primary">
                                                    <input type="file" name="profilepicture" accept="image/*" class="form-control" >
                                                    <i class="fa fa-photo"></i> Add Photo
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                First name
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="text" name="firstname" placeholder="First name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Last name
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="text" name="lastname" placeholder="Last name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Age
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input name="age" type="number" min="18" max="65" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Marital Status
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <select name="marital_status" class="form-control">
                                                    <option value="">Choose a value</option>
                                                    <option value="Married">Married</option>
                                                    <option value="Single">Single</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Religion
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <select name="religion" class="form-control">
                                                    <option value="">Choose a value</option>
                                                    <option value="Christianity">Christianity</option>
                                                    <option value="Islam">Islam</option>
                                                    <option value="None">None</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Phone number
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="tel" name="phonenumber" placeholder="09082818382" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Mobile number
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="tel" name="mobilenumber" placeholder="09082818382" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Experience
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <textarea name="experience" rows="5" placeholder="Experience" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Email
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="email" name="email" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Home Address
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="text" name="homeaddress" placeholder="13, Mokola Street" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Location
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <select name="location" class="form-control">
                                                    <option value="">Choose A State</option>
                                                    @foreach($states as $state)
                                                        <option value="{{ $state }}">{{ $state }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Residence
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="text" name="residence" placeholder="Ikeja" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Account number
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="text" name="accountnumber" placeholder="0023381833" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3">
                                                Bank Name
                                            </label>
                                            <div class="col-md-9 col-sm-9">
                                                <input type="text" name="bank_name" placeholder="E.g First Bank" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-10">
                                                <button class="btn btn-primary pull-right">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
@endsection