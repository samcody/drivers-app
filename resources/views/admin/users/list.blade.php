@extends('layouts.admin')

@section('title','Users')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="table-rep-plugin">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    @if(count($users) < 1)
                                        <br><br>
                                        <div class="alert alert-info text-center">
                                            <p>No Users yet!</p>
                                        </div>
                                    @else
                                        <table id="tech-companies-1" class="table  table-striped">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Full Name</th>
                                                <th>Mobile Number</th>
                                                <th>Email</th>
                                                <th>Date</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($users as $user)
                                                <tr>
                                                    <td>#</td>
                                                    <td>{{ $user->full_name }}</td>
                                                    <td>{{ $user->mobile_number }}</td>
                                                    <td>{{ $user->email }}</td>
                                                    <td>{{ date_format($user->created_at,'d-m-Y') }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                                {{ $users->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection