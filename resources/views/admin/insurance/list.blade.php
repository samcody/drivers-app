@extends('layouts.admin')

@section('title','Insurance')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="table-rep-plugin">
                                <div class="table-responsive" data-pattern="priority-columns">
                                    @if(count($insurances) < 1)
                                        <br><br>
                                        <div class="alert alert-info text-center">
                                            <p>No Insurance request yet!</p>
                                        </div>
                                    @else
                                        <table id="tech-companies-1" class="table  table-striped">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Name</th>
                                                <th>Address</th>
                                                <th>Vehicle Type</th>
                                                <th>Email</th>
                                                <th>Date</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($insurances as $insurance)
                                                <tr>
                                                    <td>#</td>
                                                    <td>{{ $insurance->name }}</td>
                                                    <td>{{ $insurance->address }}</td>
                                                    <td>{{ $insurance->vehicle_type }}</td>
                                                    <td>{{ $insurance->email}}</td>
                                                    <td>{{ date_format($insurance->created_at,'d-m-Y') }}</td>
                                                    <td><a href="{{ url('insurance',['id' => $insurance->id]) }}" class="btn btn-primary">View</a></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                                {{ $insurances->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection