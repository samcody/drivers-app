@extends('layouts.admin')

@section('title','Insurance')

@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">View Insurance Request By {{ $insurance->name }}</h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>Client: {{ $insurance->name }}</p>
                                    <p>Client's Mobile Number: {{ $insurance->phone }}</p>
                                    <p>Client's Address: {{ $insurance->address }}</p>
                                    <p>Client's Vehicle Type: {{ $insurance->vehicle_type }}</p>
                                    <p>Client's Vehicle Transmission: {{ $insurance->vehicle_transmission }}</p>
                                    <p>Client's Email : {{ $insurance->email }}</p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection