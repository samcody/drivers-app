@extends('layouts.master')

@section('title','404')

@section('content')
    <section class="height-100 bg--site text-center">
        <div class="container pos-vertical-center">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="h1--large">404</h1>
                    <p class="lead">
                        What brings you here? The page you were looking for was not found.
                    </p>
                    <a href="{{ url('/') }}">Go back to home page</a>
                </div>
            </div>
        </div>
    </section>
@endsection