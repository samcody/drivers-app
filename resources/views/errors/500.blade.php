@extends('layouts.master')

@section('title','500')

@section('content')
    <section class="height-100 bg--site text-center">
        <div class="container pos-vertical-center">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="h1--large">500</h1>
                    <p class="lead">
                        An unexpected error has occured preventing the page from loading. Our Engineers are currently working on it.
                    </p>
                    <a href="{{ url('/') }}">Go back to home page</a>
                </div>
            </div>
        </div>
    </section>
@endsection