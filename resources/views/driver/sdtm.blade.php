@extends('layouts.driver-dashboard')

@section('title','About SDTM')

@section('content')
    <section class="r">
        <div class="container">
            <div class="boxed boxed--lg boxed--border">
            <div class="row">
            <div class="col-md-8">
                <div class="col-md-10">
                    <h4>ABOUT THE SDTM PROGRAM</h4>
                    <p class="">
                        The SDTM (Smart Driver &amp; Transport Management) Program is A MUST for every driver that passes
                        through DRIVERSNG platform, before they are placed or matched to jobs. The program exposes
                        vehicle drivers to all spheres of techniques, trainings, checks and vetting that ensures these drivers are
                        ready in their mindset, disposition and comportment to offer a legendary lasting and laudable service.
                        Below are the highlights of the program. This a compulsory five days program.
                    </p>
                    <br />
                </div>
            

            <div class="row">
                <div class="col-md-10">
                    <h4>SMART TRAINING</h4>
                    <p class="">
                        This is a course program that captures and focuses on Improving and preparing drivers skill set;
                        comportment and soft skills, ensure driver are psychological prepared for the experience of working with
                        their probable employers or clienteles. Also, topics and simulations are carried on drivers to improve
                        driver’s road sense, navigation process, attitudinal displays and respect for road use and signs.

                    </p>
                    <br />
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <h4>TERMS OF SERVICE, VALUES AND BENEFITS</h4>
                    <p class="">
                        All drivers going through this platform, MUST BE AWARE of the values, terms and benefits the company
                        stands for and must be able to represent all these values when posted or matched to a job to represent
                        DRIVERSNG.

                    </p>
                    <br />
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <h4>VERIFICATION | MEDICALS</h4>
                    <p class="">
                        During this period driver’s documentation, background checks and criminal checks are all done, so as to
                        know drivers better and be able to insure and guarantee drivers when posted to clients or employers.
                        Also, proper medicals are carried out on all drivers to confirm their health status and fitness level.
                    </p>
                    <br />
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <h4>DRESS CODE | SUBSCRIPTION CARD</h4>
                    <p class="">
                        We expect all our drivers on full time to be decorated in uniform, ties and subscription card. While short
                        term and ride sharing platform drivers are to be decorated in ties and subscription card. All drivers from
                        our platform understands properly how to dress professionally and behave themselves in a comported
                        manner. All drivers on our platform must purchase this subscription card for identity
                        
                    </p>
                    <br />
                </div>
            </div>
        </div>
        <div class="col-sm-4">
                <img alt="Image" src="{{ asset('assets/img/bg-2.jpg') }}" />
                <br>
        </div>
    </div>
</div>
        </div>
    </section>
@endsection