@extends('layouts.auth')

@section('title','Create A Driver Account')

@section('content')
    <form class="form-horizontal" method="POST" action="{{ route('driver.register') }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('full_name') ? ' has-error' : '' }}">
            <div class="col-md-12">
                <input id="full_name" type="text" name="full_name" placeholder="Full Name" value="{{ old('full_name') }}" required autofocus>

                @if ($errors->has('full_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('full_name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <div class="col-md-12">
                <input id="email" type="email" name="email" placeholder="Email Address" value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('mobile_number') ? ' has-error' : '' }}">
            <div class="col-md-12">
                <input id="mobile_number" type="tel" placeholder="Mobile Number" name="mobile_number" required>
                @if ($errors->has('mobile_number'))
                    <span class="help-block">
                        <strong>{{ $errors->first('mobile_number') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <div class="col-md-12">
                <input id="password" type="password" name="password" placeholder="Password" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-12 col-md-offset-4">
                <button type="submit" class="btn btn--primary type--uppercase">
                    Register
                </button>
            </div>
        </div>
    </form>
    <span class="type--fine-print block">
        Already an account?
        <a href="{{ url('drive') }}"> Login</a>
    </span>
    <span class="type--fine-print block">Forgot your username or password?
        <a href="{{ route('driver.password.request') }}"> Recover account</a>
    </span>
@endsection
