@extends('layouts.driver-dashboard')

@section('title','Get Driving Jobs In Nigeria')

@section('banner')
    <section class="cover unpad--bottom switchable bg--secondary text-center-xs">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                        <img alt="Image" src="{{ asset('assets/img/bg-1.jpg') }}" />
                        <br>
                </div>
                <div class="col-sm-6 col-md-5 mt--3">
                    <h1>
                        Join Reliable <br> Drivers  in Nigeria
                    </h1>
                    <p class="lead">
                        Get Posted to Jobs Around your Residence &amp; Shop for Driver's Accessories.
                    </p>
                    <a class="btn btn--primary type--uppercase" href="{{ route('driver.register') }}">
                        <span class="btn__text">
                            Get Started
                        </span>
                    </a>
                    <a class="btn btn--primary type--uppercase" href="{{ route('driver.login') }}">
                        <span class="btn__text">
                            Login
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>GET TRAINED</h4>
                        <img alt="Image" src="{{ asset('assets/img/Employee.png') }}" />
                        <p>
                            Get freely trained at SDTM Program to become Drivers NG certified Professional Drivers.
                        </p>
                        <a class="btn btn--primary type--uppercase" href="{{ route('driver.register') }}">
                        <span class="btn__text">
                            Become a Driver
                        </span>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>GET MATCHED TO JOBS</h4>
                        <img alt="Image" src="{{ asset('assets/img/Car.png') }}" />
                        <p>
                            Afterwards, you get posted and Matched to jobs around your location.
                        </p>
                        <a class="btn btn--primary type--uppercase" href="{{ route('driver.register') }}">
                        <span class="btn__text">
                             Become a Driver
                        </span>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>RECEIVE QUICK PAYMENT</h4>
                        <img alt="Image" src="{{ asset('assets/img/Money.png') }}" />
                        <p>
                            Receive Salaries and cash for job completion and enjoy other benefits.
                        </p>
                        <a class="btn btn--primary type--uppercase" href="{{ route('driver.register') }}">
                        <span class="btn__text">
                            Become a Driver
                        </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="switchable feature-large bg--secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-5">
                    <img alt="image" src="{{ asset('assets/img/device-1.png') }}" />
                </div>
                <div class="col-sm-6 col-md-7">
                    <div class="switchable__text">
                        <h2 class="text-center">Welcome! You are in the right place</h2>
                        <p class="lead">
                            DriversNg is the new HOME FOR DRIVERS. This is where drivers can freely come to get more trainings, reformation and transformation and then are  placed in a job within Their locations and convenience. On this platform we work With experienced drivers, who have quality skills and passion For driving. We welcome all kinds of vehicle drivers, from cars, buses, Trucks, vans, to cab hailing services and ride sharing platforms. We are Extremely open to all and there are no favorites. We are not AGENTS, we Are a platform that completely understands how driver’s lifestyle should Be spelt out. We are DRIVERSNG
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
        <section class=" ">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="slider slider--inline-arrows" data-arrows="true">
                        <ul class="slides">
                            <li>
                                <div class="row">
                                    <div class="testimonial">
                                        <div class="col-md-9 col-md-offset-1 col-sm-8 col-xs-12">
                                            <span class="h3">
                                                &ldquo;I live on the island part of Lagos, i am a driver member on Drivers Ng, since i started recommending more drivers and making them become members also, i have been recieving credit alert of over NGN 100,000 monthly from driversng&rdquo;
                                            </span>
                                            <h5>Mr. Johnson</h5>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="testimonial">
                                        <div class="col-md-9 col-md-offset-1 col-sm-8 col-xs-12">
                                            <span class="h3">
                                                &ldquo;i am a driver member on driversng, i make alot of money from the uber program. i ensure that my car is always neat, however Drivers Ng always help out with tips and guides on how to make more money for all their drivers members &rdquo;
                                            </span>
                                            <h5>Mrs. Phelicia</h5>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="testimonial">
                                        <div class="col-md-9 col-md-offset-1 col-sm-8 col-xs-12">
                                            <span class="h3">
                                                &ldquo;In the month of november, 2017 i made over NGN 200,000.00 on drivers Ng for just recommending over hundred drivers to become driver members. i love this platform, it is another extra way of making income to feed and carter for my family&rdquo;
                                            </span>
                                            <h5>Mr. Samuel</h5>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="testimonial">
                                        <div class="col-md-9 col-md-offset-1 col-sm-8 col-xs-12">
                                            <span class="h3">
                                                &ldquo;I could not believe that I got placed in a job after 5days of training, verification and becoming a driver member. At first i thought it was going to take very long&rdquo;
                                            </span>
                                            <h5>Mr. Ikechukwu</h5>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="testimonial">
                                        <div class="col-md-9 col-md-offset-1 col-sm-8 col-xs-12">
                                            <span class="h3">
                                                &ldquo;I got a short term job on drivers Ng after becoming a driver member and this is already helping me to get extra income aside from my full term salary job. i want to thank drivers Ng for bringing this platform to us &rdquo;
                                            </span>
                                            <h5>Mr. Matthew</h5>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="text-center imagebg" data-gradient-bg='#4876BD,#8F48BD,#BD48B1'>
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-6">
                    <div class="cta">
                        <h2>Becoming A Verified and Tested Driver Is Now Easier</h2>
                        <p class="lead">
                            
                        </p>
                        <a class="btn btn--primary type--uppercase" href="{{ route('driver.register') }}">
                            <span class="btn__text">
                                Get Started
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection