@extends('layouts.driver-dashboard')

@section('title','Earn Extra Cash')

@section('content')
    <section class="r">
        <div class="container">
            <div class="boxed boxed--lg boxed--border">
            <div class="row">
                <div class="col-md-8">
                <div class="col-md-10">
                    <h4>DriversNg Earn Extra Cash Program</h4>
                    <p class="">
                        The Earn Extra Cash is designed towards creating 
                        a community for smart professionally trained and verified driver, 
                        thereby giving us a body to make more income, stronger voice and ensure 
                        that all driving and driver’s related services passes through this community. 
                        This network and community would make extra cash by the power of a 
                        SUB/ACCESS/MEMBERSHIP CARD which would also stand as our identity, 
                        which grants us access to discharge our services and also gives 
                        access to more benefits than just networking.
                    </p>
                    <br />
                </div>
            

            <div class="row">
                <div class="col-md-10">
                    <h4>The benefits of this card are as follow:</h4>
                    <p class="">
                        <ol type="a">
                            <li>Professional identification</li>
                            <li>Profile Management</li>
                            <li>Administration, verification and documentation</li>
                            <li>Access to Health Management</li>
                            <li>Access to Loan facilities</li>
                            <li>Access to all year round job matches privilege</li>
                            <li>Access to retraining programs</li>
                            <li>Membership for a year</li>
                            </ol>
                    </p>
                    <br />
                </div>
            </div>

            <div class="row">
                    <div class="col-md-10">
                        <h4>Requirement to participate in the program:</h4>
                        <p class="">
                            <ol type="a">
                                <li>You must purchase the card</li>
                                <li>You must purchase a valid driver’s license</li>
                                <li>You must have 5 years of driving experience</li>
                                <li>You must have been properly verified and trained</li>
                                
                            </ol>
                        </p>
                        <br />
                    </div>
                </div>

                <div class="row">
                        <div class="col-md-10">
                            <h4>How you start making extra cash:</h4>
                            <p class="">
                                <ul>
                                    <li>When you refer ONE (1) driver that gets the card = &#8358;1,000 FOR YOU.</li>
                                    <li>When you refer TEN (10) drivers that gets the card = &#8358;10,000 FOR YOU.</li>
                                    <li>When you refer HUNDRED (100) drivers that gets the card = &#8358;100,000 FOR YOU.</li>
                                    <li>When you refer ONE THOUSAND (1000) drivers that gets the card = &#8358;1,000,000 FOR YOU.</li>
                                    
                                </ul>
                            </p>
                            <br />
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                        <img alt="Image" src="{{ asset('assets/img/bg-2.jpg') }}" />
                        <br>
                </div>
                </div>
            </div>
        </div>
    </section>
@endsection