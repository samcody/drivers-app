<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ config('app.name') }} | @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="DriversNG | No. 1 platform to hire professional drivers">
    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.png') }}">
    <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('assets/css/stack-interface.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('assets/css/socicon.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('assets/css/lightbox.min.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('assets/css/flickity.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('assets/css/iconsmind.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('assets/css/jquery.steps.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('assets/css/theme.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('assets/css/font-rubiklato.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700%7CRubik:300,400,500" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://use.fontawesome.com/6e5582a159.js"></script>
    <script src="https://cdn.jsdelivr.net/clipboard.js/1.5.12/clipboard.min.js"></script>
</head>
<body class="">
<a id="start"></a>
<style>
    .awesome {

      width:100%;
      text-align: center;
      color:#313131;
      -webkit-animation:colorchange 5s infinite alternate;
      
      
    }

    @-webkit-keyframes colorchange {
      0% {
        
        color: black;
      }
      
      10% {
        
        color: #8e44ad;
      }
      
      20% {
        
        color: #1abc9c;
      }
      
      30% {
        
        color: #d35400;
      }
      
      40% {
        
        color: blue;
      }
      
      50% {
        
        color: orange;
      }
      
      60% {
        
        color: blue;
      }
      
      70% {
        
        color: #2980b9;
      }
      80% {
     
        color: #f1c40f;
      }
      
      90% {
     
        color: #2980b9;
      }
      
      100% {
        
        color: darkslategray;
      }
    }
</style>

<div class="nav-container ">
    <div class="bar bar--sm visible-xs">
        <div class="container">
            <div class="row">
                <div class="col-xs-3 col-sm-2">
                    <a href="{{ url('/') }}">
                        <img class="logo logo-dark" style="max-height: 5.4em; margin-top: -22px;" alt="logo" src="{{ asset('assets/img/logo.png') }}" />
                        <img class="logo logo-light" style="max-height: 5.4em; margin-top: -22px;" alt="logo" src="{{ asset('assets/img/logo.png') }}" />
                    </a>
                </div>
                <div class="col-xs-9 col-sm-10 text-right">
                    <a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs">
                        <i class="icon icon--sm stack-interface stack-menu"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!--end bar-->
    <nav id="menu1" class="bar bar--md bar-1 hidden-xs bar--absolute">
        <div class="container">
            
            <div class="row">
                <div class="col-md-1 col-sm-2 hidden-xs">
                    <div class="bar__module">
                        <a href="{{ url('/') }}">
                            <img class="logo logo-dark" style="max-height: 5.4em; margin-top: -25px;" alt="logo" src="{{ asset('assets/img/logo.png') }}" />
                            <img class="logo logo-light" style="max-height: 5.4em; margin-top: -25px" alt="logo" src="{{ asset('assets/img/logo.png') }}" />
                        </a>
                    </div>
                </div>
                <div class="col-md-11 col-sm-12 text-right text-left-xs text-left-sm">
                    <div class="bar__module">
                        <a style="color: #666666;" class="type--uppercase" href="{{ url('drive/sdtm') }}"><span>About SDTM Program</span></a>
                    </div>
                    <div class="bar__module">
                        <a style="color: #666666;" class="type--uppercase" href="{{ url('drive/earn') }}"><span>Earn Extra Cash</span></a>
                    </div>
                    <div class="bar__module">
                        <a style="color: #666666;" class="type--uppercase" href="http://driversnigeria.com"><span>Blog</span></a>
                    </div>
                    <div class="bar__module">
                        <a style="color: #666666;" class="type--uppercase" href="{{ url('drive/shop') }}"><span>Driver's Shop</span></a>
                    </div>
                    <div class="bar__module">
                        <a style="color: #666666;" class="type--uppercase" href=""><span class=""></span></a>
                    </div>
                    
            
                    @guest
                        <div class="bar__module">
                            <a class="btn btn--sm type--uppercase" href="{{ url('drive') }}">
                                    <span class="btn__text">
                                        Login
                                    </span>
                            </a>
                            <a class="btn btn--sm btn--primary type--uppercase" href="{{ url('drive/register') }}">
                            <span class="btn__text">
                                Register
                            </span>
                            </a>
                        </div>
                    @else
                        <div class="bar__module">
                            <ul class="menu-horizontal text-left">
                                <li class="dropdown">
                                    <span class="dropdown__trigger">Hi, {{ Auth::User()->full_name }}</span>
                                    <div class="dropdown__container">
                                        <div class="container">
                                            <div class="row">
                                                <div class="dropdown__content col-md-2 col-sm-4">
                                                    <ul class="menu-vertical">
                                                        <li><a href="{{ url('drive/home') }}"><span>Dashboard</span></a></li>
                                                        <li><a href="{{ url('drive/list') }}"><span>Profile</span></a></li>
                                                        <li><a href="{{ url('drive/refer') }}"><span><h6  class="awesome">Invite Friends for &#8358;1,000</h6></span></a></li>
                                                        <li>
                                                            <a href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span>Logout</span></a>
                                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                                {{ csrf_field() }}
                                                            </form>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    @endguest
                    <div class="alert-warning" style="margin: 10px auto;">
                            <marquee><p style="font-family: comic sans ms; font-size: 20px">All Driver Member Shall Be Liable For Membership Fee of &#8358;5,000 Yearly</p></marquee>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>
<div class="main-container"><br><br>
    @yield('banner')
    @yield('content')

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 text-center">
                    <h4 class="centered headline">CITIES WE SERVE</h4>
                    <div class="row">
                        <div class="col-md-2 col-sm-2 col-xs-6">Lagos</div>
                        <div class="col-md-2 col-sm-2 col-xs-6">Abuja</div>
                        <div class="col-md-2 col-sm-2 col-xs-6">Port Harcourt</div>
                        <div class="col-md-2 col-sm-2 col-xs-6">Ibadan</div>
                        <div class="col-md-2 col-sm-2 col-xs-6">Kaduna</div>
                        <div class="col-md-2 col-sm-2 col-xs-6">Uyo</div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-sm-2 col-xs-6">Abeokuta</div>
                        <div class="col-md-2 col-sm-2 col-xs-6">Kano</div>
                        <div class="col-md-2 col-sm-2 col-xs-6">Benin</div>
                        <div class="col-md-2 col-sm-2 col-xs-6">Benue</div>
                        <div class="col-md-2 col-sm-2 col-xs-6">Delta</div>
                        <div class="col-md-2 col-sm-2 col-xs-6">Jos</div>
                    </div>
                </div>
            </div>
            <br><br>
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="copyrights">
                        {{--<img src="{{ asset('assets/img/location.svg') }}" class="img-responsive center-block" style="width: 10%;" alt="">--}}
                        <b>22, Ayodele Street, Fadeyi, Yaba, Lagos | 29, Mambilla Street, Maitama, Abuja</b>
                        <br>
                        <b>MONDAYS - FRIDAYS</b> (9AM - 5PM)
                        <br><b>08163555265</b>, <b>07062483241</b>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer class="text-center-xs space--xs bg--dark ">
        <div class="container">
            <div class="row">
                <div class="col-sm-7">
                    <ul class="list-inline">
                        <li>
                            <a href="{{ url('partner') }}">
                                <span class="h6 type--uppercase">Partnerships</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('package') }}">
                                <span class="h6 type--uppercase">Hire Driver</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('mobile-app') }}">
                                <span class="h6 type--uppercase">Mobile App</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-5 text-right text-center-xs">
                    <ul class="social-list list-inline list--hover">
                        <li>
                            <a href="https://twitter.com/Driversng">
                                <i class="socicon socicon-twitter icon icon--xs"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/driversng/">
                                <i class="socicon socicon-facebook icon icon--xs"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/company-beta/10005897">
                                <i class="socicon socicon-linkedin icon icon--xs"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!--end of row-->
            <div class="row">
                <div class="col-sm-7">
                            <span class="type--fine-print">&copy;
                              2015 - {{ date('Y') }} {{ config('app.name') }}.</span>
                    <a class="type--fine-print" href="https://medium.com/@driversng">Medium</a>
                    <a class="type--fine-print" href="{{ url('terms') }}">Terms & Condition</a>
                </div>
                <div class="col-sm-5 text-right text-center-xs">
                    <a class="type--fine-print" href="#">business@driversng.com</a>
                </div>
            </div>
        </div>
    </footer>
</div>
<!--<div class="loader"></div>-->
<a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
    <i class="stack-interface stack-up-open-big"></i>
</a>
<script src="{{ asset('assets/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('assets/js/flickity.min.js') }}"></script>
<script src="{{ asset('assets/js/easypiechart.min.js') }}"></script>
<script src="{{ asset('assets/js/parallax.js') }}"></script>
<script src="{{ asset('assets/js/typed.min.js') }}"></script>
<script src="{{ asset('assets/js/datepicker.js') }}"></script>
<script src="{{ asset('assets/js/isotope.min.js') }}"></script>
<script src="{{ asset('assets/js/ytplayer.min.js') }}"></script>
<script src="{{ asset('assets/js/lightbox.min.js') }}"></script>
<script src="{{ asset('assets/js/granim.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.steps.min.js') }}"></script>
<script src="{{ asset('assets/js/jquerysteps.js') }}"></script>
<script src="{{ asset('assets/js/countdown.min.js') }}"></script>
<script src="{{ asset('assets/js/twitterfetcher.min.js') }}"></script>
<script src="{{ asset('assets/js/spectragram.min.js') }}"></script>
<script src="{{ asset('assets/js/smooth-scroll.min.js') }}"></script>
<script src="{{ asset('assets/js/scripts.js') }}"></script>
</body>

</html>