<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ config('app.name') }} | @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="DriversNG | No. 1 platform to hire professional drivers">
    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.png') }}">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,600' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="{{ asset('edmm/package/css/reset.css') }}"> <!-- CSS reset -->
    <link rel="stylesheet" href="{{ asset('edmm/package/css/calendar.css') }}"> <!-- CSS calendar -->
    <link rel="stylesheet" href="{{ asset('edmm/package/css/style.css') }}"> <!-- Resource style -->
    <link href="{{ asset('dashboard/css/bootstrap.min.css') }}" rel="stylesheet" media="all" type="text/css">
    <script src="{{ asset('edmm/package/js/modernizr.js') }}"></script> <!-- Modernizr -->
    <script src="{{ asset('edmm/package/js/calendar.js') }}"></script> <!-- calendar js -->
</head>
    

<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" style="font-family:tahoma;" href="#"><span style="color:black;">Drivers</span><span style="color:gold;">ng</span></a>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav navbar-left">
      <div style="padding-top:8px;" class="dropdown">
  <a class="btn btn-secondary dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class = "glyphicon glyphicon-globe"></span>&nbsp;&nbsp;Browse
  </a>
  <div style="text-align:center;" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <li><a class="dropdown-item" href="#">Short Term Drivers</a>
    <a class="dropdown-item" href="#">Uber Drivers</a>
    <a class="dropdown-item" href="#">Vehicle Owner Drivers</a>
    <a class="dropdown-item" href="#">Driving Tutors</a>
    <a class="dropdown-item" href="#">Driving Schools</a>
    <a class="dropdown-item" href="#">Driverlancers</a></li>
  </div>
</div>
    </ul>
    <div class="col-sm-3 col-md-3">
        <form class="navbar-form" role="search">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" name="q">
            <div class="input-group-btn">
                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
            </div>
        </div>
        </form>
    </div>
    <ul class="nav navbar-nav navbar-right">
        <li><a href="{{ url('marketplace/posts')}}"><span class = "glyphicon glyphicon-edit"></span>&nbsp;&nbsp;Post Task</a></li>
        <li><a href="#"><span class = "glyphicon glyphicon-user"></span>&nbsp;&nbsp;My Account</a></li>
        <li>
                <a href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span>Logout</span></a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
    </ul>
  </div><!-- /.navbar-collapse -->
</nav>
              <style>
                body{
                         background: whitesmoke;
                    }
            </style>

              <br /><br /><br />
              <div class="main-container">
                    @yield('banner')
                
                    @yield('content')
                
                    <section>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 text-center">
                                    <h4 class="centered headline">CITIES WE SERVE</h4>
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-xs-6">Lagos</div>
                                        <div class="col-md-2 col-sm-2 col-xs-6">Abuja</div>
                                        <div class="col-md-2 col-sm-2 col-xs-6">Port Harcourt</div>
                                        <div class="col-md-2 col-sm-2 col-xs-6">Ibadan</div>
                                        <div class="col-md-2 col-sm-2 col-xs-6">Kaduna</div>
                                        <div class="col-md-2 col-sm-2 col-xs-6">Uyo</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-xs-6">Abeokuta</div>
                                        <div class="col-md-2 col-sm-2 col-xs-6">Kano</div>
                                        <div class="col-md-2 col-sm-2 col-xs-6">Benin</div>
                                        <div class="col-md-2 col-sm-2 col-xs-6">Benue</div>
                                        <div class="col-md-2 col-sm-2 col-xs-6">Delta</div>
                                        <div class="col-md-2 col-sm-2 col-xs-6">Jos</div>
                                    </div>
                                </div>
                            </div>
                            <br><br>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <div class="copyrights">
                                        {{--<img src="{{ asset('assets/img/location.svg') }}" class="img-responsive center-block" style="width: 10%;" alt="">--}}
                                        <b>22, Ayodele Street, Fadeyi, Yaba, Lagos | 29, Mambilla Street, Maitama, Abuja</b>
                                        <br>
                                        <b>MONDAYS - FRIDAYS</b> (9AM - 5PM)
                                        <br><b>08163555265</b>, <b>07062483241</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <footer class="text-center-xs space--xs bg--dark ">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-7">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="{{ url('partner') }}">
                                                <span class="h6 type--uppercase">Partnerships</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ url('package') }}">
                                                <span class="h6 type--uppercase">Hire Driver</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ url('mobile-app') }}">
                                                <span class="h6 type--uppercase">Mobile App</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-sm-5 text-right text-center-xs">
                                    <ul class="social-list list-inline list--hover">
                                        <li>
                                            <a href="https://twitter.com/Driversng">
                                                <i class="socicon socicon-twitter icon icon--xs"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.facebook.com/driversng/">
                                                <i class="socicon socicon-facebook icon icon--xs"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.linkedin.com/company-beta/10005897">
                                                <i class="socicon socicon-linkedin icon icon--xs"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!--end of row-->
                            <div class="row">
                                <div class="col-sm-7">
                                            <span class="type--fine-print">&copy;
                                              2015 - {{ date('Y') }} {{ config('app.name') }}.</span>
                                    <a class="type--fine-print" href="https://medium.com/@driversng">Medium</a>
                                    <a class="type--fine-print" href="{{ url('terms') }}">Terms & Condition</a>
                                </div>
                                <div class="col-sm-5 text-right text-center-xs">
                                    <a class="type--fine-print" href="#">business@driversng.com</a>
                                </div>
                            </div>
                        </div>
                    </footer>
                </div>
           
        </body>
        <script src="{{ asset('edmm/package/js/jquery-2.1.4.js') }}"></script>
        <script src="{{ asset('edmm/package/js/velocity.min.js') }}"></script>
        <script src="{{ asset('edmm/package/js/main.js') }}"></script> <!-- Resource jQuery -->
        <script src="{{ asset('dashboard/js/jquery.min.js') }}"></script>
        <script src="{{ asset('dashboard/js/bootstrap.min.js') }}"></script>

</body>

</html>