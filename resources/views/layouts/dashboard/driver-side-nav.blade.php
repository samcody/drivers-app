<div class="col-md-2">
        <div class="panel">
                <div class="label" style = "color:orange">Driver</div>
            <div class="text-block text-center">
                    @foreach($drivers as $driver)
                    <br /><br />
            <span class="h5">{{ Auth::User()->full_name }}{{' '}}
                @if($driver->is_verified == true)
                <img alt="background" width="16px" height="16px" src="{{ asset('assets/img/tick.png') }}" /></span>
                @endif
                <span>{{ Auth::User()->mobile_number }}</span><br />
                <span>{{ $driver->serial_number }}</span>
                @endforeach
            
        </div>
        <hr>
        <div class="text-block">
            <ul class="menu-vertical">
                
                <li>
                    <a href="{{ url('drive/list') }}">Profile</a>
                </li>
                <li>
                    <a href="{{ url('drive/home') }}">EDM Job Alerts</a>
                </li>
                <li>
                    <a href="{{ url('drive/train') }}">Training Sessions</a>
                </li>
                <li>
                    <a href="{{ url('drive/refer') }}"> Driver Referrals</a>
                </li>
                <li>
                    <a href="{{ url('drive/collect-cash') }}">Referral Bonuses</a>
                </li>
                <li>
                    <a href="{{ url('drive/home') }}">Driver's Ratings</a>
                </li>
                <li>
                    <a href="{{ url('drive/train') }}">Testing &amp; Certifications</a>
                </li>
                <li>
                    <a href="{{ url('drive/navigator') }}">Navigation and Mapping</a>
                </li>
                
                
            </ul>
        </div>
    </div>
</div>