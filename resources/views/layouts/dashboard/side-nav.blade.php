<div class="col-md-3">
    <div class="boxed boxed--lg boxed--border">
        <div class="text-block text-center">
            <span class="h5">{{ Auth::User()->full_name }}</span>
            <span>{{ Auth::User()->mobile_number }}</span>
            <span class="label">User</span>
        </div>
        <hr>
        <div class="text-block">
            <ul class="menu-vertical">
                <li>
                    <a href="{{ url('home') }}">Dashboard</a>
                </li>
                <li>
                    <a href="{{ url('package') }}">Hire Driver</a>
                </li>
                <li>
                    <a href="{{ url('dashboard/full-time') }}">Full-Time Bookings</a>
                </li>
                <li>
                    <a href="{{ url('dashboard/short-time') }}">Short-Term Bookings</a>
                </li>
                <li>
                    <a href="{{ url('dashboard/uber') }}">Uber Driver Bookings</a>
                </li>
                <li>
                    <a href="{{ url('dashboard/transfer') }}">Transfer Bookings</a>
                </li>
                <li>
                    <a href="{{ url('dashboard/pay-use') }}">Pay Per Use Bookings</a>
                </li>
                <li>
                    <a href="{{ url('edit-profile') }}">Profile</a>
                </li>
            </ul>
        </div>
    </div>
</div>