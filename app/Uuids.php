<?php
/**
 * Created by PhpStorm.
 * User: goodnesskay
 * Date: 12/9/17
 * Time: 10:45 PM
 */

namespace App;

use Webpatser\Uuid\Uuid;


trait Uuids
{

    /**
     * Boot function from laravel.
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = Uuid::generate()->string;
        });
    }
}