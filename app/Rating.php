<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    use Uuids;
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
