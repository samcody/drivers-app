<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EdmBooking extends Model
{
    use Uuids;
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function driver()
    {
        return $this->belongsTo('App\Driver');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $table = 'edm_bookings';
}
