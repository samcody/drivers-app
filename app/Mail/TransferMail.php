<?php

namespace App\Mail;

use App\Transfer;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TransferMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $transfer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Transfer $transfer)
    {
        $this->transfer = $transfer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('business@driversng.com')
            ->subject('DriversNG: Booking Success')
            ->view('emails.booking.transfer.success')->with([
                'user' => $this->transfer->user->full_name,
                'transferType' => $this->transfer->transfer_type,
                'createdAt' => $this->transfer->created_at,
                'driverNumber' => $this->transfer->number_of_driver,
            ]);
    }
}
