<?php

namespace App\Mail;

use App\Match;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Remit;
use App\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;

class Remittance extends Mailable
{
    public $report;

    public function __construct($report)
    {
        $this->report = $report;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $report = $this->report;
        $user = Remit::where('id','=',$report->remit_id)->where('is_deleted','=',false)->orderBy('created_at', 'desc')->paginate(25);
        $pdf = PDF::loadView('admin.remit.report-sheet',['report' => $report],['user' => $user]);
        return $this->from('business@driversng.com')
            ->subject('DriversNG: Weekly Report')
            ->view('admin.remit.report-sheet',['user' => $user, 'report' => $report])
            ;
    }
}
