<?php

namespace App\Mail;

use App\FullTimeBooking;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FullTime extends Mailable
{
    use Queueable, SerializesModels;

    protected $fullTime;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(FullTimeBooking $fullTime)
    {
        $this->fullTime = $fullTime;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('business@driversng.com')
                    ->subject('DriversNG: Booking Success')
                    ->view('emails.booking.full-time.success')->with([
                            'user' => $this->fullTime->user->full_name,
                            'package' => $this->fullTime->package,
                            'createdAt' => $this->fullTime->created_at,
                            'driverNumber' => $this->fullTime->number_of_driver,
                    ]);
    }
}
