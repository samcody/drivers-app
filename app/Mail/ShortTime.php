<?php

namespace App\Mail;

use App\ShortTimeBooking;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ShortTime extends Mailable
{
    use Queueable, SerializesModels;

    protected $shortTime;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ShortTimeBooking $shortTime)
    {
        $this->shortTime = $shortTime;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('business@driversng.com')
            ->subject('DriversNG: Booking Success')
            ->view('emails.booking.short-time.success')->with([
                'user' => $this->shortTime->user->full_name,
                'driver' => $this->shortTime->driver->firstname.' '.$this->shortTime->driver->lastname,
                'createdAt' => $this->shortTime->created_at,
                'amount' => $this->shortTime->amount,
                'id' => $this->shortTime->driver->id,
                'profile' => url('driver/pdf/'),
            ]);
    }
}
