<?php

namespace App\Mail;

use App\UberBooking;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Uber extends Mailable
{
    use Queueable, SerializesModels;

    protected $uber;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(UberBooking $uber)
    {
        $this->uber = $uber;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('business@driversng.com')
            ->subject('DriversNG: Booking Success')
            ->view('emails.booking.full-time.success')->with([
                'user' => $this->uber->user->full_name,
                'package' => $this->uber->package,
                'createdAt' => $this->uber->created_at,
                'driverNumber' => $this->uber->number_of_driver,
            ]);
    }
}
