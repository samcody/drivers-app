<?php

namespace App\Mail;

use App\PayUse;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PayUseMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $payUse;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(PayUse $payUse)
    {
        $this->payUse = $payUse;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('business@driversng.com')
            ->subject('DriversNG: Booking Success')
            ->view('emails.booking.pay-use.success')->with([
                'user' => $this->payUse->user->full_name,
                'duration' => $this->payUse->duration,
                'createdAt' => $this->payUse->created_at,
                'driverNumber' => $this->payUse->number_of_driver,
            ]);
    }
}
