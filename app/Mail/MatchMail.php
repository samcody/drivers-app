<?php

namespace App\Mail;

use App\Match;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MatchMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $match;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Match $match)
    {
        $this->match = $match;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('business@driversng.com')
            ->subject('DriversNG: Driver Match Successful')
            ->view('emails.match.success')->with([
                'user' => $this->match->user->full_name,
                'bookingType' => $this->match->booking_type,
                'createdAt' => $this->match->created_at
            ]);
    }
}
