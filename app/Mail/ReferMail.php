<?php

namespace App\Mail;

use App\Match;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Driver;
use App\Drive;
use App\Driven;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReferMail extends Mailable
{
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        
        $drivers = Driver::where('email','=', Auth::User()->email )->paginate(25);
        $refers = Drive::where('ref_id','=', Auth::User()->email )->paginate(25);
        return $this->from('business@driversng.com')
            ->subject('DriversNG: Invitation to join DriversNG')
            ->view('emails.referral.refer',['drivers' => $drivers],['refers' => $refers]);
    }
}
