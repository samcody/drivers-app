<?php

namespace App\Mail;

use App\Payment;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaymentMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $payment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('business@driversng.com')
            ->subject('DriversNG: Booking Payment Success')
            ->view('emails.payment.success')->with([
                'user' => $this->payment->user->full_name,
                'package' => $this->payment->hire_type,
                'createdAt' => $this->payment->created_at,
                'amount' => $this->payment->amount,
            ]);
    }
}
