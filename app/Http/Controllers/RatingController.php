<?php

namespace App\Http\Controllers;

use App\Driver;
use App\Rating;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RatingController extends Controller
{
    public function ratingView($id)
    {
        $driver = Driver::find($id);
        return view('main.rating.view',['driver' => $driver]);
    }

    public function rate(Request $request)
    {
        $validators = $this->validate($request,[
            'driver_id' => 'required',
            'comment' => 'required',
            'neatness' => 'required',
            'dressing' => 'required',
            'service_quality' => 'required',
            'punctuality' => 'required',
            'integrity' => 'required',
        ]);

        $user = User::find(Auth::User()->id);
        $rating = new Rating();

        $rating->driver_id = $request->driver_id;
        $rating->comment = $request->comment;
        $rating->neatness = $request->neatness;
        $rating->dressing = $request->dressing;
        $rating->service_quality = $request->service_quality;
        $rating->punctuality = $request->punctuality;
        $rating->integrity = $request->integrity;

        if ($user->ratings()->save($rating)) {
            return redirect()->back()->with('alert','You have rated this driver');
        } else {
            return redirect()->back()->withErrors($validators);
        }
    }

    /**
     * User View All Ratings Made
     */
    public function viewRatings()
    {
        $ratings = Rating::where('user_id','=',Auth::User()->id)->paginate(20);
        return view('dashboard.rating.list',['ratings' => $ratings]);
    }

    public function edit($id)
    {
        $rating = Rating::find($id);
        return view('dashboard.rating.edit',['rating' => $rating]);
    }

    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'comment' => 'required',
            'neatness' => 'required',
            'dressing' => 'required',
            'service_quality' => 'required',
            'punctuality' => 'required',
            'integrity' => 'required',
        ]);

        $rating = Rating::find($id);

        $rating->comment = $request->comment;
        $rating->neatness = $request->neatness;
        $rating->dressing = $request->dressing;
        $rating->service_quality = $request->service_quality;
        $rating->punctuality = $request->punctuality;
        $rating->integrity = $request->integrity;

        if ($rating->save()) {
            return redirect()->back()->with('alert','You have updated the rating for this driver');
        } else {
            return redirect()->back()->withErrors($validators);
        }
    }
}
