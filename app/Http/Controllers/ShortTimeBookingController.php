<?php

namespace App\Http\Controllers;

use App\Driver;
use App\Http\Controllers\Traits\Countries;
use App\Http\Controllers\Traits\States;
use App\Match;
use App\Rating;
use App\ShortTimeBooking;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use PDF;

class ShortTimeBookingController extends Controller
{
    use States;
    use Countries;
    public function selectLocation()
    {
        $locations = Driver::select('location')->distinct()->get();
        return view('main.short.location',['locations' => $locations]);
    }

    public function getDrivers(Request $request)
    {
        $drivers = Driver::where('location','=',$request->location)
                        ->where('driver_type','=','SHORT-TIME')
                        //->where('is_matched','=',false)
                        ->where('is_deleted','=',false)
                        ->where('is_verified','=',true)
                        ->paginate(20);
        return view('main.short.list',['drivers' => $drivers,'request' => $request]);
    }

    
    public function viewDriver($id)
    {
        $driver = Driver::find($id);
        $ratings = Rating::where('driver_id','=',$driver->id)->where('is_deleted','=',false)->orderBy('id','desc')->paginate(5);
        return view('main.short.view',['driver' => $driver,'ratings' => $ratings]);
    }

    public function book($id)
    {
        $driver = Driver::find($id);
        return view('main.short.book',['driver' => $driver, 'states' => $this->listStates(), 'countries' => $this->listCountry()]);
    }

    public function save(Request $request,$driverID)
    {
        $validator = $this->validate($request,[
            'meeting_point' => 'required',
            'nationality' => 'required',
            'govt_id_no' => 'required',
            'expiry_date' => 'required',
            'state_of_residence' => 'required',
            'next_of_kin' => 'required',
            'mobile_number' => 'required',
            'date_of_birth' => 'required',
            'marital_status' => 'required',
            'home_address' => 'required',
            'office_address' => 'required'
        ]);

        $user = User::find(Auth::User()->id);
        $booking = new ShortTimeBooking();
        $booking->booking_id = 'DST'.rand(1111,9999);
        $booking->meeting_point = $request->meeting_point;
        $booking->nationality = $request->nationality;
        $booking->govt_id_no = $request->govt_id_no;
        $booking->expiry_date = $request->expiry_date;
        $booking->state_of_residence = $request->state_of_residence;
        $booking->next_of_kin = $request->next_of_kin;
        $booking->mobile_number = $request->mobile_number;
        $booking->date_of_birth = $request->date_of_birth;
        $booking->marital_status = $request->marital_status;
        $booking->home_address = $request->home_address;
        $booking->office_address = $request->office_address;
        $booking->alternate_mobile_number = $request->alternate_mobile_number;
        $booking->package = $request->service;
        $booking->amount = $request->service * $request->total_number;
        $booking->pickup_date = $request->pickup_date;
        $booking->pickup_time = $request->pickup_time;
        $booking->end_date = $request->end_date;
        $booking->driver_id = $driverID;

        $message = "Your driver will be available within 24 Hours after confirmation of payment, 
                    then once we confirm payment, at the backend we should be able to upload 
                    the driver image to that same place where the text was.";

        if ($user->shortTimeBookings()->save($booking)) {
            $driver = Driver::find($driverID);
            $driver->is_matched = true;

            $match = new Match();
            $match->user_id = Auth::User()->id;
            $match->booking_id = $booking->id;
            $match->booking_type = 'SHORT-TIME';
            $match->driver_id = json_encode($driver->id);

            $match->save();
            $driver->save();
            $recipients = ['business@driverng.com','driversng@gmail.com',$user->email];
            foreach ($recipients as $recipient) {
                $email = new \App\Mail\ShortTime($booking);
                Mail::to($recipient)->send($email);
            }
            return view('main.short.pay',['request' => $request,'bookingID' => $booking->id])->with('alert',$message);
        } else{
            return redirect()->back()->with($validator);
        }
    }
}
