<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\States;
use App\Transfer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;

class TransferBookingController extends Controller
{
    use States;

    public function create()
    {
        return view('main.transfer.book',['states' => $this->listStates()]);
    }

    public function save(Request $request)
    {
        //To add validations later
        $validator = $this->validate($request,[

        ]);

        $user = User::find(Auth::User()->id);
        $booking = new Transfer();

        if ($request->hasFile('luggage_image')) {
            $luggage_image = $request->file('luggage_image');
            $filename = time().'.'.$luggage_image->getClientOriginalExtension();
            Image::make($luggage_image)->resize(300,300)->save(public_path('/transfer/'.$filename));

            $booking->luggage_image =$filename;
        }

        $booking->booking_id = 'DT'.rand(1111,9999);
        $booking->no_of_persons = $request->input('no_of_persons');
        $booking->no_of_luggage = $request->input('no_of_luggage');
        $booking->resumption_time = $request->input('resumption_time');
        $booking->vehicle_type = $request->input('vehicle_type');
        $booking->no_of_vehicle = $request->input('no_of_vehicle');
        $booking->number_of_driver = $request->input('no_of_vehicle');
        $booking->number_of_driver_unmatched = $request->input('no_of_vehicle');
        $booking->transfer_type = $request->input('transfer_type');
        $booking->transfer_mode = $request->input('transfer_mode');
        $booking->no_of_persons = $request->input('no_of_persons');
        $booking->start_date = $request->input('start_date');
        $booking->end_date = $request->input('end_date');
        $booking->start_state = $request->input('start_state');
        $booking->end_state = $request->input('end_state');
        $booking->pick_up_point = $request->input('pick_up_point');
        $booking->drop_off_point = $request->input('drop_off_point');
        $booking->govt_id_no = $request->input('govt_id_no');

        $message = "Your driver will be available within 24 Hours after confirmation of payment, 
                        then once we confirm payment, at the backend we should be able to upload 
                        the driver image to that same place where the text was.";

        if ($user->transferBookings()->save($booking)) {
            $recipients = ['business@driverng.com','driversng@gmail.com',$user->email];
            foreach ($recipients as $recipient) {
                $email = new \App\Mail\TransferMail($booking);
                Mail::to($recipient)->send($email);
            }
            return view('main.transfer.terms',['request' => $request, 'bookingID' => $booking->id])->with('alert',$message);
        } else {
            return redirect()->back()->with($validator);
        }
    }
}
