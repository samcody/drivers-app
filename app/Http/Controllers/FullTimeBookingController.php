<?php

namespace App\Http\Controllers;

use App\FullTimeBooking;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class FullTimeBookingController extends Controller
{
    public function viewPackage()
    {
        return view('main.full-time.package');
    }

    public function getPackage(Request $request)
    {
        return view('main.full-time.book',['request' => $request]);
    }

    public function save(Request $request)
    {
        //To add validations later
        $validator = $this->validate($request,[
            'package' => 'required',
            'number_of_driver' => 'required',
            'resumption_date' => 'required',
            'driver_stay' => 'required',
            'use_type' => 'required',
            'resumption_time' => 'required',
            'state_of_residence' => 'required',
            'vehicle_type' => 'required',
            'closing_time' => 'required',
            'nearest_location' => 'required',
            'driver_gender' => 'required',
            'transmission' => 'required',
            'salary_package' => 'required',
            'salary_frequency' => 'required',
            'insurance_type' => 'required',
            'provide_driver_accommodation' => 'required',
            
        ]);

        $user = User::find(Auth::User()->id);

        $fullTime = new FullTimeBooking();
        $fullTime->booking_id = 'DFT'.rand(1111,9999);
        $fullTime->package = $request->package;
        $fullTime->number_of_driver = $request->number_of_driver;
        $fullTime->number_of_driver_unmatched = $request->number_of_driver;
        $fullTime->resumption_date = $request->resumption_date;
        $fullTime->driver_stay = $request->driver_stay;
        $fullTime->use_type = $request->use_type;
        $fullTime->resumption_time = $request->resumption_time;
        $fullTime->state_of_residence = $request->state_of_residence;
        $fullTime->vehicle_type = $request->vehicle_type;
        $fullTime->closing_time = $request->closing_time;
        $fullTime->nearest_location = $request->nearest_location;
        $fullTime->driver_gender = $request->driver_gender;
        $fullTime->transmission = $request->transmission;
        $fullTime->salary_package = $request->salary_package;
        $fullTime->salary_frequency = $request->salary_frequency;
        $fullTime->insurance_type = $request->insurance_type;
        $fullTime->provide_driver_accommodation = $request->provide_driver_accommodation;

        $message = "Your driver will be available within 24 Hours after confirmation of payment, 
                        then once we confirm payment, at the backend we should be able to upload 
                        the driver image to that same place where the text was.";

        if ($user->fullTimeBookings()->save($fullTime)) {
            $recipients = ['business@driverng.com','driversng@gmail.com',$user->email];
            foreach ($recipients as $recipient) {
                $email = new \App\Mail\FullTime($fullTime);
                Mail::to($recipient)->send($email);
            }
            return view('main.full-time.pay',['request' => $request, 'bookingID' => $fullTime->id])->with('alert',$message);
        } else {
            return redirect()->back()->with($validator);
        }
    }
    public function makePayment($id)
    {
        $full = FullTImeBooking::find($id);
        return view('main.full-time.makepay',['full' => $full]);
    }

}
