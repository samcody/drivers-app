<?php

namespace App\Http\Controllers;
use App\Driver;
use App\Drive;
use App\Driven;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\States;
use Illuminate\Support\Facades\Auth;
use Image;
use Mail;

class DriverController extends Controller
{
    use States;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:driver');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        $drivers = Driver::where('email','=', Auth::User()->email )->paginate(25);
        return view('driverdash.home',['drivers' => $drivers]);
    }
    public function sdtm()
    {
        return view('driver.sdtm');
    }
    public function training()
    {
        return view('driverdash.training');
    }
    public function smartbook()
    {
        return view('driverdash.smartbook');
    }
    public function navigator()
    {
        return view('driverdash.navigator');
    }
    public function membership()
    {
        $drivers = Driver::where('email','=', Auth::User()->email )->paginate(25);
        return view('driverdash.membership',['drivers' => $drivers]);
    }
    public function refer()
    {
        $drivers = Driver::where('email','=', Auth::User()->email )->paginate(25);
        $refers = Drive::where('ref_id','=', Auth::User()->email )->paginate(25);
          return view('driverdash.refer',['refers' => $refers],['drivers' => $drivers]);
        }

    public function referMail(Request $request)
    {
        $recipients = [$request->email];
            foreach ($recipients as $recipient) {
            $email = new \App\Mail\ReferMail;
            Mail::to($recipient)->send($email);
            return redirect()->back()->with('alert',' Invitation sent successfully!');
            }

    }   

    public function cash()
    {
        return view('driver.earn-extra-cash');
    }
    public function update()
    {
        return view('driverdash.form');
    }
    public function full_time()
    {
        return view('driverdash.full-time');
    }
    public function short_term()
    {
        return view('driverdash.short-term');
    }
    public function uber()
    {
        return view('driverdash.uber');
    }
    public function vehicle()
    {
        return view('driverdash.vehicle');
    }
    public function fullSuccess()
    {
        
        return view('driverdash.full-time-success');
    }
    public function shortSuccess()
    {
        return view('driverdash.short-term-success');
    }
    public function uberSuccess()
    {
        return view('driverdash.uber-success');
    }
    
     

   
}
