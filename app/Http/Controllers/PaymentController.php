<?php

namespace App\Http\Controllers;

use App\FullTimeBooking;
use App\Payment;
use App\PayUse;
use App\ShortTimeBooking;
use App\UberBooking;
use App\EdmBooking;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Paystack;

class PaymentController extends Controller
{

    /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function redirectToGateway()
    {
        return Paystack::getAuthorizationUrl()->redirectNow();
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {
        $paymentDetails = Paystack::getPaymentData();

        $paymentDetail = $paymentDetails['data'];
        $paymentData = $paymentDetails['data']['metadata'];

        $payment = new Payment();
        $payment->booking_id = $paymentData['booking_id'];
        $payment->user_id =  $paymentData['user_id'];
        $payment->hire_type = $paymentData['hire_type'];
        $payment->amount = $paymentDetail['amount'];
        $payment->reference_key = $paymentDetail['reference'];

        $message = "Your Payment is successful. Your driver will be available within 24 Hours after confirmation of payment, 
                        then once we confirm payment, at the backend we should be able to upload 
                        the driver image to that same place where the text was.";

        if ($payment->save()) {
            if ($payment->hire_type == 'FULL-TIME') {
                $booking = FullTimeBooking::find($payment->booking_id);
                $booking->has_paid = true;
                $booking->save();

                $recipients = ['business@driverng.com','driversng@gmail.com',Auth::User()->email];
            foreach ($recipients as $recipient) {
                $email = new \App\Mail\PaymentMail($payment);
                Mail::to($recipient)->send($email);
            }
            return redirect()->route('home')->with('alert',$message);
            }
            if ($payment->hire_type == 'UBER') {
                $booking = UberBooking::find($payment->booking_id);
                $booking->has_paid = true;
                $booking->save();

                $recipients = ['business@driverng.com','driversng@gmail.com',Auth::User()->email];
            foreach ($recipients as $recipient) {
                $email = new \App\Mail\PaymentMail($payment);
                Mail::to($recipient)->send($email);
            }
            return redirect()->route('home')->with('alert',$message);
            }
            if ($payment->hire_type == 'SHORT-TIME') {
                $booking = ShortTimeBooking::find($payment->booking_id);
                $booking->has_paid = true;
                $booking->save();

                $recipients = ['business@driverng.com','driversng@gmail.com',Auth::User()->email];
            foreach ($recipients as $recipient) {
                $email = new \App\Mail\PaymentMail($payment);
                Mail::to($recipient)->send($email);
            }
            return redirect()->route('home')->with('alert',$message);
            }
            if ($payment->hire_type == 'PAY-USE') {
                $booking = PayUse::find($payment->booking_id);
                $booking->has_paid = true;
                $booking->save();

                $recipients = ['business@driverng.com','driversng@gmail.com',Auth::User()->email];
            foreach ($recipients as $recipient) {
                $email = new \App\Mail\PaymentMail($payment);
                Mail::to($recipient)->send($email);
            }
            return redirect()->route('home')->with('alert',$message);
            }
            if ($payment->hire_type == 'hour') {
                $exp = strtotime("+1 hour");
                $booking = new EdmBooking();
                $booking->user_id = Auth::user()->id;
                $booking->booking_id = 'EDM'.rand(1111,9999);
                $booking->package = $payment->hire_type;
                $booking->start_time= date("Y-m-d h:i:s");
                $booking->end_time=date("Y-m-d h:i:s",$exp);
                $booking->has_paid = true;
                $booking->save();

                $recipients = ['business@driverng.com','driversng@gmail.com',Auth::User()->email];
            foreach ($recipients as $recipient) {
                $email = new \App\Mail\PaymentMail($payment);
                Mail::to($recipient)->send($email);
            }
            return redirect()->route('marketplace.home')->with('alert',$message);
            }
            if ($payment->hire_type == 'day') {
                $exp = strtotime("tomorrow");
                $booking = new EdmBooking();
                $booking->user_id = Auth::user()->id;
                $booking->booking_id = 'EDM'.rand(1111,9999);
                $booking->package = $payment->hire_type;
                $booking->start_time= date("Y-m-d h:i:s");
                $booking->end_time=date("Y-m-d h:i:s",$exp);
                $booking->has_paid = true;
                $booking->save();

                $recipients = ['business@driverng.com','driversng@gmail.com',Auth::User()->email];
            foreach ($recipients as $recipient) {
                $email = new \App\Mail\PaymentMail($payment);
                Mail::to($recipient)->send($email);
            }
            return redirect()->route('marketplace.home')->with('alert',$message);
            }
            if ($payment->hire_type == 'week') {
                $exp = strtotime("+1 week");
                $booking = new EdmBooking();
                $booking->user_id = Auth::user()->id;
                $booking->booking_id = 'EDM'.rand(1111,9999);
                $booking->package = $payment->hire_type;
                $booking->start_time= date("Y-m-d h:i:s");
                $booking->end_time=date("Y-m-d h:i:s",$exp);
                $booking->has_paid = true;
                $booking->save();

                $recipients = ['business@driverng.com','driversng@gmail.com',Auth::User()->email];
            foreach ($recipients as $recipient) {
                $email = new \App\Mail\PaymentMail($payment);
                Mail::to($recipient)->send($email);
            }
            return redirect()->route('marketplace.home')->with('alert',$message);
            }
            if ($payment->hire_type == 'month') {
                $exp = strtotime("+1 month");
                $booking = new EdmBooking();
                $booking->user_id = Auth::user()->id;
                $booking->booking_id = 'EDM'.rand(1111,9999);
                $booking->package = $payment->hire_type;
                $booking->start_time= date("Y-m-d h:i:s");
                $booking->end_time=date("Y-m-d h:i:s",$exp);
                $booking->has_paid = true;
                $booking->save();

                $recipients = ['business@driverng.com','driversng@gmail.com',Auth::User()->email];
            foreach ($recipients as $recipient) {
                $email = new \App\Mail\PaymentMail($payment);
                Mail::to($recipient)->send($email);
            }
            return redirect()->route('marketplace.home')->with('alert',$message);
            }
            if ($payment->hire_type == 'year') {
                $exp = strtotime("+1 year");
                $booking = new EdmBooking();
                $booking->user_id = Auth::user()->id;
                $booking->booking_id = 'EDM'.rand(1111,9999);
                $booking->package = $payment->hire_type;
                $booking->start_time= date("Y-m-d h:i:s");
                $booking->end_time=date("Y-m-d h:i:s",$exp);
                $booking->has_paid = true;
                $booking->save();

                $recipients = ['business@driverng.com','driversng@gmail.com',Auth::User()->email];
            foreach ($recipients as $recipient) {
                $email = new \App\Mail\PaymentMail($payment);
                Mail::to($recipient)->send($email);
            }
            return redirect()->route('marketplace.home')->with('alert',$message);
            }
            
        }

    }

    public function show()
    {
        $payments = Payment::paginate(30);
        return view('admin.payment.list',['payments' => $payments]);
    }
}