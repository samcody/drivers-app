<?php

namespace App\Http\Controllers;

use App\Driver;
use App\FullTimeBooking;
use App\ShortTimeBooking;
use App\UberBooking;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        $users = User::all();
        $search = User::select('full_name')->distinct()->get();
        $shortTimeDrivers = Driver::where('driver_type','=','SHORT-TIME')->get();
        $fullTimeDrivers = Driver::where('driver_type','=','FULL-TIME')->get();
        $uberDrivers = Driver::where('driver_type','=','UBER')->get();
        $fullTimeBookings = FullTimeBooking::all();
        $shortTimeBookings = ShortTimeBooking::all();
        $uberBookings = UberBooking::all();
        return view('admin.dashboard',[
            'users' => $users,
            'shortTimeDrivers' => $shortTimeDrivers,
            'fullTimeDrivers' => $fullTimeDrivers,
            'uberDrivers' => $uberDrivers,
            'fullTimeBookings' => $fullTimeBookings,
            'shortTimeBookings' => $shortTimeBookings,
            'uberBookings' => $uberBookings,
            'search' => $search
        ]);
    }
}
