<?php

namespace App\Http\Controllers;

use App\RiderBooking;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class RiderBookingController extends Controller
{
    public function viewPackage()
    {
        return view('main.rider.package');
    }

    public function getPackage(Request $request)
    {
        return view('main.rider.book',['request' => $request]);
    }

    public function save(Request $request)
    {
        //To add validations later
        $validator = $this->validate($request,[
            'package' => 'required',
            'number_of_driver' => 'required',
            'resumption_date' => 'required',
            'driver_stay' => 'required',
            'use_type' => 'required',
            'resumption_time' => 'required',
            'state_of_residence' => 'required',
            'vehicle_type' => 'required',
            'closing_time' => 'required',
            'nearest_location' => 'required',
            'driver_gender' => 'required',
            'transmission' => 'required',
            'salary_package' => 'required',
            'salary_frequency' => 'required',
            'insurance_type' => 'required',
            'provide_driver_accommodation' => 'required',
            
        ]);

        $user = User::find(Auth::User()->id);

        $rider = new RiderBooking();
        $rider->booking_id = 'DRD'.rand(1111,9999);
        $rider->package = $request->package;
        $rider->number_of_driver = $request->number_of_driver;
        $rider->number_of_driver_unmatched = $request->number_of_driver;
        $rider->resumption_date = $request->resumption_date;
        $rider->driver_stay = $request->driver_stay;
        $rider->use_type = $request->use_type;
        $rider->resumption_time = $request->resumption_time;
        $rider->state_of_residence = $request->state_of_residence;
        $rider->vehicle_type = $request->vehicle_type;
        $rider->closing_time = $request->closing_time;
        $rider->nearest_location = $request->nearest_location;
        $rider->driver_gender = $request->driver_gender;
        $rider->transmission = $request->transmission;
        $rider->salary_package = $request->salary_package;
        $rider->salary_frequency = $request->salary_frequency;
        $rider->insurance_type = $request->insurance_type;
        $rider->provide_driver_accommodation = $request->provide_driver_accommodation;

        $message = "Your rider will be available within 24 Hours after confirmation of payment, 
                        then once we confirm payment, at the backend we should be able to upload 
                        the driver image to that same place where the text was.";

        if ($user->riderBookings()->save($rider)) {
            $recipients = ['business@driverng.com','driversng@gmail.com',$user->email];
            foreach ($recipients as $recipient) {
                $email = new \App\Mail\rider($rider);
                Mail::to($recipient)->send($email);
            }
            return view('main.rider.pay',['request' => $request, 'bookingID' => $rider->id])->with('alert',$message);
        } else {
            return redirect()->back()->with($validator);
        }
    }
    public function makePayment($id)
    {
        $full = riderBooking::find($id);
        return view('main.rider.makepay',['full' => $full]);
    }

}
