<?php

namespace App\Http\Controllers\Driver;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Driver;
use App\Driven;

//Validator facade used in validator method
use Illuminate\Support\Facades\Validator;

//Driver Model
use App\Drive;

//Auth Facade used in guard
use Auth;

class RegisterController extends Controller
{

    protected $redirectPath = 'drive/list';

    //shows registration form to Driver
    public function showRegistrationForm()
    {
        
        return view('driver.register');
    }

  //Handles registration request for Driver
    public function register(Request $request)
    {

       //Validates data
        $this->validator($request->all())->validate();

       //Create Driver
        $driver = $this->create($request->all());

        //Authenticates Driver
        $this->guard()->login($driver);

       //Redirects Drivers
        return redirect($this->redirectPath);
    }

    /**
     * register drivers via referrals
     *
     */

    //shows registration form to referred Driver
    public function showRegistrationForms($id)
    {
        $driver = Driver::find($id);
        return view('driver.refregister',['driver' => $driver]);
    }

  //Handles registration request for referred Driver
    public function refReg(Request $request)
    {
       //Validates data
        $this->validator($request->all())->validate();

       //Create Driver
        $driver = $this->newRef($request->all());

        //Authenticates Driver
        $this->guard()->login($driver);

       //Redirects Drivers
        return redirect($this->redirectPath);
    }

   /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'full_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:driver',
            'mobile_number' => 'required|unique:driver',
            'password' => 'required|string|min:6',
        ]);
    }

   /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        
        return Drive::create([
            'full_name' => $data['full_name'],
            'email' => $data['email'],
            'mobile_number' => $data['mobile_number'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function newRef(array $data)
    {
        return Drive::create([
            'full_name' => $data['full_name'],
            'email' => $data['email'],
            'ref_id' => $data['ref_id'],
            'mobile_number' => $data['mobile_number'],
            'password' => bcrypt($data['password']),
            
        ]);
    }


    //Get the guard to authenticate Driver
   protected function guard()
   {
       return Auth::guard('driver');
   }

}