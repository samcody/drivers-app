<?php

namespace App\Http\Controllers;

use App\Driver;
use App\FullTimeBooking;
use App\Match;
use App\PayUse;
use App\ShortTimeBooking;
use App\Transfer;
use App\UberBooking;

class MatchController extends Controller
{
    public function show()
    {
        $matches = Match::where('is_deleted','=',false)->where('status','=',true)->orderBy('created_at','desc')->paginate(30);
        return view('admin.match.list',['matches' => $matches]);
    }

    public function delete($id)
    {
        $match = Match::find($id);
        $match->is_deleted = true;


        if ($match->save()) {
            foreach (json_decode($match->driver_id) as $driver) {
                $driver = Driver::find($driver);
                $driver->is_matched = false;
                if ($match->booking_type == 'FULL-TIME') {
                    $booking = FullTimeBooking::find($match->booking_id);
                    $booking->driver_id = null;
                    $booking->is_matched = false;
                    $booking->save();
                }
                if ($match->booking_type == 'UBER') {
                    $booking = UberBooking::find($match->booking_id);
                    $booking->driver_id = null;
                    $booking->is_matched = false;
                    $booking->save();
                }
                if ($match->booking_type == 'SHORT-TIME') {
                    $booking = ShortTimeBooking::find($match->booking_id);
                    $booking->driver_id = null;
                    $booking->is_matched = false;
                    $booking->save();
                }
                if ($match->booking_type == 'TRANSFER') {
                    $booking = Transfer::find($match->booking_id);
                    $booking->driver_id = null;
                    $booking->is_matched = false;
                    $booking->save();
                }
                if ($match->booking_type == 'PAYUSE') {
                    $booking = PayUse::find($match->booking_id);
                    $booking->driver_id = null;
                    $booking->is_matched = false;
                    $booking->save();
                }
                $driver->save();
            }

            return redirect()->back()->with('alert','Matching has been disabled successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }
    }
}
