<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\Countries;
use App\CarTracker;
use Illuminate\Http\Request;

class CarTrackerController extends Controller
{
    public function create()
    {
        
        return view('main.car-tracker-tool');
    }

    public function store(Request $request)
    {
        $tracker = new CarTracker();
        $tracker->name = $request->input('name');
        $tracker->address = $request->input('address');
        $tracker->phone = $request->input('phone');
        $tracker->email = $request->input('email');
        $tracker->vehicle_type = $request->input('vehicle_type');
        $tracker->vehicle_transmission = $request->input('vehicle_transmission');
        
        if ($tracker->save()) {
            return redirect()->back()->with('alert', 'Car Tracker Request submitted successfully!');
        } else {
            return redirect()->back()->with('error', 'Sorry! an error occurred. Check your inputs again');
        }
    }

    public function show()
    {
        $trackers = CarTracker::where('is_deleted','=',false)->paginate(20);
        return view('admin.tracker.list',['trackers' => $trackers]);
    }

    public function view($id)
    {
        $tracker = CarTracker::find($id);
        return view('admin.tracker.view',['tracker' => $tracker]);
    }
}
