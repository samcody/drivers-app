<?php

namespace App\Http\Controllers;

use App\Driver;
use App\Drive;
use Illuminate\Http\Request;
use Image;
use App\Http\Controllers\Traits\States;
use PDF;
use AfricasTalking\AfricasTalkingGatewayException;
use AfricasTalking\AfricasTalkingGateway;
use Mail;

class RiderController extends Controller
{
    use States;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        return view('admin.rider.new',['states' => $this->listStates()]);
    }

    public function store(Request $request)
    {
        $validator = $this->validate($request,[
            'firstname' => 'required',
            'lastname' => 'required',
            'age' => 'required',
            'marital_status' => 'required',
            'religion' => 'required',
            'phonenumber' => 'required',
            'mobilenumber' => 'required',
            'email' => 'required|unique:drivers',
            'homeaddress' => 'required',
            'location' => 'required',
            'residence' => 'required',
            'profilepicture' => 'mimes:jpg,jpeg,gif,png,tiff,bmp,svg|max:10240',
            'accountnumber' => 'required',
            'bank_name' => 'required',
            'experience' => 'required',
            'license' => 'required',
            'license_date' => 'required',
            'can_drive' => 'required',
        ]);

        $driver = new Driver();

        if ($request->hasFile('profilepicture')) {
            $profilepicture = $request->file('profilepicture');
            $filename = time().'.'.$profilepicture->getClientOriginalExtension();
            Image::make($profilepicture)->resize(300,300)->save(public_path('/avatar/'.$filename));

            $driver->profilepicture =$filename;
        }


        $driver->serial_number = 'DNG'.rand(1111,9999);
        $driver->firstname = $request->firstname;
        $driver->lastname = $request->lastname;
        $driver->age = $request->age;
        $driver->marital_status = $request->marital_status;
        $driver->religion = $request->religion;
        $driver->phonenumber = $request->phonenumber;
        $driver->mobilenumber = $request->mobilenumber;
        $driver->email = $request->email;
        $driver->homeaddress = $request->homeaddress;
        $driver->location = ucfirst($request->location);
        $driver->residence = $request->residence;
        $driver->accountnumber = $request->accountnumber;
        $driver->bank_name = $request->bank_name;
        $driver->experience = $request->experience;
        $driver->can_drive = $request->can_drive;
        $driver->license = $request->license;
        $driver->lasdri = $request->lasdri;
        $driver->license_date = $request->license_date;
        $driver->lasdri_date = $request->lasdri_date;
        $driver->driver_type = 'RIDER';

        if ($driver->save()) {
            return redirect()->back()->with('alert', $driver->driver_type.' Driver added successfully!');
        } else {
            return redirect()->back()->withErrors($validator);
        }

    }

    public function show()
    {
        $drivers = Driver::where('driver_type','=','RIDER')->where('is_deleted','=',false)->orderBy('created_at', 'desc')->paginate(25);
        return view('admin.rider.list',['drivers' => $drivers]);
    }

    public function pdfdownload($id)
    {
        $driver = Driver::find($id);
        $pdf = PDF::loadView('admin.rider.profile',['driver' => $driver,'states' => $this->listStates()]);
        // download pdf
        return $pdf->download('{{driver-profile.pdf');

    }
    public function showFull($id)
    {
        $driver = Driver::find($id);
        return view('admin.rider.profile',['driver' => $driver,'states' => $this->listStates()]);

        if ($request->hasFile('profilepicture')) {
            $profilepicture = $request->file('profilepicture');
            $filename = time().'.'.$profilepicture->getClientOriginalExtension();
            Image::make($profilepicture)->resize(300,300)->save(public_path('/avatar/'.$filename));
            $driver->profilepicture =$filename;
        }

        

    }

    public function edit($id)
    {
        $driver = Driver::find($id);
        return view('admin.rider.edit',['driver' => $driver,'states' => $this->listStates()]);
    }

    public function update(Request $request, $id)
    {
        $validator = $this->validate($request,[
            'age' => 'required',
            'marital_status' => 'required',
            'religion' => 'required',
            'location' => 'required',
            'residence' => 'required',
            'profilepicture' => 'mimes:jpg,jpeg,gif,png,tiff,bmp,svg|max:10240',
            'accountnumber' => 'required',
            'bank_name' => 'required',
            'license' => 'required',
            'license_date' => 'required',
            'can_drive' => 'required',
        ]);

        $driver = Driver::find($id);

        if ($request->hasFile('profilepicture')) {
            $profilepicture = $request->file('profilepicture');
            $filename = time().'.'.$profilepicture->getClientOriginalExtension();
            Image::make($profilepicture)->resize(300,300)->save(public_path('/avatar/'.$filename));
            $driver->profilepicture =$filename;
        }


        $driver->age = $request->age;
        $driver->marital_status = $request->marital_status;
        $driver->religion = $request->religion;
        $driver->location = ucfirst($request->location);
        $driver->residence = $request->residence;
        $driver->accountnumber = $request->accountnumber;
        $driver->bank_name = $request->bank_name;
        $driver->experience = $request->experience;
        $driver->can_drive = $request->can_drive;
        $driver->license = $request->license;
        $driver->lasdri = $request->lasdri;
        $driver->license_date = $request->license_date;
        $driver->lasdri_date = $request->lasdri_date;
        $driver->driver_type = 'RIDER';
        $driver->gender = $request->gender;
        $driver->hair_color = $request->hair_color;
        $driver->height = $request->height;
        $driver->weight = $request->weight;
        $driver->uniform_size = $request->uniform_size;
        $driver->educational_profile = $request->educational_profile;
        $driver->father_name = $request->father_name;
        $driver->father_number = $request->father_number;
        $driver->mother_name = $request->mother_name;
        $driver->mother_number = $request->mother_number;
        $driver->wife_name = $request->wife_name;
        $driver->wife_number = $request->wife_number;
        $driver->sisterInLaw_name = $request->sisterInLaw_name;
        $driver->sisterInLaw_number = $request->sisterInLaw_number;
        $driver->uncle_name = $request->uncle_name;
        $driver->uncle_number = $request->uncle_number;
        $driver->neighbour_name = $request->neighbour_name;
        $driver->neighbour_number = $request->neighbour_number;
        $driver->fatherInLaw_name = $request->fatherInLaw_name;
        $driver->fatherInLaw_number = $request->fatherInLaw_number;
        $driver->uncleInLaw_name = $request->uncleInLaw_name;
        $driver->uncleInLaw_number = $request->uncleInLaw_number;
        $driver->brother_name = $request->brother_name;
        $driver->brother_number = $request->brother_number;
        $driver->brotherInLaw_name = $request->brotherInLaw_name;
        $driver->brotherInLaw_number = $request->brotherInLaw_number;
        $driver->auntyInLaw_name = $request->auntyInLaw_name;
        $driver->auntyInLaw_number = $request->auntyInLaw_number;
        $driver->english = $request->english;
        $driver->yoruba = $request->yoruba;
        $driver->igbo = $request->igbo;
        $driver->hausa = $request->hausa;
        $driver->employer_name = $request->employer_name;
        $driver->employer_number = $request->employer_number;

        if ($driver->save()) {
            return redirect()->back()->with('alert', $driver->driver_type.' Driver updated successfully!');
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }

    public function viewriderDocUpload($id)
    {
        $driver = Driver::find($id);
        return view('admin.upload.full-time-guarantor',['booking' => $booking]);
    }

    public function guarantor(Request $request,$id)
    {
        $validators = $this->validate($request,[
            'guarantors_form' => 'required|mimes:pdf,doc',
        ]);
        $driver = Driver::find($id);

        if ($request->hasFile('guarantors_form')) {
            $filename = rand(1111111,999999).time().'.'.$request->guarantor_form->getClientOriginalExtension();
            $request->guarantor_form->move(public_path('files'), $filename);
            $driver->guarantor_form = $filename;
        }

        if ($driver->save()) {
            return redirect()->back()->with('alert','Documents Uploaded successfully');
        }else{
            return redirect()->back()->withErrors($validators);
        }
    }

    public function delete($id)
    {
        $driver = Driver::find($id);
        $driver->is_deleted = true;

        if ($driver->save()) {
            return redirect()->back()->with('alert','Driver has been deleted successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }
    }

    public function verify($id)
    {
        $driver = Driver::find($id);
        $driver->is_verified = true;
        $driver->is_blacklisted = false;

        $refers = Drive::where('email','=', $driver->email )->paginate(25);
        foreach ($refers as $refer) {
        $refer->is_verified = true;
        }

           /* $email = new \App\Mail\VerifyMail;
            Mail::to($driver->email)->send($email);*/
            


        if ($driver->save() && $refer->save()) {
            return redirect()->back()->with('alert','Driver has been verified successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }

       


    }

    public function card($id)
    {
        $d=strtotime("next Year");

        $driver = Driver::find($id);
        $driver->has_card = true;
        $driver->card_date = date("Y-m-d");
        $driver->card_exp = date("Y-m-d", $d);

        $email = new \App\Mail\CardMail;
            Mail::to($driver->email)->send($email);


        $refers = Drive::where('email','=', $driver->email )->paginate(25);
        foreach ($refers as $refer) {
            $refer->has_card = true;
            $refer->card_date = date("Y-m-d");
            $refer->card_exp = date("Y-m-d", $d);
        }

        if ($driver->save() && $refer->save()) {
            return redirect()->back()->with('alert','Driver card activated successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while activating');
        }

       


    }

    public function blacklist($id)
    {
        $driver = Driver::find($id);
        $driver->is_blacklisted = true;
        $driver->is_verified = false;

        $refers = Drive::where('email','=', $driver->email )->paginate(25);
        foreach ($refers as $refer) {
        $refer->is_verified = false;
        }

        if ($driver->save() && $refer->save()) {
            return redirect()->back()->with('alert','Driver has been blacklisted successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }
    }
}
