<?php

namespace App\Http\Controllers;

use App\UberBooking;
use App\User;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class UberBookingController extends Controller
{
    public function package()
    {
        return view('main.uber.package');
    }

    public function getPackage(Request $request)
    {
        return view('main.uber.new',['request' => $request]);
    }

    public function uberRequest(Request $request)
    {
        $validator = $this->validate($request,[
            'number_of_driver' => 'required',
            'home_address' => 'required',
            'office_address' => 'required',
            'vehicle_specification' => 'required',
            'vehicle_brand' => 'required',
            'vehicle_model' => 'required',
            'vehicle_year' => 'required',
            'vehicle_identification_number' => 'required',
            'vehicle_registration_number' => 'required',
            'vehicle_transmission' => 'required',
            'type_of_vehicle' => 'required',
            'insurance_type' => 'required',
            'vehicle_documents' => 'required',
            'vehicle_pictures' => 'required'
        ]);


        $user = User::find(Auth::User()->id);
        $uber = new UberBooking();
        $uber->booking_id = 'DNU'.rand(1111,9999);
        $uber->package = $request->package;
        $uber->number_of_driver = $request->input('number_of_driver');
        $uber->home_address = $request->input('home_address');
        $uber->office_address = $request->input('office_address');
        $uber->vehicle_specification = $request->input('vehicle_specification');
        $uber->vehicle_brand = $request->input('vehicle_brand');
        $uber->vehicle_model = $request->input('vehicle_model');
        $uber->vehicle_year = $request->input('vehicle_year');
        $uber->vehicle_identification_number = $request->input('vehicle_identification_number');
        $uber->vehicle_registration_number = $request->input('vehicle_registration_number');
        $uber->vehicle_transmission = $request->input('vehicle_transmission');
        $uber->type_of_vehicle = $request->input('type_of_vehicle');
        $uber->insurance_type = $request->input('insurance_type');
        $uber->is_matched = 0;

        if ($request->hasFile('vehicle_pictures')) {
            $vehicle_pictures = $request->file('vehicle_pictures');
            $filename = time().'.'.$vehicle_pictures->getClientOriginalExtension();
            Image::make($vehicle_pictures)->resize(300,300)->save(public_path('/vehicle-pictures/'.$filename));

            $uber->vehicle_pictures =$filename;
        }

        if ($request->hasFile('vehicle_documents')) {
            $vehicle_documents = $request->file('vehicle_documents');
            $filename = time().'.'.$vehicle_documents->getClientOriginalExtension();
            Image::make($vehicle_documents)->resize(300,300)->save(public_path('/vehicle-docs/'.$filename));

            $uber->vehicle_documents =$filename;
        }


        /*
        if ($files = $request->file('vehicle_pictures')) {
            foreach ($files as $picture) {
                $filename = $picture.$picture->getClientOriginalExtension();
                $picture->move(base_path('public/vehicle-pictures'), $filename);
            }
            $uber->vehicle_pictures = json_encode($request->vehicle_pictures);
        }

        if ($files = $request->file('vehicle_documents')) {
            foreach ($files as $doc) {
                $filename = $doc.$doc->getClientOriginalExtension();
                $doc->move(base_path('public/vehicle-docs'), $filename);
            }
            $uber->vehicle_documents = json_encode($request->vehicle_documents);
        }*/

        $message = "Your driver will be available within 24 Hours after confirmation of payment, 
                    then once we confirm payment, at the backend we should be able to upload 
                    the driver image to that same place where the text was.";

        if ($user->uberBookings()->save($uber)) {
            /*$recipients = ['business@driverng.com','driversng@gmail.com',$user->email];
            foreach ($recipients as $recipient) {
                $email = new \App\Mail\Uber($uber);
                Mail::to($recipient)->send($email);
            }*/
            return view('main.uber.pay',['request' => $request,'bookingID' => $uber->id])->with('alert',$message);
        } else{
            return view('main.uber.new',['request' => $request])->withErrors($validator);
        }
    }

    public function makePayment($id)
    {
        $uber = UberBooking::find($id);
        return view('main.uber.makepay',['uber' => $uber]);
    }
}
