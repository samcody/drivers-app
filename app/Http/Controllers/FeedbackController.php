<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\Countries;
use App\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public function create()
    {
        
        return view('main.client-feedback-tool');
    }

    public function store(Request $request)
    {
        $feedback = new Feedback();
        $feedback->name = $request->input('name');
        $feedback->phone = $request->input('phone');
        $feedback->email = $request->input('email');
        $feedback->dissatisfied = $request->input('dissatisfied');
        $feedback->support = $request->input('support');
        $feedback->helped = $request->input('helped');
        
        if ($feedback->save()) {
            return redirect()->back()->with('alert', 'Feedback submitted successfully!');
        } else {
            return redirect()->back()->with('error', 'Sorry! an error occurred. Check your inputs again');
        }
    }

    public function show()
    {
        $feedbacks = Feedback::where('is_deleted','=',false)->paginate(20);
        return view('admin.feedback.list',['feedbacks' => $feedbacks]);
    }

    public function view($id)
    {
        $feedback = Feedback::find($id);
        return view('admin.feedback.view',['feedback' => $feedback]);
    }
}
