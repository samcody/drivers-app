<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Driver;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Validation\Validator;
use PDF;

class PdfController extends Controller
{
    public function showPdf($id)
    {
        $driver = Driver::find($id);
        $pdf = PDF::loadView('admin.fulltime.profile',['driver' => $driver]);
        // download pdf
        return $pdf->stream();

    }
}
