<?php

namespace App\Http\Controllers;

use App\Report;
use App\Remit;
use Illuminate\Http\Request;
use Image;
use App\Http\Controllers\Traits\States;
use PDF;
use AfricasTalking\AfricasTalkingGatewayException;
use AfricasTalking\AfricasTalkingGateway;
use Mail;

class ReportController extends Controller
{
    use States;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create($id)
    {
        $remit = Remit::find($id);
        return view('admin.remit.new-report',['remit' => $remit]);
    }

    public function store(Request $request)
    {
        $validator = $this->validate($request,[
            'week' => 'required',
            
        ]);

        $report = new Report();

        $report->week = $request->week;
        $report->month = $request->month;
        $report->week_days = $request->week_days;
        $report->days_worked = $request->days_worked;
        $report->drivers_name = $request->drivers_name;
        $report->car_reg = $request->reg_num;
        $report->car_model = $request->car_model;
        $report->expected_returns = $request->expected_returns;
        $report->uber_returns = $request->uber_returns;
        $report->management_fee = $request->management_fee;
        $report->maintenance = $request->maintenance;
        $report->remit_id = $request->remit_id;
        
       
        if ($report->save()) {
            return redirect()->back()->with('alert', $report->first_name.' added successfully!');
        } else {
            return redirect()->back()->withErrors($validator);
        }

    }

    public function show($id)
    {
        $user = Remit::find($id);
        
        $report = Report::where('remit_id','=',$user->id)->where('is_deleted','=',false)->orderBy('created_at', 'desc')->paginate(25);
        return view('admin.remit.list-report',['report' => $report],['user' => $user]);
    }

    public function pdfdownload($id)
    {
        $report = Report::find($id);
        $user = Remit::where('id','=',$report->remit_id)->where('is_deleted','=',false)->orderBy('created_at', 'desc')->paginate(25);
        $pdf = PDF::loadView('admin.remit.report-sheet',['report' => $report],['user' => $user]);
        // download pdf
        return $pdf->download('remittance.pdf');

    }
    public function sendmail($id)
    {
        $report = Report::find($id);
        $user = Remit::where('id','=',$report->remit_id)->where('is_deleted','=',false)->orderBy('created_at', 'desc')->paginate(1);
        $pdf = PDF::loadView('admin.remit.report-sheet',['report' => $report],['user' => $user]);
        // download pdf
        // return $pdf->download('remittance.pdf');
        $email =new \App\Mail\Remittance($report);
        foreach ($user as $user) {
            $recipient = $user->email;
        }
        
        Mail::to($recipient)->send($email);
        return redirect()->back()->with('alert', 'Mail sent successfully to '.$recipient);

    }

    public function showFull($id)
    {
        $report = Report::find($id);
        return view('admin.remit.report-sheet');

        if ($request->hasFile('profilepicture')) {
            $profilepicture = $request->file('profilepicture');
            $filename = time().'.'.$profilepicture->getClientOriginalExtension();
            Image::make($profilepicture)->resize(300,300)->save(public_path('/avatar/'.$filename));
            $report->profilepicture =$filename;
        }

        

    }

    public function edit($id)
    {
        $report = Report::find($id);
        return view('admin.remit.edit-report',['report' => $report]);
    }

    public function update(Request $request, $id)
    {
        $validator = $this->validate($request,[
            'week' => 'required',
        ]);

        $report = Report::find($id);

        $report->week = $request->week;
        $report->month = $request->month;
        $report->week_days = $request->week_days;
        $report->days_worked = $request->days_worked;
        $report->drivers_name = $request->drivers_name;
        $report->car_reg = $request->reg_num;
        $report->car_model = $request->car_model;
        $report->expected_returns = $request->expected_returns;
        $report->uber_returns = $request->uber_returns;
        $report->management_fee = $request->management_fee;
        $report->maintenance = $request->maintenance;
        $report->remit_id = $request->remit_id;
        

        if ($report->save()) {
            return redirect()->back()->with('alert',' Report updated successfully!');
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }


}
