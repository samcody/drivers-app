<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class ShortTermMessageController extends Controller
{
    public function create()
    {
        return view('driverdash.short-term');
    }

    public function store(Request $request)
    {
        Mail::send('emails.driver.short-term',[
            'msg' => $request->message

        ], function($mail) use($request){

            $mail->from($request->email, $request->name);

            $mail->to('business@driversng.com')->subject('Short Term Driver');
        });

    return redirect('drive');
    }


}