<?php

namespace App\Http\Controllers;


use App\User;
use Auth;
use Validator;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function show()
    {
        $users = User::where('is_deleted','=',false)->where('role','=','ROLE_USER')->paginate(30);
        return view('admin.users.list',['users' => $users]);
    }

    public function userDetails()
    {
        $users = User::all();
        return response()->json([
            'success' => $users], 200);
    }

    public function userDetail($id)
    {
        $user = User::find($id);
        return response()->json(['success' => $user], 200);
    }

    public function userLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
            'password' => 'required|string|min:6'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'data' => null,
                'message' => $validator->errors()
            ], 401);
        }
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] = $user->createToken('MyApp')->accessToken;
            return response()->json([
                'status' => 'success',
                'message' => 'Welcome '.$user->full_name,
                'data' => $success,
                
            ], 200);
        }
        else {
            return response()->json([
                'status' => 'error',
                'message' => 'Unauthorized'], 401);
        }
        
    }

    public function userRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'mobile_number' => 'required|unique:users',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'data' => null,
                'message' => $validator->errors()
            ], 401);
        }
        $input = $request->all();
        $input['password'] =bcrypt($request['password']);
        $user = User::create($input);
        $success['token'] = $user->createToken('MyApp')->accessToken;
        $success['name'] = $user->full_name;
        
        return response()->json([
            'status' => 'success',
            'data' => $success,
            'message' => 'User created successfully'
        ], 200);
    }

}
