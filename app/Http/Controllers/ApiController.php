<?php

namespace App\Http\Controllers;


use App\User;
use Auth;
use Validator;
use App\Driver;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function userDetails()
    {
        $users = User::all();
        return response()->json([
            'success' => $users], 200);
    }

    public function userDetail($id)
    {
        $user = Auth::user();        
        return response()->json(['success' => $user], 200);
    }

    public function userLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
            'password' => 'required|string|min:6'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'data' => null,
                'message' => $validator->errors()
            ], 401);
        }
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] = $user->createToken('MyApp')->accessToken;
            $success['name'] = $user->full_name;
            $success['email'] = $user->email;
            $success['phone'] = $user->mobile_number;
            return response()->json([
                'status' => 'success',
                'message' => 'Welcome '.$user->full_name,
                'data' => $success
                
            ], 200);
        }
        else {
            return response()->json([
                'status' => 'error',
                'message' => 'Unauthorized'], 401);
        }
        
    }

    public function userRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'mobile_number' => 'required|unique:users',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'data' => null,
                'message' => $validator->errors()
            ], 401);
        }
        $input = $request->all();
        $input['password'] =bcrypt($request['password']);
        $user = User::create($input);
        $success['token'] = $user->createToken('MyApp')->accessToken;
        $success['name'] = $user->full_name;
        $success['phone'] = $user->mobile_number;
        $success['email'] = $user->email;
        
        return response()->json([
            'status' => 'success',
            'data' => $success,
            'message' => 'User created successfully'
        ], 200);
    }
    
    public function isRegistered(Request $request){
        if (User::where('email', '=',  request('email'))->count() > 0) {
               // user found
              return response()->json([
                'status' => 'success',
                'data' => User::where('email', '=',  request('email')),
                'message' => "User found"
            ], 200);
        }
        
      
         return response()->json([
                'status' => 'error',
                'data' => null,
                'message' => "User not found"
            ], 200);
        
    }

    public function userUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'mobile_number' => 'required|unique:users',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'data' => null,
                'message' => $validator->errors()
            ], 401);
        }
        $id =Auth::User()->id;
        $user = User::find($id);
        $user->full_name = $request['full_name'];
        $user->mobile_number = $request['mobile_number'];
        $user->email = $request['email'];
        $user->save();
        $success['name'] = $user->full_name;
        $success['phone'] = $user->mobile_number;
        $success['email'] = $user->email;
        
        return response()->json([
            'status' => 'success',
            'data' => $success,
            'message' => 'User details updated successfully'
        ], 200);
    }

    public function fullTime()
    {
        $fullTime = Driver::where('driver_type','=', 'FULL-TIME' )->where('is_deleted','=',false)->paginate(25);
        return response()->json([
            'status' => 'success',
            'success' => $fullTime,
            'message' => 'List of full time drivers'
        ], 200);
    }
    public function fullTimeLoc(Request $request)
    {
        $drivers = Driver::where('location','=',$request->location)
                        ->where('driver_type','=','FULL-TIME')
                        ->where('is_deleted','=',false)
                        ->where('is_verified','=',true)
                        ->paginate(20);
        return response()->json([
            'status' => 'success',
            'success' => $drivers,
            'message' => 'List of full time drivers in'. $request->location
        ], 200);
    }

    public function shortTime()
    {
        $shortTime = Driver::where('driver_type','=', 'SHORT-TIME' )->where('is_deleted','=',false)->paginate(25);
        return response()->json([
            'status' => 'success',
            'success' => $shortTime,
            'message' => 'List of short term drivers'
        ], 200);
    }
    public function shortTimeLoc(Request $request)
    {
        $drivers = Driver::where('location','=',$request->location)
                        ->where('driver_type','=','SHORT-TIME')
                        ->where('is_deleted','=',false)
                        ->where('is_verified','=',true)
                        ->paginate(20);
        return response()->json([
            'status' => 'success',
            'success' => $drivers,
            'message' => 'List of short time drivers in'. $request->location
        ], 200);
    }

    public function uber()
    {
        $uber = Driver::where('driver_type','=', 'UBER' )->where('is_deleted','=',false)->paginate(25);
        return response()->json([
            'status' => 'success',
            'success' => $uber,
            'message' => 'List of uber drivers'
        ], 200);
    }
    public function uberLoc(Request $request)
    {
        $drivers = Driver::where('location','=',$request->location)
                        ->where('driver_type','=','UBER')
                        ->where('is_deleted','=',false)
                        ->where('is_verified','=',true)
                        ->paginate(20);
        return response()->json([
            'status' => 'success',
            'success' => $drivers,
            'message' => 'List of uber drivers in'. $request->location
        ], 200);
    }

    public function bookFullTime(Request $request)
    {
        $validator = $this->validate($request,[
            'package' => 'required',
            'number_of_driver' => 'required',
            'resumption_date' => 'required',
            'driver_stay' => 'required',
            'use_type' => 'required',
            'resumption_time' => 'required',
            'state_of_residence' => 'required',
            'vehicle_type' => 'required',
            'closing_time' => 'required',
            'nearest_location' => 'required',
            'driver_gender' => 'required',
            'transmission' => 'required',
            'salary_package' => 'required',
            'salary_frequency' => 'required',
            'insurance_type' => 'required',
            'provide_driver_accommodation' => 'required',
            
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'data' => null,
                'message' => $validator->errors()
            ], 401);
        }
        $user = User::find(Auth::User()->id);
        $input = $request->all();
        $fullTime = FullTimeBooking::create($input);
        $success['token'] = $user->createToken('MyApp')->accessToken;
        $success['name'] = $user->full_name;
        
        return response()->json([
            'status' => 'success',
            'data' => $success,
            'message' => 'Full time booking created successfully'
        ], 200);
    }
    public function addDriver(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstname' => 'required',
            'lastname' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'data' => null,
                'message' => $validator->errors()
            ], 401);
        }
        $input = $request->all();
        $driver = Driver::create($input);
        $success['name'] = $driver->firstname;
        
        return response()->json([
            'status' => 'success',
            'data' => $success,
            'message' => 'Driver created successfully'
        ], 200);
    }
}





















