<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Driver;
use App\Http\Controllers\Traits\Countries;
use App\Http\Controllers\Traits\States;

class MainController extends Controller
{
    use States;
    use Countries;
    public function landing()
    {
        $locations = Driver::select('location')->distinct()->get();
        return view('main.welcome',['locations' => $locations]);
    }

    public function package()
    {
        return view('main.package');
    }

    public function about()
    {
        return view('main.about');
    }
    public function faq()
    {
        return view('main.faq');
    }

    public function mobile()
    {
        return view('main.mobile');
    }

    public function how()
    {
        return view('main.how-it-works');
    }

    public function terms()
    {
        return view('main.terms');
    }

    public function insurance()
    {
        return view('main.vehicle-insurance');
    }

    public function diamond()
    {
        return view('main.diamond-tool');
    }

    public function taxify()
    {
        return view('main.taxify-tool');
    }

    public function rider()
    {
        return view('main.rider-tool');
    }

    public function tracker()
    {
        return view('main.car-tracker-tool');
    }

    public function learning()
    {
        return view('main.learning-driving-tool');
    }

    public function feedback()
    {
        return view('main.client-feedback-tool');
    }

    public function api()
    {
        return view('main.api-tool');
    }

    public function marketplace()
    {
        return view('main.marketplace-tool');
    }

    public function welcome()
    {
        return view('driver.home');
    }

    public function sdtm()
    {
        return view('driver.sdtm');
    }

    public function earn()
    {
        return view('driver.earn-extra-cash');
    }
}
