<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class FullTimeMessageController extends Controller
{
    public function create()
    {
        return view('driverdash.full-time');
    }

    public function store(Request $request)
    {
        Mail::send('emails.driver.full-time',[
            'msg' => $request->message

        ], function($mail) use($request){

            $mail->from($request->email, $request->name);

            $mail->to('business@driversng.com')->subject('Full Time Driver');
        });

    return redirect('drive');
    }


}