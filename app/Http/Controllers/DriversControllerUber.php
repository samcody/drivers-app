<?php

namespace App\Http\Controllers;

use App\Driver;
use App\Driven;
use Illuminate\Http\Request;
use Image;
use Mail;
use App\Http\Controllers\Traits\States;
use Illuminate\Support\Facades\Auth;

class DriversControllerUber extends Controller
{
    use States;

    public function __construct()
    {
        $this->middleware('auth:driver');
    }

    public function create()
    {
        return view('driverdash.edit-profile-uber',['states' => $this->listStates()]);
    }

    public function store(Request $request)
    {
        $validator = $this->validate($request,[
            'firstname' => 'required',
            'lastname' => 'required',
            'age' => 'required',
            'marital_status' => 'required',
            'religion' => 'required',
            'phonenumber' => 'required',
            'mobilenumber' => 'required',
            'homeaddress' => 'required',
            'location' => 'required',
            'residence' => 'required',
            'profilepicture' => 'mimes:jpg,jpeg,gif,png,tiff,bmp,svg|max:10240',
            'accountnumber' => 'required',
            'bank_name' => 'required',
            'license' => 'required',
            'license_date' => 'required',
            'can_drive' => 'required',
        ]);

        $driver = new Driver();

        if ($request->hasFile('profilepicture')) {
            $profilepicture = $request->file('profilepicture');
            $filename = time().'.'.$profilepicture->getClientOriginalExtension();
            Image::make($profilepicture)->resize(300,300)->save(public_path('/avatar/'.$filename));

            $driver->profilepicture =$filename;
        }


        $driver->serial_number = 'DNG'.rand(1111,9999);
        $driver->firstname = $request->firstname;
        $driver->lastname = $request->lastname;
        $driver->age = $request->age;
        $driver->marital_status = $request->marital_status;
        $driver->religion = $request->religion;
        $driver->phonenumber = $request->phonenumber;
        $driver->mobilenumber = $request->mobilenumber;
        $driver->email = Auth::User()->email;
        $driver->homeaddress = $request->homeaddress;
        $driver->location = ucfirst($request->location);
        $driver->residence = $request->residence;
        $driver->accountnumber = $request->accountnumber;
        $driver->bank_name = $request->bank_name;
        $driver->experience = $request->experience;
        $driver->can_drive = $request->can_drive;
        $driver->license = $request->license;
        $driver->lasdri = $request->lasdri;
        $driver->license_date = $request->license_date;
        $driver->lasdri_date = $request->lasdri_date;
        $driver->job_interest = $request->job_interest;
        $driver->relocate = $request->relocate;
        $driver->sdtm = $request->sdtm;
        $driver->driver_type = 'UBER';

        $msgs = $request->only(['firstname', 'lastname', 'email', 'residence', 'location']);

        Mail::send('emails.driver.uber',[
            'msg' => $msgs,
            'email' => Auth::User()->email


        ], function($mail) use($request){

            $mail->from(Auth::User()->email);

            $mail->to('business@driversng.com','toke@driversng.com','sarah@driversng.com','dickson.godwin@driversng.com')->subject('New Uber Driver');
        });

        Mail::send('emails.driver.uber-success',[
            'msg' => $request->message

        ], function($mail) use($request){

            $mail->from('business@driversng.com');

            $mail->to(Auth::User()->email)->subject('DriversNg Drivers Application');
        });

        if ($driver->save()) {
            return redirect('drive/uber-success')->with('alert', $driver->driver_type.'Account Created successfully!');
        } else {
            return redirect()->back()->withErrors($validator);
        }

    }

    public function show()
    {
        $drivers = Driver::where('email','=', Auth::User()->email )->where('is_deleted','=',false)->paginate(25);
        return view('driverdash.list-uber',['drivers' => $drivers]);
    }

    public function edit($id)
    {
        $driver = Driver::find($id);
        return view('driverdash.update-profile-uber',['driver' => $driver,'states' => $this->listStates()]);
    }

    public function update(Request $request, $id)
    {
        $validator = $this->validate($request,[
            'firstname' => 'required',
            'lastname' => 'required',
            'age' => 'required',
            'marital_status' => 'required',
            'religion' => 'required',
            'phonenumber' => 'required',
            'mobilenumber' => 'required',
            'email' => 'required|distinct',
            'homeaddress' => 'required',
            'location' => 'required',
            'residence' => 'required',
            'profilepicture' => 'mimes:jpg,jpeg,gif,png,tiff,bmp,svg|max:10240',
            'accountnumber' => 'required',
            'bank_name' => 'required',
            'license' => 'required',
            'license_date' => 'required',
            'can_drive' => 'required',
        ]);

        $driver = Driver::find($id);

        if ($request->hasFile('profilepicture')) {
            $profilepicture = $request->file('profilepicture');
            $filename = time().'.'.$profilepicture->getClientOriginalExtension();
            Image::make($profilepicture)->resize(300,300)->save(public_path('/avatar/'.$filename));
            $driver->profilepicture =$filename;
        }


        $driver->firstname = $request->firstname;
        $driver->lastname = $request->lastname;
        $driver->age = $request->age;
        $driver->marital_status = $request->marital_status;
        $driver->religion = $request->religion;
        $driver->phonenumber = $request->phonenumber;
        $driver->mobilenumber = $request->mobilenumber;
        $driver->email = $request->email;
        $driver->homeaddress = $request->homeaddress;
        $driver->location = ucfirst($request->location);
        $driver->residence = $request->residence;
        $driver->accountnumber = $request->accountnumber;
        $driver->bank_name = $request->bank_name;
        $driver->experience = $request->experience;
        $driver->can_drive = $request->can_drive;
        $driver->license = $request->license;
        $driver->lasdri = $request->lasdri;
        $driver->license_date = $request->license_date;
        $driver->lasdri_date = $request->lasdri_date;
        $driver->driver_type = 'UBER';

       

        if ($driver->save()) {
            return redirect('drive/uber-success')->with('alert', $driver->driver_type.' Account Created successfully!');
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }

    public function delete($id)
    {
        $driver = Driver::find($id);
        $driver->is_deleted = true;

        if ($driver->save()) {
            return redirect()->back()->with('alert','Driver has been deleted successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }
    }
}
