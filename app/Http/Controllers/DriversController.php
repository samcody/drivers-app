<?php

namespace App\Http\Controllers;

use App\Driver;
use App\Driven;
use Illuminate\Http\Request;
use Image;
use Mail;
use App\Http\Controllers\Traits\States;
use Illuminate\Support\Facades\Auth;

class DriversController extends Controller
{
    use States;

    public function __construct()
    {
        $this->middleware('auth:driver');
    }

    public function create()
    {
        return view('driverdash.edit-profile',['states' => $this->listStates()]);
    }

    public function store(Request $request)
    {
        $validator = $this->validate($request,[
            'firstname' => 'required',
            'lastname' => 'required',
            'age' => 'required',
            'marital_status' => 'required',
            'religion' => 'required',
            'phonenumber' => 'required',
            'mobilenumber' => 'required',
            'homeaddress' => 'required',
            'location' => 'required',
            'residence' => 'required',
            'profilepicture' => 'mimes:jpg,jpeg,gif,png,tiff,bmp,svg|max:10240',
            'accountnumber' => 'required',
            'bank_name' => 'required',
            'experience' => 'required',
            'license' => 'required',
            'license_date' => 'required',
            'can_drive' => 'required',
        ]);
        $driver = new Driver();

        if ($request->hasFile('profilepicture')) {
            $profilepicture = $request->file('profilepicture');
            $filename = time().'.'.$profilepicture->getClientOriginalExtension();
            Image::make($profilepicture)->resize(300,300)->save(public_path('/avatar/'.$filename));

            $driver->profilepicture =$filename;
        }


        $driver->serial_number = 'DNG'.rand(1111,9999);
        $driver->firstname = $request->firstname;
        $driver->lastname = $request->lastname;
        $driver->age = $request->age;
        $driver->marital_status = $request->marital_status;
        $driver->religion = $request->religion;
        $driver->phonenumber = $request->phonenumber;
        $driver->mobilenumber = $request->mobilenumber;
        $driver->email = Auth::User()->email;
        $driver->homeaddress = $request->homeaddress;
        $driver->location = ucfirst($request->location);
        $driver->residence = $request->residence;
        $driver->accountnumber = $request->accountnumber;
        $driver->bank_name = $request->bank_name;
        $driver->experience = $request->experience;
        $driver->can_drive = $request->can_drive;
        $driver->license = $request->license;
        $driver->lasdri = $request->lasdri;
        $driver->license_date = $request->license_date;
        $driver->lasdri_date = $request->lasdri_date;
        $driver->time_of_contract = $request->time_of_contract;
        $driver->job_interest = $request->job_interest;
        $driver->relocate = $request->relocate;
        $driver->sdtm = $request->sdtm;
        $driver->driver_type = 'FULL-TIME';
        
        $msgs = $request->only(['firstname', 'lastname', 'email', 'residence', 'location']);
        
        Mail::send('emails.driver.full-time',[
            'msg' => $msgs,
            'email' => Auth::User()->email

        ], function($mail) use($request){

            $mail->from(Auth::User()->email);

            $mail->to('business@driversng.com','toke@driversng.com','sarah@driversng.com','dickson.godwin@driversng.com')->subject('New Full Term Driver');
        });

        Mail::send('emails.driver.full-time-success',[
            'msg' => $request->message

        ], function($mail) use($request){

            $mail->from('business@driversng.com');

            $mail->to(Auth::User()->email)->subject('DriversNg Drivers Application');
        });

        

        if ($driver->save()) {
            return redirect('drive/full-time-success')->with('alert', $driver->driver_type.'Account Created successfully!');
        } else {
            return redirect()->back()->withErrors($validator);
        }

    }

    public function profile()
    {
        return view('driverdash.profile');
    }
    

    public function show()
    {
        $drivers = Driver::where('email','=', Auth::User()->email )->where('is_deleted','=',false)->paginate(25);
        return view('driverdash.list',['drivers' => $drivers]);
    }

    public function edit($id)
    {
        $driver = Driver::find($id);
        return view('driverdash.update-profile',['driver' => $driver,'states' => $this->listStates()]);
    }

    public function update(Request $request, $id)
    {
        $validator = $this->validate($request,[
            'age' => 'required',
            'religion' => 'required',
            'location' => 'required',
            'residence' => 'required',
            'profilepicture' => 'mimes:jpg,jpeg,gif,png,tiff,bmp,svg|max:10240',
            'accountnumber' => 'required',
            'bank_name' => 'required',
            'license' => 'required',
            'license_date' => 'required',
            'can_drive' => 'required',
        ]);

        $driver = Driver::find($id);

        if ($request->hasFile('profilepicture')) {
            $profilepicture = $request->file('profilepicture');
            $filename = time().'.'.$profilepicture->getClientOriginalExtension();
            Image::make($profilepicture)->resize(300,300)->save(public_path('/avatar/'.$filename));
            $driver->profilepicture =$filename;
        }


        
        
        $driver->age = $request->age;
        $driver->religion = $request->religion;
        $driver->location = ucfirst($request->location);
        $driver->residence = $request->residence;
        $driver->accountnumber = $request->accountnumber;
        $driver->bank_name = $request->bank_name;
        $driver->experience = $request->experience;
        $driver->can_drive = $request->can_drive;
        $driver->license = $request->license;
        $driver->lasdri = $request->lasdri;
        $driver->license_date = $request->license_date;
        $driver->lasdri_date = $request->lasdri_date;
        $driver->gender = $request->gender;
        $driver->hair_color = $request->hair_color;
        $driver->height = $request->height;
        $driver->weight = $request->weight;
        $driver->uniform_size = $request->uniform_size;
        $driver->educational_profile = $request->educational_profile;
        $driver->father_name = $request->father_name;
        $driver->father_number = $request->father_number;
        $driver->mother_name = $request->mother_name;
        $driver->mother_number = $request->mother_number;
        $driver->wife_name = $request->wife_name;
        $driver->wife_number = $request->wife_number;
        $driver->sisterInLaw_name = $request->sisterInLaw_name;
        $driver->sisterInLaw_number = $request->sisterInLaw_number;
        $driver->uncle_name = $request->uncle_name;
        $driver->uncle_number = $request->uncle_number;
        $driver->neighbour_name = $request->neighbour_name;
        $driver->neighbour_number = $request->neighbour_number;
        $driver->fatherInLaw_name = $request->fatherInLaw_name;
        $driver->fatherInLaw_number = $request->fatherInLaw_number;
        $driver->uncleInLaw_name = $request->uncleInLaw_name;
        $driver->uncleInLaw_number = $request->uncleInLaw_number;
        $driver->brother_name = $request->brother_name;
        $driver->brother_number = $request->brother_number;
        $driver->brotherInLaw_name = $request->brotherInLaw_name;
        $driver->brotherInLaw_number = $request->brotherInLaw_number;
        $driver->auntyInLaw_name = $request->auntyInLaw_name;
        $driver->auntyInLaw_number = $request->auntyInLaw_number;
        $driver->english = $request->english;
        $driver->yoruba = $request->yoruba;
        $driver->igbo = $request->igbo;
        $driver->hausa = $request->hausa;
        $driver->employer_name = $request->employer_name;
        $driver->employer_number = $request->employer_number;

        

        if ($driver->save()) {
            return redirect()->back()->with('alert', $driver->driver_type.' Driver updated successfully!');
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }

    public function editSingle($id)
    {
        $driver = Driver::find($id);
        return view('driverdash.update-profile-single',['driver' => $driver,'states' => $this->listStates()]);
    }

    public function updateSingle(Request $request, $id)
    {
        $validator = $this->validate($request,[
            'age' => 'required',
            'religion' => 'required',
            'location' => 'required',
            'residence' => 'required',
            'profilepicture' => 'mimes:jpg,jpeg,gif,png,tiff,bmp,svg|max:10240',
            'accountnumber' => 'required',
            'bank_name' => 'required',
            'license' => 'required',
            'license_date' => 'required',
            'can_drive' => 'required',
        ]);

        $driver = Driver::find($id);

        if ($request->hasFile('profilepicture')) {
            $profilepicture = $request->file('profilepicture');
            $filename = time().'.'.$profilepicture->getClientOriginalExtension();
            Image::make($profilepicture)->resize(300,300)->save(public_path('/avatar/'.$filename));
            $driver->profilepicture =$filename;
        }


        
        
        $driver->age = $request->age;
        $driver->religion = $request->religion;
        $driver->location = ucfirst($request->location);
        $driver->residence = $request->residence;
        $driver->accountnumber = $request->accountnumber;
        $driver->bank_name = $request->bank_name;
        $driver->experience = $request->experience;
        $driver->can_drive = $request->can_drive;
        $driver->license = $request->license;
        $driver->lasdri = $request->lasdri;
        $driver->license_date = $request->license_date;
        $driver->lasdri_date = $request->lasdri_date;
        $driver->gender = $request->gender;
        $driver->hair_color = $request->hair_color;
        $driver->height = $request->height;
        $driver->weight = $request->weight;
        $driver->uniform_size = $request->uniform_size;
        $driver->educational_profile = $request->educational_profile;
        $driver->father_name = $request->father_name;
        $driver->father_number = $request->father_number;
        $driver->mother_name = $request->mother_name;
        $driver->mother_number = $request->mother_number;
        $driver->wife_name = $request->wife_name;
        $driver->wife_number = $request->wife_number;
        $driver->uncle_name = $request->uncle_name;
        $driver->uncle_number = $request->uncle_number;
        $driver->neighbour_name = $request->neighbour_name;
        $driver->neighbour_number = $request->neighbour_number;
        $driver->brother_name = $request->brother_name;
        $driver->brother_number = $request->brother_number;
        $driver->english = $request->english;
        $driver->yoruba = $request->yoruba;
        $driver->igbo = $request->igbo;
        $driver->hausa = $request->hausa;
        $driver->employer_name = $request->employer_name;
        $driver->employer_number = $request->employer_number;

        

        if ($driver->save()) {
            return redirect('drive/home')->back()->with('alert', $driver->driver_type.' Your Profile has been updated successfully!');
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }

    
}
