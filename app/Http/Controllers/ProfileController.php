<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function edit()
    {
        return view('dashboard.profile.edit-profile');
    }

    public function update(Request $request)
    {
        $validator = $this->validate($request, [
            'email' => 'required|email|distinct',
            'full_name' => 'required',
            'mobile_number' => 'required'
        ]);

        $user = User::find(Auth::User()->id);

        $user->full_name = $request->full_name;
        $user->email = $request->email;
        $user->mobile_number = $request->mobile_number;

        if ($user->save()) {
            return redirect()->back()->with('alert', 'Profile updated successfully!');
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }

    /**
     *  Password Reset View
     *
     */
    public function passwordResetView($id)
    {
        $user = User::find($id);
        return view('dashboard.profile.password-reset',['users',Auth::User()])->with('user',$user);
    }

    /**
     * Handles Password Reset
     *
     */
    public function passwordReset(Request $request,$id)
    {
        $this->validate($request,[
            'password' => 'required|min:6|confirmed'
        ]);

        $user = User::find($id);
        $user->password = bcrypt($request->input('password'));

        if ($user->save())
        {
            return redirect()->back()->with('alert','Your password has been changed successfully');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }
}
