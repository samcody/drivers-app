<?php

namespace App\Http\Controllers;

use App\Driver;
use App\FullTimeBooking;
use App\Match;
use App\PayUse;
use App\Rating;
use App\ShortTimeBooking;
use App\Transfer;
use App\UberBooking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listFullTime()
    {
        $bookings = FullTimeBooking::where('is_deleted','=',false)->where('user_id','=', Auth::User()->id)->orderBy('created_at','desc')->paginate(20);
        return view('dashboard.full-time.list',['bookings' => $bookings]);
    }

    public function listShortTime()
    {
        $bookings = ShortTimeBooking::where('is_deleted','=',false)->where('user_id','=', Auth::User()->id)->orderBy('created_at','desc')->paginate(20);
        return view('dashboard.short.list',['bookings' => $bookings]);
    }

    public function listUber()
    {
        $bookings = UberBooking::where('is_deleted','=',false)->where('user_id','=', Auth::User()->id)->orderBy('created_at','desc')->paginate(20);
        return view('dashboard.uber.list',['bookings' => $bookings]);
    }

    public function listTransfer()
    {
        $bookings = Transfer::where('is_deleted','=',false)->where('user_id','=', Auth::User()->id)->orderBy('created_at','desc')->paginate(20);
        return view('dashboard.transfer.list',['bookings' => $bookings]);
    }

    public function listPayUse()
    {
        $bookings = PayUse::where('is_deleted','=',false)->where('user_id','=', Auth::User()->id)->orderBy('created_at','desc')->paginate(20);
        return view('dashboard.pay-use.list',['bookings' => $bookings]);
    }

    public function viewMatchedDriver($id)
    {
        $driver = Driver::find($id);
        $ratings = Rating::where('driver_id','=',$driver->id)->where('is_deleted','=',false)->orderBy('id','desc')->paginate(5);
        return view('dashboard.driver.matched.view',['driver' =>$driver,'ratings' => $ratings]);
    }

    public function viewMatchedDrivers($id)
    {
        $drivers = [];
        $match = Match::where('booking_id','=',$id)->first();
        foreach (json_decode($match->driver_id) as $driver) {
            $driver = Driver::find($driver);
            array_push($drivers,$driver);
        }
        return view('dashboard.driver.matched.list',['drivers' =>$drivers]);
    }

    public function viewFullTimeDocUpload($id)
    {
        $booking = FullTimeBooking::find($id);
        return view('dashboard.upload.full-time',['booking' => $booking]);
    }

    public function saveFullTimeDocUpload(Request $request,$id)
    {
        $validators = $this->validate($request,[
           'sla_document' => 'required|mimes:pdf,doc',
        ]);
        $booking = FullTimeBooking::find($id);

        if ($request->hasFile('sla_document')) {
            $filename = rand(1111111,999999).time().'.'.$request->sla_document->getClientOriginalExtension();
            $request->sla_document->move(public_path('files'), $filename);
            $booking->sla_document = $filename;
        }

        if ($booking->save()) {
            return redirect()->back()->with('alert','Documents Uploaded successfully');
        }else{
            return redirect()->back()->withErrors($validators);
        }
    }

    public function viewShortTimeDocUpload($id)
    {
        $booking = ShortTimeBooking::find($id);
        return view('dashboard.upload.short-time',['booking' => $booking]);
    }

    public function saveShortTimeDocUpload(Request $request,$id)
    {
        $validators = $this->validate($request,[
            'sla_document' => 'required|mimes:pdf,doc',
        ]);
        $booking = ShortTimeBooking::find($id);

        if ($request->hasFile('sla_document')) {
            $filename = rand(1111111,999999).time().'.'.$request->sla_document->getClientOriginalExtension();
            $request->sla_document->move(public_path('files'), $filename);
            $booking->sla_document = $filename;
        }

        if ($booking->save()) {
            return redirect()->back()->with('alert','Documents Uploaded successfully');
        }else{
            return redirect()->back()->withErrors($validators);
        }
    }

    public function viewUberDocUpload($id)
    {
        $booking = UberBooking::find($id);
        return view('dashboard.upload.uber',['booking' => $booking]);
    }

    public function saveUberDocUpload(Request $request,$id)
    {
        $validators = $this->validate($request,[
            'sla_document' => 'required|mimes:pdf,doc',
        ]);
        $booking = UberBooking::find($id);

        if ($request->hasFile('sla_document')) {
            $filename = rand(1111111,999999).time().'.'.$request->sla_document->getClientOriginalExtension();
            $request->sla_document->move(public_path('files'), $filename);
            $booking->sla_document = $filename;
        }

        if ($booking->save()) {
            return redirect()->back()->with('alert','Documents Uploaded successfully');
        }else{
            return redirect()->back()->withErrors($validators);
        }
    }

    public function viewPayUseDocUpload($id)
    {
        $booking = PayUse::find($id);
        return view('dashboard.upload.pay-use',['booking' => $booking]);
    }

    public function savePayUseDocUpload(Request $request,$id)
    {
        $validators = $this->validate($request,[
            'sla_document' => 'required|mimes:pdf,doc',
        ]);
        $booking = PayUse::find($id);

        if ($request->hasFile('sla_document')) {
            $filename = rand(1111111,999999).time().'.'.$request->sla_document->getClientOriginalExtension();
            $request->sla_document->move(public_path('files'), $filename);
            $booking->sla_document = $filename;
        }

        if ($booking->save()) {
            return redirect()->back()->with('alert','Documents Uploaded successfully');
        }else{
            return redirect()->back()->withErrors($validators);
        }
    }
}
