<?php

namespace App\Http\Controllers;

use App\PayUse;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PayBookingController extends Controller
{
    public function create()
    {
        return view('main.pay-use.book');
    }

    public function save(Request $request)
    {
        //To add validations later
        $validator = $this->validate($request,[
            'duration' => 'required',
            'number_of_driver' => 'required',
            'resumption_date' => 'required',
            'driver_stay' => 'required',
            'use_type' => 'required',
            'resumption_time' => 'required',
            'state_of_residence' => 'required',
            'vehicle_type' => 'required',
            'closing_time' => 'required',
            'nearest_location' => 'required',
            'driver_gender' => 'required',
            'transmission' => 'required',
            'salary_package' => 'required',
            'salary_frequency' => 'required',
            'insurance_type' => 'required',
            'provide_driver_accommodation' => 'required',
        ]);

        $user = User::find(Auth::User()->id);

        $booking = new PayUse();
        $booking->booking_id = 'DFT'.rand(1111,9999);
        $booking->duration = $request->duration;
        $booking->number_of_driver = $request->number_of_driver;
        $booking->number_of_driver_unmatched = $request->number_of_driver;
        $booking->resumption_date = $request->resumption_date;
        $booking->use_type = $request->use_type;
        $booking->resumption_time = $request->resumption_time;
        $booking->state_of_residence = $request->state_of_residence;
        $booking->vehicle_type = $request->vehicle_type;
        $booking->closing_time = $request->closing_time;
        $booking->nearest_location = $request->nearest_location;
        $booking->driver_gender = $request->driver_gender;
        $booking->transmission = $request->transmission;
        $booking->salary_frequency = $request->salary_frequency;
        $booking->salary_package = $request->salary_package;
        $booking->insurance_type = $request->insurance_type;
        $booking->provide_driver_accommodation = $request->provide_driver_accommodation;

        $serviceFeeList = [20000, 24000, 26000, 28000, 30000, 32000, 35000, 37000, 39000, 42000, 45000, 50000];
        $percentage = 1.5 * $request['duration'];
        $amount = $serviceFeeList[$request['duration'] - 1] + ($percentage * ($request['salary_package'] * $request['duration'])/100);

        $message = "Your driver will be available within 24 Hours after confirmation of payment, 
                        then once we confirm payment, at the backend we should be able to upload 
                        the driver image to that same place where the text was.";

        if ($user->payUseBookings()->save($booking)) {
            $recipients = ['business@driverng.com','driversng@gmail.com',$user->email];
            foreach ($recipients as $recipient) {
                $email = new \App\Mail\PayUseMail($booking);
                Mail::to($recipient)->send($email);
            }
            return view('main.pay-use.pay',['request' => $request, 'bookingID' => $booking->id,'amount' => $amount])->with('alert',$message);
        } else {
            return redirect()->back()->with($validator);
        }
    }
}
