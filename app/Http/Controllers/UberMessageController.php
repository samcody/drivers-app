<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class UberMessageController extends Controller
{
    public function create()
    {
        return view('driverdash.uber');
    }

    public function store(Request $request)
    {
        Mail::send('emails.driver.uber',[
            'msg' => $request->message

        ], function($mail) use($request){

            $mail->from($request->email, $request->name);

            $mail->to('business@driversng.com')->subject('Uber Driver');
        });

    return redirect('drive');
    }


}