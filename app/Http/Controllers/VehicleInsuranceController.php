<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\Countries;
use App\VehicleInsurance;
use Illuminate\Http\Request;

class VehicleInsuranceController extends Controller
{
    public function create()
    {
        
        return view('main.vehicle-insurance');
    }

    public function store(Request $request)
    {
        $insurance = new VehicleInsurance();
        $insurance->name = $request->input('name');
        $insurance->address = $request->input('address');
        $insurance->phone = $request->input('phone');
        $insurance->email = $request->input('email');
        $insurance->vehicle_type = $request->input('vehicle_type');
        $insurance->vehicle_transmission = $request->input('vehicle_transmission');
        
        if ($insurance->save()) {
            return redirect()->back()->with('alert', 'Vehicle Insurance Request submitted successfully!');
        } else {
            return redirect()->back()->with('error', 'Sorry! an error occurred. Check your inputs again');
        }
    }

    public function show()
    {
        $insurances = VehicleInsurance::where('is_deleted','=',false)->paginate(20);
        return view('admin.insurance.list',['insurances' => $insurances]);
    }

    public function view($id)
    {
        $insurance = VehicleInsurance::find($id);
        return view('admin.insurance.view',['insurance' => $insurance]);
    }
}
