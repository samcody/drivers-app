<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\Countries;
use App\Partner;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    use Countries;

    public function create()
    {
        $data = [
            'partnership_purposes' => [
                'Network/Introductions', 'Resources/Financials', 'Vehicles', 'Space', 'Expansion',
                'Advise', 'Volunteer Workers', 'Volunteer Interns', 'Investing'
            ],
            'countries' => $this->listCountry(),
            'price_ranges' => [
                '$25,000', '$25,000 - $75,000', '$75,000 - $150,000', '$150,000 - $500,000', '$500,000+'
            ]
        ];

        return view('main.partner.new',['data' => $data]);
    }

    public function store(Request $request)
    {
        $partnership = new Partner();
        $partnership->name = $request->input('name');
        $partnership->city = $request->input('city');
        $partnership->phone = $request->input('phone');
        $partnership->email = $request->input('email');
        $partnership->nationality = $request->input('nationality');
        $partnership->partnership_purpose = $request->input('partnership_purpose');
        $partnership->agreement = $request->input('agreement');
        $partnership->investment_price = $request->input('investment_price');
        $partnership->investor_background = $request->input('investor_background');

        if ($partnership->save()) {
            return redirect()->back()->with('alert', 'Partnership Request submitted successfully!');
        } else {
            return redirect()->back()->with('error', 'Sorry! an error occurred. Check your inputs again');
        }
    }

    public function show()
    {
        $partners = Partner::where('is_deleted','=',false)->paginate(20);
        return view('admin.partner.list',['partners' => $partners]);
    }

    public function view($id)
    {
        $partner = Partner::find($id);
        return view('admin.partner.view',['partner' => $partner]);
    }
}
