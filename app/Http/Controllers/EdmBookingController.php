<?php

namespace App\Http\Controllers;

use App\Driver;
use App\EdmBooking;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class EdmBookingController extends Controller
{
    public function viewPackage()
    {
        return view('main.edm.package');
    }
    public function welcome()
    {
        return view('main.edm.welcome');
    }
    public function post()
    {
        return view('main.edm.post');
    }
    public function home()
    {
        $short = Driver::where('driver_type','=','SHORT-TIME')->paginate(4);
        $uber = Driver::where('driver_type','=','UBER')->paginate(4);
        return view('main.edm.home',['short' => $short,'uber' => $uber]);
    }

    public function addToCart(Request $request, $id)
    {
        $cart = array($request->cart,$request->cart);
        $user = User::find(Auth::User()->id);
        $user->cart = serialize($cart);

        if ($user->save()) {
            return redirect()->back()->with('alert','Driver has been added to cart');
        } else {
            return redirect()->back()->with('error','An error occurred');
        }
    }

    public function getPackage(Request $request)
    {
        return view('main.edm.book',['request' => $request]);
    }

    public function save(Request $request)
    {
        //To add validations later
        $validator = $this->validate($request,[

        ]);

        $user = User::find(Auth::User()->id);

        $edm = new EdmBooking();
        $edm->booking_id = 'DFT'.rand(1111,9999);
        $edm->package = $request->package;
        $edm->number_of_driver = $request->number_of_driver;
        $edm->number_of_driver_unmatched = $request->number_of_driver;
        $edm->resumption_date = $request->resumption_date;
        $edm->driver_stay = $request->driver_stay;
        $edm->use_type = $request->use_type;
        $edm->resumption_time = $request->resumption_time;
        $edm->state_of_residence = $request->state_of_residence;
        $edm->vehicle_type = $request->vehicle_type;
        $edm->closing_time = $request->closing_time;
        $edm->nearest_location = $request->nearest_location;
        $edm->driver_gender = $request->driver_gender;
        $edm->transmission = $request->transmission;
        $edm->salary_package = $request->salary_package;
        $edm->salary_frequency = $request->salary_frequency;
        $edm->insurance_type = $request->insurance_type;
        $edm->provide_driver_accommodation = $request->provide_driver_accommodation;

        $message = "Your driver will be available within 24 Hours after confirmation of payment, 
                        then once we confirm payment, at the backend we should be able to upload 
                        the driver image to that same place where the text was.";

        if ($user->fullTimeBookings()->save($edm)) {
            $recipients = ['business@driverng.com','driversng@gmail.com',$user->email];
            foreach ($recipients as $recipient) {
                $email = new \App\Mail\FullTime($edm);
                Mail::to($recipient)->send($email);
            }
            return view('main.full-time.pay',['request' => $request, 'bookingID' => $edm->id])->with('alert',$message);
        } else {
            return redirect()->back()->with($validator);
        }
    }
    public function makePayment($id)
    {
        $full = FullTImeBooking::find($id);
        return view('main.full-time.makepay',['full' => $full]);
    }

}
