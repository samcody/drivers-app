<?php

namespace App\Http\Controllers;

use App\Remit;
use Illuminate\Http\Request;
use Image;
use App\Http\Controllers\Traits\States;
use PDF;
use AfricasTalking\AfricasTalkingGatewayException;
use AfricasTalking\AfricasTalkingGateway;
use Mail;

class RemitController extends Controller
{
    use States;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        return view('admin.remit.new',['states' => $this->listStates()]);
    }

    public function store(Request $request)
    {
        $validator = $this->validate($request,[
            'firstname' => 'required',
            'lastname' => 'required',
            'phonenumber' => 'required',
            'email' => 'required|unique:remit',
            
        ]);

        $remit = new Remit();

        $remit->first_name = $request->firstname;
        $remit->last_name = $request->lastname;
        $remit->mobile_number = $request->phonenumber;
        $remit->email = $request->email;
       
        if ($remit->save()) {
            return redirect()->back()->with('alert', $remit->first_name.' added successfully!');
        } else {
            return redirect()->back()->withErrors($validator);
        }

    }

    public function show()
    {
        $remit = Remit::where('status','=',true)->where('is_deleted','=',false)->orderBy('created_at', 'desc')->paginate(25);
        return view('admin.remit.list',['remit' => $remit]);
    }

    public function pdfdownload($id)
    {
        $remit = Remit::find($id);
        $pdf = PDF::loadView('admin.fulltime.profile',['driver' => $remit,'states' => $this->listStates()]);
        // download pdf
        return $pdf->download('{{driver-profile.pdf');

    }
    public function showFull($id)
    {
        $remit = Remit::find($id);
        return view('admin.fulltime.profile',['driver' => $remit,'states' => $this->listStates()]);

        if ($request->hasFile('profilepicture')) {
            $profilepicture = $request->file('profilepicture');
            $filename = time().'.'.$profilepicture->getClientOriginalExtension();
            Image::make($profilepicture)->resize(300,300)->save(public_path('/avatar/'.$filename));
            $remit->profilepicture =$filename;
        }

        

    }

    public function edit($id)
    {
        $remit = Remit::find($id);
        return view('admin.remit.edit',['remit' => $remit,'states' => $this->listStates()]);
    }

    public function update(Request $request, $id)
    {
        $validator = $this->validate($request,[
            'firstname' => 'required',
            'lastname' => 'required',
            'phonenumber' => 'required',
            
        ]);

        $remit = Remit::find($id);

        $remit->first_name = $request->firstname;
        $remit->last_name = $request->lastname;
        $remit->mobile_number = $request->phonenumber;
        $remit->email = $remit->email;

        if ($remit->save()) {
            return redirect()->back()->with('alert', $remit->firstname.'updated successfully!');
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }

    public function viewFullTimeDocUpload($id)
    {
        $remit = Remit::find($id);
        return view('admin.upload.full-time-guarantor',['booking' => $booking]);
    }

    public function guarantor(Request $request,$id)
    {
        $validators = $this->validate($request,[
            'guarantors_form' => 'required|mimes:pdf,doc',
        ]);
        $remit = Remit::find($id);

        if ($request->hasFile('guarantors_form')) {
            $filename = rand(1111111,999999).time().'.'.$request->guarantor_form->getClientOriginalExtension();
            $request->guarantor_form->move(public_path('files'), $filename);
            $remit->guarantor_form = $filename;
        }

        if ($remit->save()) {
            return redirect()->back()->with('alert','Documents Uploaded successfully');
        }else{
            return redirect()->back()->withErrors($validators);
        }
    }

    public function delete($id)
    {
        $remit = Remit::find($id);
        $remit->is_deleted = true;

        if ($remit->save()) {
            return redirect()->back()->with('alert','Driver has been deleted successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }
    }

    public function verify($id)
    {
        $remit = Remit::find($id);
        $remit->is_verified = true;
        $remit->is_blacklisted = false;

        $refers = Drive::where('email','=', $remit->email )->paginate(25);
        foreach ($refers as $refer) {
        $refer->is_verified = true;
        }

           /* $email = new \App\Mail\VerifyMail;
            Mail::to($remit->email)->send($email);*/
            


        if ($remit->save() && $refer->save()) {
            return redirect()->back()->with('alert','Driver has been verified successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }

       


    }

    public function card($id)
    {
        $d=strtotime("next Year");

        $remit = Remit::find($id);
        $remit->has_card = true;
        $remit->card_date = date("Y-m-d");
        $remit->card_exp = date("Y-m-d", $d);

        $email = new \App\Mail\CardMail;
            Mail::to($remit->email)->send($email);


        $refers = Drive::where('email','=', $remit->email )->paginate(25);
        foreach ($refers as $refer) {
            $refer->has_card = true;
            $refer->card_date = date("Y-m-d");
            $refer->card_exp = date("Y-m-d", $d);
        }

        if ($remit->save() && $refer->save()) {
            return redirect()->back()->with('alert','Driver card activated successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while activating');
        }

       


    }

    public function blacklist($id)
    {
        $remit = Remit::find($id);
        $remit->is_blacklisted = true;
        $remit->is_verified = false;

        $refers = Drive::where('email','=', $remit->email )->paginate(25);
        foreach ($refers as $refer) {
        $refer->is_verified = false;
        }

        if ($remit->save() && $refer->save()) {
            return redirect()->back()->with('alert','Driver has been blacklisted successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }
    }
}
