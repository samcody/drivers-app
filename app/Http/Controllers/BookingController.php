<?php

namespace App\Http\Controllers;

use App\Driver;
use App\FullTimeBooking;
use App\Match;
use App\PayUse;
use App\ShortTimeBooking;
use App\Transfer;
use App\UberBooking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class BookingController extends Controller
{

    /**
     * Full-Time Booking Methods
     *
     */
    public function viewFullTimeBookings()
    {
        $bookings = FullTimeBooking::where('is_deleted','=',false)->orderBy('created_at','desc')->paginate(20);
        return view('admin.bookings.full-time.list',['bookings' => $bookings]);
    }

    public function viewFullTimeBooking($id)
    {
        $booking = FullTimeBooking::find($id);
        return view('admin.bookings.full-time.view',['booking' => $booking]);
    }

    public function confirmFullTimeBookingPayment($id)
    {
        $booking = FullTimeBooking::find($id);
        $booking->has_paid = true;

        if ($booking->save()) {
            return redirect()->back()->with('alert','Booking Payment has been confirmed successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }
    }

    public function unconfirmFullTimeBookingPayment($id)
    {
        $booking = FullTimeBooking::find($id);
        $booking->has_paid = false;

        if ($booking->save()) {
            return redirect()->back()->with('alert','Booking Payment has been unconfirmed successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }
    }

    public function deleteFullTimeBooking($id)
    {
        $booking = FullTimeBooking::find($id);
        $booking->is_deleted = true;

        if ($booking->save()) {
            return redirect()->back()->with('alert','Booking has been deleted successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }
    }

    public function viewFullTimeMatchDriverToClient($id)
    {
        $booking = FullTimeBooking::find($id);
        $drivers = Driver::where('driver_type','=','FULL-TIME')->where('is_matched','=',false)->where('is_deleted','=',false)->get();
        return view('admin.bookings.full-time.match',['booking' => $booking,'drivers' => $drivers]);
    }

    public function matchFullTimeDriverToClient(Request $request,$id)
    {
        $booking = FullTimeBooking::find($id);
        $match = new Match();
        $match->user_id = $booking->user_id;
        $match->booking_id = $booking->id;
        $match->booking_type = 'FULL-TIME';


        if (1 == $booking->number_of_driver_unmatched || count($request->driver_id) < $booking->number_of_driver_unmatched) {
            $match->driver_id = $request->driver_id;
                $driver = Driver::find($request->driver_id);
                $driver->is_matched = true;
                $driver->save();
            
            $booking->is_matched = true;
            $booking->number_of_driver_unmatched = $booking->number_of_driver_unmatched - 1;
            $booking->driver_id = $match->driver_id;
            $booking->save();
            $match->save();
            $recipients = ['business@driverng.com','driversng@gmail.com',$booking->user->email];
            foreach ($recipients as $recipient) {
                $email = new \App\Mail\MatchMail($match);
                Mail::to($recipient)->send($email);
            }
            return redirect()->route('bookings.full-time')->with('alert','Match has been made successfully');
        } else {
            return redirect()->back()->with('error','Drivers selected are more than drivers not selected');
        }
    }



    /**
     * Uber Booking Methods
     *
    */
    public function viewUberBookings()
    {
        $bookings = UberBooking::where('is_deleted','=',false)->orderBy('created_at','desc')->paginate(20);
        return view('admin.bookings.uber.list',['bookings' => $bookings]);
    }

    public function viewUberBooking($id)
    {
        $booking = UberBooking::find($id);
        return view('admin.bookings.uber.view',['booking' => $booking]);
    }

    public function confirmUberBookingPayment($id)
    {
        $booking = UberBooking::find($id);
        $booking->has_paid = true;

        if ($booking->save()) {
            return redirect()->back()->with('alert','Booking Payment has been confirmed successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }
    }

    public function unconfirmUberBookingPayment($id)
    {
        $booking = UberBooking::find($id);
        $booking->has_paid = false;

        if ($booking->save()) {
            return redirect()->back()->with('alert','Booking Payment has been unconfirmed successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }
    }

    public function deleteUberBooking($id)
    {
        $booking = UberBooking::find($id);
        $booking->is_deleted = true;

        if ($booking->save()) {
            return redirect()->back()->with('alert','Booking has been deleted successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }
    }

    public function viewUberMatchDriverToClient($id)
    {
        $booking = UberBooking::find($id);
        $drivers = Driver::where('driver_type','=','UBER')->where('is_matched','=',false)->where('is_deleted','=',false)->get();
        return view('admin.bookings.uber.match',['booking' => $booking,'drivers' => $drivers]);
    }

    public function matchUberDriverToClient(Request $request,$id)
    {
        $booking = UberBooking::find($id);
        $match = new Match();
        $match->user_id = $booking->user_id;
        $match->booking_id = $booking->id;
        $match->booking_type = 'UBER';

        if (count($request->driver_id) == $booking->number_of_driver_unmatched || count($request->driver_id) < $booking->number_of_driver_unmatched) {
            $match->driver_id = $request->driver_id;
            $driver = Driver::find($request->driver_id);
            $driver->is_matched = true;
            $driver->save();
            
            $booking->is_matched = true;
            $booking->number_of_driver_unmatched = $booking->number_of_driver_unmatched - count($request->driver_id);
            $booking->driver_id = json_encode($match->driver_id);
            $booking->save();
            $match->save();
            $recipients = ['business@driverng.com','driversng@gmail.com',$booking->user->email];
            foreach ($recipients as $recipient) {
                $email = new \App\Mail\MatchMail($match);
                Mail::to($recipient)->send($email);
            }
            return redirect()->route('bookings.uber')->with('alert','Match has been made successfully');
        } else {
            return redirect()->back()->with('error','Drivers selected are more than drivers not selected');
        }

    }

    /**
     * Short-Time Booking Methods
     *
     */
    public function viewShortTimeBookings()
    {
        $bookings = ShortTimeBooking::where('is_deleted','=',false)->orderBy('created_at','desc')->paginate(20);
        return view('admin.bookings.short-time.list',['bookings' => $bookings]);
    }

    public function viewShortTimeBooking($id)
    {
        $date = date("Y-m-d");
        $booking = ShortTimeBooking::find($id);
        if($booking->end_date < $date ){
            $booking->is_matched = 0;
            $booking->driver_id = null;
            $booking->save();
            
        }
        return view('admin.bookings.short-time.view',['booking' => $booking]);
    }

    public function confirmShortTimeBookingPayment($id)
    {
        $booking = ShortTimeBooking::find($id);
        $booking->has_paid = true;

        if ($booking->save()) {
            return redirect()->back()->with('alert','Booking Payment has been confirmed successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }
    }

    public function unconfirmShortTimeBookingPayment($id)
    {
        $booking = ShortTimeBooking::find($id);
        $booking->has_paid = false;

        if ($booking->save()) {
            return redirect()->back()->with('alert','Booking Payment has been unconfirmed successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }
    }

    public function deleteShortTimeBooking($id)
    {
        $booking = ShortTimeBooking::find($id);
        $booking->is_deleted = true;

        if ($booking->save()) {
            return redirect()->back()->with('alert','Booking has been deleted successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }
    }

    public function viewShortTimeMatchDriverToClient($id)
    {
        $booking = ShortTimeBooking::find($id);
        $drivers = Driver::where('driver_type','=','SHORT-TIME')->where('is_matched','=',false)->where('is_deleted','=',false)->get();
        return view('admin.bookings.short-time.match',['booking' => $booking,'drivers' => $drivers]);
    }

    public function matchShortTimeDriverToClient(Request $request,$id)
    {
        $booking = ShortTimeBooking::find($id);
        $match = new Match();
        $match->user_id = $booking->user_id;
        $match->booking_id = $booking->id;
        $match->booking_type = 'SHORT-TIME';
        $match->driver_id = $request->driver_id;

        if ($match->save()) {
            $driver = Driver::find($match->driver_id);
            $driver->is_matched = true;
            $booking->is_matched = true;
            $booking->driver_id = $match->driver_id;
            $driver->save();
            $booking->save();
            $recipients = ['business@driverng.com','driversng@gmail.com',$booking->user->email];
            foreach ($recipients as $recipient) {
                $email = new \App\Mail\MatchMail($match);
                Mail::to($recipient)->send($email);
            }
            return redirect()->back()->with('alert','Match has been made successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }
    }

    public function viewFullTimeDocUpload($id)
    {
        $booking = FullTimeBooking::find($id);
        return view('admin.upload.full-time',['booking' => $booking]);
    }

    public function saveFullTimeDocUpload(Request $request,$id)
    {
        $validators = $this->validate($request,[
            'guarantor_form' => 'required|mimes:pdf,doc',
        ]);
        $booking = FullTimeBooking::find($id);

        if ($request->hasFile('guarantor_form')) {
            $filename = rand(1111111,999999).time().'.'.$request->guarantor_form->getClientOriginalExtension();
            $request->guarantor_form->move(public_path('files'), $filename);
            $booking->guarantor_form = $filename;
        }

        if ($booking->save()) {
            return redirect()->back()->with('alert','Documents Uploaded successfully');
        }else{
            return redirect()->back()->withErrors($validators);
        }
    }

    public function viewFullTimeSlaDocUpload($id)
    {
        $booking = FullTimeBooking::find($id);
        return view('admin.upload.full-time-agreement',['booking' => $booking]);
    }

    public function saveFullTimeSlaDocUpload(Request $request,$id)
    {
        $validators = $this->validate($request,[
            'sla_document' => 'required|mimes:pdf,doc',
        ]);
        $booking = FullTimeBooking::find($id);

        if ($request->hasFile('sla_document')) {
            $filename = rand(1111111,999999).time().'.'.$request->sla_document->getClientOriginalExtension();
            $request->sla_document->move(public_path('files'), $filename);
            $booking->sla_document = $filename;
        }

        if ($booking->save()) {
            return redirect()->back()->with('alert','Documents Uploaded successfully');
        }else{
            return redirect()->back()->withErrors($validators);
        }
    }

    public function viewShortTimeDocUpload($id)
    {
        $booking = ShortTimeBooking::find($id);
        return view('admin.upload.short-time',['booking' => $booking]);
    }

    public function saveShortTimeDocUpload(Request $request,$id)
    {
        $validators = $this->validate($request,[
            'guarantor_form' => 'required|mimes:pdf,doc',
        ]);
        $booking = ShortTimeBooking::find($id);

        if ($request->hasFile('guarantor_form')) {
            $filename = rand(1111111,999999).time().'.'.$request->guarantor_form->getClientOriginalExtension();
            $request->guarantor_form->move(public_path('files'), $filename);
            $booking->guarantor_form = $filename;
        }

        if ($booking->save()) {
            return redirect()->back()->with('alert','Documents Uploaded successfully');
        }else{
            return redirect()->back()->withErrors($validators);
        }
    }

    public function viewShortTimeSlaDocUpload($id)
    {
        $booking = ShortTimeBooking::find($id);
        return view('admin.upload.short-time-agreement',['booking' => $booking]);
    }

    public function saveShortTimeSlaDocUpload(Request $request,$id)
    {
        $validators = $this->validate($request,[
            'sla_document' => 'required|mimes:pdf,doc',
        ]);
        $booking = ShortTimeBooking::find($id);

        if ($request->hasFile('sla_document')) {
            $filename = rand(1111111,999999).time().'.'.$request->sla_document->getClientOriginalExtension();
            $request->sla_document->move(public_path('files'), $filename);
            $booking->sla_document = $filename;
        }

        if ($booking->save()) {
            return redirect()->back()->with('alert','Documents Uploaded successfully');
        }else{
            return redirect()->back()->withErrors($validators);
        }
    }

    public function viewUberDocUpload($id)
    {
        $booking = UberBooking::find($id);
        return view('admin.upload.uber',['booking' => $booking]);
    }

    public function saveUberDocUpload(Request $request,$id)
    {
        $validators = $this->validate($request,[
            'guarantor_form' => 'required|mimes:pdf,doc',
        ]);
        $booking = UberBooking::find($id);

        if ($request->hasFile('guarantor_form')) {
            $filename = rand(1111111,999999).time().'.'.$request->guarantor_form->getClientOriginalExtension();
            $request->guarantor_form->move(public_path('files'), $filename);
            $booking->guarantor_form = $filename;
        }

        if ($booking->save()) {
            return redirect()->back()->with('alert','Documents Uploaded successfully');
        }else{
            return redirect()->back()->withErrors($validators);
        }
    }

    public function viewUberSlaDocUpload($id)
    {
        $booking = UberBooking::find($id);
        return view('admin.upload.uber-agreement',['booking' => $booking]);
    }

    public function saveUberSlaDocUpload(Request $request,$id)
    {
        $validators = $this->validate($request,[
            'sla_document' => 'required|mimes:pdf,doc',
        ]);
        $booking = UberBooking::find($id);

        if ($request->hasFile('sla_document')) {
            $filename = rand(1111111,999999).time().'.'.$request->sla_document->getClientOriginalExtension();
            $request->sla_document->move(public_path('files'), $filename);
            $booking->sla_document = $filename;
        }

        if ($booking->save()) {
            return redirect()->back()->with('alert','Documents Uploaded successfully');
        }else{
            return redirect()->back()->withErrors($validators);
        }
    }

    /**
     * Transfer Booking Methods
     *
     */
    public function viewTransferBookings()
    {
        $bookings = Transfer::where('is_deleted','=',false)->orderBy('created_at','desc')->paginate(20);
        return view('admin.bookings.transfer.list',['bookings' => $bookings]);
    }

    public function viewTransferBooking($id)
    {
        $booking = Transfer::find($id);
        return view('admin.bookings.transfer.view',['booking' => $booking]);
    }

    public function confirmTransferBookingPayment($id)
    {
        $booking = Transfer::find($id);
        $booking->has_paid = true;

        if ($booking->save()) {
            return redirect()->back()->with('alert','Booking Payment has been confirmed successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }
    }

    public function unconfirmTransferBookingPayment($id)
    {
        $booking = Transfer::find($id);
        $booking->has_paid = false;

        if ($booking->save()) {
            return redirect()->back()->with('alert','Booking Payment has been unconfirmed successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }
    }

    public function deleteTransferBooking($id)
    {
        $booking = Transfer::find($id);
        $booking->is_deleted = true;

        if ($booking->save()) {
            return redirect()->back()->with('alert','Booking has been deleted successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }
    }

    public function viewTransferMatchDriverToClient($id)
    {
        $booking = Transfer::find($id);
        $drivers = Driver::where('is_matched','=',false)->where('is_deleted','=',false)->get();
        return view('admin.bookings.transfer.match',['booking' => $booking,'drivers' => $drivers]);
    }

    public function matchTransferDriverToClient(Request $request,$id)
    {
        $booking = Transfer::find($id);
        $match = new Match();
        $match->user_id = $booking->user_id;
        $match->booking_id = $booking->id;
        $match->booking_type = 'TRANSFER';

        if (count($request->driver_id) == $booking->number_of_driver_unmatched || count($request->driver_id) < $booking->number_of_driver_unmatched) {
            $match->driver_id = json_encode($request->driver_id);
            foreach ($request->driver_id as $singleDriver) {
                $driver = Driver::find($singleDriver);
                $driver->is_matched = true;
                $driver->save();
            }
            $booking->is_matched = true;
            $booking->number_of_driver_unmatched = $booking->number_of_driver_unmatched - count($request->driver_id);
            $booking->driver_id = json_encode($match->driver_id);
            $booking->save();
            $match->save();
            $recipients = ['business@driverng.com','driversng@gmail.com',$booking->user->email];
            foreach ($recipients as $recipient) {
                $email = new \App\Mail\MatchMail($match);
                Mail::to($recipient)->send($email);
            }
            return redirect()->route('bookings.transfer')->with('alert','Match has been made successfully');
        } else {
            return redirect()->back()->with('error','Drivers selected are more than drivers not selected');
        }

    }

    public function viewTransferDocUpload($id)
    {
        $booking = Transfer::find($id);
        return view('admin.upload.transfer',['booking' => $booking]);
    }

    public function saveTransferDocUpload(Request $request,$id)
    {
        $validators = $this->validate($request,[
            'transfer_document' => 'required|mimes:pdf,doc',
            'amount' => 'required'
        ]);
        $booking = Transfer::find($id);

        if ($request->hasFile('transfer_document')) {
            $filename = rand(1111111,999999).time().'.'.$request->transfer_document->getClientOriginalExtension();
            $request->transfer_document->move(public_path('files'), $filename);
            $booking->transfer_document = $filename;
        }
        $booking->amount = $request->amount;

        if ($booking->save()) {
            return redirect()->back()->with('alert','Documents Uploaded successfully');
        }else{
            return redirect()->back()->withErrors($validators);
        }
    }

    /**
     * Pay-Use Booking Methods
     *
     */
    public function viewPayUseBookings()
    {
        $bookings = PayUse::where('is_deleted','=',false)->orderBy('created_at','desc')->paginate(20);
        return view('admin.bookings.pay-use.list',['bookings' => $bookings]);
    }

    public function viewPayUseBooking($id)
    {
        $booking = PayUse::find($id);
        return view('admin.bookings.pay-use.view',['booking' => $booking]);
    }

    public function confirmPayUseBookingPayment($id)
    {
        $booking = PayUse::find($id);
        $booking->has_paid = true;

        if ($booking->save()) {
            return redirect()->back()->with('alert','Booking Payment has been confirmed successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }
    }

    public function unconfirmPayUseBookingPayment($id)
    {
        $booking = PayUse::find($id);
        $booking->has_paid = false;

        if ($booking->save()) {
            return redirect()->back()->with('alert','Booking Payment has been unconfirmed successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }
    }

    public function deletePayUseBooking($id)
    {
        $booking = PayUse::find($id);
        $booking->is_deleted = true;

        if ($booking->save()) {
            return redirect()->back()->with('alert','Booking has been deleted successfully');
        } else {
            return redirect()->back()->with('error','An error occurred while deleting');
        }
    }

    public function viewPayUseMatchDriverToClient($id)
    {
        $booking = PayUse::find($id);
        $drivers = Driver::where('is_matched','=',false)->where('is_deleted','=',false)->get();
        return view('admin.bookings.pay-use.match',['booking' => $booking,'drivers' => $drivers]);
    }

    public function matchPayUseDriverToClient(Request $request,$id)
    {
        $booking = PayUse::find($id);
        $match = new Match();
        $match->user_id = $booking->user_id;
        $match->booking_id = $booking->id;
        $match->booking_type = 'PAYUSE';

        if (count($request->driver_id) == $booking->number_of_driver_unmatched || count($request->driver_id) < $booking->number_of_driver_unmatched) {
            $match->driver_id = json_encode($request->driver_id);
            foreach ($request->driver_id as $singleDriver) {
                $driver = Driver::find($singleDriver);
                $driver->is_matched = true;
                $driver->save();
            }
            $booking->is_matched = true;
            $booking->number_of_driver_unmatched = $booking->number_of_driver_unmatched - count($request->driver_id);
            $booking->driver_id = json_encode($match->driver_id);
            $booking->save();
            $match->save();
            $recipients = ['business@driverng.com','driversng@gmail.com',$booking->user->email];
            foreach ($recipients as $recipient) {
                $email = new \App\Mail\MatchMail($match);
                Mail::to($recipient)->send($email);
            }
            return redirect()->route('bookings.pay-use')->with('alert','Match has been made successfully');
        } else {
            return redirect()->back()->with('error','Drivers selected are more than drivers not selected');
        }

    }

    public function viewPayUseDocUpload($id)
    {
        $booking = PayUse::find($id);
        return view('admin.upload.pay-use',['booking' => $booking]);
    }

    public function savePayUseDocUpload(Request $request,$id)
    {
        $validators = $this->validate($request,[
            'guarantor_form' => 'required|mimes:pdf,doc',
        ]);
        $booking = PayUse::find($id);

        if ($request->hasFile('guarantor_form')) {
            $filename = rand(1111111,999999).time().'.'.$request->guarantor_form->getClientOriginalExtension();
            $request->guarantor_form->move(public_path('files'), $filename);
            $booking->guarantor_form = $filename;
        }

        if ($booking->save()) {
            return redirect()->back()->with('alert','Documents Uploaded successfully');
        }else{
            return redirect()->back()->withErrors($validators);
        }
    }
}
