<?php
/**
 * Created by PhpStorm.
 * User: goodnesskay
 * Date: 11/30/17
 * Time: 6:34 AM
 */

namespace App\Http\Controllers\Traits;


trait States
{
    public function listStates()
    {
        $states = ['Abia','Adamawa','Anambra','Akwa Ibom','Bauchi','Bayelsa','Benue','Borno','Cross River','Delta', 'Ebonyi',
                 'Enugu','Edo','Ekiti','Gombe','Imo','Jigawa','Kaduna','Kano','Katsina','Kebbi','Kogi','Kwara','Lagos','Nasarawa',
                 'Niger','Ogun','Ondo','Osun','Oyo','Plateau','Rivers','Sokoto','Taraba','Yobe','Zamfara','Federal Capital Territory (FCT)'];

        return $states;
    }
}