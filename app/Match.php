<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    use Uuids;
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    public function driver()
    {
        return $this->belongsTo('App\Driver');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
