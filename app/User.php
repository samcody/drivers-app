<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'email','mobile_number', 'password','role','status','is_deleted'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function ratings()
    {
        return $this->hasMany('App\Rating');
    }

    public function matches()
    {
        return $this->hasMany('App\Match');
    }

    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    public function fullTimeBookings()
    {
        return $this->hasMany('App\FullTimeBooking');
    }

    public function shortTimeBookings()
    {
        return $this->hasMany('App\ShortTimeBooking');
    }

    public function uberBookings()
    {
        return $this->hasMany('App\UberBooking');
    }

    public function payUseBookings()
    {
        return $this->hasMany('App\PayUse');
    }

    public function transferBookings()
    {
        return $this->hasMany('App\Transfer');
    }
}
